# Configuration parameters for ConsumerConfig

```
--consume-private-updates                          True when private wikis should be updated.
                                          Boolean  default: false

--elasticsearch-bulk-complete-failure-max-retries  Maximum number of retries attempted in case of request failing completely, e.g. due to a timeout, see elasticsearch-socket-timeout
                                          Integer  default: 5

--elasticsearch-bulk-complete-failure-retry-delay  Time to wait per retry, multiplied by the current attempt, see elasticsearch-bulk-complete-failure-max-retries
                                         Duration  default: PT10S

--elasticsearch-bulk-flush-interval                Flush threshold, duration
                                                    * Each item written to the buffer will create a timer triggering a flush.
                                                    * Only maxed out while the buffer holds less than 819 (elasticsearch-bulk-flush-max-actions) items.
                                         Duration  default: PT5M

--elasticsearch-bulk-flush-max-actions             Flush threshold, in number of actions
                                                   Derived from 16 mb (elasticsearch-bulk-flush-max-size) / 20 kb (= avg. action size)
                                          Integer  default: 819

--elasticsearch-bulk-flush-max-size                Flush threshold, in bytes
                                                   This is implied by the elasticsearch instance/cluster, it defaults to 100mb
                                       MemorySize  default: 16 mb

--elasticsearch-bulk-max-action-size               Max. size per action, in bytes
                                                   This is a hard limit, currently it's capped at wgCirrusSearchDocumentSizeLimiterProfile.max_size (2 * 4 mb), because it's at most passed twice: as upsert document and as script parameter
                                                   A constant amount is added by default to make room for overhead
                                       MemorySize  default: 9 mb

--elasticsearch-connection-request-timeout         Timeout for requesting a connection from a pool
                                         Duration  
--elasticsearch-connection-timeout                 Timeout for establishing a connection
                                         Duration  
--elasticsearch-max-buffered-requests              Maximum number of requests buffered by the sink writer operator.
                                                   Currently each event is mapped to exactly one bulk action, so this is equal to the number of events. Please consider the following parameters that impact the time during which the buffer fills:
                                                    * we expect roughly 100 updates per second per sink
                                                    * we allow a timeout of PT2M0.1S (elasticsearch-socket-timeout) per batch (bulk request)
                                                    * each request may be retried up 5 (elasticsearch-bulk-complete-failure-max-retries) times
                                                    * each retry may expand the previous delay by PT10S (elasticsearch-bulk-complete-failure-retry-delay) (multiplied by # attempt)
                                                   For now, we use a multiple of the elasticsearch-bulk-flush-max-actions
                                          Integer  default: 2457

--elasticsearch-socket-timeout                     Timeout for receiving a response
                                         Duration  default: PT2M0.1S

--elasticsearch-urls                               Map from cirrus cluster group name to cluster URL
                                              Map  
--fetch-connections                                
                                              Map  default: {PAGE_RERENDER=700, default=100}

--fetch-error-stream                               
                                           String  default: cirrussearch.update_pipeline.fetch_error.v1

--fetch-error-topic                                
                                           String  
--fetch-queue-capacity                             Accumulated size of (parallel) queues to hold updates until they are processed. Avoids immediate backpressure while waiting for responses. Consider the following stats: 
                                                    * On average, source rate is 300 updates/s
                                                    * 85%, are PAGE_RERENDER updates
                                                    * 50% of all fetches take 5s or less per request
                                                    * 1% of all fetches need a second attempt, which doubles the time and adds 1s delay
                                                   Needs to be larger if order is kept (default-key).
                                                   See also fetch-connections
                                              Map  default: {PAGE_RERENDER=1000, default=200}

--fetch-request-timeout                            Timeout per fetch request, defaults to http-request-timeout
                                         Duration  default: PT10S

--fetch-retry-max                                  
                                          Integer  default: 3

--fetch-retry-max-age                              Max age of the events for which fetching the revision content is retried regardless of fetch-retry-max
                                         Duration  default: PT10S

--private-legacy-update-stream                     Bridging (legacy) stream for private wikis between producer (aggregator) and consumer (indexer)
                                                   This is used to fetch the event schema as well as a list of kafka topics from stream configuration, see event-stream-config-url
                                           String  
--public-legacy-update-stream                      Bridging (legacy) stream for public wikis between producer (aggregator) and consumer (indexer)
                                                   This is used to fetch the event schema as well as a list of kafka topics from stream configuration, see event-stream-config-url
                                           String  
--saneitize                                        Enable saneitizer for configured wikis
                                                   If false implicitly sets saneitize-max-runtime to 0s
                                          Boolean  default: false

--saneitize-batch-size                             The maximum number of pages sent to the backend in a single request.
                                          Integer  default: 100

--saneitize-capacity                               Sanity check async request capacity
                                          Integer  default: 20

--saneitize-cluster-group                          Name, in CirrusSearch cluster configuration, of the target cluster group.
                                           String  
--saneitize-loop-duration                          Saneitizer loop duration
                                         Duration  default: PT336H

--saneitize-max-retries                            The number of times to attempt the same SanityCheck API request before giving up.
                                          Integer  default: 3

--saneitize-max-runtime                            Stop saneitizer after this long.
                                                   Provides bounded stream for integration testing and/or when running alongside a bounded kafka source.
                                         Duration  
--saneitize-request-timeout                        Timeout for sanity check api requests. By default this is a batch of 100 pages which typically takes one second to process.
                                         Duration  default: PT2M

--saneitize-rerender-frequency                     How often, in number of loops, a page should be re-rendered
                                          Integer  default: 16

--use-null-sink                                    Sink all events to /dev/null, for measuring pipeline throughput.
                                          Boolean  default: false

```

# Configuration parameters for CommonConfig

```
--dry-run                                  
                                  Boolean  default: false

--event-stream-config-url                  
                                   String  
--event-stream-http-routes                 A map of 'source_host: dest_host' used by event streams for schema loading.
                                      Map  
--event-stream-json-schema-urls            
                                 String[]  
--http-rate-limit-max-retries              Maximum number of retries attempted in case of a 429  (rate-limited, too many requests) response
                                           A value < 0 means unbounded retries
                                  Integer  default: -1

--http-rate-limit-per-second               Maximum number of requests per second permitted per http-user-agent
                                           A value < 0 means no client-side rate-limiting
                                  Integer  default: 1000

--http-rate-limit-retry-interval           Retry interval in case of a 429 (rate-limited, too many requests) response
                                 Duration  default: PT0.5S

--http-request-connection-timeout-divisor  Portion of the overall http-request-timeout after establishing a connection times out
                                     Long  default: 10

--http-request-timeout                     Timeout for HTTP requests
                                 Duration  default: PT10S

--http-routes                              A map of '00_sorting_key: original_uri_regular_expression=alternate_uri'
                                      Map  
--http-user-agent                          User agent to provide with all http requests
                                   String  default: WMF/cirrus-streaming-updater

--kafka-sink-config                        Map of kafka sink configuration properties, defaults to kafka-source-config
                                      Map  
--kafka-source-config                      Map of kafka source configuration properties
                                           Minimal configuration:
                                            * bootstrap.servers
                                            * group.id
                                      Map  
--kafka-source-end-offset                  Source start offsets, format topic[:partition]=offset
                                      Map  
--kafka-source-end-time                    Source end instant (ISO format), used to determine last offset before that time
                                           Alternatively, specify kafka-source-end-offset
                                   String  
--kafka-source-start-offset                Source end offsets, format topic[:partition]=offset
                                      Map  
--kafka-source-start-time                  Source start instant (ISO format), used to determine first offset after that time
                                           Alternatively, specify kafka-source-start-offset
                                   String  
--mediawiki-auth-token                     NetworkSession token to provide to mediawiki to access private wikis
                                   String  
--pipeline.name                            The job name used for printing and logging.
                                   String  
--private-update-stream                    Bridging stream for private wikis between producer (aggregator) and consumer (indexer)
                                           This is used to fetch the event schema as well as a list of kafka topics from stream configuration, see event-stream-config-url
                                   String  default: cirrussearch.update_pipeline.update.private.v1

--public-update-stream                     Bridging stream for public wikis between producer (aggregator) and consumer (indexer)
                                           This is used to fetch the event schema as well as a list of kafka topics from stream configuration, see event-stream-config-url
                                   String  default: cirrussearch.update_pipeline.update.v1

--wikiids                                  List of wiki database names we will process, when not provided all wikis are processed.
                                 String[]  
```

This file has been auto-generated by `org.wikimedia.discovery.cirrus.updater.common.config.ConfigRenderer`.
