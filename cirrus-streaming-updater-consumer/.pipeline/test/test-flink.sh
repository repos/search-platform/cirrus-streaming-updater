#!/bin/sh -x

export LOG4J_LEVEL=INFO

exec /bin/java -classpath 'lib/*:usrlib/*' \
    org.wikimedia.discovery.cirrus.updater.consumer.ConsumerApplication \
    --event-stream-config-url "file:$(pwd)/resources/event-stream-config.json" \
    --event-stream-json-schema-urls "file:$(pwd)/resources/schema_repo" \
    --elasticsearch-urls.omega https://localhost:9243 \
    --kafka-source-config.bootstrap.servers PLAINTEXT://localhost:9092 \
    --kafka-source-config.group.id test \
    --fetch-error-topic eqiad.cirrussearch.update_pipeline.fetch_error.rc0 \
    --fetch-request-timeout 5s \
    --fetch-retry-max-age 15s \
    --pipeline.name eqiad.test.consumer \
    --dry-run true
