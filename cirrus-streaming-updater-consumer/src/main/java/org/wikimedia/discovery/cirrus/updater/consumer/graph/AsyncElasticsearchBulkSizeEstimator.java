package org.wikimedia.discovery.cirrus.updater.consumer.graph;

import java.util.Map;

import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.update.UpdateRequest;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import lombok.experimental.UtilityClass;

/**
 * Estimator for bulk request size. Introduced as workaround for limitations in the underlying
 * elasticsearch library.
 *
 * <p>{@code nElasticsearchWriter} uses {@code RequestIndexer} as a facade for ES' {@link
 * org.elasticsearch.action.bulk.BulkProcessor}. Originally, only the {@code add(…)} methods are
 * delegated. Under the hood, the {@link org.elasticsearch.action.bulk.BulkProcessor} takes care of
 * flushing bulk requests to the server when any of the configurable limits is hit (there's another
 * bug: flushing will happen after the limit has exceeded).
 *
 * <p>By replacing this logic we get estimates, that are closer to (our) reality.
 *
 * <ul>
 *   <li><a href="https://github.com/elastic/elasticsearch/issues/103406">BulkRequest does not
 *       consider script parameters in size estimation #103406</a>
 *   <li><a href="https://issues.apache.org/jira/browse/FLINK-33857">Expose BulkProcessor.flush via
 *       RequestIndexer to allow custom flush logic</a>
 * </ul>
 */
@UtilityClass
@SuppressWarnings("HideUtilityClassConstructor")
@SuppressFBWarnings(
        value = "HideUtilityClassConstructor",
        justification = "lombok takes care of that")
public class AsyncElasticsearchBulkSizeEstimator {

    /**
     * Minimum length of a bulk action. Replaces the original value of {@value #REQUEST_OVERHEAD} with
     * a more realistic value based on
     *
     * <ul>
     *   <li>average delete request: ~55 chars, and
     *   <li>average update (w/o script, comes in separate line): ~80 chars
     * </ul>
     */
    static final int REQUEST_OVERHEAD = 80;

    @SuppressFBWarnings("UP_UNUSED_PARAMETER")
    public static int estimate(DeleteRequest request) {
        return REQUEST_OVERHEAD;
    }

    public static int estimate(IndexRequest request) {
        return (request.source() != null ? request.source().length() : 0) + REQUEST_OVERHEAD;
    }

    public static int estimate(UpdateRequest request) {
        int size = 0;

        if (request.doc() != null) {
            size += request.doc().source().length();
        }
        if (request.upsertRequest() != null) {
            size += request.upsertRequest().source().length();
        }
        if (request.script() != null) {
            size += request.script().getIdOrCode().length() * 2;
            size += estimateMapSize(request.script().getParams());
        }
        return size + REQUEST_OVERHEAD;
    }

    private static int estimateKeySize(String key) {
        // Add size for string length and quotes
        return key.length() + 2;
    }

    private static int estimateListSize(Iterable<?> list) {
        int size = 0;

        // Add size for array brackets
        size += 2;

        // Add size for each list element
        for (Object element : list) {
            size += estimateValueSize(element) + 1; // 1 for the comma
        }

        return size;
    }

    private static int estimateMapSize(Map<String, Object> map) {
        int size = 0;

        // Add size for object braces
        size += 2;

        // Add size for each key-value pair
        for (Map.Entry<String, Object> entry : map.entrySet()) {
            size +=
                    estimateKeySize(entry.getKey())
                            + estimateValueSize(entry.getValue())
                            + 2; // 2 for quotes around keys
        }

        return size;
    }

    private static int estimateValueSize(Object value) {
        int size = 0;

        if (value instanceof Map) {
            // Recursive estimation for nested maps
            size += estimateMapSize((Map<String, Object>) value);
        } else if (value instanceof Iterable) {
            // Recursive estimation for nested lists
            size += estimateListSize((Iterable<?>) value);
        } else if (value instanceof String) {
            // Add size for string length and quotes
            size += ((String) value).length() + 2;
        } else if (value instanceof Number) {
            // Add size for number of digits
            size += String.valueOf(value).length();
        } else if (value instanceof Boolean) {
            // Add size for "true" or "false"
            size += ((boolean) value) ? 4 : 5;
        } else if (value == null) {
            // Add size for "null"
            size += 4;
        }

        return size;
    }
}
