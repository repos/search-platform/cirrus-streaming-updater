package org.wikimedia.discovery.cirrus.updater.consumer.graph;

import static org.wikimedia.discovery.cirrus.updater.common.logging.LoggingContext.log;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.apache.flink.api.java.typeutils.RowTypeInfo;
import org.apache.flink.types.Row;
import org.wikimedia.discovery.cirrus.updater.common.PageIdMismatchException;
import org.wikimedia.discovery.cirrus.updater.common.model.CirrusDocJsonHelper;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateEvent;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateEvent.Update;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateEvent.UpdateEventBuilder;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import lombok.experimental.UtilityClass;
import lombok.extern.log4j.Log4j2;

/**
 * Utility class for merging a document fetched from MediaWiki/CirrusSearch into an {@link
 * UpdateEvent}.
 */
@UtilityClass
@SuppressWarnings("HideUtilityClassConstructor")
@SuppressFBWarnings(
        value = "HideUtilityClassConstructor",
        justification = "lombok takes care of that")
@Log4j2
public class CirrusDocMerger {

    public static UpdateEvent merge(UpdateEvent originalEvent, CirrusDocJsonHelper doc) {
        final UpdateEventBuilder mergedEventBuilder = originalEvent.toBuilder();

        if (originalEvent.getTargetDocument().getPageId() != null
                && originalEvent.getTargetDocument().getPageId() != doc.getPageId()) {

            throw new PageIdMismatchException(
                    "Page ID mismatch: expected "
                            + originalEvent.getTargetDocument().getPageId()
                            + " but fetched "
                            + doc.getPageId()
                            + "; "
                            + originalEvent);
        }

        if (originalEvent.getRevId() != null && originalEvent.getRevId() != doc.getRevId()) {
            throw new IllegalArgumentException(
                    "Revision ID mismatch: expected "
                            + originalEvent.getRevId()
                            + " but fetched "
                            + doc.getRevId()
                            + "; "
                            + originalEvent);
        }

        mergedEventBuilder.revId(doc.getRevId());

        mergedEventBuilder.targetDocument(
                originalEvent.getTargetDocument().toBuilder()
                        .pageId(doc.getPageId())
                        .indexName(doc.getIndexName())
                        .pageNamespace(doc.getNamespace())
                        .clusterGroup(doc.getClusterGroup())
                        .build());

        mergedEventBuilder.update(mergeUpdate(originalEvent, doc));

        verifyAndLogExtraFields(doc, originalEvent);
        return mergedEventBuilder.build();
    }

    private static Update mergeUpdate(UpdateEvent originalEvent, CirrusDocJsonHelper doc) {
        final Update originalUpdate = originalEvent.getUpdate();

        final Map<String, String> mergedNoopHints = new HashMap<>();

        final Update.UpdateBuilder mergedUpdateBuilder;

        if (originalUpdate != null) {
            final Row mergedRawFields = doc.getRawFields();
            final Row originalFields = originalUpdate.getRawFields();
            if (originalFields != null) {
                // merge the fields computed from the producer job with the ones received from the doc
                // endpoint the fields from the produce job should supersede the new ones
                mergeRawFields(mergedRawFields, originalFields);
            }
            mergedUpdateBuilder = originalUpdate.toBuilder();
            final Map<String, String> originalNoopHints = originalUpdate.getNoopHints();
            if (originalNoopHints != null) {
                mergedNoopHints.putAll(originalNoopHints);
            }
            mergedUpdateBuilder.rawFields(mergedRawFields);
        } else {
            mergedUpdateBuilder = Update.builder();
            mergedUpdateBuilder.rawFields(doc.getRawFields());
        }

        final Map<String, String> docNoopHints = doc.getNoopHints();
        if (docNoopHints != null) {
            docNoopHints.forEach(mergedNoopHints::putIfAbsent);
        }
        mergedUpdateBuilder.noopHints(mergedNoopHints);
        mergedUpdateBuilder.rawFieldsFetchedAt(doc.getFetchedAt());

        return mergedUpdateBuilder.build();
    }

    private static void mergeRawFields(Row mergedRawFields, Row originalFields) {
        // The Row from the originalEvent and the Row built from CirrusDocJsonHelper might not share
        // the same set of field names. The originalEvent might have been serialized using a previous
        // serializer
        Set<String> docFieldNames = Objects.requireNonNull(mergedRawFields.getFieldNames(true));
        originalFields.getFieldNames(true).stream()
                .filter(docFieldNames::contains)
                .forEach(
                        fieldName -> {
                            if (originalFields.getField(fieldName) != null) {
                                mergedRawFields.setField(fieldName, originalFields.getField(fieldName));
                            }
                        });
    }

    public static void verifyAndLogExtraFields(CirrusDocJsonHelper doc, UpdateEvent updateEvent) {
        List<String> extraFieldNames =
                StreamSupport.stream(
                                Spliterators.spliteratorUnknownSize(
                                        doc.getCirrusDoc().fieldNames(), Spliterator.ORDERED),
                                false)
                        .filter(
                                f -> ((RowTypeInfo) doc.getFieldsSchema().getProducedType()).getFieldIndex(f) == -1)
                        .collect(Collectors.toList());
        if (!extraFieldNames.isEmpty()) {
            log(
                    log::atWarn,
                    () ->
                            String.format(
                                    Locale.ENGLISH,
                                    "Found unexpected field names %s for page %d in %s",
                                    String.join(",", extraFieldNames),
                                    updateEvent.getTargetDocument().getPageId(),
                                    updateEvent.getTargetDocument().getWikiId()),
                    updateEvent);
        }
    }
}
