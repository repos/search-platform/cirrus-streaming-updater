package org.wikimedia.discovery.cirrus.updater.consumer.graph;

import java.io.IOException;
import java.util.Collection;
import java.util.List;

import org.apache.flink.annotation.VisibleForTesting;
import org.apache.flink.connector.base.sink.AsyncSinkBase;
import org.apache.flink.connector.base.sink.writer.BufferedRequestState;
import org.apache.flink.connector.base.sink.writer.ElementConverter;
import org.apache.flink.connector.base.sink.writer.config.AsyncSinkWriterConfiguration;
import org.apache.flink.connector.base.sink.writer.config.AsyncSinkWriterConfiguration.AsyncSinkWriterConfigurationBuilder;
import org.apache.flink.connector.base.sink.writer.strategy.RateLimitingStrategy;
import org.apache.flink.connector.base.sink.writer.strategy.RequestInfo;
import org.apache.flink.connector.base.sink.writer.strategy.ResultInfo;
import org.apache.flink.core.io.SimpleVersionedSerializer;
import org.wikimedia.discovery.cirrus.updater.consumer.graph.AsyncElasticsearchBulkRequestBridge.AsyncElasticsearchBulkRequestBridgeFactory;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import lombok.Builder;

@SuppressWarnings("serial")
@SuppressFBWarnings(value = "SE_NO_SERIALVERSIONID")
public class AsyncElasticsearchSink<I> extends AsyncSinkBase<I, AsyncElasticsearchBulkRequest> {

    @VisibleForTesting protected final AsyncElasticsearchBulkRequestBridgeFactory bridgeFactory;

    @Builder
    protected AsyncElasticsearchSink(
            ElementConverter<I, AsyncElasticsearchBulkRequest> elementConverter,
            AsyncElasticsearchBulkRequestBridgeFactory bridgeFactory,
            AsyncSinkWriterConfiguration writerConfig) {
        super(
                elementConverter,
                writerConfig.getMaxBatchSize(),
                writerConfig.getMaxInFlightRequests(),
                writerConfig.getMaxBufferedRequests(),
                writerConfig.getMaxBatchSizeInBytes(),
                writerConfig.getMaxTimeInBufferMS(),
                writerConfig.getMaxRecordSizeInBytes());
        this.bridgeFactory = bridgeFactory;
    }

    @Override
    public StatefulSinkWriter<I, BufferedRequestState<AsyncElasticsearchBulkRequest>> createWriter(
            InitContext context) throws IOException {
        return new AsyncElasticsearchWriter<>(
                getElementConverter(), context, toWriterConfiguration().build(), bridgeFactory, List.of());
    }

    protected AsyncSinkWriterConfigurationBuilder toWriterConfiguration() {
        final AsyncSinkWriterConfigurationBuilder builder = new AsyncSinkWriterConfigurationBuilder();
        builder.setMaxBatchSize(getMaxBatchSize());
        builder.setMaxInFlightRequests(getMaxInFlightRequests());
        builder.setMaxBufferedRequests(getMaxBufferedRequests());
        builder.setMaxBatchSizeInBytes(getMaxBatchSizeInBytes());
        builder.setMaxTimeInBufferMS(getMaxTimeInBufferMS());
        builder.setMaxRecordSizeInBytes(getMaxRecordSizeInBytes());
        builder.setRateLimitingStrategy(
                new MaxInFlightRequestsRateLimitingStrategy(getMaxBatchSize(), getMaxInFlightRequests()));
        return builder;
    }

    @Override
    public StatefulSinkWriter<I, BufferedRequestState<AsyncElasticsearchBulkRequest>> restoreWriter(
            InitContext context,
            Collection<BufferedRequestState<AsyncElasticsearchBulkRequest>> recoveredState)
            throws IOException {
        return new AsyncElasticsearchWriter<>(
                getElementConverter(),
                context,
                toWriterConfiguration().build(),
                bridgeFactory,
                recoveredState);
    }

    @Override
    public SimpleVersionedSerializer<BufferedRequestState<AsyncElasticsearchBulkRequest>>
            getWriterStateSerializer() {
        return new AsyncElasticsearchSinkWriterSerializer();
    }

    /** A rate limiting strategy that enforces the maximum number of in-flight requests. */
    public static final class MaxInFlightRequestsRateLimitingStrategy
            implements RateLimitingStrategy {

        private final int maxBatchSize;
        private final int maxInFlightRequests;
        private int inFlightRequestsCount;

        public MaxInFlightRequestsRateLimitingStrategy(int maxBatchSize, int maxInFlightRequests) {
            this.maxBatchSize = maxBatchSize;
            this.maxInFlightRequests = maxInFlightRequests;
        }

        @Override
        public void registerInFlightRequest(RequestInfo requestInfo) {
            if (inFlightRequestsCount == maxInFlightRequests) {
                throw new IllegalStateException(
                        "Maximum of in flight requests of " + maxInFlightRequests + " has been reached");
            }
            ++inFlightRequestsCount;
        }

        @Override
        public void registerCompletedRequest(ResultInfo resultInfo) {
            --inFlightRequestsCount;
        }

        @Override
        public boolean shouldBlock(RequestInfo requestInfo) {
            return inFlightRequestsCount == maxInFlightRequests;
        }

        @Override
        public int getMaxBatchSize() {
            return maxBatchSize;
        }
    }
}
