package org.wikimedia.discovery.cirrus.updater.consumer.sanity;

import java.time.Duration;
import java.time.Instant;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import javax.annotation.Nullable;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.flink.annotation.VisibleForTesting;
import org.apache.flink.metrics.Counter;
import org.apache.flink.metrics.Gauge;
import org.apache.flink.metrics.MetricGroup;
import org.apache.flink.util.Preconditions;
import org.wikimedia.discovery.cirrus.updater.common.model.SanityCheck;
import org.wikimedia.discovery.cirrus.updater.common.util.KeyedPriorityQueue;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * The scheduling mechanism for the CirrusSearch Saneitizer.
 *
 * <p>On first startup, without a checkpoint, the loop will start with an empty state. On first
 * invocation it will poll the wikiSupplier and create a LoopState instance for each provided
 * domain. These LoopState instances track the progress of the loop and are persisted into flink
 * checkpoints. Any restart of the flink app will continue the loop from the states stored in the
 * checkpoint. The wiki and max page id suppliers are polled on a daily basis to keep the state of
 * this long-running process aligned with the state of the world.
 *
 * <p>The loop expects the caller to query the nextInvokeAt() method, wait until at or after the
 * specified time, and then invoke the next() method. Invoking next() prior to the specified time is
 * allowed but will not produce any records. This process should repeat in a simple loop.
 *
 * <p>The SaneitizeLoop will sequentially emit SanityCheck events for each domain starting from page
 * id 0 and going up to the max page id, with batchSize page id's per event. It will emit these
 * events at a constant rate evenly spread over the loopDuration. From the target loop duration, max
 * page id, and batchSize we calculate a constant time delay between events. The next event is
 * always scheduled for when the previous event was emitted, plus this delay. The only catchup
 * applied is that we run 1% faster than strict calculation would imply, which results in a 14 day
 * loop finishing 3 hours early. This isn't strictly required, but aims to help the loop finish
 * before the target duration. Without this we would always finish after the loop was expected to
 * complete. We still might, but hopefully not typically.
 */
@Slf4j
public class SaneitizeLoop implements Loop {
    public static final Duration RECONCILIATION_FREQUENCY = Duration.ofDays(1);
    public static final Duration RECONCILIATION_FREQUENCY_ON_ERROR = Duration.ofMinutes(10);
    private final Metrics metrics;
    /**
     * Provides the set of domains that should be checked. This is polled occasionally and the
     * existing state is reconciled against the expected list of wikis.
     */
    private final SanitySource.WikiSupplier wikiSupplier;
    /** Accepts the domain name of a wiki and returns the max page id of that wiki. */
    private final SanitySource.MaxPageIdLookup maxPageIdLookup;
    /**
     * Target duration between loops. This is treated as a minimum duration, we will never go faster
     * than this. We may go slower.
     */
    private final Duration loopDuration;
    /** The number of page ids to emit in a single SanityCheck. */
    private final int batchSize;
    /** (Optional) When the loop should end. */
    @Nullable private final Instant endAt;
    /**
     * Current loop states with pending batches. Note that this is maintained in parallel to
     * SanitySourceSplitState::state. Care must be taken to keep them aligned.
     */
    private final KeyedPriorityQueue<String, LoopState> pq =
            new KeyedPriorityQueue<>(LoopState::getDomain, Comparator.comparing(this::nextSanityCheck));
    /**
     * When to perform the next reconciliation between wikiSupplier and runtime state. This is
     * per-instance and not mirrored into the split state. Default value ensures we always perform a
     * reconciliation on startup, in case of configuration change.
     */
    private Instant nextWikiReconciliation = Instant.MIN;
    /**
     * Keeps track of wikis that have failed their last max page id refresh. The value is the earliest
     * time to allow the next refresh. This is also per-instance and not mirrored into the split
     * state. After restart any LoopState with pending refresh will refresh on first event generation
     * and re-add to here if there are still problems.
     */
    private final Map<String, Instant> failingMaxPageIdLookups = new HashMap<>();

    public SaneitizeLoop(
            Collection<LoopState> state,
            MetricGroup metricGroup,
            SanitySource.WikiSupplier wikiSupplier,
            SanitySource.MaxPageIdLookup maxPageIdLookup,
            Duration loopDuration,
            int batchSize,
            @Nullable Instant endAt) {
        this.metrics = new Metrics(metricGroup, state);
        this.wikiSupplier = wikiSupplier;
        this.maxPageIdLookup = maxPageIdLookup;
        this.loopDuration = loopDuration;
        this.batchSize = batchSize;
        this.endAt = endAt;
        this.pq.addAll(state);
    }

    private boolean hasPendingRecords(Instant now) {
        if (this.pq.isEmpty()) {
            log.warn("Polled for records but current state is empty. Waiting for wiki reconciliation");
            return false;
        }
        return !now.isBefore(nextSanityCheck(this.pq.peek()));
    }

    /**
     * Produce any records scheduled prior to now.
     *
     * @param now The current clock time.
     * @return True when the loop is finished and will never again produce records.
     */
    @Override
    public boolean next(Instant now, Consumer<SanitySourceRecord> consumer) {
        if (endAt != null && !now.isBefore(endAt)) {
            return true;
        }
        maybeReconcileSetOfWikis(now, consumer);
        while (hasPendingRecords(now)) {
            LoopState loopState = this.pq.poll();
            Preconditions.checkState(loopState != null);
            // Reset prior to updating max page id. Shouldn't happen enough to matter but
            // avoids problem where the max page id keeps moving and the loop never finishes.
            if (loopState.isFinished()) {
                loopState = loopState.resetForNextLoop(now);
            }
            if (now.isAfter(nextMaxPageIdRefreshAfter(loopState))) {
                loopState = refreshMaxPageId(loopState, now);
            }
            // Note that this is maintaining parallel state, the current pq state needs to
            // align with the SanitySourceSplitState, which is updated with the LoopState
            // in SanitySourceRecordEmitter.
            LoopState next = loopState.emitNextBatch(now, batchSize);
            consumer.accept(
                    SanitySourceRecord.forNextState(
                            next,
                            new SanityCheck(
                                    loopState.getDomain(),
                                    loopState.getLastEmittedPageId() + 1,
                                    batchSize,
                                    now,
                                    loopState.getLoopId())));
            this.pq.add(next);
            this.metrics.onNextEvent(loopState, next);
        }
        return false;
    }

    /** @return The next time we would like SaneitizeLoop::next to be invoked */
    @Override
    public Instant nextInvokeAt() {
        if (pq.isEmpty()) {
            return ObjectUtils.min(nextWikiReconciliation, endAt);
        }
        return ObjectUtils.min(nextSanityCheck(pq.peek()), nextWikiReconciliation, endAt);
    }

    private double pagesPerSecond(LoopState loopState) {
        return loopState.getMaxPageId() / (float) loopDuration.toSeconds();
    }

    private Instant nextSanityCheck(LoopState loopState) {
        if (loopState.isFinished()) {
            return loopState.getStartedAt().plus(loopDuration);
        }
        // Special case for starting the first loop. The lastEmittedAt value is set to
        // the loop start time to avoid having null values, but without this
        // case the first event would be fired secondsPerBatch after the loop start
        // instead of at loop start.
        if (loopState.isStartingNewLoop()) {
            return loopState.getStartedAt();
        }
        double secondsPerBatch = 0.99 * batchSize / pagesPerSecond(loopState);
        return loopState.getLastEmittedAt().plusMillis((long) (secondsPerBatch * 1000));
    }

    private void maybeReconcileSetOfWikis(Instant now, Consumer<SanitySourceRecord> consumer) {
        if (now.isBefore(nextWikiReconciliation)) {
            return;
        }
        this.nextWikiReconciliation = now.plus(RECONCILIATION_FREQUENCY);
        wikiSupplier
                .get()
                .ifPresentOrElse(
                        expected -> reconcileSetOfWikis(now, consumer, expected),
                        () -> retryReconciliationLater(now));
    }

    private void reconcileSetOfWikis(
            Instant now, Consumer<SanitySourceRecord> consumer, Set<String> expected) {
        Set<String> current = this.pq.keySet();
        // domains that exist but should not
        current.stream()
                .filter(Predicate.not(expected::contains))
                // We have to collect to avoid concurrent modification errors
                .collect(Collectors.toList())
                .forEach(
                        domain -> {
                            log.info("Removing no-longer requested domain {}", domain);
                            consumer.accept(SanitySourceRecord.removeFromState(domain));
                            pq.removeIf(loopState -> domain.equals(loopState.getDomain()));
                        });
        // domains that do not exist but should
        expected.stream()
                .filter(Predicate.not(current::contains))
                .forEach(
                        domain ->
                                maxPageIdLookup
                                        .apply(domain)
                                        .ifPresentOrElse(
                                                maxPageId -> {
                                                    log.info("Added new domain {} with maxPageId of {}", domain, maxPageId);
                                                    LoopState loopState = new LoopState(domain, maxPageId, now);
                                                    pq.add(loopState);
                                                    consumer.accept(SanitySourceRecord.forNextState(loopState, null));
                                                    metrics.addDomain(domain);
                                                },
                                                () -> retryReconciliationLater(now)));
    }

    private void retryReconciliationLater(Instant now) {
        Instant onError = now.plus(RECONCILIATION_FREQUENCY_ON_ERROR);
        // Avoid spamming duplicate messages if multiple max page id lookups fail
        if (!nextWikiReconciliation.equals(onError)) {
            log.warn("Failed wiki reconciliation, will try again at {}", onError);
            nextWikiReconciliation = onError;
        }
    }

    /**
     * The earliest time to allow a max page id refresh. Refresh happens during the next sanity check
     * event generation after this instant.
     */
    private Instant nextMaxPageIdRefreshAfter(LoopState loopState) {
        return ObjectUtils.getFirstNonNull(
                () -> failingMaxPageIdLookups.get(loopState.getDomain()),
                () -> loopState.getLastMaxPageIdRefresh().plus(RECONCILIATION_FREQUENCY));
    }

    private LoopState refreshMaxPageId(LoopState loopState, Instant now) {
        return maxPageIdLookup
                .apply(loopState.getDomain())
                .map(
                        maxPageId -> {
                            failingMaxPageIdLookups.remove(loopState.getDomain());
                            return loopState.refreshMaxPageId(now, maxPageId);
                        })
                .orElseGet(
                        () -> {
                            failingMaxPageIdLookups.put(
                                    loopState.getDomain(), now.plus(RECONCILIATION_FREQUENCY_ON_ERROR));
                            return loopState;
                        });
    }

    /**
     * Allows tests to skip the reconciliation on startup, which can hide LoopState invoke times from
     * this::nextInvokeAt.
     */
    @VisibleForTesting
    public void setNextWikiReconciliation(Instant instant) {
        this.nextWikiReconciliation = instant;
    }

    class Metrics {
        static final String NUM_FAILING_MAX_PAGE_ID_LOOKUPS = "num_failing_max_page_id_lookups";
        static final String NUM_EMITTED_EVENTS_METRIC_GROUP = "num_events_emitted";
        static final String EVENT_LATENESS_METRIC_GROUP = "event_late_seconds";
        static final String LOOP_COMPLETION_METRIC_GROUP = "loop_completion_percent";

        private final MetricGroup metricGroup;
        /** Per-domain metric incremented for each SanityCheck event. */
        private final Map<String, Counter> numEventsEmitted = new HashMap<>();
        /**
         * Per-domain metric updated on each SanityCheck event. There are no clocks in SaneitizeLoop, we
         * need the MutableGauge to remember the provided instants.
         */
        private final Map<String, MutableGauge<Long>> eventLateness = new HashMap<>();

        Metrics(MetricGroup metricGroup, Collection<LoopState> state) {
            this.metricGroup = metricGroup;
            state.stream().map(LoopState::getDomain).forEach(this::addDomain);
            // We never use this directly, it's simply polled, but it seems odd
            // to create it in the constructor but not hold onto it.
            metricGroup.gauge(NUM_FAILING_MAX_PAGE_ID_LOOKUPS, failingMaxPageIdLookups::size);
        }

        void addDomain(String domain) {
            if (numEventsEmitted.containsKey(domain)) {
                // Should be exceptionally rare, but we don't have any way to unregister metrics.
                // If a domain were to be added, removed, and re-added, it would still be here.
                return;
            }
            MetricGroup domainGroup = metricGroup.addGroup("domain", domain);
            numEventsEmitted.put(domain, domainGroup.counter(NUM_EMITTED_EVENTS_METRIC_GROUP));
            // uses null so that on startup before the first event is emitted per-domain the metric
            // reports a break in the line and not, for example, 0.
            eventLateness.put(
                    domain, domainGroup.gauge(EVENT_LATENESS_METRIC_GROUP, new MutableGauge<>(null)));

            domainGroup.gauge(
                    LOOP_COMPLETION_METRIC_GROUP,
                    () -> pq.get(domain).map(this::completionPercent).orElse(0f));
        }

        private float completionPercent(LoopState loopState) {
            return Math.max(
                    0, Math.min(1, loopState.getLastEmittedPageId() / (float) loopState.getMaxPageId()));
        }

        /**
         * @return The time that the next batch of page id's should be emitted, assuming we are 100% on
         *     schedule. Used to report when the loops are falling behind the expected runtime. Must not
         *     be used to decide when to emit events. Note that max page id updates will cause small
         *     step changes in the reported estimate.
         */
        private Instant idealizedRuntimeEstimate(LoopState loopState) {
            long nextBatchStart = 1 + loopState.getLastEmittedPageId();
            double secondsPerPage = 1 / pagesPerSecond(loopState);
            return loopState.getStartedAt().plusSeconds((int) (nextBatchStart * secondsPerPage));
        }

        void onNextEvent(LoopState previous, LoopState next) {
            numEventsEmitted.get(next.getDomain()).inc();
            Instant expectedAt = idealizedRuntimeEstimate(previous);
            Duration lateness = Duration.between(expectedAt, next.getLastEmittedAt());
            eventLateness.get(next.getDomain()).setValue(lateness.toSeconds());
        }
    }

    @AllArgsConstructor
    @Getter
    @Setter
    private static final class MutableGauge<T> implements Gauge<T> {
        private T value;
    }
}
