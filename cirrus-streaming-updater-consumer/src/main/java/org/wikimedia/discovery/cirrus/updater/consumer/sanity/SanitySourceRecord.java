package org.wikimedia.discovery.cirrus.updater.consumer.sanity;

import java.util.Optional;

import org.wikimedia.discovery.cirrus.updater.common.model.SanityCheck;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * The record is a temporary object that is emitted by the split reader. It carries information
 * about changes to apply to the split state, and an optional event to emit.
 */
@EqualsAndHashCode
abstract class SanitySourceRecord {
    static SanitySourceRecord forNextState(LoopState nextLoopState, SanityCheck sanityCheck) {
        return new UpdateSplitState(nextLoopState, sanityCheck);
    }

    static SanitySourceRecord removeFromState(String domain) {
        return new RemoveFromState(domain);
    }

    public abstract void applyStateUpdate(SanitySourceSplitState splitState);

    public abstract Optional<SanityCheck> getEvent();

    @AllArgsConstructor
    @EqualsAndHashCode
    @ToString
    private static final class UpdateSplitState extends SanitySourceRecord {
        private final LoopState nextLoopState;
        private final SanityCheck event;

        @Override
        public void applyStateUpdate(SanitySourceSplitState splitState) {
            splitState.put(nextLoopState);
        }

        @Override
        public Optional<SanityCheck> getEvent() {
            return Optional.ofNullable(event);
        }
    }

    @AllArgsConstructor
    @EqualsAndHashCode
    @ToString
    private static final class RemoveFromState extends SanitySourceRecord {
        private final String domain;

        @Override
        public void applyStateUpdate(SanitySourceSplitState splitState) {
            splitState.remove(domain);
        }

        @Override
        public Optional<SanityCheck> getEvent() {
            return Optional.empty();
        }
    }
}
