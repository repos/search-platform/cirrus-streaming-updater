package org.wikimedia.discovery.cirrus.updater.consumer.sanity;

import java.io.IOException;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;

import org.apache.flink.connector.base.source.reader.RecordsBySplits;
import org.apache.flink.connector.base.source.reader.RecordsWithSplitIds;
import org.apache.flink.connector.base.source.reader.splitreader.SplitReader;
import org.apache.flink.connector.base.source.reader.splitreader.SplitsAddition;
import org.apache.flink.connector.base.source.reader.splitreader.SplitsChange;
import org.apache.flink.util.SerializableObject;
import org.wikimedia.eventutilities.core.SerializableClock;

import de.thetaphi.forbiddenapis.SuppressForbidden;

class SanitySourceSplitReader implements SplitReader<SanitySourceRecord, SanitySourceSplit> {
    private static final Duration NO_SPLITS_SLEEP_TIME = Duration.ofMinutes(1);

    /** Used to coordinate sleep/wakeup for our simulated polling. */
    private final Object sleeper = new SerializableObject();

    /**
     * Set of running splits. Map from splitId to runtime implementation for that split. The
     * SanitySourceSplitEnumerator guarantees there will only ever be one split, and it will either be
     * running or paused, but we use map's for simplicity of aligning with the flink api.
     */
    private final Map<String, Loop> running = new HashMap<>(1);
    /**
     * Set of paused splits. Map from splitId to runtime implementation for that split. The
     * SanitySourceSplitEnumerator guarantees there will only ever be one split, and it will either be
     * running or paused, but we use map's for simplicity of aligning with the flink api.
     */
    private final Map<String, Loop> paused = new HashMap<>(1);
    /** Factory for converting state into the runtime implementation. */
    private final Function<Collection<LoopState>, Loop> loopFactory;

    private final SerializableClock clock;

    SanitySourceSplitReader(
            Function<Collection<LoopState>, Loop> loopFactory, SerializableClock clock) {
        this.loopFactory = loopFactory;
        this.clock = clock;
    }

    /** Simulates long-polling. Wait for records to be available, then yield them. */
    @Override
    public RecordsWithSplitIds<SanitySourceRecord> fetch() throws IOException {
        waitForRecords(clock.get());
        return fetch(clock.get());
    }

    private RecordsWithSplitIds<SanitySourceRecord> fetch(Instant now) {
        RecordsBySplits.Builder<SanitySourceRecord> records = new RecordsBySplits.Builder<>();
        List<String> toRemove = new ArrayList<>();
        running.forEach(
                (splitId, loop) -> {
                    boolean finished = loop.next(now, sourceRecord -> records.add(splitId, sourceRecord));
                    if (finished) {
                        records.addFinishedSplit(splitId);
                        toRemove.add(splitId);
                    }
                });
        // Should probably try to synchronize with split state? but we always have 1 split
        // and are shutting down imminently. Even this is plausibly unnecessary.
        toRemove.forEach(running::remove);
        return records.build();
    }

    @Override
    public void handleSplitsChanges(SplitsChange<SanitySourceSplit> splitsChange) {
        if (!(splitsChange instanceof SplitsAddition)) {
            throw new UnsupportedOperationException("Only SplitsAddition is supported");
        }
        splitsChange
                .splits()
                .forEach(split -> running.put(split.splitId(), loopFactory.apply(split.state.values())));
    }

    @Override
    @SuppressForbidden
    public void wakeUp() {
        synchronized (sleeper) {
            sleeper.notifyAll();
        }
    }

    /** Optional.empty() is returned if the next invocation is in the past. */
    private Optional<Duration> timeToNextRecord(Instant now) {
        if (running.isEmpty()) {
            return Optional.of(NO_SPLITS_SLEEP_TIME);
        }
        return running.values().stream()
                .map(Loop::nextInvokeAt)
                .min(Instant::compareTo)
                .filter(nextInvokeAt -> nextInvokeAt.isAfter(now))
                .map(nextInvokeAt -> Duration.between(now, nextInvokeAt));
    }

    @SuppressForbidden
    private void waitForRecords(Instant now) {
        Optional<Duration> waitTime = timeToNextRecord(now);
        if (waitTime.isEmpty()) {
            return;
        }

        // Add a ms to make sure we wake up after the target time.
        long millis = 1 + waitTime.get().toMillis();
        synchronized (sleeper) {
            try {
                sleeper.wait(millis);
            } catch (InterruptedException e) {
                // ending before the desired wait time will do an empty fetch and return to flink.
                Thread.currentThread().interrupt();
            }
        }
    }

    @Override
    public void close() throws Exception {
        // Nothing to clean up
    }

    @Override
    public void pauseOrResumeSplits(
            Collection<SanitySourceSplit> splitsToPause, Collection<SanitySourceSplit> splitsToResume) {
        splitsToPause.forEach(split -> paused.put(split.splitId(), running.remove(split.splitId())));
        splitsToResume.forEach(split -> running.put(split.splitId(), paused.remove(split.splitId())));
    }
}
