package org.wikimedia.discovery.cirrus.updater.consumer.sanity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.apache.flink.api.connector.source.SplitEnumerator;
import org.apache.flink.api.connector.source.SplitEnumeratorContext;
import org.jetbrains.annotations.Nullable;

import lombok.extern.slf4j.Slf4j;

@Slf4j
class SanitySourceSplitEnumerator
        implements SplitEnumerator<SanitySourceSplit, SanitySourceEnumState> {
    private final SplitEnumeratorContext<SanitySourceSplit> context;
    private final List<SanitySourceSplit> pendingAssignment;
    private boolean shutdown;

    SanitySourceSplitEnumerator(
            SplitEnumeratorContext<SanitySourceSplit> context,
            Collection<SanitySourceSplit> pendingAssignment) {
        this.context = context;
        this.pendingAssignment = new ArrayList<>(pendingAssignment);
    }

    @Override
    public void start() {
        log.info("Starting enumerator with {} registered readers", context.registeredReaders().size());
        assignPendingPartitionSplits(context.registeredReaders().keySet());
    }

    @Override
    public void handleSplitRequest(int subtaskId, @Nullable String requesterHostname) {
        // We need to notify no more splits from the enumerator, but the enumerator is never
        // told about splits completing. We instead have the split reader issue a split request
        // when it's finished allowing us to signal no more splits.
        // We only ever have 1 split, but parallelism might convince flink to start additional
        // subtasks. Shut them all down.
        log.info("Received split request, shutting down");
        shutdown = true;
        context.registeredReaders().keySet().forEach(context::signalNoMoreSplits);
    }

    @Override
    public void addSplitsBack(List<SanitySourceSplit> splits, int subtaskId) {
        pendingAssignment.addAll(splits);
        assignPendingPartitionSplits(context.registeredReaders().keySet());
    }

    @Override
    public void addReader(int subtaskId) {
        if (shutdown) {
            log.info("Attempting to add a reader during shutdown. Tell it to shut down too.");
            context.signalNoMoreSplits(subtaskId);
        } else {
            assignPendingPartitionSplits(Collections.singleton(subtaskId));
        }
    }

    @Override
    public SanitySourceEnumState snapshotState(long checkpointId) throws Exception {
        return new SanitySourceEnumState(pendingAssignment);
    }

    @Override
    public void close() throws IOException {
        // Nothing to clean up
    }

    private void assignPendingPartitionSplits(Set<Integer> pendingReaders) {
        // We should only have a single split, assign everything to the first available reader
        log.info(
                "Attempting to assign {} splits with {} pending readers",
                pendingAssignment.size(),
                pendingReaders.size());
        pendingReaders.stream()
                .findFirst()
                .ifPresent(
                        subtask -> {
                            log.info("Assigning splits to subtask {}", subtask);
                            for (SanitySourceSplit split : pendingAssignment) {
                                context.assignSplit(split, subtask);
                            }
                            pendingAssignment.clear();
                        });
    }
}
