package org.wikimedia.discovery.cirrus.updater.consumer.graph;

import java.net.URI;

import org.wikimedia.discovery.cirrus.updater.common.graph.CirrusEndpoint;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateEvent;

import com.fasterxml.jackson.databind.JsonNode;

public class CombinedCirrusDocEndpoint implements CirrusEndpoint<UpdateEvent, UpdateEvent> {

    private final RevisionCirrusDocEndpoint revBasedEndpoint;
    private final RerenderCirrusDocEndpoint rerenderCirrusDocEndpoint;

    public CombinedCirrusDocEndpoint(
            RevisionCirrusDocEndpoint revBasedEndpoint,
            RerenderCirrusDocEndpoint rerenderCirrusDocEndpoint) {
        this.revBasedEndpoint = revBasedEndpoint;
        this.rerenderCirrusDocEndpoint = rerenderCirrusDocEndpoint;
    }

    @Override
    public URI buildURI(UpdateEvent event) {
        return getEndpoint(event).buildURI(event);
    }

    @Override
    public UpdateEvent extractAndAugment(UpdateEvent event, JsonNode response) {
        return getEndpoint(event).extractAndAugment(event, response);
    }

    private CirrusEndpoint<UpdateEvent, UpdateEvent> getEndpoint(UpdateEvent updateEvent) {
        switch (updateEvent.getChangeType()) {
            case REV_BASED_UPDATE:
                return revBasedEndpoint;
            case PAGE_RERENDER_UPSERT:
            case PAGE_RERENDER:
                return rerenderCirrusDocEndpoint;
            default:
                throw new IllegalArgumentException("Unsupported operation " + updateEvent.getChangeType());
        }
    }
}
