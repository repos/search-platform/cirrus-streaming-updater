package org.wikimedia.discovery.cirrus.updater.consumer.graph;

import java.net.URI;

import org.wikimedia.discovery.cirrus.updater.common.InvalidMWApiResponseException;
import org.wikimedia.discovery.cirrus.updater.common.RevisionHiddenException;
import org.wikimedia.discovery.cirrus.updater.common.RevisionNotFoundException;
import org.wikimedia.discovery.cirrus.updater.common.graph.CirrusEndpoint;
import org.wikimedia.discovery.cirrus.updater.common.model.ChangeType;
import org.wikimedia.discovery.cirrus.updater.common.model.CirrusDocJsonHelper;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateEvent;
import org.wikimedia.eventutilities.core.SerializableClock;
import org.wikimedia.eventutilities.flink.formats.json.JsonRowDeserializationSchema;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.BooleanNode;
import com.google.common.base.Preconditions;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class RevisionCirrusDocEndpoint implements CirrusEndpoint<UpdateEvent, UpdateEvent> {

    private static final String MW_API_PATH_AND_QUERY =
            "/w/api.php?action=query&format=json&cbbuilders=content%7Clinks&prop=cirrusbuilddoc&formatversion=2&format=json&revids=";
    private final ObjectMapper objectMapper;
    private final JsonRowDeserializationSchema fieldsSchema;
    private final SerializableClock clock;

    private static boolean isRevisionBasedEvent(UpdateEvent updateEvent) {
        return updateEvent.getChangeType() == ChangeType.REV_BASED_UPDATE;
    }

    @Override
    public URI buildURI(UpdateEvent event) {
        checkEvent(event);

        Long revId = event.getRevId();
        final String domain = event.getTargetDocument().getDomain();

        return URI.create("https://" + domain + MW_API_PATH_AND_QUERY + revId);
    }

    @Override
    public UpdateEvent extractAndAugment(UpdateEvent event, JsonNode response) {
        checkEvent(event);

        long eventRevId = event.getRevId();
        final JsonNode missingFlagNode = response.at("/query/badrevids/" + eventRevId + "/missing");
        final JsonNode pageNode = response.at("/query/pages/0");
        if (missingFlagNode instanceof BooleanNode && missingFlagNode.asBoolean()) {
            throw new RevisionNotFoundException("Revision " + eventRevId + " is missing");
        } else if (pageNode.has("texthidden")) {
            throw new RevisionHiddenException("Revision " + eventRevId + " is not publicly visible");
        } else if (!pageNode.has("cirrusbuilddoc")) {
            // NOTE: InvalidMWApiResponseException error is retryable, from our POV it is unclear if we
            // need to retry or not so we assume that retrying might yield different results.
            // Might be interesting to learn what common unrecoverable errors are so that we save a couple
            // api requests and properly identify them earlier either from MW or here.
            throw new InvalidMWApiResponseException(
                    "Unexpected response: /query/badrevids/"
                            + eventRevId
                            + "/missing is not set but neither is /query/pages/0/cirrusbuilddoc:\n"
                            + response.toPrettyString());
        }

        return CirrusDocMerger.merge(
                event, new CirrusDocJsonHelper(fieldsSchema, objectMapper, pageNode, clock.get()));
    }

    private static void checkEvent(UpdateEvent updateEvent) {
        Preconditions.checkArgument(
                !updateEvent.hasFetchedFields(), "Event already populated with a cirrus document");

        Preconditions.checkArgument(
                isRevisionBasedEvent(updateEvent), "Event is not a revision based event");

        Preconditions.checkArgument(updateEvent.getRevId() != null, "Event lacks a revision ID");

        Preconditions.checkArgument(
                updateEvent.getTargetDocument().getDomain() != null, "Event lacks a domain");
    }
}
