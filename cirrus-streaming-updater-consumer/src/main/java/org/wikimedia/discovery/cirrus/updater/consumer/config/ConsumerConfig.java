package org.wikimedia.discovery.cirrus.updater.consumer.config;

import static org.wikimedia.discovery.cirrus.updater.common.config.EventStreamConstants.FETCH_ERROR_STREAM;
import static org.wikimedia.discovery.cirrus.updater.common.config.ReadableConfigReader.getRequired;

import java.time.Duration;
import java.util.Arrays;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.annotation.Nullable;

import org.apache.flink.configuration.ConfigOption;
import org.apache.flink.configuration.ConfigOptions;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.configuration.MemorySize;
import org.apache.flink.configuration.PipelineOptions;
import org.apache.flink.configuration.description.Description;
import org.apache.flink.configuration.description.LinkElement;
import org.apache.flink.configuration.description.TextElement;
import org.wikimedia.discovery.cirrus.updater.common.config.CommonConfig;
import org.wikimedia.discovery.cirrus.updater.common.model.ChangeType;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.experimental.Accessors;
import lombok.experimental.FieldDefaults;
import lombok.experimental.SuperBuilder;

@Getter
@SuperBuilder
@Accessors(fluent = true)
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
public final class ConsumerConfig extends CommonConfig {

    /** Avg. size of a cirrusbuilddoc. */
    public static final MemorySize AVG_FETCH_DOC_SIZE = MemorySize.parse("10kb");
    /** Avg. size of an action, derived from 2 * {@link #AVG_FETCH_DOC_SIZE}. */
    public static final MemorySize AVG_ACTION_SIZE = AVG_FETCH_DOC_SIZE.multiply(2);

    public static final int ES_MAX_PARALLEL_REQUESTS = 1;
    /** Max. request size accepted by elasticsearch, in bytes. */
    public static final MemorySize ES_MAX_REQUEST_SIZE = MemorySize.parse("16mb");

    public static final long ES_MAX_ACTIONS =
            ES_MAX_REQUEST_SIZE.getBytes() / AVG_ACTION_SIZE.getBytes();
    public static final long BUFFER_MAX_ACTIONS = ES_MAX_ACTIONS * 3;
    /**
     * Allow cluster state to be committed (30s) and acknowledged (90s) before timing out to avoid
     * unnecessary application failures due to requests considered failed too early. See <a
     * href="https://www.elastic.co/guide/en/elasticsearch/reference/current/cluster-state-publishing.html">publishing
     * the cluster state</a>.
     */
    public static final Duration ES_SOCKET_TIMEOUT = Duration.ofMinutes(2).plusMillis(100);

    public static final String JOB_NAME = "cirrus-streaming-updater-consumer";
    /**
     * Max. size of a cirrusbuilddoc, see {@code wgCirrusSearchDocumentSizeLimiterProfile.max_size}.
     */
    public static final MemorySize MAX_FETCH_DOC_SIZE = MemorySize.parse("4mb");
    /** Max. size of an elasticsearch bulk action, derived from 2 * {@link #MAX_FETCH_DOC_SIZE}. */
    public static final MemorySize MAX_ACTION_SIZE =
            MAX_FETCH_DOC_SIZE.multiply(2).add(MemorySize.parse("1mb"));

    public static final float FETCH_RERENDER_SHARE = 0.85f;
    public static final int FETCH_POOL_SIZE = 300;
    public static final int FETCH_RERENDER_POOL_SIZE = (int) (FETCH_POOL_SIZE * FETCH_RERENDER_SHARE);

    public static final ConfigOption<MemorySize> PARAM_ELASTICSEARCH_BULK_FLUSH_MAX_ACTION_SIZE =
            ConfigOptions.key("elasticsearch-bulk-max-action-size")
                    .memoryType()
                    .defaultValue(MAX_ACTION_SIZE)
                    .withDescription(
                            Description.builder()
                                    .text("Max. size per action, in bytes")
                                    .linebreak()
                                    .text("This is a hard limit, currently it's capped at")
                                    .text(
                                            " wgCirrusSearchDocumentSizeLimiterProfile.max_size (2 * %s)",
                                            TextElement.text(MAX_FETCH_DOC_SIZE.toString()))
                                    .text(
                                            ", because it's at most passed twice: as upsert document and as script parameter")
                                    .linebreak()
                                    .text("A constant amount is added by default to make room for overhead")
                                    .build());
    public static final ConfigOption<MemorySize> PARAM_ELASTICSEARCH_BULK_FLUSH_MAX_SIZE =
            ConfigOptions.key("elasticsearch-bulk-flush-max-size")
                    .memoryType()
                    .defaultValue(ES_MAX_REQUEST_SIZE)
                    .withDescription(
                            Description.builder()
                                    .text("Flush threshold, in bytes")
                                    .linebreak()
                                    .text(
                                            "This is implied by the elasticsearch instance/cluster, it defaults to 100mb")
                                    .build());
    public static final ConfigOption<Integer> PARAM_ELASTICSEARCH_BULK_FLUSH_MAX_ACTIONS =
            ConfigOptions.key("elasticsearch-bulk-flush-max-actions")
                    .intType()
                    .defaultValue((int) ES_MAX_ACTIONS)
                    .withDescription(
                            Description.builder()
                                    .text("Flush threshold, in number of actions")
                                    .linebreak()
                                    .text(
                                            "Derived from %s / %s (= avg. action size)",
                                            LinkElement.link(
                                                    PARAM_ELASTICSEARCH_BULK_FLUSH_MAX_SIZE.key(),
                                                    PARAM_ELASTICSEARCH_BULK_FLUSH_MAX_SIZE.defaultValue().toString()),
                                            TextElement.text(AVG_ACTION_SIZE.toString()))
                                    .build());
    public static final ConfigOption<Duration> PARAM_ELASTICSEARCH_BULK_FLUSH_INTERVAL =
            ConfigOptions.key("elasticsearch-bulk-flush-interval")
                    .durationType()
                    .defaultValue(Duration.ofMinutes(5))
                    .withDescription(
                            Description.builder()
                                    .text("Flush threshold, duration")
                                    .list(
                                            TextElement.text(
                                                    "Each item written to the buffer will create a timer triggering a flush."),
                                            TextElement.text(
                                                    "Only maxed out while the buffer holds less than %s items.",
                                                    LinkElement.link(
                                                            PARAM_ELASTICSEARCH_BULK_FLUSH_MAX_ACTIONS.key(),
                                                            PARAM_ELASTICSEARCH_BULK_FLUSH_MAX_ACTIONS
                                                                    .defaultValue()
                                                                    .toString())))
                                    .build());
    public static final ConfigOption<Duration> PARAM_ELASTICSEARCH_CONNECTION_REQUEST_TIMEOUT =
            ConfigOptions.key("elasticsearch-connection-request-timeout")
                    .durationType()
                    .noDefaultValue()
                    .withDescription("Timeout for requesting a connection from a pool");
    public static final ConfigOption<Duration> PARAM_ELASTICSEARCH_CONNECTION_TIMEOUT =
            ConfigOptions.key("elasticsearch-connection-timeout")
                    .durationType()
                    .noDefaultValue()
                    .withDescription("Timeout for establishing a connection");
    public static final ConfigOption<Duration> PARAM_ELASTICSEARCH_SOCKET_TIMEOUT =
            ConfigOptions.key("elasticsearch-socket-timeout")
                    .durationType()
                    .defaultValue(ES_SOCKET_TIMEOUT)
                    .withDescription("Timeout for receiving a response");
    public static final ConfigOption<Integer> PARAM_ELASTICSEARCH_BULK_COMPLETE_FAILURE_MAX_RETRIES =
            ConfigOptions.key("elasticsearch-bulk-complete-failure-max-retries")
                    .intType()
                    .defaultValue(5)
                    .withDescription(
                            Description.builder()
                                    .text(
                                            "Maximum number of retries attempted in case of request failing completely, e.g. due to a timeout, see %s",
                                            LinkElement.link(PARAM_ELASTICSEARCH_SOCKET_TIMEOUT.key()))
                                    .build());
    public static final ConfigOption<Duration> PARAM_ELASTICSEARCH_BULK_COMPLETE_FAILURE_RETRY_DELAY =
            ConfigOptions.key("elasticsearch-bulk-complete-failure-retry-delay")
                    .durationType()
                    .defaultValue(Duration.ofSeconds(10L))
                    .withDescription(
                            Description.builder()
                                    .text(
                                            "Time to wait per retry, multiplied by the current attempt, see %s",
                                            LinkElement.link(PARAM_ELASTICSEARCH_BULK_COMPLETE_FAILURE_MAX_RETRIES.key()))
                                    .build());
    public static final ConfigOption<Integer> PARAM_ELASTICSEARCH_MAX_BUFFERED_REQUESTS =
            ConfigOptions.key("elasticsearch-max-buffered-requests")
                    .intType()
                    .defaultValue((int) BUFFER_MAX_ACTIONS)
                    .withDescription(
                            Description.builder()
                                    .text("Maximum number of requests buffered by the sink writer operator.")
                                    .linebreak()
                                    .text(
                                            "Currently each event is mapped to exactly one bulk action, "
                                                    + "so this is equal to the number of events. "
                                                    + "Please consider the following parameters that impact the time during which the buffer fills:")
                                    .list(
                                            TextElement.text("we expect roughly 100 updates per second per sink"),
                                            TextElement.text(
                                                    "we allow a timeout of %s per batch (bulk request)",
                                                    LinkElement.link(
                                                            PARAM_ELASTICSEARCH_SOCKET_TIMEOUT.key(),
                                                            PARAM_ELASTICSEARCH_SOCKET_TIMEOUT.defaultValue().toString())),
                                            TextElement.text(
                                                    "each request may be retried up %s times",
                                                    LinkElement.link(
                                                            PARAM_ELASTICSEARCH_BULK_COMPLETE_FAILURE_MAX_RETRIES.key(),
                                                            PARAM_ELASTICSEARCH_BULK_COMPLETE_FAILURE_MAX_RETRIES
                                                                    .defaultValue()
                                                                    .toString())),
                                            TextElement.text(
                                                    "each retry may expand the previous delay by %s (multiplied by # attempt)",
                                                    LinkElement.link(
                                                            PARAM_ELASTICSEARCH_BULK_COMPLETE_FAILURE_RETRY_DELAY.key(),
                                                            PARAM_ELASTICSEARCH_BULK_COMPLETE_FAILURE_RETRY_DELAY
                                                                    .defaultValue()
                                                                    .toString())))
                                    .linebreak()
                                    .text(
                                            "For now, we use a multiple of the %s",
                                            LinkElement.link(PARAM_ELASTICSEARCH_BULK_FLUSH_MAX_ACTIONS.key()))
                                    .build());
    public static final ConfigOption<Map<String, String>> PARAM_ELASTICSEARCH_URLS =
            ConfigOptions.key("elasticsearch-urls")
                    .mapType()
                    .noDefaultValue()
                    .withDescription("Map from cirrus cluster group name to cluster URL");
    public static final ConfigOption<String> PARAM_FETCH_ERROR_STREAM =
            ConfigOptions.key("fetch-error-stream").stringType().defaultValue(FETCH_ERROR_STREAM);

    public static final ConfigOption<String> PARAM_FETCH_ERROR_TOPIC =
            ConfigOptions.key("fetch-error-topic").stringType().noDefaultValue();
    public static final ConfigOption<Duration> PARAM_FETCH_REQUEST_TIMEOUT =
            ConfigOptions.key("fetch-request-timeout")
                    .durationType()
                    .defaultValue(PARAM_HTTP_REQUEST_TIMEOUT.defaultValue())
                    .withFallbackKeys(PARAM_HTTP_REQUEST_TIMEOUT.key())
                    .withDescription(
                            Description.builder()
                                    .text(
                                            "Timeout per fetch request, defaults to %s",
                                            LinkElement.link(PARAM_HTTP_REQUEST_TIMEOUT.key()))
                                    .build());
    public static final ConfigOption<Integer> PARAM_RETRY_FETCH_MAX =
            ConfigOptions.key("fetch-retry-max").intType().defaultValue(3);
    public static final ConfigOption<Duration> PARAM_FETCH_RETRY_MAX_AGE =
            ConfigOptions.key("fetch-retry-max-age")
                    .durationType()
                    .defaultValue(Duration.ofSeconds(10))
                    .withDescription(
                            "Max age of the events for which fetching the revision content is retried regardless of "
                                    + PARAM_RETRY_FETCH_MAX.key());

    public static final ConfigOption<Boolean> PARAM_USE_NULL_SINK =
            ConfigOptions.key("use-null-sink")
                    .booleanType()
                    .defaultValue(Boolean.FALSE)
                    .withDescription("Sink all events to /dev/null, for measuring pipeline throughput.");
    private static final String DEFAULT_MAP_KEY = "default";
    public static final ConfigOption<Map<String, String>> PARAM_FETCH_CONNECTIONS =
            ConfigOptions.key("fetch-connections")
                    .mapType()
                    .defaultValue(Map.of(DEFAULT_MAP_KEY, "100", ChangeType.PAGE_RERENDER.name(), "700"))
                    .withDescription(Description.builder().build());
    public static final ConfigOption<Map<String, String>> PARAM_FETCH_QUEUE_CAPACITY =
            ConfigOptions.key("fetch-queue-capacity")
                    .mapType()
                    .defaultValue(
                            Map.of(
                                    DEFAULT_MAP_KEY,
                                    "200", // see simulation
                                    ChangeType.PAGE_RERENDER.name(),
                                    "1000"))
                    .withDescription(
                            Description.builder()
                                    .text(
                                            "Accumulated size of (parallel) queues to hold updates until they are processed."
                                                    + " Avoids immediate backpressure while waiting for responses. Consider the following stats: ")
                                    .list(
                                            TextElement.text("On average, source rate is 300 updates/s"),
                                            TextElement.text(
                                                    "%s, are %s updates",
                                                    TextElement.text(
                                                            String.format(Locale.ENGLISH, "%.0f%%", FETCH_RERENDER_SHARE * 100)),
                                                    TextElement.code(ChangeType.PAGE_RERENDER.name())),
                                            TextElement.text("50% of all fetches take 5s or less per request"),
                                            TextElement.text(
                                                    "1% of all fetches need a second attempt,"
                                                            + " which doubles the time and adds 1s delay"))
                                    .linebreak()
                                    .text(
                                            "Needs to be larger if order is kept (%s-key).",
                                            TextElement.code(DEFAULT_MAP_KEY))
                                    .linebreak()
                                    .text("See also %s", LinkElement.link(PARAM_FETCH_CONNECTIONS.key()))
                                    .build());

    public static final ConfigOption<Boolean> PARAM_CONSUME_PRIVATE_UPDATES =
            ConfigOptions.key("consume-private-updates")
                    .booleanType()
                    .defaultValue(Boolean.FALSE)
                    .withDescription("True when private wikis should be updated.");

    public static final ConfigOption<String> PARAM_SANEITIZER_CLUSTER_GROUP =
            ConfigOptions.key("saneitize-cluster-group")
                    .stringType()
                    .noDefaultValue()
                    .withDescription(
                            "Name, in CirrusSearch cluster configuration, of the target cluster group.");

    public static final ConfigOption<Duration> PARAM_SANEITIZER_LOOP_DURATION =
            ConfigOptions.key("saneitize-loop-duration")
                    .durationType()
                    .defaultValue(Duration.ofDays(14))
                    .withDescription("Saneitizer loop duration");

    public static final ConfigOption<Integer> PARAM_SANEITIZER_BATCH_SIZE =
            ConfigOptions.key("saneitize-batch-size")
                    .intType()
                    .defaultValue(100)
                    .withDescription("The maximum number of pages sent to the backend in a single request.");

    public static final ConfigOption<Duration> PARAM_SANEITIZER_REQUEST_TIMEOUT =
            ConfigOptions.key("saneitize-request-timeout")
                    .durationType()
                    .defaultValue(Duration.ofSeconds(120))
                    .withDescription(
                            "Timeout for sanity check api requests. By default this is a batch of 100 pages "
                                    + "which typically takes one second to process.");

    public static final ConfigOption<Integer> PARAM_SANEITIZER_RETRY_SANITY_CHECK_MAX =
            ConfigOptions.key("saneitize-max-retries")
                    .intType()
                    .defaultValue(3)
                    .withDescription(
                            "The number of times to attempt the same SanityCheck API request before giving up.");

    public static final ConfigOption<Integer> PARAM_SANEITIZER_CAPACITY =
            ConfigOptions.key("saneitize-capacity")
                    .intType()
                    .defaultValue(20)
                    .withDescription("Sanity check async request capacity");

    public static final ConfigOption<Integer> PARAM_SANEITIZER_RERENDER_FREQUENCY =
            ConfigOptions.key("saneitize-rerender-frequency")
                    .intType()
                    .defaultValue(16)
                    .withDescription("How often, in number of loops, a page should be re-rendered");

    public static final ConfigOption<Duration> PARAM_SANEITIZER_BOUNDED_RUNTIME =
            ConfigOptions.key("saneitize-max-runtime")
                    .durationType()
                    .noDefaultValue()
                    .withDescription(
                            Description.builder()
                                    .text("Stop saneitizer after this long.")
                                    .linebreak()
                                    .text(
                                            "Provides bounded stream for integration testing and/or when running alongside a bounded kafka source.")
                                    .build());

    public static final ConfigOption<Boolean> PARAM_SANEITIZE =
            ConfigOptions.key("saneitize")
                    .booleanType()
                    .defaultValue(Boolean.FALSE)
                    .withDescription(
                            Description.builder()
                                    .text("Enable saneitizer for configured wikis")
                                    .linebreak()
                                    .text(
                                            "If %s implicitly sets %s to 0s",
                                            TextElement.code("false"),
                                            LinkElement.link(PARAM_SANEITIZER_BOUNDED_RUNTIME.key()))
                                    .build());

    public static final ConfigOption<String> PARAM_PUBLIC_LEGACY_UPDATE_STREAM =
            ConfigOptions.key("public-legacy-update-stream")
                    .stringType()
                    .noDefaultValue()
                    .withDescription(
                            Description.builder()
                                    .text(
                                            "Bridging (legacy) stream for public wikis between producer (aggregator) and consumer (indexer)")
                                    .linebreak()
                                    .text(
                                            "This is used to fetch the event schema "
                                                    + "as well as a list of kafka topics from stream configuration, see %s",
                                            LinkElement.link(PARAM_EVENT_STREAM_CONFIG_URL.key()))
                                    .build());

    public static final ConfigOption<String> PARAM_PRIVATE_LEGACY_UPDATE_STREAM =
            ConfigOptions.key("private-legacy-update-stream")
                    .stringType()
                    .noDefaultValue()
                    .withDescription(
                            Description.builder()
                                    .text(
                                            "Bridging (legacy) stream for private wikis between producer (aggregator) and consumer (indexer)")
                                    .linebreak()
                                    .text(
                                            "This is used to fetch the event schema "
                                                    + "as well as a list of kafka topics from stream configuration, see %s",
                                            LinkElement.link(PARAM_EVENT_STREAM_CONFIG_URL.key()))
                                    .build());

    Map<String, String> elasticSearchUrls;

    @Builder.Default boolean consumePrivateUpdates = PARAM_CONSUME_PRIVATE_UPDATES.defaultValue();

    @Builder.Default
    Map<ChangeType, Integer> fetchConnections =
            perChangeTypeMap(PARAM_FETCH_CONNECTIONS.defaultValue());

    @Builder.Default String fetchErrorStream = PARAM_FETCH_ERROR_STREAM.defaultValue();
    String fetchErrorTopic;

    @Builder.Default
    Map<ChangeType, Integer> fetchQueueCapacity =
            perChangeTypeMap(PARAM_FETCH_QUEUE_CAPACITY.defaultValue());

    @Builder.Default Duration fetchRequestTimeout = PARAM_FETCH_REQUEST_TIMEOUT.defaultValue();
    @Builder.Default int retryFetchMax = PARAM_RETRY_FETCH_MAX.defaultValue();
    @Builder.Default Duration retryFetchMaxAge = PARAM_FETCH_RETRY_MAX_AGE.defaultValue();

    @Builder.Default boolean shouldSaneitize = PARAM_SANEITIZE.defaultValue();
    String saneitizerClusterGroup;
    @Builder.Default Duration saneitizerLoopDuration = PARAM_SANEITIZER_LOOP_DURATION.defaultValue();
    @Builder.Default int saneitizerBatchSize = PARAM_SANEITIZER_BATCH_SIZE.defaultValue();

    @Builder.Default
    Duration saneitizerRequestTimeout = PARAM_SANEITIZER_REQUEST_TIMEOUT.defaultValue();

    @Builder.Default int saneitizerRetryMax = PARAM_SANEITIZER_RETRY_SANITY_CHECK_MAX.defaultValue();
    @Builder.Default int saneitizerCapacity = PARAM_SANEITIZER_CAPACITY.defaultValue();

    @Builder.Default
    int saneitizerRerenderFrequency = PARAM_SANEITIZER_RERENDER_FREQUENCY.defaultValue();

    Duration saneitizerBoundedRuntime;

    @Builder.Default boolean useNullSink = PARAM_USE_NULL_SINK.defaultValue();

    @Nullable String publicLegacyUpdateStream;

    @Nullable String privateLegacyUpdateStream;

    public static ConsumerConfig of(Configuration configuration) {
        configuration
                .getOptional(PipelineOptions.NAME)
                .ifPresentOrElse((name) -> {}, () -> configuration.set(PipelineOptions.NAME, JOB_NAME));

        final ConsumerConfigBuilder<?, ?> builder =
                CommonConfig.superBuilder(configuration, ConsumerConfig::builder)
                        .fetchErrorStream(getRequired(configuration, PARAM_FETCH_ERROR_STREAM))
                        .fetchErrorTopic(getRequired(configuration, PARAM_FETCH_ERROR_TOPIC))
                        .elasticSearchUrls(getRequired(configuration, PARAM_ELASTICSEARCH_URLS))
                        .useNullSink(getRequired(configuration, PARAM_USE_NULL_SINK))
                        .shouldSaneitize(getRequired(configuration, PARAM_SANEITIZE))
                        .saneitizerLoopDuration(getRequired(configuration, PARAM_SANEITIZER_LOOP_DURATION))
                        .saneitizerBatchSize(getRequired(configuration, PARAM_SANEITIZER_BATCH_SIZE))
                        .saneitizerRequestTimeout(getRequired(configuration, PARAM_SANEITIZER_REQUEST_TIMEOUT))
                        .saneitizerRetryMax(getRequired(configuration, PARAM_SANEITIZER_RETRY_SANITY_CHECK_MAX))
                        .saneitizerCapacity(getRequired(configuration, PARAM_SANEITIZER_CAPACITY))
                        .saneitizerRerenderFrequency(
                                getRequired(configuration, PARAM_SANEITIZER_RERENDER_FREQUENCY));

        if (getRequired(configuration, PARAM_SANEITIZE)) {
            // Only required when saneitizer is enabled. No default value is reasonable.
            builder.saneitizerClusterGroup(getRequired(configuration, PARAM_SANEITIZER_CLUSTER_GROUP));
        }

        configuration
                .getOptional(PARAM_CONSUME_PRIVATE_UPDATES)
                .ifPresent(builder::consumePrivateUpdates);

        configuration.getOptional(PARAM_FETCH_RETRY_MAX_AGE).ifPresent(builder::retryFetchMaxAge);
        configuration.getOptional(PARAM_RETRY_FETCH_MAX).ifPresent(builder::retryFetchMax);

        configuration
                .getOptional(PARAM_FETCH_QUEUE_CAPACITY)
                .map(ConsumerConfig::perChangeTypeMap)
                .ifPresent(builder::fetchQueueCapacity);

        configuration
                .getOptional(PARAM_FETCH_CONNECTIONS)
                .map(ConsumerConfig::perChangeTypeMap)
                .ifPresent(builder::fetchConnections);

        final Optional<Duration> httpRequestTimeout =
                configuration.getOptional(PARAM_HTTP_REQUEST_TIMEOUT);
        configuration
                .getOptional(PARAM_FETCH_REQUEST_TIMEOUT)
                .or(() -> httpRequestTimeout)
                .ifPresent(builder::fetchRequestTimeout);
        configuration
                .getOptional(PARAM_SANEITIZER_BOUNDED_RUNTIME)
                .ifPresent(builder::saneitizerBoundedRuntime);

        configuration
                .getOptional(PARAM_PUBLIC_LEGACY_UPDATE_STREAM)
                .ifPresent(builder::publicLegacyUpdateStream);

        configuration
                .getOptional(PARAM_PRIVATE_LEGACY_UPDATE_STREAM)
                .ifPresent(builder::privateLegacyUpdateStream);

        return builder.build();
    }

    private static Map<ChangeType, Integer> perChangeTypeMap(Map<String, String> raw) {
        String defaultValue = raw.getOrDefault(DEFAULT_MAP_KEY, "0");
        return Arrays.stream(ChangeType.values())
                .collect(
                        Collectors.toMap(
                                Function.identity(),
                                changeType -> Integer.valueOf(raw.getOrDefault(changeType.name(), defaultValue))));
    }
}
