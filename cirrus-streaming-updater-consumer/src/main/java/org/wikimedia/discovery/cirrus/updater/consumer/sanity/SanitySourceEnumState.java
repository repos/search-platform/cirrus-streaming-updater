package org.wikimedia.discovery.cirrus.updater.consumer.sanity;

import java.io.IOException;
import java.util.List;

import org.apache.flink.core.io.SimpleVersionedSerialization;
import org.apache.flink.core.io.SimpleVersionedSerializer;
import org.apache.flink.core.memory.DataInputDeserializer;
import org.apache.flink.core.memory.DataOutputSerializer;
import org.apache.flink.util.Preconditions;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;

@AllArgsConstructor
@EqualsAndHashCode
class SanitySourceEnumState {
    public final List<SanitySourceSplit> pendingAssignment;

    @AllArgsConstructor
    static class Serializer implements SimpleVersionedSerializer<SanitySourceEnumState> {
        private static final int VERSION = 0;
        private SimpleVersionedSerializer<SanitySourceSplit> splitSerializer;

        @Override
        public int getVersion() {
            return VERSION;
        }

        @Override
        public byte[] serialize(SanitySourceEnumState obj) throws IOException {
            int sizeEstimate =
                    obj.pendingAssignment.stream()
                                    .mapToInt(SanitySourceSplit.Serializer::sizeEstimate)
                                    .map(i -> i + 4) // per-element list overhead
                                    .reduce(Integer::sum)
                                    .orElse(0)
                            // overall list overhead
                            + 8;
            DataOutputSerializer out = new DataOutputSerializer(sizeEstimate);
            SimpleVersionedSerialization.writeVersionAndSerializeList(
                    splitSerializer, obj.pendingAssignment, out);
            return out.getSharedBuffer();
        }

        @Override
        public SanitySourceEnumState deserialize(int version, byte[] serialized) throws IOException {
            Preconditions.checkArgument(version == VERSION, "Unknown serialized version");
            DataInputDeserializer in = new DataInputDeserializer(serialized);
            List<SanitySourceSplit> pendingAssignment =
                    SimpleVersionedSerialization.readVersionAndDeserializeList(splitSerializer, in);
            return new SanitySourceEnumState(pendingAssignment);
        }
    }
}
