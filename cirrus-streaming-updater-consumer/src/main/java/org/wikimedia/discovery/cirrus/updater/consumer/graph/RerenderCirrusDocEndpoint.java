package org.wikimedia.discovery.cirrus.updater.consumer.graph;

import java.net.URI;

import org.jetbrains.annotations.NotNull;
import org.wikimedia.discovery.cirrus.updater.common.InvalidMWApiResponseException;
import org.wikimedia.discovery.cirrus.updater.common.graph.CirrusEndpoint;
import org.wikimedia.discovery.cirrus.updater.common.model.ChangeType;
import org.wikimedia.discovery.cirrus.updater.common.model.CirrusDocJsonHelper;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateEvent;
import org.wikimedia.eventutilities.core.SerializableClock;
import org.wikimedia.eventutilities.flink.formats.json.JsonRowDeserializationSchema;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.BooleanNode;
import com.google.common.base.Preconditions;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class RerenderCirrusDocEndpoint implements CirrusEndpoint<UpdateEvent, UpdateEvent> {

    // Use content + links so that redirects are refreshed as well
    private static final String MW_API_PATH_AND_QUERY =
            "/w/api.php?action=query&format=json&cbbuilders=content%7Clinks&prop=cirrusbuilddoc&formatversion=2&format=json&pageids=";

    private final ObjectMapper objectMapper;
    private final JsonRowDeserializationSchema fieldsSchema;
    private final SerializableClock clock;

    @Override
    public URI buildURI(UpdateEvent event) {
        checkEvent(event);
        final String domain = event.getTargetDocument().getDomain();

        Long pageId = pageId(event);
        return URI.create("https://" + domain + MW_API_PATH_AND_QUERY + pageId);
    }

    @NotNull
    private static Long pageId(UpdateEvent updateEvent) {
        final Long pageId = updateEvent.getTargetDocument().getPageId();
        Preconditions.checkArgument(pageId != null, "pageId must be set");
        return pageId;
    }

    @Override
    public UpdateEvent extractAndAugment(UpdateEvent event, JsonNode response) {
        checkEvent(event);

        final JsonNode missingFlagNode = response.at("/query/pages/0/missing");
        final JsonNode pageNode = response.at("/query/pages/0");

        Long eventPageId = pageId(event);
        if (missingFlagNode instanceof BooleanNode && missingFlagNode.asBoolean()) {
            throw new PageNotFoundException("Page " + eventPageId + " is missing");
        } else if (!pageNode.has("cirrusbuilddoc")) {
            // NOTE: InvalidMWApiResponseException error is retryable, from our POV it is unclear if we
            // need to retry
            // or not so we assume that retrying might yield different results.
            // Might be interesting to learn what common unrecoverable errors are so that we save a couple
            // api requests and properly identify them earlier either from MW or here.
            throw new InvalidMWApiResponseException(
                    "Unexpected response: /query/pages/0/cirrusbuilddoc missing for page id "
                            + eventPageId
                            + " and /missing is not set:\n"
                            + response.toPrettyString());
        }

        CirrusDocJsonHelper doc =
                new CirrusDocJsonHelper(fieldsSchema, objectMapper, pageNode, clock.get());

        Long pageId = doc.getPageId();
        if (!pageId.equals(eventPageId)) {
            throw new IllegalArgumentException(
                    "Received response for wrong page id (" + pageId + " != " + eventPageId + ")");
        }

        return CirrusDocMerger.merge(event, doc);
    }

    private static void checkEvent(UpdateEvent updateEvent) {
        Preconditions.checkArgument(
                !updateEvent.hasFetchedFields(), "Event already populated with a cirrus document");
        Preconditions.checkArgument(
                updateEvent.getChangeType() == ChangeType.PAGE_RERENDER
                        || updateEvent.getChangeType() == ChangeType.PAGE_RERENDER_UPSERT,
                "Event is not a page re-render event");

        Preconditions.checkArgument(
                updateEvent.getTargetDocument().getPageId() != null, "Event lacks a page ID");

        Preconditions.checkArgument(
                updateEvent.getTargetDocument().getDomain() != null, "Event lacks a domain");
    }
}
