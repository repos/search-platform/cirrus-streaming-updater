package org.wikimedia.discovery.cirrus.updater.consumer.graph;

import org.wikimedia.discovery.cirrus.updater.common.CirrusDocFetchException;

public class PageNotFoundException extends CirrusDocFetchException {
    public PageNotFoundException(String s) {
        super(s);
    }
}
