package org.wikimedia.discovery.cirrus.updater.consumer.sanity;

import java.time.Instant;
import java.util.function.Consumer;

interface Loop {
    /**
     * @return The next time we would like Loop::next to be invoked. Invoking next prior to this time
     *     will not produce any records.
     */
    Instant nextInvokeAt();

    /**
     * Produce any records scheduled prior to now.
     *
     * @param now The current clock time.
     * @return True when the loop is finished and will never again produce records.
     */
    boolean next(Instant now, Consumer<SanitySourceRecord> consumer);
}
