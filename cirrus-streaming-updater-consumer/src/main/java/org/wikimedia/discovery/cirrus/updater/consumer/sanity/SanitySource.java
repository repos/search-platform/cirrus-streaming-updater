package org.wikimedia.discovery.cirrus.updater.consumer.sanity;

import java.time.Duration;
import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Supplier;

import javax.annotation.Nullable;

import org.apache.flink.api.connector.source.Boundedness;
import org.apache.flink.api.connector.source.Source;
import org.apache.flink.api.connector.source.SourceReader;
import org.apache.flink.api.connector.source.SourceReaderContext;
import org.apache.flink.api.connector.source.SplitEnumerator;
import org.apache.flink.api.connector.source.SplitEnumeratorContext;
import org.apache.flink.core.io.SimpleVersionedSerializer;
import org.apache.flink.metrics.MetricGroup;
import org.wikimedia.discovery.cirrus.updater.common.model.SanityCheck;
import org.wikimedia.discovery.cirrus.updater.common.util.function.SerializableFunction;
import org.wikimedia.eventutilities.core.SerializableClock;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * The SanitySource is the scheduling layer of the CirrusSearch Saneitization process.
 *
 * <p>Saneitization keeps the search indices sane. It is a process of visiting all possible page ids
 * over a time period and comparing the state between the SQL and ElasticSearch databases. When
 * mismatches are found between the two we apply remediation's to bring the index back to a sane
 * state. This provides both a way to re-align the search indices with the databases after any
 * problems, and a metric that measures how often the search indices have problematic data. It
 * additionally re-renders a percentage of pages in each loop, guaranteeing re-render of all pages
 * within the last n loops.
 *
 * <p>SanitySource is a flink source that emits SanityCheck events on a schedule determined by the
 * number of page ids in a wiki and the targeted time to complete each loop over those page ids.
 * Each SanityCheck event represents a request for a single call to the CirrusSearch API. The events
 * feed into the CirrusSanityCheckEndpoint which performs the actual sanity check and generates
 * UpdateEvent's as remediation's for problems found.
 *
 * <p>The SanitySource must be provided with suppliers for both the set of domains to be processed,
 * and the max page id of a given domain. From these it will perform daily reconciliation to match
 * the runtime state with the suppliers. If the suppliers return an empty optional it will be
 * considered an error and reconciliation will be retried in 10 minutes.
 *
 * <p>At the implementation level the SanitySource consists of a single worker that contains a
 * collection of LoopState instances managed by a single SaneitizeLoop instance. These LoopState
 * instances each represent a single domain and are persisted into flink checkpoints. The
 * SaneitizeLoop decides based on the LoopStates when to next emit events, and when reconciliation
 * needs to happen. While many parts of this may look to support multiple splits that is only for
 * ease of integration with flink. We only ever populate a single split, resulting in only ever
 * having a single SplitReader / SaneitizeLoop.
 */
@AllArgsConstructor
@Slf4j
public class SanitySource implements Source<SanityCheck, SanitySourceSplit, SanitySourceEnumState> {
    /** Provides set of domain names to be operated on. */
    public interface WikiSupplier extends Supplier<Optional<Set<String>>> {}

    public interface WikiSupplierFactory extends SerializableFunction<MetricGroup, WikiSupplier> {}
    /** Provides the max page id for the given domain name. */
    public interface MaxPageIdLookup extends Function<String, Optional<Long>> {}

    public interface MaxPageIdLookupFactory
            extends SerializableFunction<MetricGroup, MaxPageIdLookup> {}

    /**
     * Returns the set of wikis that should be checked. This is invoked on startup to align state with
     * the requested set, and is re-invoked daily to update the set of wikis processed. Optional.none
     * indicates there was a problem sourcing the list, the invocation will be retried in a few
     * minutes.
     */
    private final WikiSupplierFactory wikiSupplierFactory;
    /**
     * Returns a function that accepts the domain name of a wiki and returns the max page id of that
     * wiki.
     */
    private final MaxPageIdLookupFactory maxPageIdLookupFactory;

    private final SerializableClock clock;
    /**
     * The minimum amount of time between loops over all page ids. Used to determine checking rate.
     */
    private final Duration loopDuration;
    /** The number of pages to check in a single batch. */
    private final int batchSize;

    /** When to stop producing events. Provides a bounded stream for integration testing. */
    @Nullable private final Instant endAt;
    /**
     * If true the source will attach to the graph but never do anything. Any existing state will stay
     * as it was. This allows to pause the saneitizer via configuration without changing the shape of
     * the graph.
     */
    private final boolean paused;

    @Override
    public Boundedness getBoundedness() {
        if (endAt == null) {
            return Boundedness.CONTINUOUS_UNBOUNDED;
        } else {
            return Boundedness.BOUNDED;
        }
    }

    @Override
    public SplitEnumerator<SanitySourceSplit, SanitySourceEnumState> createEnumerator(
            SplitEnumeratorContext<SanitySourceSplit> enumContext) throws Exception {
        log.info(
                "Either no splits exist or all splits have completed. Create new enumerator with new split.");
        return new SanitySourceSplitEnumerator(
                enumContext,
                // We only ever have one split, the choice of splitId is arbitrary. The addition of
                // current millis to the name ensures flink will not have any stored state about
                // the split previously completing if it was run in bounded mode.
                List.of(new SanitySourceSplit("all_wikis_" + clock.get().toEpochMilli())));
    }

    @Override
    public SplitEnumerator<SanitySourceSplit, SanitySourceEnumState> restoreEnumerator(
            SplitEnumeratorContext<SanitySourceSplit> enumContext, SanitySourceEnumState checkpoint)
            throws Exception {
        log.info(
                "Restoring enumerator from checkpoint with {} pending assignment(s)",
                checkpoint.pendingAssignment.size());
        return new SanitySourceSplitEnumerator(enumContext, checkpoint.pendingAssignment);
    }

    @Override
    public SimpleVersionedSerializer<SanitySourceSplit> getSplitSerializer() {
        return new SanitySourceSplit.Serializer(new LoopState.Serializer());
    }

    @Override
    public SimpleVersionedSerializer<SanitySourceEnumState> getEnumeratorCheckpointSerializer() {
        return new SanitySourceEnumState.Serializer(getSplitSerializer());
    }

    @Override
    public SourceReader<SanityCheck, SanitySourceSplit> createReader(
            SourceReaderContext readerContext) throws Exception {
        log.info(
                "Creating SanitySourceReader instance for subtask {}", readerContext.getIndexOfSubtask());
        return new SanitySourceReader(
                state -> {
                    if (paused) {
                        // By replacing the inner loop implementation but leaving everything
                        // else in place we ensure that pausing a running process from external
                        // configuration will leave the current state untouched and ready to be un-paused.
                        log.info("Initializing noop loop to end at {}", endAt);
                        return new NoopLoop(endAt);
                    }
                    log.info("Initializing loop with {} state entries to end at {}", state.size(), endAt);
                    MetricGroup metricGroup = readerContext.metricGroup().addGroup("saneitizer");
                    return new SaneitizeLoop(
                            state,
                            metricGroup,
                            wikiSupplierFactory.apply(metricGroup.addGroup("wiki")),
                            maxPageIdLookupFactory.apply(metricGroup.addGroup("max_page_id")),
                            loopDuration,
                            batchSize,
                            endAt);
                },
                clock,
                readerContext.getConfiguration(),
                readerContext);
    }
}
