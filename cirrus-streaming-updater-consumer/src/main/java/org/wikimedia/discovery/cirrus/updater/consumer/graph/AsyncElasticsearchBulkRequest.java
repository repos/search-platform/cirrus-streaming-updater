package org.wikimedia.discovery.cirrus.updater.consumer.graph;

import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.time.Instant;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import org.elasticsearch.action.DocWriteRequest;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.update.UpdateRequest;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.ToString;

/**
 * Wrapper around {@link org.elasticsearch.action.bulk.BulkRequest}.
 *
 * <p><b>CAVEAT:</b> Instances of this class are only supposed to be serialized via {@link
 * AsyncElasticsearchSinkWriterSerializer}, calling {@link #writeObject(ObjectOutputStream)} will
 * throw an exception. This class only implements {@link Serializable} to comply with the generic
 * constraints in {@link org.apache.flink.connector.base.sink.AsyncSinkBase} and {@link
 * org.apache.flink.connector.base.sink.writer.AsyncSinkWriter}, see <a
 * href="https://issues.apache.org/jira/browse/FLINK-27537">FLINK-27537</a>.
 */
@SuppressWarnings("serial")
@ToString
@SuppressFBWarnings(value = "SE_NO_SERIALVERSIONID")
public class AsyncElasticsearchBulkRequest implements Serializable {

    @Getter private final transient String id;

    private final transient AtomicInteger attempt;

    /**
     * Container used to hold {@code 0..*} actions.
     *
     * <p>Only used as a convenient container. <b>CAVEAT:</b> only {@link BulkRequest#requests()} are
     * considered.
     */
    @ToString.Exclude
    @Getter(AccessLevel.PACKAGE)
    private final transient BulkRequest bulkRequest;

    @Getter private final transient Instant eventTime;

    public AsyncElasticsearchBulkRequest(String id, DocWriteRequest<?> action, Instant eventTime) {
        this(id, 0, new BulkRequest().add(action), eventTime);
    }

    /**
     * Constructor intended for deserialization.
     *
     * @param id original event ID
     * @param attempt number of times this action has been attempted
     * @param bulkRequest underlying bulk request, see also {@link #bulkRequest}
     * @param eventTime the event time of the indexing request
     * @see AsyncElasticsearchSinkWriterSerializer
     */
    AsyncElasticsearchBulkRequest(
            String id, int attempt, BulkRequest bulkRequest, Instant eventTime) {
        this.id = id;
        this.attempt = new AtomicInteger(attempt);
        this.bulkRequest = bulkRequest;
        this.eventTime = eventTime;
    }

    public long getEstimatedSizeInBytes() {
        return bulkRequest.requests().stream()
                .mapToLong(
                        action -> {
                            if (action instanceof IndexRequest) {
                                return AsyncElasticsearchBulkSizeEstimator.estimate((IndexRequest) action);
                            } else if (action instanceof UpdateRequest) {
                                return AsyncElasticsearchBulkSizeEstimator.estimate((UpdateRequest) action);
                            } else if (action instanceof DeleteRequest) {
                                return AsyncElasticsearchBulkSizeEstimator.estimate((DeleteRequest) action);
                            }
                            throw new IllegalArgumentException(
                                    "Unable to estimate size of bulk action " + action);
                        })
                .sum();
    }

    public List<DocWriteRequest<?>> requests() {
        return bulkRequest.requests();
    }

    public int incrementAndGetAttempt() {
        return attempt.incrementAndGet();
    }

    public int getAttempt() {
        return attempt.get();
    }

    private void writeObject(ObjectOutputStream oos) {
        throw new UnsupportedOperationException(
                "Instances of "
                        + getClass()
                        + "  are only supposed to be serialized via AsyncElasticsearchSinkWriterSerializer");
    }
}
