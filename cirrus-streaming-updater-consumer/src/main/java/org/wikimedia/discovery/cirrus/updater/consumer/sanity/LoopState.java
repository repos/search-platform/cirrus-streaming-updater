package org.wikimedia.discovery.cirrus.updater.consumer.sanity;

import java.io.IOException;
import java.time.Instant;

import org.apache.flink.core.io.SimpleVersionedSerializer;
import org.apache.flink.core.memory.DataInputDeserializer;
import org.apache.flink.core.memory.DataOutputSerializer;
import org.apache.flink.util.Preconditions;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Value;

/** State machine that tracks the state of the saneitizer process for a single wiki. */
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@Value
public class LoopState {
    int loopId;
    String domain;
    Instant startedAt;
    long maxPageId;
    long lastEmittedPageId;
    Instant lastEmittedAt;
    Instant lastMaxPageIdRefresh;

    public LoopState(String domain, long maxPageId, Instant now) {
        this(0, domain, now, maxPageId, -1, now, now);
        Preconditions.checkArgument(maxPageId > 0, "max page id must be positive");
    }

    public LoopState refreshMaxPageId(Instant now, long maxPageId) {
        Preconditions.checkArgument(maxPageId > 0, "max page id must be positive");
        return new LoopState(
                loopId, domain, startedAt, maxPageId, lastEmittedPageId, lastEmittedAt, now);
    }

    public LoopState resetForNextLoop(Instant now) {
        return new LoopState(
                loopId + 1, domain, now, maxPageId, -1, lastEmittedAt, lastMaxPageIdRefresh);
    }

    public LoopState emitNextBatch(Instant now, long batchSize) {
        return new LoopState(
                loopId,
                domain,
                startedAt,
                maxPageId,
                lastEmittedPageId + batchSize,
                now,
                lastMaxPageIdRefresh);
    }

    public boolean isFinished() {
        return lastEmittedPageId >= maxPageId;
    }

    public boolean isStartingNewLoop() {
        return lastEmittedPageId == -1;
    }

    public static class Serializer implements SimpleVersionedSerializer<LoopState> {
        // loopId of 4 bytes, two bools flagging nulls, 5 more 8 byte fields,
        // and an observed max domain of length of 32, all ascii and a 2 byte
        // string overhead.
        public static final int SERIALIZED_BYTE_ESTIMATE = 4 + 2 + 5 * 8 + 32 + 2;
        public static final int VERSION = 0;

        @Override
        public int getVersion() {
            return VERSION;
        }

        @Override
        @SuppressFBWarnings(value = "CE_CLASS_ENVY")
        public byte[] serialize(LoopState obj) throws IOException {
            DataOutputSerializer out = new DataOutputSerializer(SERIALIZED_BYTE_ESTIMATE);
            out.writeInt(obj.getLoopId());
            out.writeUTF(obj.getDomain());
            out.writeLong(obj.getStartedAt().getEpochSecond());
            out.writeLong(obj.getMaxPageId());
            out.writeLong(obj.getLastEmittedPageId());
            out.writeBoolean(obj.getLastEmittedAt() != null);
            if (obj.getLastEmittedAt() != null) {
                out.writeLong(obj.getLastEmittedAt().getEpochSecond());
            }
            out.writeBoolean(obj.getLastMaxPageIdRefresh() != null);
            if (obj.getLastMaxPageIdRefresh() != null) {
                out.writeLong(obj.getLastMaxPageIdRefresh().getEpochSecond());
            }
            return out.getSharedBuffer();
        }

        @Override
        public LoopState deserialize(int version, byte[] serialized) throws IOException {
            Preconditions.checkArgument(version == VERSION, "Unknown serialized version");
            DataInputDeserializer in = new DataInputDeserializer(serialized);
            int loopId = in.readInt();
            String domain = in.readUTF();
            Instant startedAt = Instant.ofEpochSecond(in.readLong());
            long maxPageId = in.readLong();
            long lastEmittedPageId = in.readLong();
            boolean hasLastEmittedAt = in.readBoolean();
            Instant lastEmittedAt = hasLastEmittedAt ? Instant.ofEpochSecond(in.readLong()) : null;
            boolean hasLastMaxPageIdRefresh = in.readBoolean();
            Instant lastMaxPageIdRefresh =
                    hasLastMaxPageIdRefresh ? Instant.ofEpochSecond(in.readLong()) : null;
            return new LoopState(
                    loopId,
                    domain,
                    startedAt,
                    maxPageId,
                    lastEmittedPageId,
                    lastEmittedAt,
                    lastMaxPageIdRefresh);
        }
    }
}
