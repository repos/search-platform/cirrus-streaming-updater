package org.wikimedia.discovery.cirrus.updater.consumer.sanity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.apache.flink.api.connector.source.SourceSplit;
import org.apache.flink.core.io.SimpleVersionedSerialization;
import org.apache.flink.core.io.SimpleVersionedSerializer;
import org.apache.flink.core.memory.DataInputDeserializer;
import org.apache.flink.core.memory.DataOutputSerializer;
import org.apache.flink.util.Preconditions;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;

/** Immutable information about a split. */
@EqualsAndHashCode
class SanitySourceSplit implements SourceSplit {
    private final String splitId;
    final Map<String, LoopState> state;

    SanitySourceSplit(String splitId) {
        this.splitId = splitId;
        this.state = Map.of();
    }

    SanitySourceSplit(String splitId, Map<String, LoopState> state) {
        this.splitId = splitId;
        this.state = Map.copyOf(state);
    }

    @Override
    public String splitId() {
        return splitId;
    }

    @AllArgsConstructor
    static class Serializer implements SimpleVersionedSerializer<SanitySourceSplit> {
        private static final int VERSION = 0;
        SimpleVersionedSerializer<LoopState> loopStateSerializer;

        @Override
        public int getVersion() {
            return VERSION;
        }

        public static int sizeEstimate(SanitySourceSplit split) {
            // splitId should be ascii, one byte per plus 2 byte string overhead
            return 2
                    + split.splitId.length()
                    // list overhead
                    + 8
                    // 4 is per-element list overhead
                    + (4 + LoopState.Serializer.SERIALIZED_BYTE_ESTIMATE) * split.state.size();
        }

        @Override
        public byte[] serialize(SanitySourceSplit split) throws IOException {
            DataOutputSerializer out = new DataOutputSerializer(sizeEstimate(split));
            out.writeUTF(split.splitId);
            SimpleVersionedSerialization.writeVersionAndSerializeList(
                    loopStateSerializer, new ArrayList<>(split.state.values()), out);
            return out.getSharedBuffer();
        }

        @Override
        public SanitySourceSplit deserialize(int version, byte[] serialized) throws IOException {
            Preconditions.checkArgument(version == VERSION, "Unknown serialized version");
            DataInputDeserializer in = new DataInputDeserializer(serialized);
            String splitId = in.readUTF();
            Map<String, LoopState> loopStates =
                    SimpleVersionedSerialization.readVersionAndDeserializeList(loopStateSerializer, in)
                            .stream()
                            .collect(Collectors.toMap(LoopState::getDomain, Function.identity()));
            return new SanitySourceSplit(splitId, loopStates);
        }
    }
}
