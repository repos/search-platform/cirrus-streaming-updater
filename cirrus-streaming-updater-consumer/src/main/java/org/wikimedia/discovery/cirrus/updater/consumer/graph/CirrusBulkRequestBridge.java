package org.wikimedia.discovery.cirrus.updater.consumer.graph;

import java.net.SocketTimeoutException;
import java.time.Duration;
import java.util.Arrays;
import java.util.Collection;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.apache.flink.annotation.VisibleForTesting;
import org.apache.flink.metrics.Counter;
import org.apache.flink.metrics.MetricGroup;
import org.apache.flink.util.Preconditions;
import org.elasticsearch.ElasticsearchStatusException;
import org.elasticsearch.action.ActionListener;
import org.elasticsearch.action.DocWriteRequest;
import org.elasticsearch.action.DocWriteResponse.Result;
import org.elasticsearch.action.bulk.BulkItemResponse;
import org.elasticsearch.action.bulk.BulkItemResponse.Failure;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.unit.TimeValue;
import org.wikimedia.discovery.cirrus.updater.common.http.HttpClientFactory;
import org.wikimedia.discovery.cirrus.updater.common.model.ChangeType;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/** A bridge that only logs partial failed bulk actions and retries on timeouts. */
@Slf4j
public class CirrusBulkRequestBridge implements AsyncElasticsearchBulkRequestBridge {

    static final Map<ChangeType, Set<Result>> CHANGE_TYPE_RESULTS =
            Map.of(
                    ChangeType.REV_BASED_UPDATE,
                    EnumSet.complementOf(EnumSet.of(Result.DELETED)),
                    ChangeType.TAGS_UPDATE,
                    EnumSet.complementOf(EnumSet.of(Result.DELETED, Result.CREATED)),
                    ChangeType.PAGE_RERENDER,
                    EnumSet.complementOf(EnumSet.of(Result.DELETED, Result.CREATED)),
                    ChangeType.REDIRECT_UPDATE,
                    EnumSet.complementOf(EnumSet.of(Result.DELETED, Result.CREATED)),
                    ChangeType.PAGE_DELETE,
                    EnumSet.of(Result.DELETED, Result.NOT_FOUND),
                    ChangeType.PAGE_RERENDER_UPSERT,
                    EnumSet.complementOf(EnumSet.of(Result.DELETED)));

    private static Predicate<? super Exception> isInstance(Class<? extends Exception> type) {
        return type::isInstance;
    }

    private static Map.Entry<Class<? extends Exception>, Predicate<? super Exception>>
            isInstanceEntry(Class<? extends Exception> type) {
        return Map.entry(type, isInstance(type));
    }

    @VisibleForTesting
    static final Map<Class<? extends Exception>, Predicate<? super Exception>> RETRYABLE_EXCEPTIONS =
            Map.ofEntries(
                    isInstanceEntry(SocketTimeoutException.class),
                    Map.entry(
                            ElasticsearchStatusException.class,
                            isInstance(ElasticsearchStatusException.class)
                                    .and(e -> ((ElasticsearchStatusException) e).status().getStatus() == 503)));

    private final Counter actionsCounter;
    private final Counter actionsSizeCounter;
    private final Map<String, Counter> actionCounters;

    private final Map<Class<? extends Exception>, Counter> onFailureCounters;

    private final int onFailureMaxAttempts;
    private final Duration onFailureRetryDelay;

    private final Duration socketTimeout;

    private final RestHighLevelClient client;

    private final Duration connectionTimeoutOverhead =
            Duration.ofMillis(HttpClientFactory.ENVOY_OVERHEAD_MS);

    public CirrusBulkRequestBridge(
            AsyncElasticsearchClientConfig clientConfig, MetricGroup metricGroup) {

        client = ElasticsearchClientFactory.buildClient(clientConfig, metricGroup);

        actionsSizeCounter = metricGroup.counter("actions_size");
        actionsCounter = metricGroup.counter("actions");

        actionCounters =
                Arrays.stream(ChangeType.values())
                        .sequential()
                        .flatMap(
                                changeType ->
                                        Stream.concat(
                                                        CHANGE_TYPE_RESULTS.get(changeType).stream(), Stream.of((Result) null))
                                                .map(result -> new BulkActionMetricKey(changeType, result)))
                        .collect(
                                Collectors.toMap(
                                        BulkActionMetricKey::toKey,
                                        key -> {
                                            final MetricGroup keyedGroup =
                                                    createTaggedGroup(metricGroup, key.toKeyValuePairs());
                                            return keyedGroup.counter("actions");
                                        }));

        final MetricGroup onFailureMetricGroup = metricGroup.addGroup("failure");
        this.onFailureCounters =
                RETRYABLE_EXCEPTIONS.keySet().stream()
                        .collect(
                                Collectors.toMap(
                                        Function.identity(),
                                        type ->
                                                onFailureMetricGroup
                                                        .addGroup("type", type.getSimpleName())
                                                        .counter("requests")));

        this.onFailureMaxAttempts = clientConfig.getOnFailureMaxRetries();
        this.onFailureRetryDelay = clientConfig.getOnFailureRetryDelay();
        this.socketTimeout = clientConfig.getSocketTimeout();
    }

    @Override
    public void close() throws Exception {
        client.close();
    }

    @Override
    public void submit(List<AsyncElasticsearchBulkRequest> batch, BatchFuture batchFuture) {
        final BulkRequestWithOptions bulkRequestWithOptions = createRequestWithOptions(batch);
        final BulkRequest request = bulkRequestWithOptions.getBulkRequest();
        if (socketTimeout.compareTo(connectionTimeoutOverhead) > 0) {
            request.timeout(new TimeValue(socketTimeout.minus(connectionTimeoutOverhead).toMillis()));
        }

        actionsCounter.inc(request.numberOfActions());
        actionsSizeCounter.inc(
                batch.stream().mapToLong(AsyncElasticsearchBulkRequest::getEstimatedSizeInBytes).sum());

        client.bulkAsync(
                request,
                bulkRequestWithOptions.getRequestOptions(),
                new ActionListener<>() {
                    @Override
                    public void onResponse(BulkResponse bulkResponse) {
                        CirrusBulkRequestBridge.this.onResponse(batch, request, bulkResponse, batchFuture);
                    }

                    @Override
                    public void onFailure(Exception failure) {
                        CirrusBulkRequestBridge.this.onFailure(batch, request, failure, batchFuture);
                    }
                });
    }

    protected void onResponse(
            List<AsyncElasticsearchBulkRequest> batch,
            BulkRequest request,
            BulkResponse response,
            BatchFuture batchFuture) {
        BulkItemResponse[] items = response.getItems();

        final int responseCount = items.length;
        final int requestCount = request.requests().size();

        Preconditions.checkArgument(
                requestCount == responseCount,
                "Unexpected number of requests/responses: " + requestCount + " != " + responseCount);

        IntStream.range(0, requestCount)
                .forEach(
                        i -> {
                            final DocWriteRequest<?> itemRequest = request.requests().get(i);
                            final String changeType;
                            if (itemRequest instanceof DeleteRequest) {
                                changeType = ChangeType.PAGE_DELETE.name();
                            } else if (itemRequest instanceof UpdateRequest) {
                                changeType =
                                        (String)
                                                ((UpdateRequest) itemRequest)
                                                        .script()
                                                        .getParams()
                                                        .getOrDefault("change_type", "_MISSING");
                            } else {
                                changeType = "_UNKNOWN";
                            }

                            BulkItemResponse itemResponse = items[i];

                            final String actionCounterKey =
                                    BulkActionMetricKey.formatKey(
                                            changeType,
                                            itemResponse.isFailed() ? null : itemResponse.getResponse().getResult());

                            final Counter counter = actionCounters.get(actionCounterKey);
                            if (counter == null) {
                                log.warn("Missing counter for key {}", actionCounterKey);
                            } else {
                                log.trace("Incrementing counter {}", actionCounterKey);
                                counter.inc();
                            }

                            if (!itemResponse.isFailed()) {
                                return;
                            }

                            final Failure failure = itemResponse.getFailure();
                            if (failure.getMessage().contains("document_missing_exception")) {
                                log.warn(
                                        "document_missing_exception for page id {} in index {}",
                                        failure.getId(),
                                        failure.getIndex());
                            } else if (failure.getMessage().contains("index_not_found_exception")) {
                                log.warn("index_not_found_exception for index {}", failure.getIndex());
                            } else {
                                log.warn(
                                        "bulk action failed with status {}: {}",
                                        itemResponse.status(),
                                        failure,
                                        failure.getCause());
                            }
                        });

        batchFuture.complete();
    }

    protected void onFailure(
            List<AsyncElasticsearchBulkRequest> batch,
            BulkRequest bulkRequest,
            Exception exception,
            BatchFuture batchFuture) {
        if (RETRYABLE_EXCEPTIONS.values().stream().anyMatch(predicate -> predicate.test(exception))) {
            final Counter counter = onFailureCounters.get(exception.getClass());
            if (counter != null) {
                counter.inc();
            }

            final long attempt =
                    batch.stream()
                            .mapToInt(AsyncElasticsearchBulkRequest::incrementAndGetAttempt)
                            .max()
                            .orElse(0);

            if (onFailureMaxAttempts < 0 || attempt <= onFailureMaxAttempts) {
                batchFuture.completeAndRetry(batch, calculateRetryDelay(attempt));
                return;
            }
        }

        batchFuture.completeExceptionally(exception);
    }

    protected Duration calculateRetryDelay(long attempt) {
        return onFailureRetryDelay.multipliedBy(attempt);
    }

    protected static MetricGroup createTaggedGroup(
            MetricGroup metricGroup, Stream<Entry<String, String>> keyValuePairs) {
        return keyValuePairs.reduce(
                metricGroup,
                (parent, pair) -> parent.addGroup(pair.getKey(), pair.getValue()),
                (left, right) -> {
                    throw new UnsupportedOperationException();
                });
    }

    protected BulkRequestWithOptions createRequestWithOptions(
            Collection<AsyncElasticsearchBulkRequest> batch) {
        final BulkRequest bulkRequest = new BulkRequest();
        batch.stream()
                .flatMap(partialBatch -> partialBatch.requests().stream())
                .forEach(bulkRequest::add);
        return new BulkRequestWithOptions(bulkRequest);
    }

    @Getter
    @AllArgsConstructor
    protected static final class BulkRequestWithOptions {
        private final BulkRequest bulkRequest;
        private final RequestOptions requestOptions;

        public BulkRequestWithOptions(BulkRequest bulkRequest) {
            this(bulkRequest, RequestOptions.DEFAULT);
        }
    }

    protected static final class BulkActionMetricKey {

        final ChangeType changeType;

        final Result result;

        BulkActionMetricKey(ChangeType changeType, @Nullable Result result) {
            this.changeType = changeType;
            this.result = result;
        }

        @Nonnull
        static String formatKey(@Nonnull String changeType, @Nullable Result result) {
            return changeType + ":" + mapResult(result);
        }

        String toKey() {
            return formatKey(changeType.name(), result);
        }

        Stream<Map.Entry<String, String>> toKeyValuePairs() {
            return Stream.of(
                    Map.entry("change_type", changeType.name()), Map.entry("result", mapResult(result)));
        }

        @Nonnull
        private static String mapResult(@Nullable Result result) {
            return result == null ? "_FAILURE" : result.name();
        }
    }
}
