package org.wikimedia.discovery.cirrus.updater.consumer.sanity;

import org.apache.flink.api.connector.source.SourceOutput;
import org.apache.flink.connector.base.source.reader.RecordEmitter;
import org.wikimedia.discovery.cirrus.updater.common.model.SanityCheck;

class SanitySourceRecordEmitter
        implements RecordEmitter<SanitySourceRecord, SanityCheck, SanitySourceSplitState> {
    public void emitRecord(
            SanitySourceRecord element,
            SourceOutput<SanityCheck> output,
            SanitySourceSplitState splitState) {
        // Emitter is responsible for both emitting the final element and updating the mutable split
        // state.
        element
                .getEvent()
                .ifPresent(event -> output.collect(event, event.getEventTime().toEpochMilli()));
        element.applyStateUpdate(splitState);
    }
}
