package org.wikimedia.discovery.cirrus.updater.consumer.graph;

import java.time.Duration;
import java.util.List;

import org.apache.flink.metrics.MetricGroup;
import org.apache.flink.util.function.SerializableFunction;
import org.elasticsearch.action.bulk.BulkRequest;
import org.wikimedia.discovery.cirrus.updater.consumer.graph.AsyncElasticsearchBulkRequestBridge.AsyncElasticsearchBulkRequestBridgeFactory.InitContext;

/**
 * Interface logic bridging between {@link BulkRequest} and {@link
 * org.apache.flink.connector.base.sink.writer.AsyncSinkWriter}.
 */
public interface AsyncElasticsearchBulkRequestBridge extends AutoCloseable {

    void submit(List<AsyncElasticsearchBulkRequest> batch, BatchFuture batchFuture);

    interface AsyncElasticsearchBulkRequestBridgeFactory
            extends SerializableFunction<InitContext, AsyncElasticsearchBulkRequestBridge> {

        interface InitContext {

            MetricGroup metricGroup();
        }
    }

    /**
     * Interface to asynchronously inform the calling {@link
     * org.apache.flink.connector.base.sink.writer.AsyncSinkWriter} about the batch processing result.
     */
    interface BatchFuture {
        default void complete() {
            completeAndRetry(List.of(), Duration.ZERO);
        }

        void completeAndRetry(List<AsyncElasticsearchBulkRequest> batch, Duration delay);

        void completeExceptionally(Exception exception);
    }
}
