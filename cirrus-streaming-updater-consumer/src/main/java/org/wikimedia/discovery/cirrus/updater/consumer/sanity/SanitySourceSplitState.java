package org.wikimedia.discovery.cirrus.updater.consumer.sanity;

import java.util.HashMap;
import java.util.Map;

import lombok.AllArgsConstructor;

/** Mutable variant of SanitySourceSplit. */
@AllArgsConstructor
public class SanitySourceSplitState {
    private final String splitId;
    Map<String, LoopState> state;

    SanitySourceSplitState(SanitySourceSplit split) {
        splitId = split.splitId();
        state = new HashMap<>(split.state);
    }

    public SanitySourceSplit toSanitySourceSplit() {
        return new SanitySourceSplit(splitId, Map.copyOf(state));
    }

    public void put(LoopState loopState) {
        state.put(loopState.getDomain(), loopState);
    }

    public void remove(String domain) {
        state.remove(domain);
    }
}
