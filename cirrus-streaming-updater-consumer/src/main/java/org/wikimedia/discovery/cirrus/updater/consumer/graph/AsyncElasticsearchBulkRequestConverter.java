package org.wikimedia.discovery.cirrus.updater.consumer.graph;

import static com.google.common.collect.Maps.newHashMap;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.annotation.Nonnull;

import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.api.connector.sink2.Sink;
import org.apache.flink.api.connector.sink2.SinkWriter.Context;
import org.apache.flink.connector.base.sink.writer.ElementConverter;
import org.apache.flink.types.Row;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.script.Script;
import org.elasticsearch.script.ScriptType;
import org.wikimedia.discovery.cirrus.updater.common.graph.WeightedTagMonitor;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateEvent;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateEvent.TargetDocument;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateFields;
import org.wikimedia.eventutilities.flink.formats.json.JsonRowSerializationSchema;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;

@SuppressFBWarnings("SE_NO_SERIALVERSIONID")
public class AsyncElasticsearchBulkRequestConverter
        implements ElementConverter<UpdateEvent, AsyncElasticsearchBulkRequest> {

    private static final String LANG_NOOP = "super_detect_noop";
    private static final int REDIRECT_MAX_SIZE = 1024;

    private final JsonRowSerializationSchema schema;

    private final ObjectMapper mapper;
    private transient WeightedTagMonitor weightedTagMonitor;

    public AsyncElasticsearchBulkRequestConverter(
            TypeInformation<Row> fieldsType, ObjectMapper mapper) {
        this.schema =
                JsonRowSerializationSchema.builder()
                        .withTypeInfo(fieldsType)
                        .withObjectMapper(mapper)
                        .withoutNormalization()
                        .build();
        this.mapper = mapper;
    }

    @Override
    public void open(Sink.InitContext context) {
        weightedTagMonitor = new WeightedTagMonitor(context.metricGroup().addGroup("weighted_tags"));
    }

    @Nonnull
    private static DeleteRequest createDeleteRequest(UpdateEvent updateEvent) {
        return new DeleteRequest(
                updateEvent.getTargetDocument().getIndexName(),
                Long.toString(updateEvent.getTargetDocument().getPageId()));
    }

    @Nonnull
    private UpdateRequest createUpdateRequest(UpdateEvent updateEvent, boolean upsert)
            throws IOException {
        final Row rawFields = updateEvent.getUpdate().getRawFields();
        Map<String, String> hints = updateEvent.getUpdate().getNoopHints();
        final Map<String, Object> scriptSourceParam = toMap(rawFields);
        final Map<String, Object> upsertSource = upsert ? new HashMap<>(scriptSourceParam) : null;

        if (scriptSourceParam.containsKey("redirect")) {
            // If the script source has a redirect array then that is fresh from cirrusdoc and shouldn't
            // need mutation.
            // Skip any mutations and remove the related noop hints.
            hints = new HashMap<>(hints);
            hints.remove("redirect");
        } else if (updateEvent.getUpdate().getRedirectAdd() != null
                || updateEvent.getUpdate().getRedirectRemove() != null) {
            NoopSetParameters<Map<String, Object>> parameters = new NoopSetParameters<>();
            parameters.setMaxSize(REDIRECT_MAX_SIZE);

            Optional.ofNullable(updateEvent.getUpdate().getRedirectAdd())
                    .map(
                            redirectsToAdd ->
                                    redirectsToAdd.stream().map(this::encodeRedirect).collect(Collectors.toList()))
                    .ifPresent(parameters::setAdd);

            Optional.ofNullable(updateEvent.getUpdate().getRedirectRemove())
                    .map(
                            redirectsToAdd ->
                                    redirectsToAdd.stream().map(this::encodeRedirect).collect(Collectors.toList()))
                    .ifPresent(parameters::setRemove);

            scriptSourceParam.put("redirect", encodeRedirectNoopSetParameters(parameters));
        }

        if (updateEvent.getUpdate().getWeightedTags() != null) {
            scriptSourceParam.put("weighted_tags", updateEvent.getUpdate().getWeightedTags());
        }

        final Map<String, Object> params = new HashMap<>(Map.of("source", scriptSourceParam));

        if (hints != null && !hints.isEmpty()) {
            params.put("handlers", hints);
        }

        // Kept only for inspecting the bulk response
        params.put("change_type", updateEvent.getChangeType().name());

        final Script script = new Script(ScriptType.INLINE, LANG_NOOP, LANG_NOOP, params);

        final String index = updateEvent.getTargetDocument().getIndexName();
        final String id = Long.toString(updateEvent.getTargetDocument().getPageId());

        final UpdateRequest request = new UpdateRequest(index, id).script(script).retryOnConflict(5);
        if (upsertSource != null) {
            request.upsert(upsertSource);
        }
        return request;
    }

    private static Map<String, Object> encodeRedirectNoopSetParameters(
            NoopSetParameters<Map<String, Object>> parameters) {
        final HashMap<String, Object> map = newHashMap();
        if (parameters.getAdd() != null) {
            map.put(UpdateFields.NOOP_HINTS_SET_ADD, parameters.getAdd());
        }
        if (parameters.getRemove() != null) {
            map.put(UpdateFields.NOOP_HINTS_SET_REMOVE, parameters.getRemove());
        }
        if (parameters.getMaxSize() != null) {
            map.put(UpdateFields.NOOP_HINTS_SET_MAX_SIZE, parameters.getMaxSize());
        }
        return map;
    }

    private Map<String, Object> encodeRedirect(TargetDocument targetDocument) {
        return Map.of(
                "namespace", targetDocument.getPageNamespace(),
                "title", targetDocument.getUnprefixedPageTitleTextForm());
    }

    private Map<String, Object> toMap(Row fields) throws IOException {
        if (fields == null) {
            return newHashMap();
        }
        final byte[] encodedJson = schema.serialize(fields);
        final JsonNode decodedJson = mapper.reader().readTree(encodedJson);
        return newHashMap(
                mapper.convertValue(decodedJson, new TypeReference<Map<String, Object>>() {}));
    }

    @Override
    @SneakyThrows
    public AsyncElasticsearchBulkRequest apply(UpdateEvent element, Context context) {
        weightedTagMonitor.report(element);
        switch (element.getChangeType()) {
            case PAGE_RERENDER_UPSERT:
            case REV_BASED_UPDATE:
                return new AsyncElasticsearchBulkRequest(
                        element.getMeta().getId(), createUpdateRequest(element, true), element.getEventTime());
            case TAGS_UPDATE:
                // prefer meta.dt to avoid reporting very inaccurate lag for tag updates in case these are
                // from a backfill. See https://phabricator.wikimedia.org/T387038
                return new AsyncElasticsearchBulkRequest(
                        element.getMeta().getId(),
                        createUpdateRequest(element, false),
                        element.getMeta().getDt());
            case PAGE_RERENDER:
            case REDIRECT_UPDATE:
                return new AsyncElasticsearchBulkRequest(
                        element.getMeta().getId(), createUpdateRequest(element, false), element.getEventTime());
            case PAGE_DELETE:
                return new AsyncElasticsearchBulkRequest(
                        element.getMeta().getId(), createDeleteRequest(element), element.getEventTime());
            default:
                throw new UnsupportedOperationException(
                        "Unable to emit elasticsearch command for update operation " + element.getChangeType());
        }
    }

    @Getter
    @Setter
    public static class NoopSetParameters<T> {

        List<T> add;
        List<T> remove;
        Integer maxSize;
    }
}
