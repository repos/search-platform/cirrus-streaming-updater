package org.wikimedia.discovery.cirrus.updater.consumer.graph;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.time.Instant;

import org.apache.flink.connector.base.sink.writer.AsyncSinkWriterStateSerializer;
import org.elasticsearch.Version;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.common.io.stream.InputStreamStreamInput;
import org.elasticsearch.common.io.stream.OutputStreamStreamOutput;

import lombok.extern.slf4j.Slf4j;

/**
 * Serializes {@link AsyncElasticsearchBulkRequest} instances.
 *
 * <p>Leverages the {@link org.elasticsearch.common.io.stream.StreamInput} and {@link
 * org.elasticsearch.common.io.stream.StreamOutput} to serialize and deserialize the underlying
 * {@link BulkRequest}. <b>CAVEAT:</b> Despite implementing {@link
 * org.apache.flink.core.io.SimpleVersionedSerializer} the abstract implementation {@link
 * AsyncSinkWriterStateSerializer} ignores {@link #getVersion()}.
 */
@Slf4j
public class AsyncElasticsearchSinkWriterSerializer
        extends AsyncSinkWriterStateSerializer<AsyncElasticsearchBulkRequest> {

    @Override
    public int getVersion() {
        return 2;
    }

    /**
     * Serializes a single {@link AsyncElasticsearchBulkRequest}.
     *
     * <p>{@link AsyncSinkWriterStateSerializer} delegates to this method. However, it ignores {@link
     * #getVersion()}, which is why we add it per request item.
     *
     * @param asyncElasticsearchBulkRequest a single (buffered) item
     * @param dataOutputStream output stream to write to
     */
    @Override
    protected void serializeRequestToStream(
            AsyncElasticsearchBulkRequest asyncElasticsearchBulkRequest,
            DataOutputStream dataOutputStream)
            throws IOException {

        dataOutputStream.writeInt(getVersion());
        dataOutputStream.writeUTF(asyncElasticsearchBulkRequest.getId());
        dataOutputStream.writeInt(asyncElasticsearchBulkRequest.getAttempt());
        dataOutputStream.writeLong(asyncElasticsearchBulkRequest.getEventTime().toEpochMilli());

        try (ByteArrayOutputStream out = new ByteArrayOutputStream();
                OutputStreamStreamOutput streamOutput = new OutputStreamStreamOutput(out)) {

            Version.writeVersion(streamOutput.getVersion(), streamOutput);
            asyncElasticsearchBulkRequest.getBulkRequest().writeTo(streamOutput);
            streamOutput.flush();
            out.flush();

            dataOutputStream.write(out.toByteArray());
        }
    }

    /**
     * Deserializes a single {@link AsyncElasticsearchBulkRequest}.
     *
     * <p>{@link AsyncSinkWriterStateSerializer} delegates to this method. In our case {@code
     * requestSize} holds the estimated size of the resulting JSON so it does not match the number of
     * bytes to be read from {@code dataInputStream}
     *
     * @param requestSize estimated size stored in {@link
     *     org.apache.flink.connector.base.sink.writer.RequestEntryWrapper#getSize()}
     * @param dataInputStream input stream to read from
     */
    @Override
    protected AsyncElasticsearchBulkRequest deserializeRequestFromStream(
            long requestSize, DataInputStream dataInputStream) throws IOException {

        final int version = dataInputStream.readInt();

        if (version != 1 && version != getVersion()) {
            throw new UnsupportedOperationException(
                    "Unable to deserialize version " + version + " from " + getVersion());
        }

        final String updateEventId = dataInputStream.readUTF();
        final int attempt = dataInputStream.readInt();
        final Instant eventTime;
        if (version >= 2) {
            eventTime = Instant.ofEpochMilli(dataInputStream.readLong());
        } else {
            eventTime = Instant.now();
        }

        try (InputStreamStreamInput streamInput = new InputStreamStreamInput(dataInputStream)) {
            streamInput.setVersion(Version.readVersion(streamInput));
            return new AsyncElasticsearchBulkRequest(
                    updateEventId, attempt, new BulkRequest(streamInput), eventTime);
        }
    }
}
