package org.wikimedia.discovery.cirrus.updater.consumer.graph;

import static org.apache.flink.util.Preconditions.checkArgument;
import static org.apache.flink.util.Preconditions.checkNotNull;
import static org.apache.http.protocol.HttpCoreContext.HTTP_REQUEST;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.SortedMap;

import org.apache.flink.metrics.MetricGroup;
import org.apache.http.HttpHost;
import org.apache.http.HttpRequest;
import org.apache.http.HttpRequestInterceptor;
import org.apache.http.HttpResponseInterceptor;
import org.apache.http.impl.conn.DefaultRoutePlanner;
import org.apache.http.impl.conn.DefaultSchemePortResolver;
import org.apache.http.protocol.HttpContext;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.elasticsearch.client.RestHighLevelClient;
import org.wikimedia.discovery.cirrus.updater.common.http.HttpClientFactory;
import org.wikimedia.discovery.cirrus.updater.common.http.MetricGroupMetricRegistryListener;
import org.wikimedia.discovery.cirrus.updater.common.http.TagNamingStrategy;
import org.wikimedia.utils.http.CustomRoutePlanner;

import com.codahale.metrics.Histogram;
import com.codahale.metrics.MetricRegistry;

import lombok.experimental.UtilityClass;

@UtilityClass
@SuppressWarnings("HideUtilityClassConstructor")
public class ElasticsearchClientFactory {

    private static final String START_INSTANT_CONTEXT_KEY = "start-instant";

    static RestHighLevelClient buildClient(
            AsyncElasticsearchClientConfig clientConfig, MetricGroup metricGroup) {
        return buildClient(clientConfig, createHttpMetricRegistry(metricGroup));
    }

    static RestHighLevelClient buildClient(
            AsyncElasticsearchClientConfig clientConfig, MetricRegistry httpMetricRegistry) {
        List<HttpHost> hosts = clientConfig.getHosts();
        checkNotNull(hosts, "hosts must be set");
        checkArgument(!hosts.isEmpty(), "hosts must be set");

        final RestClientBuilder clientBuilder = RestClient.builder(hosts.toArray(new HttpHost[0]));

        clientBuilder.setCompressionEnabled(true);

        final String pathPrefix = clientConfig.getConnectionPathPrefix();
        if (pathPrefix != null && !pathPrefix.trim().isEmpty()) {
            clientBuilder.setPathPrefix(pathPrefix);
        }

        clientBuilder.setHttpClientConfigCallback(
                (asyncClientBuilder) -> {
                    if (clientConfig.getMaxConnections() > 0) {
                        asyncClientBuilder.setMaxConnPerRoute(clientConfig.getMaxConnections());
                        asyncClientBuilder.setMaxConnTotal(clientConfig.getMaxConnections());
                    }

                    createRoutePlanner(clientConfig.getHostMap())
                            .ifPresent(asyncClientBuilder::setRoutePlanner);

                    return asyncClientBuilder
                            .addInterceptorFirst(
                                    (HttpRequest httpRequest, HttpContext httpContext) ->
                                            interceptRequest(httpContext))
                            .addInterceptorLast(
                                    (HttpRequestInterceptor)
                                            ((httpRequest, httpContext) ->
                                                    HttpClientFactory.addEnvoyHeaders(
                                                            clientConfig.getSocketTimeout(), httpRequest::addHeader)))
                            .addInterceptorFirst(buildResponseInterceptor(httpMetricRegistry));
                });

        return new RestHighLevelClient(
                clientBuilder.setRequestConfigCallback(
                        requestConfigBuilder -> {
                            if (clientConfig.getConnectionRequestTimeout() != null) {
                                requestConfigBuilder.setConnectionRequestTimeout(
                                        (int) clientConfig.getConnectionRequestTimeout().toMillis());
                            }
                            if (clientConfig.getConnectionTimeout() != null) {
                                requestConfigBuilder.setConnectTimeout(
                                        (int) clientConfig.getConnectionTimeout().toMillis());
                            }
                            if (clientConfig.getSocketTimeout() != null) {
                                requestConfigBuilder.setSocketTimeout(
                                        (int) clientConfig.getSocketTimeout().toMillis());
                            }
                            return requestConfigBuilder;
                        }));
    }

    static Optional<CustomRoutePlanner> createRoutePlanner(Map<String, HttpHost> hostMap) {

        if (hostMap != null && !hostMap.isEmpty()) {
            return Optional.of(
                    new CustomRoutePlanner(
                            hostMap, new DefaultRoutePlanner(DefaultSchemePortResolver.INSTANCE)));
        }
        return Optional.empty();
    }

    static HttpResponseInterceptor buildResponseInterceptor(MetricRegistry httpMetricRegistry) {
        return (httpResponse, httpContext) -> {
            final Object rawStartInstant = httpContext.getAttribute(START_INSTANT_CONTEXT_KEY);
            if (rawStartInstant instanceof Long) {
                org.apache.http.HttpRequest request =
                        (org.apache.http.HttpRequest) httpContext.getAttribute(HTTP_REQUEST);
                final long delta = System.currentTimeMillis() - ((long) rawStartInstant);

                final String metricName =
                        TagNamingStrategy.name("request_duration") + TagNamingStrategy.encodeRequest(request);

                final SortedMap<String, Histogram> metricCandidates =
                        httpMetricRegistry.getHistograms((name, metric) -> name.equals(metricName));

                metricCandidates.values().stream()
                        .findFirst()
                        .orElseGet(() -> httpMetricRegistry.histogram(metricName))
                        .update(delta);
            }
        };
    }

    static void interceptRequest(HttpContext httpContext) {
        httpContext.setAttribute(START_INSTANT_CONTEXT_KEY, System.currentTimeMillis());
    }

    static MetricRegistry createHttpMetricRegistry(MetricGroup metricGroup) {
        final MetricRegistry metricRegistry = new MetricRegistry();
        metricRegistry.addListener(new MetricGroupMetricRegistryListener(metricGroup.addGroup("http")));
        return metricRegistry;
    }
}
