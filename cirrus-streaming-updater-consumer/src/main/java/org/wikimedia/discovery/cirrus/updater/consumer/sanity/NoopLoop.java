package org.wikimedia.discovery.cirrus.updater.consumer.sanity;

import java.time.Duration;
import java.time.Instant;
import java.util.function.Consumer;

import javax.annotation.Nullable;

import org.apache.commons.lang3.ObjectUtils;

public class NoopLoop implements Loop {
    private static final Duration DELAY = Duration.ofMinutes(10);
    private Instant nextInvoke = Instant.MIN;

    @Nullable private final Instant endAt;

    NoopLoop(@Nullable Instant endAt) {
        this.endAt = endAt;
    }

    @Override
    public Instant nextInvokeAt() {
        // We could maybe return Instant.MAX for everything, but would require additional
        // handling elsewhere. Keep things simple and produce no records after a medium
        // length of fake-polling.
        return ObjectUtils.min(endAt, nextInvoke);
    }

    @Override
    public boolean next(Instant now, Consumer<SanitySourceRecord> consumer) {
        nextInvoke = now.plus(DELAY);
        if (endAt == null) {
            return false;
        }
        // True when the loop is finished and will never again produce records.
        return now.isAfter(endAt);
    }
}
