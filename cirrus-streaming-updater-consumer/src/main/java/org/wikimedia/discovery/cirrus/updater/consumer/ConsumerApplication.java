package org.wikimedia.discovery.cirrus.updater.consumer;

import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.slf4j.LoggerFactory;
import org.wikimedia.discovery.cirrus.updater.common.config.ParameterToolMerger;
import org.wikimedia.discovery.cirrus.updater.consumer.config.ConsumerConfig;
import org.wikimedia.discovery.cirrus.updater.consumer.graph.ConsumerGraphFactory;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import lombok.experimental.UtilityClass;

@UtilityClass
@SuppressWarnings("HideUtilityClassConstructor")
@SuppressFBWarnings(
        value = "HideUtilityClassConstructor",
        justification = "lombok takes care of that")
public class ConsumerApplication {

    public static void main(String[] args) throws Exception {
        final Configuration configuration =
                ParameterToolMerger.fromDefaultsWithOverrides(
                                "/cirrus-streaming-updater-consumer.properties", args)
                        .getConfiguration();

        final ConsumerConfig config = ConsumerConfig.of(configuration);
        try (ConsumerGraphFactory factory = new ConsumerGraphFactory(config)) {
            StreamExecutionEnvironment env = factory.prepareEnvironment();
            if (config.dryRun()) {
                LoggerFactory.getLogger(ConsumerApplication.class).info("DRY RUN DONE");
                return;
            }
            env.execute(config.jobName());
        }
    }
}
