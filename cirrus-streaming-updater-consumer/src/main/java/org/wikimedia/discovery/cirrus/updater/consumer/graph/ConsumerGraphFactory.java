package org.wikimedia.discovery.cirrus.updater.consumer.graph;

import static org.wikimedia.discovery.cirrus.updater.common.model.UpdateEvent.KEY_SELECTOR;

import java.net.URI;
import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.Nonnull;

import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.common.functions.FilterFunction;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.api.connector.sink2.Sink;
import org.apache.flink.api.connector.sink2.SinkWriter;
import org.apache.flink.api.java.typeutils.EitherTypeInfo;
import org.apache.flink.api.java.typeutils.ListTypeInfo;
import org.apache.flink.connector.base.sink.writer.config.AsyncSinkWriterConfiguration.AsyncSinkWriterConfigurationBuilder;
import org.apache.flink.connector.kafka.source.KafkaSource;
import org.apache.flink.connector.kafka.source.enumerator.initializer.OffsetsInitializer;
import org.apache.flink.streaming.api.datastream.AsyncDataStream;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.functions.ProcessFunction;
import org.apache.flink.streaming.api.functions.async.AsyncRetryStrategy;
import org.apache.flink.streaming.api.operators.async.AsyncWaitOperatorFactory;
import org.apache.flink.streaming.util.retryable.AsyncRetryStrategies;
import org.apache.flink.types.Either;
import org.apache.flink.types.Row;
import org.apache.flink.util.Collector;
import org.apache.flink.util.OutputTag;
import org.apache.http.HttpHost;
import org.apache.kafka.clients.consumer.OffsetResetStrategy;
import org.wikimedia.discovery.cirrus.updater.common.TransformOperator;
import org.wikimedia.discovery.cirrus.updater.common.config.EventStreamConstants;
import org.wikimedia.discovery.cirrus.updater.common.graph.BypassingCirrusDocFetcher;
import org.wikimedia.discovery.cirrus.updater.common.graph.CirrusEndpoint;
import org.wikimedia.discovery.cirrus.updater.common.graph.CirrusFetcher;
import org.wikimedia.discovery.cirrus.updater.common.graph.CommonGraphFactory;
import org.wikimedia.discovery.cirrus.updater.common.graph.IngestionTimeAssigner;
import org.wikimedia.discovery.cirrus.updater.common.graph.LagAwareRetryStrategy;
import org.wikimedia.discovery.cirrus.updater.common.graph.RouteFetchFailures;
import org.wikimedia.discovery.cirrus.updater.common.graph.WikiFilter;
import org.wikimedia.discovery.cirrus.updater.common.http.MediaWikiHttpClient;
import org.wikimedia.discovery.cirrus.updater.common.model.ChangeType;
import org.wikimedia.discovery.cirrus.updater.common.model.FetchFailure;
import org.wikimedia.discovery.cirrus.updater.common.model.FetchFailureEncoder;
import org.wikimedia.discovery.cirrus.updater.common.model.FetchFailureTypeInformation;
import org.wikimedia.discovery.cirrus.updater.common.model.PageKey;
import org.wikimedia.discovery.cirrus.updater.common.model.SanityCheck;
import org.wikimedia.discovery.cirrus.updater.common.model.SanityCheckTypeInfoFactory;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateEvent;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateFields;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateRowTranscoder;
import org.wikimedia.discovery.cirrus.updater.common.util.function.SerializableFunction;
import org.wikimedia.discovery.cirrus.updater.common.util.function.SerializablePredicate;
import org.wikimedia.discovery.cirrus.updater.consumer.config.ConsumerConfig;
import org.wikimedia.discovery.cirrus.updater.consumer.graph.AsyncElasticsearchClientConfig.AsyncElasticsearchClientConfigBuilder;
import org.wikimedia.discovery.cirrus.updater.consumer.graph.AsyncElasticsearchSink.AsyncElasticsearchSinkBuilder;
import org.wikimedia.discovery.cirrus.updater.consumer.sanity.SanitySource;
import org.wikimedia.discovery.cirrus.updater.consumer.sanity.SiteInfoMaxPageIdLookup;
import org.wikimedia.discovery.cirrus.updater.consumer.sanity.SiteMatrixDomainLookup;
import org.wikimedia.eventutilities.core.event.EventStream;
import org.wikimedia.eventutilities.flink.EventRowTypeInfo;
import org.wikimedia.eventutilities.flink.formats.json.JsonRowDeserializationSchema;
import org.wikimedia.eventutilities.flink.formats.json.JsonRowDeserializationSchema.Builder;
import org.wikimedia.eventutilities.flink.formats.json.KafkaRecordTimestampStrategy;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import lombok.extern.slf4j.Slf4j;

@SuppressWarnings({"checkstyle:classfanoutcomplexity"})
@Slf4j
public class ConsumerGraphFactory extends CommonGraphFactory<ConsumerConfig> {

    public static final TypeInformation<PageKey> KEY_TYPE_INFO = TypeInformation.of(PageKey.class);

    /**
     * This appears rather high, but keep in mind, that this must leave room for an unknown number of
     * 429 retries.
     *
     * @see org.wikimedia.discovery.cirrus.updater.common.http.HttpClientFactory
     */
    public static final Duration ASYNC_FETCH_OVERALL_TIMEOUT = Duration.ofMinutes(5);

    private final ObjectMapper objectMapper = new ObjectMapper();

    public ConsumerGraphFactory(ConsumerConfig config) {
        super(config);
    }

    public DataStream<UpdateEvent> createDataStreamSource() {
        DataStream<UpdateEvent> fromKafka = createKafkaSources();
        // The sanity source is always connected, even if disabled, to ensure
        // a constant shape to the flink graph. Changing the graph shape based
        // on configuration makes deployments tedious and susceptible to problems.
        return fromKafka.union(createSanitySource());
    }

    private SanitySource.WikiSupplierFactory createWikiSupplier() {
        MediaWikiHttpClient.Factory httpClientFactory = createMediWikiHttpClientFactory(2);
        Predicate<JsonNode> filter = createSiteMatrixFilter();
        return metricGroup -> new SiteMatrixDomainLookup(httpClientFactory.apply(metricGroup), filter);
    }

    private SerializablePredicate<JsonNode> createSiteMatrixFilter() {
        // The saneitizer doesn't work off wikiids, it needs domain names. So we use the site matrix
        // lookup to resolve those.
        SerializablePredicate<JsonNode> filter =
                WikiFilter.createFilter(SiteMatrixDomainLookup::wikiExtractor, config.wikiIds())
                        .orElse(json -> true);
        if (!config.consumePrivateUpdates()) {
            filter = filter.and(SiteMatrixDomainLookup.disallowPrivateWikis());
        }
        return filter;
    }

    private SanitySource.MaxPageIdLookupFactory createMaxPageIdLookup() {
        MediaWikiHttpClient.Factory httpClientFactory = createMediWikiHttpClientFactory(2);
        return metricGroup -> new SiteInfoMaxPageIdLookup(httpClientFactory.apply(metricGroup));
    }

    private Instant endSaneitizerAt() {
        // Returning non-null here means that the saneitizer will run bounded. We want bounded
        // behaviour in both tests and backfills, but never for the standard streaming operations.
        // Keep it simple and direct, no magic propagation from related config, to reduce surprise
        // outcomes when configuring.
        if (config.saneitizerBoundedRuntime() == null) {
            log.info("Saneitizer is unbounded");
            return null;
        }
        Instant endAt = config.clock().get().plus(config.saneitizerBoundedRuntime());
        log.info(
                "Saneitizer is explicitly bounded to {}. This should never be set in production.", endAt);
        return endAt;
    }

    private DataStream<UpdateEvent> createSanitySource() {
        SanitySource sanitySource =
                new SanitySource(
                        createWikiSupplier(),
                        createMaxPageIdLookup(),
                        config.clock(),
                        config.saneitizerLoopDuration(),
                        config.saneitizerBatchSize(),
                        endSaneitizerAt(),
                        !config.shouldSaneitize());

        TransformOperator<SanityCheck, Either<List<UpdateEvent>, FetchFailure<SanityCheck>>> operator =
                sanityCheckOperator();

        return env.fromSource(
                        sanitySource,
                        WatermarkStrategy.noWatermarks(),
                        "saneitizer",
                        SanityCheckTypeInfoFactory.create())
                .uid("saneitizer")
                .transform(
                        operator.nameAndUuidPrefix + operator.operatorName,
                        operator.outTypeInformation,
                        operator.operatorFactory)
                .uid(operator.nameAndUuidPrefix + "fetch")
                .name(operator.nameAndUuidPrefix + "fetch")
                .flatMap(
                        (value, out) -> {
                            // We don't really need a dead-letter queue of saneitizer failures, typical
                            // logging should suffice. Throw out failures.
                            if (value.isLeft()) {
                                value.left().forEach(out::collect);
                            }
                        },
                        getOrCreateUpdateTypeInfo())
                .uid(operator.nameAndUuidPrefix + "collect")
                .name(operator.nameAndUuidPrefix + "collect");
    }

    private DataStream<Row> createKafkaSource(String stream) {
        KafkaSource<Row> source = createKafkaSourceBuilder(stream).build();
        String sourceName = stream + "-source";
        return env.fromSource(source, WatermarkStrategy.noWatermarks(), sourceName).uid(sourceName);
    }

    private DataStream<UpdateEvent> createKafkaSources() {
        final UpdateRowTranscoder decoder = UpdateRowTranscoder.decoder();

        DataStream<UpdateEvent> dataStream =
                createKafkaSource(config.publicUpdateStream())
                        .map(
                                row -> {
                                    UpdateEvent event = decoder.decode(row);
                                    event.setFromPrivateStream(Boolean.FALSE);
                                    return event;
                                },
                                super.getOrCreateUpdateTypeInfo())
                        .uid("decode")
                        .name("decode");
        if (config.consumePrivateUpdates()) {
            dataStream =
                    dataStream.union(
                            createKafkaSource(config.privateUpdateStream())
                                    .map(
                                            row -> {
                                                UpdateEvent event = decoder.decode(row);
                                                event.setFromPrivateStream(Boolean.TRUE);
                                                return event;
                                            },
                                            super.getOrCreateUpdateTypeInfo())
                                    .uid("decode-private")
                                    .name("decode-private"));
        }

        if (config.publicLegacyUpdateStream() != null) {
            dataStream =
                    dataStream.union(
                            createKafkaSource(config.publicLegacyUpdateStream())
                                    .map(
                                            row -> {
                                                UpdateEvent event = decoder.decode(row);
                                                event.setFromPrivateStream(Boolean.FALSE);
                                                return event;
                                            },
                                            super.getOrCreateUpdateTypeInfo())
                                    .uid("decode-legacy")
                                    .name("decode-legacy"));
        }

        if (config.privateLegacyUpdateStream() != null) {
            dataStream =
                    dataStream.union(
                            createKafkaSource(config.privateLegacyUpdateStream())
                                    .map(
                                            row -> {
                                                UpdateEvent event = decoder.decode(row);
                                                event.setFromPrivateStream(Boolean.TRUE);
                                                return event;
                                            },
                                            super.getOrCreateUpdateTypeInfo())
                                    .uid("decode-private-legacy")
                                    .name("decode-private-legacy"));
        }

        return dataStream
                .map(new IngestionTimeAssigner(config.clock()), super.getOrCreateUpdateTypeInfo())
                .uid("assign-ingestion-time")
                .name("assign-ingestion-time")
                .filter(CommonGraphFactory.createFilter(config.wikiIds()))
                .uid("filter-by-wiki")
                .name("filter-by-wiki");
    }

    public Sink<UpdateEvent> createElasticSearchSink(TypeInformation<Row> fieldsType, URI uri) {
        if (config.useNullSink()) {
            return new NullSink<>();
        }

        final AsyncSinkWriterConfigurationBuilder writerConfigBuilder =
                getAsyncSinkWriterConfigurationBuilder();

        final AsyncElasticsearchClientConfig clientConfig =
                getAsyncElasticsearchClientConfigBuilder(uri).build();

        final AsyncElasticsearchSinkBuilder<UpdateEvent> sinkBuilder = AsyncElasticsearchSink.builder();

        sinkBuilder.elementConverter(
                new AsyncElasticsearchBulkRequestConverter(fieldsType, objectMapper));
        sinkBuilder.bridgeFactory(
                (context) ->
                        new CirrusBulkRequestBridge(clientConfig, context.metricGroup().addGroup("bulk")));
        sinkBuilder.writerConfig(writerConfigBuilder.build());

        return sinkBuilder.build();
    }

    public OutputTag<Row> createFetchFailureOutputTag() {
        return new OutputTag<>(
                "fetch_errors",
                eventDataStreamFactory.rowTypeInfo(
                        EventStreamConstants.FETCH_ERROR_STREAM,
                        EventStreamConstants.FETCH_ERROR_SCHEMA_VERSION));
    }

    public ProcessFunction<Either<UpdateEvent, FetchFailure<UpdateEvent>>, UpdateEvent>
            createFetchFailureRouter(OutputTag<Row> failureTag) {
        final EventRowTypeInfo failureTypeInfo = (EventRowTypeInfo) failureTag.getTypeInfo();
        // Used only for creating the serializer, no need to consult private stream as well.
        final EventStream rawEventStream = getOrCreateStream(config.publicUpdateStream());
        final EventRowTypeInfo rawEventRowTypeInfo = getEventRowTypeInfo(rawEventStream);

        return new RouteFetchFailures(
                config.jobName(),
                failureTag,
                FetchFailureEncoder.encoder(
                        failureTypeInfo, eventDataStreamFactory, rawEventStream, rawEventRowTypeInfo));
    }

    public Sink<Row> createFetchFailureSink() {
        return createKafkaSinkBuilder(
                        config.fetchErrorStream(),
                        config.fetchErrorTopic(),
                        KafkaRecordTimestampStrategy.FLINK_RECORD_EVENT_TIME)
                .build();
    }

    private Duration connectionTimeout(Duration requestTimeout) {
        return requestTimeout
                .dividedBy(config.httpRequestConnectionTimeoutDivisor())
                .truncatedTo(ChronoUnit.MILLIS);
    }

    public TransformOperator<SanityCheck, Either<List<UpdateEvent>, FetchFailure<SanityCheck>>>
            createSanityCheckOperator(
                    CirrusEndpoint<SanityCheck, List<UpdateEvent>> endpoint,
                    AsyncDataStream.OutputMode outputMode,
                    String nameAndUuidPrefix,
                    int capacity) {

        final CirrusFetcher<SanityCheck, List<UpdateEvent>> fetcher =
                new CirrusFetcher<>(
                        endpoint,
                        createMediaWikiHttpConfigBuilder(capacity)
                                .requestTimeout(config.saneitizerRequestTimeout())
                                .connectionTimeout(connectionTimeout(config.saneitizerRequestTimeout()))
                                .build());

        final AsyncRetryStrategy<Either<List<UpdateEvent>, FetchFailure<SanityCheck>>>
                asyncRetryStrategy =
                        new AsyncRetryStrategies.ExponentialBackoffDelayRetryStrategyBuilder<
                                        Either<List<UpdateEvent>, FetchFailure<SanityCheck>>>(
                                        10, Duration.ofSeconds(1).toMillis(), Duration.ofMinutes(2).toMillis(), 2)
                                .build();

        final long retryFetchFailTimeoutMillis = ASYNC_FETCH_OVERALL_TIMEOUT.toMillis();

        final AsyncWaitOperatorFactory<
                        SanityCheck, Either<List<UpdateEvent>, FetchFailure<SanityCheck>>>
                fetchRevisionOperatorFactory =
                        new AsyncWaitOperatorFactory<>(
                                fetcher, retryFetchFailTimeoutMillis, capacity, outputMode, asyncRetryStrategy);

        TypeInformation<List<UpdateEvent>> updateTypeInfo =
                new ListTypeInfo<>(getOrCreateUpdateTypeInfo());
        TypeInformation<SanityCheck> sanityCheckTypeInfo = SanityCheckTypeInfoFactory.create();
        final TypeInformation<Either<List<UpdateEvent>, FetchFailure<SanityCheck>>>
                enrichedTypeInformation =
                        new EitherTypeInfo<>(
                                updateTypeInfo, FetchFailureTypeInformation.create(sanityCheckTypeInfo));
        return new TransformOperator<>(
                "cirrus-sanity-check",
                nameAndUuidPrefix,
                // No filtering is applied at this level, the SanitySource filters event generation.
                x -> true,
                enrichedTypeInformation,
                fetchRevisionOperatorFactory);
    }

    public TransformOperator<UpdateEvent, Either<UpdateEvent, FetchFailure<UpdateEvent>>>
            createFetchRevisionOperator(
                    CirrusEndpoint<UpdateEvent, UpdateEvent> endpoint,
                    FilterFunction<UpdateEvent> filterFunction,
                    AsyncDataStream.OutputMode outputMode,
                    String nameAndUuidPrefix,
                    EnumSet<ChangeType> changeTypes) {

        final int connections =
                Math.max(
                        1,
                        changeTypes.stream().mapToInt(config.fetchConnections()::get).max().orElse(1)
                                / env.getParallelism());
        final CirrusFetcher<UpdateEvent, UpdateEvent> revisionFetcher =
                new CirrusFetcher<>(
                        endpoint,
                        createMediaWikiHttpConfigBuilder(connections)
                                .requestTimeout(config.fetchRequestTimeout())
                                .connectionTimeout(
                                        config
                                                .fetchRequestTimeout()
                                                .dividedBy(config.httpRequestConnectionTimeoutDivisor())
                                                .truncatedTo(ChronoUnit.MILLIS))
                                .build());

        final BypassingCirrusDocFetcher bypassingRevisionFetcher =
                new BypassingCirrusDocFetcher(revisionFetcher);

        final AsyncRetryStrategy<Either<UpdateEvent, FetchFailure<UpdateEvent>>> asyncRetryStrategy =
                new LagAwareRetryStrategy(
                        config.retryFetchMax(), config.retryFetchMaxAge(), config.clock());

        final long retryFetchFailTimeoutMillis = ASYNC_FETCH_OVERALL_TIMEOUT.toMillis();

        final int capacity =
                Math.max(
                        1,
                        changeTypes.stream().mapToInt(config.fetchQueueCapacity()::get).max().orElse(1)
                                / env.getParallelism());
        final AsyncWaitOperatorFactory<UpdateEvent, Either<UpdateEvent, FetchFailure<UpdateEvent>>>
                fetchRevisionOperatorFactory =
                        new AsyncWaitOperatorFactory<>(
                                bypassingRevisionFetcher,
                                retryFetchFailTimeoutMillis,
                                capacity,
                                outputMode,
                                asyncRetryStrategy);

        TypeInformation<UpdateEvent> updateTypeInfo = super.getOrCreateUpdateTypeInfo();
        final TypeInformation<Either<UpdateEvent, FetchFailure<UpdateEvent>>> enrichedTypeInformation =
                new EitherTypeInfo<>(updateTypeInfo, FetchFailureTypeInformation.create(updateTypeInfo));
        return new TransformOperator<>(
                "enrich-page-change-with-revision",
                nameAndUuidPrefix,
                filterFunction,
                enrichedTypeInformation,
                fetchRevisionOperatorFactory);
    }

    @Nonnull
    @Override
    protected OffsetsInitializer getDefaultKafkaStartingOffsets() {
        return OffsetsInitializer.committedOffsets(OffsetResetStrategy.EARLIEST);
    }

    @Override
    protected void createStreamGraph() {
        final DataStream<UpdateEvent> dataStreamSource = createDataStreamSource();
        final TypeInformation<Row> fieldsType =
                getOrCreateUpdateRowTypeInfo().getTypeAt(UpdateFields.FIELDS);

        final DataStream<UpdateEvent> fetch = fetch(dataStreamSource, new Builder(fieldsType).build());

        final Map<String, String> clusterGroupUrls = config.elasticSearchUrls();
        final Map<String, OutputTag<UpdateEvent>> clusterGroupTags =
                clusterGroupUrls.keySet().stream()
                        .collect(
                                Collectors.toMap(
                                        Function.identity(),
                                        clusterGroup ->
                                                new OutputTag<>(clusterGroup, super.getOrCreateUpdateTypeInfo())));

        final SingleOutputStreamOperator<UpdateEvent> splitByClusterGroup =
                fetch
                        .process(
                                new SplitByValue<>(
                                        clusterGroupTags,
                                        updateEvent -> updateEvent.getTargetDocument().getClusterGroup()),
                                super.getOrCreateUpdateTypeInfo())
                        .uid("split-by-cluster_group")
                        .name("split-by-cluster_group");

        clusterGroupTags.forEach(
                (clusterGroup, value) ->
                        splitByClusterGroup
                                .getSideOutput(value)
                                .sinkTo(
                                        createElasticSearchSink(
                                                fieldsType, URI.create(clusterGroupUrls.get(clusterGroup))))
                                .uid("elasticsearch-sink-" + clusterGroup)
                                .name("elasticsearch-sink-" + clusterGroup));

        splitByClusterGroup
                .map(
                        x -> {
                            log.debug(
                                    "No sink configured for cluster group {}",
                                    x.getTargetDocument().getClusterGroup());
                            return x;
                        },
                        getOrCreateUpdateTypeInfo())
                .uid("elasticsearch-sink-unknown-log")
                .name("elasticsearch-sink-unknown-log")
                .sinkTo(new NullSink<>())
                .uid("elasticsearch-sink-unknown")
                .name("elasticsearch-sink-unknown");
    }

    private TransformOperator<UpdateEvent, Either<UpdateEvent, FetchFailure<UpdateEvent>>>
            revisionBasedFetchOperator(JsonRowDeserializationSchema fieldsSchema) {
        // While we only expect revision based events here, on first deployment flink state
        // can still have rerenders. This can be removed after first deployment.
        CombinedCirrusDocEndpoint endpoint =
                new CombinedCirrusDocEndpoint(
                        new RevisionCirrusDocEndpoint(this.objectMapper, fieldsSchema, config.clock()),
                        new RerenderCirrusDocEndpoint(this.objectMapper, fieldsSchema, config.clock()));

        return createFetchRevisionOperator(
                endpoint,
                e ->
                        e.getChangeType() != ChangeType.PAGE_RERENDER
                                && e.getChangeType() != ChangeType.PAGE_RERENDER_UPSERT,
                AsyncDataStream.OutputMode.ORDERED,
                "",
                EnumSet.complementOf(EnumSet.of(ChangeType.PAGE_RERENDER)));
    }

    private TransformOperator<UpdateEvent, Either<UpdateEvent, FetchFailure<UpdateEvent>>>
            rerendersFetchOperator(JsonRowDeserializationSchema fieldsSchema) {
        return createFetchRevisionOperator(
                new RerenderCirrusDocEndpoint(this.objectMapper, fieldsSchema, config.clock()),
                e ->
                        e.getChangeType() == ChangeType.PAGE_RERENDER
                                || e.getChangeType() == ChangeType.PAGE_RERENDER_UPSERT,
                AsyncDataStream.OutputMode.UNORDERED,
                "rerender-",
                EnumSet.of(ChangeType.PAGE_RERENDER));
    }

    private TransformOperator<SanityCheck, Either<List<UpdateEvent>, FetchFailure<SanityCheck>>>
            sanityCheckOperator() {
        return createSanityCheckOperator(
                new CirrusSanityCheckEndpoint(
                        config.saneitizerClusterGroup(),
                        config.clock(),
                        UUID::randomUUID,
                        config.saneitizerRerenderFrequency()),
                AsyncDataStream.OutputMode.UNORDERED,
                "saneitizer-",
                config.saneitizerCapacity());
    }

    /**
     * Key the stream via PageKey, attach the fetch operation and route fetch failures to a
     * side-output.
     */
    private DataStream<UpdateEvent> fetch(
            DataStream<UpdateEvent> stream, JsonRowDeserializationSchema fieldsSchema) {
        final OutputTag<Row> fetchFailureOutputTag = createFetchFailureOutputTag();
        final Sink<Row> fetchFailureSink = createFetchFailureSink();
        final ProcessFunction<Either<UpdateEvent, FetchFailure<UpdateEvent>>, UpdateEvent>
                fetchFailureRouter = createFetchFailureRouter(fetchFailureOutputTag);

        final OutputTag<UpdateEvent> rerenderTag =
                new OutputTag<>(ChangeType.PAGE_RERENDER.name(), super.getOrCreateUpdateTypeInfo());
        final Map<ChangeType, OutputTag<UpdateEvent>> changeTypeTags =
                Stream.of(ChangeType.PAGE_RERENDER, ChangeType.PAGE_RERENDER_UPSERT)
                        .reduce(
                                new EnumMap<>(ChangeType.class),
                                (map, changeType) -> {
                                    map.put(changeType, rerenderTag);
                                    return map;
                                },
                                (left, right) -> {
                                    throw new UnsupportedOperationException();
                                });

        final SingleOutputStreamOperator<UpdateEvent> changeTypeSplit =
                stream
                        .process(
                                new SplitByValue<>(changeTypeTags, UpdateEvent::getChangeType),
                                super.getOrCreateUpdateTypeInfo())
                        .uid("split-by-change_type")
                        .name("split-by-change_type");

        return attachFetchOperator(
                        changeTypeSplit,
                        revisionBasedFetchOperator(fieldsSchema),
                        fetchFailureRouter,
                        fetchFailureOutputTag,
                        fetchFailureSink)
                .union(
                        attachFetchOperator(
                                changeTypeSplit.getSideOutput(rerenderTag),
                                rerendersFetchOperator(fieldsSchema),
                                fetchFailureRouter,
                                fetchFailureOutputTag,
                                fetchFailureSink));
    }

    private SingleOutputStreamOperator<UpdateEvent> attachFetchOperator(
            DataStream<UpdateEvent> stream,
            TransformOperator<UpdateEvent, Either<UpdateEvent, FetchFailure<UpdateEvent>>> operator,
            ProcessFunction<Either<UpdateEvent, FetchFailure<UpdateEvent>>, UpdateEvent>
                    fetchFailureRouter,
            OutputTag<Row> fetchFailureOutputTag,
            Sink<Row> fetchFailureSink) {
        final SingleOutputStreamOperator<Either<UpdateEvent, FetchFailure<UpdateEvent>>>
                fetchedOrFailed =
                        stream
                                .keyBy(KEY_SELECTOR, KEY_TYPE_INFO)
                                .transform(
                                        operator.nameAndUuidPrefix + operator.operatorName,
                                        operator.outTypeInformation,
                                        operator.operatorFactory)
                                .uid(operator.nameAndUuidPrefix + "fetch")
                                .name(operator.nameAndUuidPrefix + "fetch");

        final SingleOutputStreamOperator<UpdateEvent> fetched =
                fetchedOrFailed
                        .process(fetchFailureRouter, super.getOrCreateUpdateTypeInfo())
                        .uid(operator.nameAndUuidPrefix + "route-fetch-failures")
                        .name(operator.nameAndUuidPrefix + "route-fetch-failures");

        fetched
                .getSideOutput(fetchFailureOutputTag)
                .sinkTo(fetchFailureSink)
                .uid(operator.nameAndUuidPrefix + "fetch-failure-sink")
                .name(operator.nameAndUuidPrefix + "fetch-failure-sink");

        return fetched;
    }

    private AsyncElasticsearchClientConfigBuilder getAsyncElasticsearchClientConfigBuilder(URI uri) {
        final AsyncElasticsearchClientConfigBuilder clientConfigBuilder =
                AsyncElasticsearchClientConfig.builder();

        clientConfigBuilder.maxConnections(ConsumerConfig.ES_MAX_PARALLEL_REQUESTS);

        final HttpHost mappedHost =
                getOrCreateHttpHostMapper()
                        .apply(org.apache.hc.core5.http.HttpHost.create(uri))
                        .map(
                                httpHost ->
                                        new HttpHost(
                                                httpHost.getHostName(), httpHost.getPort(), httpHost.getSchemeName()))
                        .orElseGet(() -> extractHttpHost(uri));

        clientConfigBuilder.hosts(List.of(extractHttpHost(uri)));
        clientConfigBuilder.hostMap(Map.of(uri.getHost(), mappedHost));

        final String path = uri.getPath();
        if (path != null && path.length() > 1) {
            clientConfigBuilder.connectionPathPrefix(path);
        }

        config
                .optional(ConsumerConfig.PARAM_ELASTICSEARCH_CONNECTION_REQUEST_TIMEOUT)
                .ifPresent(clientConfigBuilder::connectionRequestTimeout);
        config
                .optional(ConsumerConfig.PARAM_ELASTICSEARCH_CONNECTION_TIMEOUT)
                .ifPresent(clientConfigBuilder::connectionTimeout);
        config
                .optional(ConsumerConfig.PARAM_ELASTICSEARCH_SOCKET_TIMEOUT)
                .ifPresent(clientConfigBuilder::socketTimeout);

        config
                .optional(ConsumerConfig.PARAM_ELASTICSEARCH_BULK_COMPLETE_FAILURE_MAX_RETRIES)
                .ifPresent(clientConfigBuilder::onFailureMaxRetries);
        config
                .optional(ConsumerConfig.PARAM_ELASTICSEARCH_BULK_COMPLETE_FAILURE_RETRY_DELAY)
                .ifPresent(clientConfigBuilder::onFailureRetryDelay);

        return clientConfigBuilder;
    }

    private AsyncSinkWriterConfigurationBuilder getAsyncSinkWriterConfigurationBuilder() {
        final AsyncSinkWriterConfigurationBuilder writerConfigBuilder =
                new AsyncSinkWriterConfigurationBuilder();

        // Max. batch item size: adding an item larger than this will lead to a fatal failure
        writerConfigBuilder.setMaxRecordSizeInBytes(
                config.required(ConsumerConfig.PARAM_ELASTICSEARCH_BULK_FLUSH_MAX_ACTION_SIZE).getBytes()
                        + AsyncElasticsearchBulkSizeEstimator.REQUEST_OVERHEAD);

        // Max. overall batch size: a batch will grow until it reaches this size
        writerConfigBuilder.setMaxBatchSizeInBytes(
                config.required(ConsumerConfig.PARAM_ELASTICSEARCH_BULK_FLUSH_MAX_SIZE).getBytes());

        // Max. number of items per batch: a batch will grow until it reaches this number of items
        writerConfigBuilder.setMaxBatchSize(
                config.required(ConsumerConfig.PARAM_ELASTICSEARCH_BULK_FLUSH_MAX_ACTIONS));

        // Max. number of parallel requests (batches being shipped), 1 means sequential processing
        writerConfigBuilder.setMaxInFlightRequests(ConsumerConfig.ES_MAX_PARALLEL_REQUESTS);

        // Max. size of internal sink writer buffer (not to be confused with inter-operator buffering)
        // While the buffer is full the writer tries to eagerly flush until there's room again.
        // During that time the sink writer operator is busy and causing back-pressure,
        // checkpoints are not processed and time out.
        writerConfigBuilder.setMaxBufferedRequests(
                config.required(ConsumerConfig.PARAM_ELASTICSEARCH_MAX_BUFFERED_REQUESTS));

        // Max. time after which a flush is triggered, each batch item added creates a timer that will
        // trigger a flush
        writerConfigBuilder.setMaxTimeInBufferMS(
                config.required(ConsumerConfig.PARAM_ELASTICSEARCH_BULK_FLUSH_INTERVAL).toMillis());

        return writerConfigBuilder;
    }

    private HttpHost extractHttpHost(URI uri) {
        return new HttpHost(uri.getHost(), uri.getPort(), uri.getScheme());
    }

    @SuppressFBWarnings("IMC_IMMATURE_CLASS_BAD_SERIALVERSIONUID")
    private static final class NullSink<T> implements Sink<T> {

        private static final long serialVersionUID = -2594076302314473431L;

        @Override
        public SinkWriter<T> createWriter(InitContext context) {
            return new SinkWriter<T>() {
                @Override
                public void write(T element, Context context) {}

                @Override
                public void flush(boolean endOfInput) {}

                @Override
                public void close() {}
            };
        }
    }

    @SuppressFBWarnings("IMC_IMMATURE_CLASS_BAD_SERIALVERSIONUID")
    private static class SplitByValue<V> extends ProcessFunction<UpdateEvent, UpdateEvent> {

        private static final long serialVersionUID = 2985970496562942022L;

        private final Map<V, OutputTag<UpdateEvent>> valueTags;
        private final SerializableFunction<UpdateEvent, V> valueExtractor;

        SplitByValue(
                Map<V, OutputTag<UpdateEvent>> valueTags,
                SerializableFunction<UpdateEvent, V> valueExtractor) {
            this.valueTags = valueTags;
            this.valueExtractor = valueExtractor;
        }

        @Override
        public void processElement(
                UpdateEvent event,
                ProcessFunction<UpdateEvent, UpdateEvent>.Context ctx,
                Collector<UpdateEvent> out) {
            final OutputTag<UpdateEvent> tag = valueTags.get(valueExtractor.apply(event));
            if (tag == null) {
                out.collect(event);
            } else {
                ctx.output(tag, event);
            }
        }
    }
}
