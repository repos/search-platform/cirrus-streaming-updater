package org.wikimedia.discovery.cirrus.updater.consumer.sanity;

import static org.wikimedia.discovery.cirrus.updater.common.model.JsonPathUtils.getRequiredNode;

import java.net.URI;
import java.util.Optional;

import org.apache.hc.client5.http.classic.methods.HttpGet;
import org.wikimedia.discovery.cirrus.updater.common.http.MediaWikiHttpClient;

import com.fasterxml.jackson.databind.JsonNode;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class SiteInfoMaxPageIdLookup implements SanitySource.MaxPageIdLookup {
    public static final String API_PATH =
            "/w/api.php?action=query&meta=siteinfo&format=json&formatversion=2";
    private final MediaWikiHttpClient client;

    public SiteInfoMaxPageIdLookup(MediaWikiHttpClient client) {
        this.client = client;
    }

    public Optional<Long> apply(String domain) {
        try {
            return Optional.of(client.load(new HttpGet(buildUri(domain)), this::parse));
        } catch (IllegalStateException e) {
            log.error("Failed fetching max page id from {}", domain, e);
            return Optional.empty();
        }
    }

    private URI buildUri(String domain) {
        return URI.create("https://" + domain + API_PATH);
    }

    private long parse(JsonNode node) {
        return getRequiredNode(node, "/query/general/max-page-id").asLong();
    }
}
