package org.wikimedia.discovery.cirrus.updater.consumer.graph;

import java.time.Duration;
import java.time.Instant;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Consumer;

import org.apache.flink.api.common.operators.ProcessingTimeService;
import org.apache.flink.api.connector.sink2.Sink.InitContext;
import org.apache.flink.connector.base.sink.writer.AsyncSinkWriter;
import org.apache.flink.connector.base.sink.writer.BufferedRequestState;
import org.apache.flink.connector.base.sink.writer.ElementConverter;
import org.apache.flink.connector.base.sink.writer.config.AsyncSinkWriterConfiguration;
import org.apache.flink.metrics.Gauge;
import org.wikimedia.discovery.cirrus.updater.consumer.graph.AsyncElasticsearchBulkRequestBridge.AsyncElasticsearchBulkRequestBridgeFactory;
import org.wikimedia.discovery.cirrus.updater.consumer.graph.AsyncElasticsearchBulkRequestBridge.BatchFuture;

import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

/**
 * A writer implementation wrapping elasticsearch's bulk client.
 *
 * <h2>AsyncSinkWriter behavior</h2>
 *
 * <pre>
 * * AsyncSinkWriter#write
 *   * Does buffer has capacity?
 *     * YES: add to buffer and #nonBlockingFlush
 *     * NO: immediately #flush
 * * AsyncSinkWriter#nonBlockingFlush
 *   * While the buffer has more content than a single batch can hold and the rate limit allows it, #flush
 * * AsyncSinkWriter#flush
 *   * While the rate limit blocks, yield (wait for an in-flight request to complete which in calls #nonBlockingFlush)
 *   * Assemble batch from buffer (and send via ES client)
 * </pre>
 *
 * <h3>Writer state (buffer) serialization</h3>
 *
 * The writer is eagerly flushing while its buffer is larger than the next batch size (= max. batch
 * since, since the rate limiting does not reduce it). During that time the operator is busy (it
 * does not accept further writes and therefore causes back-pressure) and does not process
 * checkpoints. Hence, there are two scenarios in which the buffered items are serialized:
 *
 * <ul>
 *   <li>As part of a savepoint
 *   <li>While the operator idles with less items in the buffer than the next batch size, waiting
 *       for the flush timeout
 * </ul>
 */
@Slf4j
public class AsyncElasticsearchWriter<I> extends AsyncSinkWriter<I, AsyncElasticsearchBulkRequest> {

    private final ProcessingTimeService retryScheduler;
    private final AsyncElasticsearchBulkRequestBridge bridge;
    private final AtomicLong lastBatchMinEventTime = new AtomicLong(-1);
    private final AtomicLong lastBatchMaxEventTime = new AtomicLong(-1);
    private final AtomicLong lastBatchMedianEventTime = new AtomicLong(-1);
    private final ProcessingTimeService timeService;

    public AsyncElasticsearchWriter(
            ElementConverter<I, AsyncElasticsearchBulkRequest> elementConverter,
            InitContext context,
            AsyncSinkWriterConfiguration writerConfiguration,
            AsyncElasticsearchBulkRequestBridgeFactory bridgeAsyncElasticsearchBulkRequestBridgeFactory,
            Collection<BufferedRequestState<AsyncElasticsearchBulkRequest>> bufferedRequestStates) {
        super(elementConverter, context, writerConfiguration, bufferedRequestStates);
        this.retryScheduler = context.getProcessingTimeService();
        this.bridge = bridgeAsyncElasticsearchBulkRequestBridgeFactory.apply(context::metricGroup);
        this.timeService = context.getProcessingTimeService();
        context
                .metricGroup()
                .gauge(
                        "bufferedrequestentries",
                        () ->
                                snapshotState(-1).stream()
                                        .mapToInt(x -> x.getBufferedRequestEntries().size())
                                        .sum());
        context.metricGroup().gauge("lastBatchMinLag", reportLag(lastBatchMinEventTime));
        context.metricGroup().gauge("lastBatchMaxLag", reportLag(lastBatchMaxEventTime));
        context.metricGroup().gauge("lastBatchMedianLag", reportLag(lastBatchMedianEventTime));
    }

    @Override
    @SneakyThrows
    public void close() {
        bridge.close();
        super.close();
    }

    @Override
    protected void submitRequestEntries(
            List<AsyncElasticsearchBulkRequest> batch,
            Consumer<List<AsyncElasticsearchBulkRequest>> completeOrRetry) {
        bridge.submit(
                batch, new BulkResponseBatchFuture(batch, completeOrRetry, getFatalExceptionCons()));
    }

    @Override
    protected long getSizeInBytes(AsyncElasticsearchBulkRequest asyncElasticsearchBulkRequest) {
        return asyncElasticsearchBulkRequest.getEstimatedSizeInBytes();
    }

    private void collectEventTimes(List<AsyncElasticsearchBulkRequest> batch) {
        if (batch.isEmpty()) {
            return;
        }
        long[] eventTimes =
                batch.stream()
                        .map(AsyncElasticsearchBulkRequest::getEventTime)
                        .mapToLong(Instant::toEpochMilli)
                        .sorted()
                        .toArray();
        int mid = eventTimes.length / 2;
        if ((eventTimes.length & 1L) == 0) { // even
            lastBatchMedianEventTime.set((eventTimes[mid] + eventTimes[mid - 1]) / 2);
        } else { // odd
            lastBatchMedianEventTime.set(eventTimes[mid]);
        }
        lastBatchMaxEventTime.set(eventTimes[0]);
        lastBatchMinEventTime.set(eventTimes[eventTimes.length - 1]);
    }

    private Gauge<Long> reportLag(AtomicLong eventTime) {
        return () -> {
            long currentEventTime = eventTime.get();
            if (currentEventTime < 0) {
                return 0L;
            }
            long now = timeService.getCurrentProcessingTime();
            return now - currentEventTime;
        };
    }

    @AllArgsConstructor
    class BulkResponseBatchFuture implements BatchFuture {
        private final List<AsyncElasticsearchBulkRequest> pendingBatch;
        private final Consumer<List<AsyncElasticsearchBulkRequest>> resolve;
        private final Consumer<Exception> reject;

        @Override
        public void completeAndRetry(List<AsyncElasticsearchBulkRequest> batch, Duration delay) {
            if (delay.isZero() || batch.isEmpty()) {
                collectEventTimes(pendingBatch);
                resolve.accept(batch);
            } else {
                retryScheduler.registerTimer(
                        retryScheduler.getCurrentProcessingTime() + delay.toMillis(),
                        time -> resolve.accept(batch));
            }
        }

        @Override
        public void completeExceptionally(Exception exception) {
            reject.accept(exception);
        }
    }
}
