package org.wikimedia.discovery.cirrus.updater.consumer.sanity;

import java.util.Collection;
import java.util.Map;
import java.util.function.Function;

import org.apache.flink.api.connector.source.SourceReaderContext;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.connector.base.source.reader.SingleThreadMultiplexSourceReaderBase;
import org.wikimedia.discovery.cirrus.updater.common.model.SanityCheck;
import org.wikimedia.eventutilities.core.SerializableClock;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class SanitySourceReader
        extends SingleThreadMultiplexSourceReaderBase<
                SanitySourceRecord, SanityCheck, SanitySourceSplit, SanitySourceSplitState> {

    public SanitySourceReader(
            Function<Collection<LoopState>, Loop> loopFactory,
            SerializableClock clock,
            Configuration config,
            SourceReaderContext context) {
        super(
                () -> new SanitySourceSplitReader(loopFactory, clock),
                new SanitySourceRecordEmitter(),
                config,
                context);
    }

    @Override
    public void start() {
        super.start();
        log.info("Starting SanitySourceReader subtask {}", context.getIndexOfSubtask());
    }

    @Override
    protected void onSplitFinished(Map<String, SanitySourceSplitState> map) {
        // We need to tell the enumerator that everything is complete. We do that by asking
        // for more work. The enumerator will tell flink to shut us down, as no more work
        // is available.
        context.sendSplitRequest();
    }

    @Override
    protected SanitySourceSplitState initializedState(SanitySourceSplit split) {
        // Conversion of immutable checkpoint state into mutable runtime state
        return new SanitySourceSplitState(split);
    }

    @Override
    protected SanitySourceSplit toSplitType(String splitId, SanitySourceSplitState splitState) {
        // Conversion of runtime state into immutable checkpoint state for snapshotting
        return splitState.toSanitySourceSplit();
    }
}
