package org.wikimedia.discovery.cirrus.updater.consumer.sanity;

import static org.wikimedia.discovery.cirrus.updater.common.model.JsonPathUtils.getRequiredNode;

import java.net.URI;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import org.apache.hc.client5.http.classic.methods.HttpGet;
import org.wikimedia.discovery.cirrus.updater.common.http.MediaWikiHttpClient;
import org.wikimedia.discovery.cirrus.updater.common.util.function.SerializablePredicate;

import com.fasterxml.jackson.databind.JsonNode;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * Provides the set of domain names operated by a single wiki farm. When un-configured it queries
 * meta.wikimedia.org, reporting on the WMF farm.
 */
@AllArgsConstructor
@Slf4j
public class SiteMatrixDomainLookup implements SanitySource.WikiSupplier {
    public static final String DEFAULT_DOMAIN = "meta.wikimedia.org";
    public static final String API_PATH = "/w/api.php?action=sitematrix&format=json&formatversion=2";
    private final MediaWikiHttpClient client;
    private final String domain;
    private final Predicate<JsonNode> filter;

    public SiteMatrixDomainLookup(MediaWikiHttpClient client, Predicate<JsonNode> filter) {
        this(client, DEFAULT_DOMAIN, filter);
    }

    public static SerializablePredicate<JsonNode> disallowPrivateWikis() {
        return jsonNode -> !jsonNode.path("private").asBoolean(false);
    }

    public static String wikiExtractor(JsonNode node) {
        return getRequiredNode(node, "/dbname").asText();
    }

    @Override
    public Optional<Set<String>> get() {
        try {
            return Optional.of(client.load(new HttpGet(buildUri()), this::handleResponse));
        } catch (IllegalStateException e) {
            log.error("Failed fetching site matrix from {}", domain, e);
            return Optional.empty();
        }
    }

    private URI buildUri() {
        return URI.create("https://" + domain + API_PATH);
    }

    private Set<String> handleResponse(JsonNode root) {
        return Stream.concat(
                        // Most normal language wikis
                        StreamSupport.stream(root.get("sitematrix").spliterator(), false)
                                .filter(JsonNode::isObject)
                                .flatMap(node -> StreamSupport.stream(node.get("site").spliterator(), false)),
                        // Non-language wikis are off to the side
                        StreamSupport.stream(
                                getRequiredNode(root, "/sitematrix/specials").spliterator(), false))
                .filter(filter)
                .map(wiki -> URI.create(wiki.get("url").asText()).getAuthority())
                .collect(Collectors.toSet());
    }
}
