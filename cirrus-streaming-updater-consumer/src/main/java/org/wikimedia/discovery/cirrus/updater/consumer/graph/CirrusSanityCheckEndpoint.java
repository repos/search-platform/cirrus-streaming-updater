package org.wikimedia.discovery.cirrus.updater.consumer.graph;

import static org.wikimedia.discovery.cirrus.updater.common.model.JsonPathUtils.getRequiredNode;

import java.net.URI;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.function.Consumer;

import org.wikimedia.discovery.cirrus.updater.common.InvalidMWApiResponseException;
import org.wikimedia.discovery.cirrus.updater.common.graph.CirrusEndpoint;
import org.wikimedia.discovery.cirrus.updater.common.model.ChangeType;
import org.wikimedia.discovery.cirrus.updater.common.model.SanityCheck;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateEvent;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateEvent.TargetDocument;
import org.wikimedia.discovery.cirrus.updater.common.util.function.SerializableSupplier;
import org.wikimedia.eventutilities.core.SerializableClock;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@AllArgsConstructor
@Slf4j
public class CirrusSanityCheckEndpoint implements CirrusEndpoint<SanityCheck, List<UpdateEvent>> {
    /** There isn't a real stream for these events, but invent something plausible looking. */
    static final String PSEUDO_STREAM_NAME = "cirrussearch.update_pipeline.sanity_fix.pseudo_stream";

    public static final String API_PATH =
            "/w/api.php?action=cirrus-check-sanity&format=json&formatversion=2";

    /** The cirrus cluster to run the sanity check against. */
    private final String cluster;

    private final SerializableClock clock;
    /** Provides UUIDs for generated events. Typically UUID::randomUUID. */
    private final SerializableSupplier<UUID> uuidSupplier;

    private final int rerenderFrequency;

    @Override
    public URI buildURI(SanityCheck event) {
        return URI.create(
                "https://"
                        + event.getDomain()
                        + API_PATH
                        + "&cluster="
                        + cluster
                        + "&from="
                        + event.getStartPageId()
                        + "&limit="
                        + event.getBatchSize()
                        + "&sequenceid="
                        + event.getLoopId()
                        + "&rerenderfrequency="
                        + rerenderFrequency);
    }

    @Override
    public List<UpdateEvent> extractAndAugment(SanityCheck event, JsonNode response) {
        if (!response.has("problems")) {
            throw new InvalidMWApiResponseException(
                    "Unexpected response: /problems not provided:\n" + response.toPrettyString());
        }
        ArrayNode problems = (ArrayNode) getRequiredNode(response, "/problems");
        String wikiId = getRequiredNode(response, "/wikiId").asText();
        String clusterGroup = getRequiredNode(response, "/clusterGroup").asText();
        List<UpdateEvent> remediations = new ArrayList<>(problems.size());
        problems.forEach(
                problem ->
                        asUpdateEvent(clusterGroup, event.getDomain(), wikiId, problem, remediations::add));
        return remediations;
    }

    private void asUpdateEvent(
            String clusterGroup,
            String domain,
            String wikiId,
            JsonNode problem,
            Consumer<UpdateEvent> collector) {
        String errorType = getRequiredNode(problem, "/errorType").asText();
        String indexName = getRequiredNode(problem, "/indexName").asText();
        // Technically, since we provide the concrete index name, we don't need a namespaceId. But
        // it's part of the model and seemed better to fill it out.
        long namespaceId = getRequiredNode(problem, "/namespaceId").asLong();
        long pageId = getRequiredNode(problem, "/pageId").asLong();

        switch (errorType) {
            case "redirectInIndex":
                // Clear out the redirect
                collector.accept(delete(clusterGroup, domain, wikiId, indexName, namespaceId, pageId));
                // Rerender the target in case it was never written there
                String targetIndexName = getRequiredNode(problem, "/target/indexName").asText();
                long targetNamespaceId = getRequiredNode(problem, "/target/namespaceId").asLong();
                long targetPageId = getRequiredNode(problem, "/target/pageId").asLong();
                collector.accept(
                        rerender(
                                clusterGroup, domain, wikiId, targetIndexName, targetNamespaceId, targetPageId));
                break;

            case "pageNotInIndex":
                // force an update to this page
                collector.accept(
                        rerenderWithUpsert(clusterGroup, domain, wikiId, indexName, namespaceId, pageId));
                break;

            case "oldDocument":
            case "oldVersionInIndex":
                // Rerender the old page
                collector.accept(rerender(clusterGroup, domain, wikiId, indexName, namespaceId, pageId));
                break;

            case "ghostPageInIndex":
                // Delete from the index
                collector.accept(delete(clusterGroup, domain, wikiId, indexName, namespaceId, pageId));
                break;

            case "pageInWrongIndex":
                // Clear from the wrong index
                String wrongIndexName = getRequiredNode(problem, "/wrongIndexName").asText();
                collector.accept(delete(clusterGroup, domain, wikiId, wrongIndexName, namespaceId, pageId));
                // Force an update into the correct index
                collector.accept(
                        rerenderWithUpsert(clusterGroup, domain, wikiId, indexName, namespaceId, pageId));
                break;

            default:
                throw new UnsupportedOperationException("Unknown remediation: " + errorType);
        }
    }

    /**
     * Performs a simple rerender update, missing pages will not be populated. Generally safer than
     * upserting as it avoids racing with deletes.
     */
    private UpdateEvent rerender(
            String clusterGroup,
            String domain,
            String wikiId,
            String indexName,
            long namespaceId,
            long pageId) {
        // In theory A rerender could be viable with only the domain and pageId, letting the fetch phase
        // populate everything else. But currently we don't complete the target document there since
        // it normally happens in the producer side.
        return eventBuilder(
                        new TargetDocument(domain, wikiId, namespaceId, pageId)
                                .completeWith(clusterGroup, indexName))
                .changeType(ChangeType.PAGE_RERENDER)
                .build();
    }

    /** Performs a full upsert. Will populate missing pages. */
    private UpdateEvent rerenderWithUpsert(
            String clusterGroup,
            String domain,
            String wikiId,
            String indexName,
            long namespaceId,
            long pageId) {
        return eventBuilder(
                        new TargetDocument(domain, wikiId, namespaceId, pageId)
                                .completeWith(clusterGroup, indexName))
                .changeType(ChangeType.PAGE_RERENDER_UPSERT)
                .build();
    }

    private UpdateEvent delete(
            String clusterGroup,
            String domain,
            String wikiId,
            String indexName,
            long namespaceId,
            long pageId) {
        // Deletes bypass fetch and must be fully formed and ready to pass into the elasticsearch sink.
        return eventBuilder(
                        new TargetDocument(domain, wikiId, namespaceId, pageId)
                                .completeWith(clusterGroup, indexName))
                .changeType(ChangeType.PAGE_DELETE)
                .build();
    }

    private UpdateEvent.UpdateEventBuilder eventBuilder(TargetDocument target) {
        Instant eventTime = clock.get();
        String uuid = uuidSupplier.get().toString();
        return UpdateEvent.builder()
                .meta(
                        UpdateEvent.Meta.builder().id(uuid).requestId(uuid).dt(eventTime).stream(
                                        PSEUDO_STREAM_NAME)
                                .domain(target.getDomain())
                                .build())
                .ingestionTime(eventTime)
                .eventTime(eventTime)
                .targetDocument(target);
    }
}
