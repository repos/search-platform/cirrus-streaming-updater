package org.wikimedia.discovery.cirrus.updater.consumer.graph;

import java.io.Serializable;
import java.time.Duration;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpHost;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Builder.Default;
import lombok.Getter;

@Builder
@Getter
@AllArgsConstructor
public class AsyncElasticsearchClientConfig implements Serializable {

    private final List<HttpHost> hosts;

    private final Duration connectionTimeout;

    private final Duration connectionRequestTimeout;

    private final Duration socketTimeout;

    private final String connectionPathPrefix;

    @Default private final int maxConnections = -1;

    @Default private final int onFailureMaxRetries = -1;

    @Default private final Duration onFailureRetryDelay = Duration.ZERO;

    private final Map<String, HttpHost> hostMap;
}
