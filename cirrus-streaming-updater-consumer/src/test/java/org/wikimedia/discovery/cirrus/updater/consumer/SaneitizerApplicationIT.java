package org.wikimedia.discovery.cirrus.updater.consumer;

import static com.github.tomakehurst.wiremock.client.WireMock.equalTo;
import static org.assertj.core.api.Assertions.assertThat;
import static org.wikimedia.discovery.cirrus.updater.common.wiremock.WireMockExtensionUtilities.mockJsonResponse;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.UUID;
import java.util.stream.Stream;

import org.apache.hc.core5.http.HttpHeaders;
import org.junit.jupiter.api.Test;
import org.wikimedia.discovery.cirrus.updater.common.config.CommonConfig;
import org.wikimedia.discovery.cirrus.updater.consumer.config.ConsumerConfig;
import org.wikimedia.discovery.cirrus.updater.consumer.graph.CirrusSanityCheckEndpoint;
import org.wikimedia.discovery.cirrus.updater.consumer.sanity.SiteInfoMaxPageIdLookup;
import org.wikimedia.discovery.cirrus.updater.consumer.sanity.SiteMatrixDomainLookup;

import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.junit5.WireMockRuntimeInfo;
import com.github.tomakehurst.wiremock.stubbing.StubMapping;

class SaneitizerApplicationIT extends AbstractApplicationIT {
    private static final String TEST_INDEX = "saneitizer_it";
    private static final String CLUSTER_GROUP = "alpha";
    private static final String CIRRUS_CLUSTER = "dc1";

    @Test
    void test(WireMockRuntimeInfo runtimeInfo) throws Exception {
        final Instant now = Instant.now().truncatedTo(ChronoUnit.SECONDS);

        createStandardTopics();
        createSearchIndex(TEST_INDEX);

        // Responses from elasticsearch, auto-generated (but manually moved from target/ to
        // src/test/resources)
        final StubMapping elasticsearchBulkMapping =
                wireMockExtension.getSingleStubMapping(
                        UUID.fromString("f76eb326-8d02-47f4-afba-84bfc81a7734"));

        assertThat(elasticsearchBulkMapping.getName()).isEqualTo("elasticsearch__bulk_saneitizer");
        final String mediawikiAuthToken = "auth token example value";

        // Contains only test.wikipedia.org
        wireMockExtension.stubFor(
                WireMock.get(WireMock.urlEqualTo(SiteMatrixDomainLookup.API_PATH))
                        .withHeader(HttpHeaders.AUTHORIZATION, equalTo("NetworkSession " + mediawikiAuthToken))
                        .willReturn(mockJsonResponse("onewiki-sitematrix.json")));

        // Responds with max page id of 1. batchSize > max page id means only one sanity check will
        // fire.
        wireMockExtension.stubFor(
                WireMock.get(WireMock.urlEqualTo(SiteInfoMaxPageIdLookup.API_PATH))
                        .withHeader(HttpHeaders.AUTHORIZATION, equalTo("NetworkSession " + mediawikiAuthToken))
                        .willReturn(mockJsonResponse("cirrustestwiki-siteinfo.json")));

        // Responds that page id 1 is missing in the index
        wireMockExtension.stubFor(
                WireMock.get(
                                WireMock.urlEqualTo(
                                        CirrusSanityCheckEndpoint.API_PATH
                                                + "&cluster=dc1&from=0&limit=100&sequenceid=0&rerenderfrequency=16"))
                        .withHeader(HttpHeaders.AUTHORIZATION, equalTo("NetworkSession " + mediawikiAuthToken))
                        .willReturn(mockJsonResponse("sanity-check-1.json")));

        // Missing page turns into a fetch for the doc
        wireMockExtension.stubFor(
                WireMock.get(WireMock.urlMatching("/w/api.php.*pageids=1.*"))
                        .withHeader(HttpHeaders.AUTHORIZATION, equalTo("NetworkSession " + mediawikiAuthToken))
                        .willReturn(mockJsonResponse("revision.12345.json")));

        ConsumerApplication.main(
                Stream.concat(
                                baseApplicationArgs(),
                                Stream.of(
                                        "--topic-prefix-filter",
                                        "eqiad",
                                        // Kafka is empty, we are only testing saneitizer. It should end immediately
                                        param(ConsumerConfig.PARAM_KAFKA_SOURCE_START_TIME),
                                        "2024-01-01T00:00:00Z",
                                        param(ConsumerConfig.PARAM_KAFKA_SOURCE_END_TIME),
                                        "2024-01-01T00:00:01Z",
                                        param(ConsumerConfig.PARAM_ELASTICSEARCH_URLS),
                                        CLUSTER_GROUP + ":\"" + getElasticsearchUrl() + '"',
                                        param(CommonConfig.PARAM_HTTP_ROUTES) + ".00",
                                        "test\\.wikipedia\\.org=" + runtimeInfo.getHttpBaseUrl(),
                                        param(CommonConfig.PARAM_HTTP_ROUTES) + ".01",
                                        "meta\\.wikimedia\\.org=" + runtimeInfo.getHttpBaseUrl(),
                                        param(ConsumerConfig.PARAM_SANEITIZE),
                                        "true",
                                        param(ConsumerConfig.PARAM_SANEITIZER_CLUSTER_GROUP),
                                        CIRRUS_CLUSTER,
                                        param(ConsumerConfig.PARAM_SANEITIZER_BOUNDED_RUNTIME),
                                        "5 seconds",
                                        param(ConsumerConfig.PARAM_MEDIAWIKI_AUTH_TOKEN),
                                        mediawikiAuthToken))
                        .toArray(String[]::new));

        assertThat(collectFailedRecords()).isEmpty();
        verifyElasticsearch();
    }
}
