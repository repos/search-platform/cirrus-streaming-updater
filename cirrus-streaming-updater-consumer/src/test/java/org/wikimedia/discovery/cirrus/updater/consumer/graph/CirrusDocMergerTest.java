package org.wikimedia.discovery.cirrus.updater.consumer.graph;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

import java.io.IOException;
import java.time.Instant;
import java.util.List;

import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.types.Row;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.testcontainers.shaded.com.google.common.collect.ImmutableList;
import org.wikimedia.discovery.cirrus.updater.common.PageIdMismatchException;
import org.wikimedia.discovery.cirrus.updater.common.model.CirrusDocJsonHelper;
import org.wikimedia.discovery.cirrus.updater.common.model.EventDataStreamUtilities;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateEvent;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateEvent.Meta;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateEvent.TargetDocument;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateEvent.Update;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateFields;
import org.wikimedia.eventutilities.flink.EventRowTypeInfo;
import org.wikimedia.eventutilities.flink.formats.json.JsonRowDeserializationSchema;
import org.wikimedia.eventutilities.flink.formats.json.JsonRowDeserializationSchema.Builder;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@ExtendWith(MockitoExtension.class)
class CirrusDocMergerTest {

    static final ObjectMapper MAPPER = new ObjectMapper();
    static final String TEST_RESOURCE_PATH =
            "/" + CirrusDocMergerTest.class.getCanonicalName().replace(".", "/");
    static final EventRowTypeInfo UPDATE_TYPE_INFO = EventDataStreamUtilities.buildUpdateTypeInfo();
    static final TypeInformation<Row> FIELDS_TYPE_INFO =
            UPDATE_TYPE_INFO.getTypeAt(UpdateFields.FIELDS);
    static final JsonRowDeserializationSchema FIELDS_SCHEMA = new Builder(FIELDS_TYPE_INFO).build();
    static final Instant NOW = Instant.now();

    @Test
    void failsIfPageIdDoesNotMatch() throws IOException {
        final UpdateEvent event =
                UpdateEvent.builder()
                        .meta(Meta.builder().dt(NOW).requestId("test").stream("test").build())
                        .targetDocument(TargetDocument.builder().pageId(0L).build())
                        .build();

        JsonNode pageNode = load("revision.551141.json");
        final CirrusDocJsonHelper helper = createHelper(pageNode);

        assertThatExceptionOfType(PageIdMismatchException.class)
                .isThrownBy(() -> CirrusDocMerger.merge(event, helper));
    }

    @Test
    void failsIfRevisionIdDoesNotMatch() throws IOException {
        final UpdateEvent event =
                UpdateEvent.builder()
                        .meta(Meta.builder().dt(NOW).requestId("test").stream("test").build())
                        .targetDocument(TargetDocument.builder().pageId(147498L).build())
                        .revId(0L)
                        .build();

        JsonNode pageNode = load("revision.551141.json");
        final CirrusDocJsonHelper helper = createHelper(pageNode);

        assertThatExceptionOfType(IllegalArgumentException.class)
                .isThrownBy(() -> CirrusDocMerger.merge(event, helper));
    }

    @Test
    void mergeIntoEmptyUpdate() throws IOException {
        final UpdateEvent event =
                UpdateEvent.builder()
                        .meta(Meta.builder().dt(NOW).requestId("test").stream("test").build())
                        .targetDocument(TargetDocument.builder().pageId(147498L).build())
                        .build();

        JsonNode pageNode = load("revision.551141.json");
        final CirrusDocJsonHelper helper = createHelper(pageNode);

        final UpdateEvent merged = CirrusDocMerger.merge(event, helper);

        assertThat(merged.getRevId()).isEqualTo(551141L);
        assertThat(merged.getUpdate()).isNotNull();
    }

    @Test
    void mergeIntoUpdateWithWeightedTags() throws IOException {
        final Row fieldsWithWeightedTags = createEmptyFields();
        final ImmutableList<String> weightedTags = ImmutableList.of("key/value");
        fieldsWithWeightedTags.setField(UpdateFields.WEIGHTED_TAGS, weightedTags);
        final Update updateWithWeightedTags = Update.forWeightedTags(weightedTags);
        updateWithWeightedTags.setRawFields(fieldsWithWeightedTags);

        final UpdateEvent event =
                UpdateEvent.builder()
                        .meta(Meta.builder().dt(NOW).requestId("test").stream("test").build())
                        .targetDocument(TargetDocument.builder().pageId(147498L).build())
                        .update(updateWithWeightedTags)
                        .build();

        JsonNode pageNode = load("revision.551141.json");
        final CirrusDocJsonHelper helper = createHelper(pageNode);

        final UpdateEvent merged = CirrusDocMerger.merge(event, helper);

        assertThat(merged.getRevId()).isEqualTo(551141L);
        assertThat(
                        merged.getUpdate().getRawFields().<List<String>>getFieldAs(UpdateFields.WEIGHTED_TAGS))
                .containsExactly("key/value");
        assertThat(merged.getUpdate().getRawFieldsFetchedAt()).isEqualTo(NOW);
    }

    static Row createEmptyFields() {
        return UPDATE_TYPE_INFO.createEmptySubRow(UpdateFields.FIELDS);
    }

    @NotNull
    static CirrusDocJsonHelper createHelper(JsonNode pageNode) {
        return new CirrusDocJsonHelper(FIELDS_SCHEMA, CirrusDocMergerTest.MAPPER, pageNode, NOW);
    }

    static JsonNode load(String resourceName) throws IOException {
        return MAPPER
                .reader()
                .readTree(
                        CirrusDocMergerTest.class.getResourceAsStream(TEST_RESOURCE_PATH + "/" + resourceName));
    }
}
