package org.wikimedia.discovery.cirrus.updater.consumer.sanity;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.Instant;

import org.junit.jupiter.api.Test;
import org.wikimedia.discovery.cirrus.updater.common.model.SanityCheck;

class SanitySourceRecordTest {
    final String domain = "foo";
    final long maxPageId = 10;
    final int batchSize = 100;

    @Test
    void testRemoveFromStateEquality() {
        assertThat(SanitySourceRecord.removeFromState(domain))
                .isEqualTo(SanitySourceRecord.removeFromState(domain));
    }

    @Test
    void testForNextStateEquality() {
        final Instant now = Instant.parse("2024-01-01T00:00:00Z");
        final LoopState initialState = new LoopState(domain, maxPageId, now);
        final SanityCheck sanityCheck = new SanityCheck(domain, 0, batchSize, now, 0);

        assertThat(SanitySourceRecord.forNextState(initialState, sanityCheck))
                .isEqualTo(SanitySourceRecord.forNextState(initialState, sanityCheck));
    }

    @Test
    void testRemoveWikiFromState() {
        final Instant now = Instant.parse("2024-01-01T00:00:00Z");
        final LoopState initialState = new LoopState(domain, maxPageId, now);
        SanitySourceSplitState splitState = new SanitySourceSplitState(new SanitySourceSplit("0"));
        splitState.put(initialState);

        assertThat(splitState.state).containsKey(domain);
        SanitySourceRecord.removeFromState(domain).applyStateUpdate(splitState);
        assertThat(splitState.state).doesNotContainKey(domain);
    }

    @Test
    void updateWikiState() {
        final Instant now = Instant.parse("2024-01-01T00:00:00Z");
        final LoopState initialState = new LoopState(domain, maxPageId, now);
        SanitySourceSplitState splitState = new SanitySourceSplitState(new SanitySourceSplit("0"));
        splitState.put(initialState);

        assertThat(splitState.state).containsEntry(domain, initialState);
        final LoopState nextState = initialState.emitNextBatch(now, batchSize);
        SanitySourceRecord.forNextState(nextState, null).applyStateUpdate(splitState);
        assertThat(splitState.state).containsEntry(domain, nextState);
    }
}
