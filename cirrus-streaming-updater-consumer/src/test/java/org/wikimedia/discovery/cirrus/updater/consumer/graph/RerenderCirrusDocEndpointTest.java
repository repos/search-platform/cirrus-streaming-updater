package org.wikimedia.discovery.cirrus.updater.consumer.graph;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.io.IOException;
import java.net.URI;
import java.time.Instant;

import org.apache.flink.types.Row;
import org.junit.jupiter.api.Test;
import org.wikimedia.discovery.cirrus.updater.common.InvalidMWApiResponseException;
import org.wikimedia.discovery.cirrus.updater.common.model.ChangeType;
import org.wikimedia.discovery.cirrus.updater.common.model.EventDataStreamUtilities;
import org.wikimedia.discovery.cirrus.updater.common.model.MetaRowTranscoder;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateEvent;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateFields;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateRowTranscoder;
import org.wikimedia.eventutilities.core.SerializableClock;
import org.wikimedia.eventutilities.flink.EventRowTypeInfo;
import org.wikimedia.eventutilities.flink.formats.json.JsonRowDeserializationSchema;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableMap;

class RerenderCirrusDocEndpointTest {

    static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
    static final EventRowTypeInfo UPDATE_EVENT_ROW_TYPE_INFO =
            EventDataStreamUtilities.buildUpdateTypeInfo();
    static final Instant NOW = Instant.now();
    static final SerializableClock CLOCK = SerializableClock.frozenClock(NOW);

    final UpdateRowTranscoder decoder = UpdateRowTranscoder.decoder();
    RerenderCirrusDocEndpoint endpoint =
            new RerenderCirrusDocEndpoint(
                    OBJECT_MAPPER,
                    new JsonRowDeserializationSchema.Builder(
                                    UPDATE_EVENT_ROW_TYPE_INFO.getTypeAt(UpdateFields.FIELDS))
                            .build(),
                    CLOCK);

    @Test
    void buildURI() {
        UpdateEvent updateEvent = decoder.decode(createUpdateEvent());
        assertThat(endpoint.buildURI(updateEvent))
                .isEqualTo(
                        URI.create(
                                "https://local.wikimedia/w/api.php?action=query&format=json&"
                                        + "cbbuilders=content%7Clinks&prop=cirrusbuilddoc&formatversion=2&format=json&pageids=151989"));
    }

    @Test
    void testBuildURIRequiredFields() {
        final Row row = createUpdateEvent();

        assertThatThrownBy(
                        () -> {
                            UpdateEvent updateEvent = decoder.decode(row);
                            updateEvent.getTargetDocument().setPageId(null);
                            endpoint.buildURI(updateEvent);
                        })
                .isInstanceOf(IllegalArgumentException.class);

        assertThatThrownBy(
                        () -> {
                            UpdateEvent updateEvent = decoder.decode(row);
                            updateEvent.getTargetDocument().setDomain(null);
                            endpoint.buildURI(updateEvent);
                        })
                .isInstanceOf(IllegalArgumentException.class);

        assertThatThrownBy(
                        () -> {
                            UpdateEvent updateEvent = decoder.decode(row);
                            updateEvent.setTargetDocument(null);
                            endpoint.buildURI(updateEvent);
                        })
                .isInstanceOf(NullPointerException.class);
        assertThatThrownBy(
                        () -> {
                            UpdateEvent updateEvent = decoder.decode(row);
                            updateEvent.setChangeType(ChangeType.REV_BASED_UPDATE);
                            endpoint.buildURI(updateEvent);
                        })
                .isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    void extractAndAugment() throws IOException {
        JsonNode apiResponse = cirrusDocOutput("api-output");
        UpdateEvent updateEvent = decoder.decode(createUpdateEvent());
        UpdateEvent enrichedEvent = endpoint.extractAndAugment(updateEvent, apiResponse);
        assertThat(enrichedEvent).isNotSameAs(updateEvent);
        assertThat(enrichedEvent.getTargetDocument().getWikiId()).isEqualTo("testwiki");
        assertThat(enrichedEvent.getTargetDocument().getPageId()).isEqualTo(151989L);
        assertThat(enrichedEvent.getTargetDocument().getPageNamespace()).isEqualTo(2L);
        assertThat(enrichedEvent.getRevId()).isEqualTo(576347L);
        assertThat(enrichedEvent.getTargetDocument().getClusterGroup()).isEqualTo("omega");
        assertThat(enrichedEvent.getTargetDocument().getIndexName()).isEqualTo("testwiki_general");
        assertThat(enrichedEvent.getUpdate().getNoopHints())
                .containsExactlyEntriesOf(
                        ImmutableMap.of("random_noop", "handler", "version", "documentVersion"));
        assertThat(enrichedEvent.getEventTime()).isEqualTo(updateEvent.getEventTime());
        Row fields = enrichedEvent.getUpdate().getRawFields();
        // check few fields
        assertThat(fields.getField("text")).isEqualTo("A small test on my test page.");
        assertThat(fields.<Row[]>getFieldAs("coordinates")).hasSize(1);
        assertThat(enrichedEvent.getUpdate().getRawFieldsFetchedAt()).isEqualTo(NOW);
    }

    @Test
    void testExtractAndAugmentAssertions() throws IOException {
        JsonNode apiResponse = cirrusDocOutput("api-output");
        final UpdateEvent invalidPageIdEvent = decoder.decode(createUpdateEvent());
        invalidPageIdEvent.getTargetDocument().setPageId(123L);
        assertThatThrownBy(() -> endpoint.extractAndAugment(invalidPageIdEvent, apiResponse))
                .isInstanceOf(IllegalArgumentException.class);

        final UpdateEvent invalidOp = decoder.decode(createUpdateEvent());
        invalidOp.setChangeType(ChangeType.REV_BASED_UPDATE);
        assertThatThrownBy(() -> endpoint.extractAndAugment(invalidOp, apiResponse))
                .isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    void testMissingPage() throws IOException {
        JsonNode apiReJsonNode = cirrusDocOutput("api-missing-page-output");
        UpdateEvent updateEvent = decoder.decode(createUpdateEvent());

        updateEvent.getTargetDocument().setPageId(9999999L);
        assertThatThrownBy(() -> endpoint.extractAndAugment(updateEvent, apiReJsonNode))
                .isInstanceOf(PageNotFoundException.class);
    }

    @Test
    void testPartialPageResponse() throws IOException {
        // We shouldn't get these, but they happen. No missing marker, but also no
        // content.
        JsonNode apiReJsonNode = cirrusDocOutput("api-partial-page-output");
        UpdateEvent updateEvent = decoder.decode(createUpdateEvent());

        updateEvent.getTargetDocument().setPageId(9999999L);
        assertThatThrownBy(() -> endpoint.extractAndAugment(updateEvent, apiReJsonNode))
                .isInstanceOf(InvalidMWApiResponseException.class);
    }

    @Test
    void testRevisionHiddenResponse() throws IOException {
        JsonNode apiReJsonNode = cirrusDocOutput("api-revision-hidden-output");
        UpdateEvent updateEvent = decoder.decode(createUpdateEvent());

        updateEvent.getTargetDocument().setPageId(9999999L);
        assertThatThrownBy(() -> endpoint.extractAndAugment(updateEvent, apiReJsonNode))
                .isInstanceOf(InvalidMWApiResponseException.class);
    }

    @Test
    void testMWExceptionResponse() throws IOException {
        JsonNode apiReJsonNode = cirrusDocOutput("builddoc-exception");
        UpdateEvent updateEvent = decoder.decode(createUpdateEvent());
        updateEvent.getTargetDocument().setPageId(9999999L);
        assertThatThrownBy(() -> endpoint.extractAndAugment(updateEvent, apiReJsonNode))
                .isInstanceOf(InvalidMWApiResponseException.class)
                .hasMessageContaining("BuildDocumentException");
    }

    private Row createUpdateEvent() {
        Row e = UPDATE_EVENT_ROW_TYPE_INFO.createEmptyRow();
        e.setField(UpdateFields.CHANGE_TYPE, ChangeType.PAGE_RERENDER.name());
        e.setField(UpdateFields.PAGE_ID, 151989L);
        e.setField(UpdateFields.WIKI_ID, "testwiki");
        e.setField(UpdateFields.INDEX_NAME, "original_index_name");
        e.setField(UpdateFields.CLUSTER_GROUP, "original_cluster_group");
        e.setField(UpdateFields.NOOP_HINTS, ImmutableMap.of("random_noop", "handler"));
        Row meta = UPDATE_EVENT_ROW_TYPE_INFO.createEmptySubRow(MetaRowTranscoder.DEFAULT_META_FIELD);
        meta.setField(MetaRowTranscoder.META_DOMAIN, "local.wikimedia");
        e.setField(MetaRowTranscoder.DEFAULT_META_FIELD, meta);
        return e;
    }

    private JsonNode cirrusDocOutput(String resourceSuffix) throws IOException {
        return OBJECT_MAPPER
                .reader()
                .readTree(
                        this.getClass()
                                .getResourceAsStream(
                                        "/"
                                                + this.getClass().getSimpleName()
                                                + ".cirrusdoc."
                                                + resourceSuffix
                                                + ".json"));
    }
}
