package org.wikimedia.discovery.cirrus.updater.consumer.graph;

import static org.assertj.core.api.Assertions.assertThatNoException;

import java.util.stream.Stream;

import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.configuration.ConfigOption;
import org.apache.flink.configuration.PipelineOptions;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.wikimedia.discovery.cirrus.updater.consumer.config.ConsumerConfig;

class ConsumerGraphFactoryTest {

    public static Stream<Arguments> consumerParams() {
        ParameterTool baseParams =
                ParameterTool.fromArgs(
                        new String[] {
                            param(ConsumerConfig.PARAM_EVENT_STREAM_CONFIG_URL),
                            ConsumerGraphFactoryTest.class.getResource("/event-stream-config.json").toString(),
                            param(ConsumerConfig.PARAM_EVENT_STREAM_JSON_SCHEMA_URLS),
                            ConsumerGraphFactoryTest.class.getResource("/schema_repo").toString(),
                            param(ConsumerConfig.PARAM_KAFKA_SOURCE_CONFIG) + ".bootstrap.servers",
                            "localhost:9292",
                            param(ConsumerConfig.PARAM_KAFKA_SOURCE_CONFIG) + ".group.id",
                            "test",
                            param(ConsumerConfig.PARAM_PUBLIC_UPDATE_STREAM),
                            "cirrussearch.update_pipeline.update.rc0",
                            param(ConsumerConfig.PARAM_PRIVATE_UPDATE_STREAM),
                            "cirrussearch.update_pipeline.update.private.rc0",
                            param(ConsumerConfig.PARAM_ELASTICSEARCH_URLS),
                            "omega:\"http://localhost:9200\"",
                            param(ConsumerConfig.PARAM_FETCH_ERROR_TOPIC),
                            "eqiad.cirrussearch.update_pipeline.fetch_error.v1",
                            param(PipelineOptions.NAME),
                            "eqiad.test.consumer",
                        });

        return Stream.of(
                Arguments.of(baseParams),
                Arguments.of(
                        baseParams.mergeWith(
                                ParameterTool.fromArgs(
                                        new String[] {
                                            param(ConsumerConfig.PARAM_SANEITIZE), "true",
                                            param(ConsumerConfig.PARAM_SANEITIZER_CLUSTER_GROUP), "dc1"
                                        }))));
    }

    @ParameterizedTest
    @MethodSource("consumerParams")
    void createStreamGraph(ParameterTool params) {
        assertThatNoException()
                .isThrownBy(
                        () -> {
                            final StreamExecutionEnvironment env =
                                    StreamExecutionEnvironment.getExecutionEnvironment();
                            env.getConfig().setGlobalJobParameters(params);

                            ConsumerConfig config = ConsumerConfig.of(params.getConfiguration());
                            try (ConsumerGraphFactory factory = new ConsumerGraphFactory(config)) {
                                factory.prepareEnvironment();
                            }
                        });
    }

    @NotNull
    private static String param(ConfigOption<?> name) {
        return "--" + name.key();
    }
}
