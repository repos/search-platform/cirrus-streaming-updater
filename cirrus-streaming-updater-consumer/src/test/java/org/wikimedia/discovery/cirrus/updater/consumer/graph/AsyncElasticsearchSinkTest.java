package org.wikimedia.discovery.cirrus.updater.consumer.graph;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.time.Instant;
import java.util.List;

import org.apache.flink.api.connector.sink2.Sink.InitContext;
import org.apache.flink.api.connector.sink2.SinkWriter.Context;
import org.apache.flink.api.connector.sink2.StatefulSink.StatefulSinkWriter;
import org.apache.flink.connector.base.sink.writer.BufferedRequestState;
import org.apache.flink.connector.base.sink.writer.ElementConverter;
import org.apache.flink.connector.base.sink.writer.config.AsyncSinkWriterConfiguration;
import org.apache.flink.connector.base.sink.writer.config.AsyncSinkWriterConfiguration.AsyncSinkWriterConfigurationBuilder;
import org.apache.flink.core.io.SimpleVersionedSerializer;
import org.elasticsearch.action.bulk.BulkRequest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Answers;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.wikimedia.discovery.cirrus.updater.consumer.graph.AsyncElasticsearchBulkRequestBridge.AsyncElasticsearchBulkRequestBridgeFactory;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

@ExtendWith(MockitoExtension.class)
class AsyncElasticsearchSinkTest {

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    InitContext context;

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    AsyncElasticsearchBulkRequestBridgeFactory asyncElasticsearchBulkRequestBridgeFactory;

    @Test
    void createWriter() throws IOException {
        final AsyncElasticsearchSink<Integer> sink = buildSink();

        final StatefulSinkWriter<Integer, BufferedRequestState<AsyncElasticsearchBulkRequest>> writer =
                sink.createWriter(context);

        assertThat(writer).isInstanceOf(AsyncElasticsearchWriter.class);
    }

    @Test
    void serde() throws Exception {
        final AsyncElasticsearchSink<Integer> sink = buildSink();

        final SimpleVersionedSerializer<BufferedRequestState<AsyncElasticsearchBulkRequest>> serializer;
        try (StatefulSinkWriter<Integer, BufferedRequestState<AsyncElasticsearchBulkRequest>> writer =
                sink.createWriter(context)) {

            serializer = sink.getWriterStateSerializer();

            final List<BufferedRequestState<AsyncElasticsearchBulkRequest>> bufferedRequestStates =
                    writer.snapshotState(0);
            for (BufferedRequestState<AsyncElasticsearchBulkRequest> bufferedRequestState :
                    bufferedRequestStates) {
                serializer.serialize(bufferedRequestState);
            }

            final StatefulSinkWriter<Integer, BufferedRequestState<AsyncElasticsearchBulkRequest>>
                    restoredWriter = sink.restoreWriter(context, bufferedRequestStates);

            assertThat(restoredWriter).isInstanceOf(AsyncElasticsearchWriter.class);
        }
    }

    AsyncElasticsearchSink<Integer> buildSink() {
        return AsyncElasticsearchSink.<Integer>builder()
                .elementConverter(new IntegerConverter())
                .writerConfig(buildWriterConfig())
                .bridgeFactory(asyncElasticsearchBulkRequestBridgeFactory)
                .build();
    }

    AsyncSinkWriterConfiguration buildWriterConfig() {
        final AsyncSinkWriterConfigurationBuilder builder = new AsyncSinkWriterConfigurationBuilder();
        builder.setMaxBufferedRequests(10);
        builder.setMaxBatchSize(5);
        builder.setMaxInFlightRequests(1);
        builder.setMaxBatchSizeInBytes(1024);
        builder.setMaxRecordSizeInBytes(1024);
        builder.setMaxTimeInBufferMS(10000);
        return builder.build();
    }

    @SuppressWarnings("serial")
    @SuppressFBWarnings(value = "SE_NO_SERIALVERSIONID")
    static class IntegerConverter
            implements ElementConverter<Integer, AsyncElasticsearchBulkRequest> {

        @Override
        public AsyncElasticsearchBulkRequest apply(Integer integer, Context context) {
            return new AsyncElasticsearchBulkRequest(
                    Integer.toString(integer), 0, new BulkRequest(), Instant.EPOCH);
        }

        @Override
        public void open(InitContext context) {
            ElementConverter.super.open(context);
        }
    }
}
