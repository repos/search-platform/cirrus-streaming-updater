package org.wikimedia.discovery.cirrus.updater.consumer;

import static org.assertj.core.api.Assertions.assertThat;
import static org.wikimedia.discovery.cirrus.updater.common.wiremock.WireMockExtensionUtilities.getWireMockExtension;

import java.io.File;
import java.io.IOException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import javax.annotation.Nonnull;

import org.apache.commons.io.FileUtils;
import org.apache.flink.configuration.ConfigOption;
import org.apache.flink.test.junit5.MiniClusterExtension;
import org.apache.http.HttpHost;
import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.NewTopic;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.serialization.LongSerializer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.CreateIndexRequest;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testcontainers.containers.output.OutputFrame;
import org.testcontainers.elasticsearch.ElasticsearchContainer;
import org.testcontainers.utility.DockerImageName;
import org.wikimedia.discovery.cirrus.updater.common.model.EventDataStreamUtilities;
import org.wikimedia.discovery.cirrus.updater.common.wiremock.UnorderedLines;
import org.wikimedia.discovery.cirrus.updater.consumer.config.ConsumerConfig;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.junit5.WireMockExtension;
import com.github.tomakehurst.wiremock.recording.RecordSpec;
import com.github.tomakehurst.wiremock.recording.SnapshotRecordResult;
import com.google.common.collect.ImmutableMap;
import com.salesforce.kafka.test.junit5.SharedKafkaTestResource;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@ExtendWith({MiniClusterExtension.class})
@SuppressWarnings({"checkstyle:classfanoutcomplexity"})
public abstract class AbstractApplicationIT {
    protected static final ScheduledThreadPoolExecutor EXECUTOR = new ScheduledThreadPoolExecutor(5);

    @RegisterExtension
    public static final SharedKafkaTestResource SHARED_KAFKA_TEST_RESOURCE =
            new SharedKafkaTestResource();

    @RegisterExtension static final WireMockExtension wireMockExtension = getWireMockExtension();
    private static final String ELASTICSEARCH_PREFIX = "/elasticsearch";
    public static final String DT_FIELD = "dt";
    public static final String UPDATE_STREAM_NAME = "cirrussearch.update_pipeline.update.v1";
    public static final String PRIVATE_UPDATE_STREAM_NAME =
            "cirrussearch.update_pipeline.update.private.v1";
    public static final String EQIAD_UPDATE_TOPIC_NAME = "eqiad." + UPDATE_STREAM_NAME;
    public static final String EQIAD_PRIVATE_UPDATE_TOPIC_NAME =
            "eqiad." + PRIVATE_UPDATE_STREAM_NAME;
    public static final String CODFW_UPDATE_TOPIC_NAME = "codfw." + UPDATE_STREAM_NAME;
    public static final String CODFW_PRIVATE_UPDATE_TOPIC_NAME =
            "codfw." + PRIVATE_UPDATE_STREAM_NAME;

    public static final String LEGACY_UPDATE_STREAM_NAME = "cirrussearch.update_pipeline.update.rc0";
    public static final String PRIVATE_LEGACY_UPDATE_STREAM_NAME =
            "cirrussearch.update_pipeline.update.private.rc0";
    public static final String EQIAD_LEGACY_UPDATE_TOPIC_NAME = "eqiad." + LEGACY_UPDATE_STREAM_NAME;
    public static final String EQIAD_PRIVATE_LEGACY_UPDATE_TOPIC_NAME =
            "eqiad." + PRIVATE_LEGACY_UPDATE_STREAM_NAME;
    public static final String CODFW_LEGACY_UPDATE_TOPIC_NAME = "codfw." + LEGACY_UPDATE_STREAM_NAME;
    public static final String CODFW_PRIVATE_LEGACY_UPDATE_TOPIC_NAME =
            "codfw." + PRIVATE_LEGACY_UPDATE_STREAM_NAME;

    public static final String FAILURE_TOPIC_NAME =
            "eqiad.cirrussearch.update_pipeline.fetch_error.v1";
    protected static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
    /**
     * Determines if an instance of elasticsearch should be launched.
     *
     * <p>If {@code PROXY_ELASTICSEARCH=true}, launch an instance of elasticsearch and use a wiremock
     * proxy to record requests to it. Otherwise, use only wiremock to stub responses loaded from
     * {@code /wiremock/mappings}.
     *
     * <p>Recordings will be stored under {@code target/test-classes/wiremock/mappings}.
     */
    protected static final boolean PROXY_ELASTICSEARCH =
            "true".equals(System.getenv("PROXY_ELASTICSEARCH"));

    protected Producer<Long, String> producer;

    protected static Stream<String> baseApplicationArgs() {
        return Stream.of(
                param(ConsumerConfig.PARAM_EVENT_STREAM_CONFIG_URL),
                EventDataStreamUtilities.STREAM_CONFIG,
                param(ConsumerConfig.PARAM_EVENT_STREAM_JSON_SCHEMA_URLS),
                EventDataStreamUtilities.SCHEMA_REPO,
                param(ConsumerConfig.PARAM_PUBLIC_UPDATE_STREAM),
                UPDATE_STREAM_NAME,
                param(ConsumerConfig.PARAM_PRIVATE_UPDATE_STREAM),
                PRIVATE_UPDATE_STREAM_NAME,
                param(ConsumerConfig.PARAM_PUBLIC_LEGACY_UPDATE_STREAM),
                LEGACY_UPDATE_STREAM_NAME,
                param(ConsumerConfig.PARAM_PRIVATE_LEGACY_UPDATE_STREAM),
                PRIVATE_LEGACY_UPDATE_STREAM_NAME,
                param(ConsumerConfig.PARAM_KAFKA_SOURCE_CONFIG) + ".bootstrap.servers",
                SHARED_KAFKA_TEST_RESOURCE.getKafkaConnectString(),
                param(ConsumerConfig.PARAM_KAFKA_SOURCE_CONFIG) + ".group.id",
                ConsumerApplicationIT.class.getSimpleName(),
                param(ConsumerConfig.PARAM_FETCH_REQUEST_TIMEOUT),
                "1s",
                param(ConsumerConfig.PARAM_FETCH_RETRY_MAX_AGE),
                "15s",
                param(ConsumerConfig.PARAM_RETRY_FETCH_MAX),
                "5",
                param(ConsumerConfig.PARAM_FETCH_ERROR_TOPIC),
                FAILURE_TOPIC_NAME,
                param(ConsumerConfig.PARAM_PIPELINE_NAME),
                "eqiad.consumer");
    }

    protected static final ElasticsearchContainer elasticsearchContainer =
            new ElasticsearchContainer(getElasticSearchDockerImageName())
                    .withEnv(
                            ImmutableMap.of(
                                    "discovery.type", "single-node",
                                    "bootstrap.system_call_filter", "false",
                                    "action.auto_create_index", "false"))
                    .withLogConsumer(
                            frame -> {
                                final Logger logger = LoggerFactory.getLogger("ES");
                                if (frame.getType() == OutputFrame.OutputType.STDERR) {
                                    logger.error(frame.getUtf8String());
                                } else {
                                    logger.info(frame.getUtf8String());
                                }
                            });

    /**
     * Returns the operation-system-architecture-dependent docker image. Currently, there is no
     * universal image provided by WMF, see <a
     * href="https://www.mediawiki.org/wiki/MediaWiki-Docker/Configuration_recipes/ElasticSearch">documentation</a>.
     */
    @Nonnull
    private static DockerImageName getElasticSearchDockerImageName() {
        final String defaultImageName =
                "docker-registry.wikimedia.org/dev/cirrus-elasticsearch:7.10.2-s1";
        final String armImageName = "kostajh/wmf-elasticsearch-arm64:7.10.2-wmf9";
        final boolean arm = System.getProperty("os.arch").toLowerCase(Locale.US).startsWith("aa");
        return DockerImageName.parse(arm ? armImageName : defaultImageName)
                .asCompatibleSubstituteFor("docker.elastic.co/elasticsearch/elasticsearch");
    }

    protected static String getElasticsearchUrl() {
        final String wiremockBaseUrl =
                wireMockExtension.getRuntimeInfo().getHttpBaseUrl() + ELASTICSEARCH_PREFIX;
        if (PROXY_ELASTICSEARCH) {
            final String elasticsearchContainerUrl =
                    "http://"
                            + elasticsearchContainer.getHost()
                            + ":"
                            + elasticsearchContainer.getMappedPort(9200);
            wireMockExtension.stubFor(
                    WireMock.post(WireMock.urlMatching(ELASTICSEARCH_PREFIX + "/_bulk.*"))
                            .willReturn(
                                    WireMock.aResponse()
                                            .proxiedFrom(elasticsearchContainerUrl)
                                            .withProxyUrlPrefixToRemove(ELASTICSEARCH_PREFIX))
                            .persistent());
        }
        return wiremockBaseUrl;
    }

    protected static void createSearchIndex(String indexName) throws IOException {
        if (!PROXY_ELASTICSEARCH) {
            return;
        }
        HttpHost host =
                new HttpHost(elasticsearchContainer.getHost(), elasticsearchContainer.getMappedPort(9200));
        try (RestHighLevelClient client = new RestHighLevelClient(RestClient.builder(host))) {
            CreateIndexRequest request = new CreateIndexRequest(indexName);
            client.indices().create(request, RequestOptions.DEFAULT);
        }
    }

    protected static void verifyElasticsearch() throws IOException {
        if (PROXY_ELASTICSEARCH) {
            final SnapshotRecordResult snapshotRecordResult =
                    wireMockExtension.snapshotRecord(RecordSpec.forBaseUrl(ELASTICSEARCH_PREFIX));
            assertThat(snapshotRecordResult.getStubMappings()).hasSize(1);
            snapshotRecordResult
                    .getStubMappings()
                    .forEach(
                            mapping -> {
                                try {
                                    /*
                                     * The content of that file is intended to be picked up by wiremock.
                                     * For this to happen, place it under resources/wiremock/mappings (and rename it).
                                     */
                                    FileUtils.write(
                                            new File("target/" + mapping.getId().toString() + ".json"),
                                            convertToUnorderedLinesMatching(mapping.toString()),
                                            "UTF-8");
                                } catch (IOException e) {
                                    log.error("Failed to write recorded stub mapping", e);
                                }
                            });
        }

        // Requires at least one elasticsearch bulk to have been performed
        wireMockExtension.verify(
                WireMock.postRequestedFor(WireMock.urlMatching(ELASTICSEARCH_PREFIX + "/_bulk.*")));
    }

    /**
     * Changes the mapping generated by wiremock to use the unordered-lines matcher
     *
     * <p>The bulk requests to elasticsearch have multiple newline delimited json objects which
     * confuses wiremock. The generated mapping has `bodyPattern.equalsJson` set to the non-JSON bulk
     * request body. That matches indeed, but as a matter of fact it does not matter what's in
     * `bodyPattern.equalsJson`. Hence, the output needs some manual post-processing, e.g. converting
     * all lines in the request body into our custom unordered-lines request matcher.
     *
     * @param mapping
     * @throws IOException
     */
    private static String convertToUnorderedLinesMatching(String mapping) throws IOException {
        JsonNode root = OBJECT_MAPPER.readTree(mapping);
        ObjectNode request = (ObjectNode) root.path("request");

        ArrayNode bodyPatterns = (ArrayNode) request.remove("bodyPatterns");
        ArrayNode lines =
                StreamSupport.stream(bodyPatterns.spliterator(), false)
                        .flatMap(x -> Stream.of(x.get("equalToJson").textValue().split("\n")))
                        .map(AbstractApplicationIT::uncheckedReadTree)
                        .reduce(OBJECT_MAPPER.createArrayNode(), ArrayNode::add, ArrayNode::addAll);

        request.set("customMatcher", UnorderedLines.toJson(lines));

        return OBJECT_MAPPER.writeValueAsString(root);
    }

    @SneakyThrows
    private static JsonNode uncheckedReadTree(String value) {
        return OBJECT_MAPPER.readTree(value);
    }

    protected static KafkaProducer<Long, String> createProducer() {
        final Properties producerProperties = new Properties();
        // See, https://developer.ibm.com/articles/benefits-compression-kafka-messaging/
        producerProperties.put("compression.type", "snappy");
        producerProperties.put("linger.ms", 100);

        return SHARED_KAFKA_TEST_RESOURCE
                .getKafkaTestUtils()
                .getKafkaProducer(LongSerializer.class, StringSerializer.class, producerProperties);
    }

    @BeforeAll
    static void startElasticSearch() {
        if (PROXY_ELASTICSEARCH) {
            elasticsearchContainer.start();
        }
    }

    @AfterAll
    static void stopElasticSearch() {
        if (PROXY_ELASTICSEARCH) {
            elasticsearchContainer.stop();
        }
        EXECUTOR.shutdownNow();
    }

    @BeforeEach
    void startProducer() {
        producer = createProducer();
    }

    @AfterEach
    void stopProducer() {
        if (producer != null) {
            producer.close();
        }
    }

    @Nonnull
    private static ProducerRecord<Long, String> toProducerRecord(String topic, JsonNode line) {
        return new ProducerRecord<>(
                topic,
                0,
                Instant.parse(line.get(DT_FIELD).textValue()).toEpochMilli(),
                null,
                line.toString());
    }

    protected List<Instant> produceUpdateRecords(
            Stream<JsonNode> eventNodes, String topic, Function<JsonNode, JsonNode> lineMapper)
            throws ExecutionException, InterruptedException {

        List<Future<RecordMetadata>> eventSends =
                eventNodes
                        .map(lineMapper)
                        .map(line -> toProducerRecord(topic, line))
                        .map(producer::send)
                        .collect(Collectors.toList());

        List<Instant> timestamps = new ArrayList<>();
        for (Future<RecordMetadata> f : eventSends) {
            timestamps.add(Instant.ofEpochMilli(f.get().timestamp()));
        }
        return timestamps;
    }

    protected List<ConsumerRecord<String, String>> collectFailedRecords() {
        return SHARED_KAFKA_TEST_RESOURCE
                .getKafkaTestUtils()
                .consumeAllRecordsFromTopic(
                        FAILURE_TOPIC_NAME, StringDeserializer.class, StringDeserializer.class);
    }

    protected void createTopics(Stream<String> topics) {
        try (AdminClient adminClient =
                SHARED_KAFKA_TEST_RESOURCE.getKafkaTestUtils().getAdminClient()) {
            adminClient.createTopics(
                    topics.map(topic -> new NewTopic(topic, 1, (short) 1)).collect(Collectors.toList()));
        }
    }

    protected void createStandardTopics() {
        createTopics(
                Stream.of(
                        CODFW_UPDATE_TOPIC_NAME,
                        CODFW_PRIVATE_UPDATE_TOPIC_NAME,
                        EQIAD_UPDATE_TOPIC_NAME,
                        EQIAD_PRIVATE_UPDATE_TOPIC_NAME,
                        CODFW_LEGACY_UPDATE_TOPIC_NAME,
                        CODFW_PRIVATE_LEGACY_UPDATE_TOPIC_NAME,
                        EQIAD_LEGACY_UPDATE_TOPIC_NAME,
                        EQIAD_PRIVATE_LEGACY_UPDATE_TOPIC_NAME,
                        FAILURE_TOPIC_NAME));
    }

    protected static String param(ConfigOption<?> option) {
        return "--" + option.key();
    }
}
