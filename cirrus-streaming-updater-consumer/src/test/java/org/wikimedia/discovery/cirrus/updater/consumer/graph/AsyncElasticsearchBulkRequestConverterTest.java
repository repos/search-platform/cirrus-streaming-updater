package org.wikimedia.discovery.cirrus.updater.consumer.graph;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import org.apache.commons.io.FileUtils;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.api.connector.sink2.Sink;
import org.apache.flink.api.connector.sink2.SinkWriter.Context;
import org.apache.flink.types.Row;
import org.assertj.core.api.AssertFactory;
import org.assertj.core.api.InstanceOfAssertFactory;
import org.assertj.core.api.MapAssert;
import org.elasticsearch.action.DocWriteRequest;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.common.xcontent.ToXContent;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentFactory;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Named;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mockito;
import org.wikimedia.discovery.cirrus.updater.common.config.EventStreamConstants;
import org.wikimedia.discovery.cirrus.updater.common.model.EventDataStreamUtilities;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateEvent;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateFields;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateRowTranscoder;
import org.wikimedia.eventutilities.flink.formats.json.JsonRowDeserializationSchema;

import com.fasterxml.jackson.databind.JsonNode;

@SuppressWarnings({"checkstyle:classfanoutcomplexity"})
class AsyncElasticsearchBulkRequestConverterTest {
    static final TypeInformation<Row> UPDATE_TYPE_INFO =
            EventDataStreamUtilities.buildUpdateTypeInfo().getTypeAt(UpdateFields.FIELDS);
    static final AsyncElasticsearchBulkRequestConverter CONVERTER;
    static final Context CONTEXT = new TestSinkContext();
    static final Sink.InitContext SINK_INIT_CONTEXT;

    static {
        SINK_INIT_CONTEXT = Mockito.mock(Sink.InitContext.class, Mockito.RETURNS_DEEP_STUBS);
        CONVERTER =
                new AsyncElasticsearchBulkRequestConverter(
                        UPDATE_TYPE_INFO, EventDataStreamUtilities.OBJECT_MAPPER);
        CONVERTER.open(SINK_INIT_CONTEXT);
    }

    static final String REBUILD_FIXTURES_ENV = "REBUILD_FIXTURES";
    static final boolean REBUILD_FIXTURES = "true".equals(System.getenv(REBUILD_FIXTURES_ENV));

    @AfterAll
    static void failIfRebuildFixturesEnabled() {
        if (REBUILD_FIXTURES) {
            throw new IllegalStateException(
                    "Please disable " + REBUILD_FIXTURES_ENV + " for the test to pass");
        }
    }

    static Stream<UpdateEvent> events() throws IOException {
        final Spliterator<JsonNode> eventNodeSpliterator =
                Spliterators.spliteratorUnknownSize(
                        EventDataStreamUtilities.parseJson("/update.events.json").iterator(),
                        Spliterator.ORDERED);
        final JsonRowDeserializationSchema deserializer =
                EventDataStreamUtilities.FACTORY.deserializer(
                        EventStreamConstants.UPDATER_PUBLIC_OUTPUT_STREAM_NAME);

        UpdateRowTranscoder decoder = UpdateRowTranscoder.decoder();

        return StreamSupport.stream(eventNodeSpliterator, false)
                .map(deserializer::convert)
                .map(decoder::decode);
    }

    static UpdateEvent event(String id) throws IOException {
        return events()
                .filter(event -> id.equals(event.getMeta().getId()))
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("No update event found with ID " + id));
    }

    static Stream<AsyncElasticsearchBulkRequest> actions() throws IOException {
        return events().map(updateEvent -> CONVERTER.apply(updateEvent, CONTEXT));
    }

    static Stream<Arguments> eventArguments() throws IOException {
        return events()
                .map(event -> Arguments.arguments(Named.named(event.getChangeType().name(), event)));
    }

    @ParameterizedTest
    @MethodSource("eventArguments")
    void updateEvent(UpdateEvent updateEvent) {
        final AsyncElasticsearchBulkRequest result =
                CONVERTER.apply(updateEvent, new TestSinkContext());
        assertThat(result.getId()).isEqualTo(updateEvent.getMeta().getId());
        assertThat(result.getBulkRequest().numberOfActions()).isEqualTo(1);
    }

    @Test
    void emit() throws IOException {
        final AsyncElasticsearchBulkRequest updateResult = convert("update-page");
        final AsyncElasticsearchBulkRequest deleteResult = convert("delete-page");

        final UpdateRequest updateRequest =
                (UpdateRequest) updateResult.getBulkRequest().requests().get(0);
        assertThat(updateRequest.id()).isEqualTo("147498");
        assertThat(updateRequest.index()).isEqualTo("testwiki_content");

        final DeleteRequest deleteRequest =
                (DeleteRequest) deleteResult.getBulkRequest().requests().get(0);
        assertThat(deleteRequest.id()).isEqualTo("147498");
        assertThat(deleteRequest.index()).isEqualTo("testwiki_content");
    }

    @ParameterizedTest
    @ValueSource(strings = {"rerender-page", "redirect-update-add"})
    void emit_redirects_from_raw_fields(String eventId) throws IOException {
        final JsonRowDeserializationSchema fieldsSchema =
                new JsonRowDeserializationSchema.Builder(UPDATE_TYPE_INFO).build();

        UpdateEvent event = event(eventId);
        Row fields =
                fieldsSchema.deserialize(
                        "{\"redirect\": [{\"namespace\": 0, \"title\": \"Example Title\"}]}"
                                .getBytes(StandardCharsets.UTF_8));
        event.getUpdate().setRawFields(fields);

        AsyncElasticsearchBulkRequest bulk = CONVERTER.apply(event, CONTEXT);
        UpdateRequest updateRequest = (UpdateRequest) bulk.getBulkRequest().requests().get(0);

        Optional.of(updateRequest.script().getParams())
                .map(params -> (Map<String, String>) params.get("handlers"))
                .ifPresent(handlers -> assertThat(handlers).doesNotContainKey("redirect"));

        assertThat(updateRequest.script().getParams())
                .extractingByKey(
                        "source", AsyncElasticsearchBulkRequestConverterTest.<String, Object>mapAssertFactory())
                .extractingByKey("redirect")
                .isEqualTo(List.of(Map.of("title", "Example Title", "namespace", 0)));
    }

    @Test
    void emit_noop_set_handler_for_redirects() throws IOException {
        final AsyncElasticsearchBulkRequest addRedirectResult = convert("redirect-update-add");
        final AsyncElasticsearchBulkRequest removeRedirectResult = convert("redirect-update-remove");

        final UpdateRequest updateRequest =
                (UpdateRequest) addRedirectResult.getBulkRequest().requests().get(0);
        assertThat(updateRequest.id()).isEqualTo("147498");
        assertThat(updateRequest.index()).isEqualTo("testwiki_content");
        assertThat(updateRequest.script().getLang()).isEqualTo("super_detect_noop");
        assertThat(updateRequest.script().getParams()).containsKeys("source", "handlers");
        assertThat(updateRequest.script().getParams())
                .extractingByKey(
                        "handlers",
                        AsyncElasticsearchBulkRequestConverterTest.<String, String>mapAssertFactory())
                .extractingByKey("redirect")
                .isEqualTo("set");
        assertThat(updateRequest.script().getParams())
                .extractingByKey(
                        "source", AsyncElasticsearchBulkRequestConverterTest.<String, Object>mapAssertFactory())
                .extractingByKey("redirect")
                .isEqualTo(
                        Map.of(
                                "add",
                                List.of(Map.of("namespace", 2L, "title", "Redirect Source")),
                                "max_size",
                                1024));

        final UpdateRequest deleteRequest =
                (UpdateRequest) removeRedirectResult.getBulkRequest().requests().get(0);
        assertThat(deleteRequest.id()).isEqualTo("147498");
        assertThat(deleteRequest.index()).isEqualTo("testwiki_content");
        assertThat(deleteRequest.script().getLang()).isEqualTo("super_detect_noop");
        assertThat(deleteRequest.script().getParams()).containsKeys("source", "handlers");
        assertThat(deleteRequest.script().getParams())
                .extractingByKey(
                        "handlers",
                        AsyncElasticsearchBulkRequestConverterTest.<String, String>mapAssertFactory())
                .extractingByKey("redirect")
                .isEqualTo("set");
        assertThat(deleteRequest.script().getParams())
                .extractingByKey(
                        "source", AsyncElasticsearchBulkRequestConverterTest.<String, Object>mapAssertFactory())
                .extractingByKey("redirect")
                .isEqualTo(
                        Map.of(
                                "remove",
                                List.of(Map.of("namespace", 2L, "title", "Deleted Redirect Source")),
                                "max_size",
                                1024));
    }

    @Test
    void emit_no_handlers_if_noop_hints_null_or_empty() throws IOException {

        final String requestId = "empty-noop-hints";
        final AsyncElasticsearchBulkRequest emptyNoopHintsResult = convert(requestId);
        final AsyncElasticsearchBulkRequest noNoopHintsResult = convert("no-noop-hints");

        assertThat(
                        ((UpdateRequest) emptyNoopHintsResult.getBulkRequest().requests().get(0))
                                .script()
                                .getParams())
                .doesNotContainKey("handlers");
        assertThat(
                        ((UpdateRequest) noNoopHintsResult.getBulkRequest().requests().get(0))
                                .script()
                                .getParams())
                .doesNotContainKey("handlers");
    }

    @ParameterizedTest
    @MethodSource("eventArguments")
    void test_emitter_against_fixtures(UpdateEvent event) throws IOException {
        final AsyncElasticsearchBulkRequest result = CONVERTER.apply(event, CONTEXT);

        assertThat(result.getBulkRequest().numberOfActions()).isEqualTo(1);
        assertThat(result.getBulkRequest().requests().get(0))
                .isOfAnyClassIn(UpdateRequest.class, DeleteRequest.class);

        ByteArrayOutputStream os = new ByteArrayOutputStream();
        XContentBuilder builder = XContentFactory.jsonBuilder(os).prettyPrint();
        serializeFixture(builder, result.getBulkRequest().requests().get(0));
        builder.flush();

        // TODO: better way?
        String fixtureBasePath = "src/test/resources/" + getClass().getName().replace(".", "/");
        File fixture = new File(fixtureBasePath + "/fixture-for-" + event.getMeta().getId() + ".json");
        if (REBUILD_FIXTURES) {
            FileUtils.writeByteArrayToFile(fixture, os.toByteArray());
        } else {
            final JsonNode fixtureTree =
                    EventDataStreamUtilities.OBJECT_MAPPER.readTree(new FileInputStream(fixture));
            final JsonNode currentTree =
                    EventDataStreamUtilities.OBJECT_MAPPER.readTree(os.toByteArray());

            assertThat(currentTree).isEqualTo(fixtureTree);
        }
    }

    void serializeFixture(XContentBuilder builder, DocWriteRequest<?> request) throws IOException {
        builder.startObject();
        builder.field("opType", request.opType());
        builder.field("index", request.index());
        builder.field("id", request.id());
        if (request instanceof ToXContent) {
            builder.field("body");
            ((ToXContent) request).toXContent(builder, ToXContent.EMPTY_PARAMS);
        }
        builder.endObject();
    }

    static <KEY, VALUE>
            InstanceOfAssertFactory<Map, ? extends MapAssert<KEY, VALUE>> mapAssertFactory() {
        return new InstanceOfAssertFactory<>(
                Map.class,
                (AssertFactory<Map, MapAssert<KEY, VALUE>>) (handlers) -> new MapAssert<>(handlers));
    }

    static AsyncElasticsearchBulkRequest convert(String id) throws IOException {
        return CONVERTER.apply(event(id), CONTEXT);
    }

    static class TestSinkContext implements Context {

        private final long timestamp;
        private final int watermark;

        TestSinkContext() {
            this(0, System.currentTimeMillis());
        }

        TestSinkContext(int watermark, long timestamp) {
            this.watermark = watermark;
            this.timestamp = timestamp;
        }

        @Override
        public long currentWatermark() {
            return watermark;
        }

        @Override
        public Long timestamp() {
            return timestamp;
        }
    }
}
