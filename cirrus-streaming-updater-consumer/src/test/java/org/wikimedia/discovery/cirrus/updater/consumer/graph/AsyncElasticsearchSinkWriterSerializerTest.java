package org.wikimedia.discovery.cirrus.updater.consumer.graph;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.time.Instant;
import java.util.stream.Stream;

import org.elasticsearch.action.bulk.BulkRequest;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Named;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

@TestMethodOrder(OrderAnnotation.class)
class AsyncElasticsearchSinkWriterSerializerTest {

    @Test
    @Order(0)
    void version() {
        assertThat(new AsyncElasticsearchSinkWriterSerializer().getVersion()).isEqualTo(2);
    }

    @Order(1)
    @ParameterizedTest
    @MethodSource("actions")
    void serde(AsyncElasticsearchBulkRequest originalActions) throws IOException {
        if (originalActions.getBulkRequest().numberOfActions() == 1) {
            originalActions =
                    new AsyncElasticsearchBulkRequest(
                            originalActions.getId(),
                            originalActions.getAttempt() + 100,
                            new BulkRequest()
                                    .add(originalActions.getBulkRequest().requests().get(0))
                                    .add(originalActions.getBulkRequest().requests().get(0)),
                            Instant.EPOCH);
        }
        final AsyncElasticsearchSinkWriterSerializer serializer =
                new AsyncElasticsearchSinkWriterSerializer();
        try (ByteArrayOutputStream byteOutputStream = new ByteArrayOutputStream();
                DataOutputStream dataOutputStream = new DataOutputStream(byteOutputStream)) {
            serializer.serializeRequestToStream(originalActions, dataOutputStream);

            dataOutputStream.flush();
            byteOutputStream.flush();

            final byte[] serializedData = toByteArray(byteOutputStream);

            try (ByteArrayInputStream byteInputStream = new ByteArrayInputStream(serializedData);
                    DataInputStream dataInputStream = new DataInputStream(byteInputStream)) {

                final AsyncElasticsearchBulkRequest deserializedActions =
                        serializer.deserializeRequestFromStream(serializedData.length, dataInputStream);

                assertThat(originalActions.getId()).isEqualTo(deserializedActions.getId());
                assertThat(originalActions.getAttempt()).isEqualTo(deserializedActions.getAttempt());
                assertThat(originalActions.getBulkRequest().numberOfActions())
                        .isEqualTo(deserializedActions.getBulkRequest().numberOfActions());
            }
        }
    }

    private byte[] toByteArray(ByteArrayOutputStream byteOutputStream, byte... suffix) {
        final byte[] original = byteOutputStream.toByteArray();
        if (suffix.length == 0) {
            return original;
        }
        final byte[] copy = new byte[original.length + suffix.length];
        System.arraycopy(original, 0, copy, 0, original.length);
        System.arraycopy(suffix, 0, copy, original.length, suffix.length);
        return copy;
    }

    static Stream<Arguments> actions() throws IOException {
        return Stream.concat(
                AsyncElasticsearchBulkRequestConverterTest.actions()
                        .map(actions -> Arguments.of(Named.named("actions for " + actions.getId(), actions))),
                Stream.of(
                        Arguments.of(
                                Named.named(
                                        "empty actions",
                                        new AsyncElasticsearchBulkRequest(
                                                "empty", 0, new BulkRequest(), Instant.EPOCH)))));
    }
}
