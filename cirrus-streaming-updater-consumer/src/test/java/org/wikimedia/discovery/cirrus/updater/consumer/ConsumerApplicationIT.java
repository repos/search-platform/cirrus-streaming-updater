package org.wikimedia.discovery.cirrus.updater.consumer;

import static com.github.tomakehurst.wiremock.client.WireMock.equalTo;
import static org.assertj.core.api.Assertions.assertThat;
import static org.wikimedia.discovery.cirrus.updater.common.wiremock.WireMockExtensionUtilities.mockJsonResponse;

import java.io.IOException;
import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.UnaryOperator;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import javax.annotation.Nonnull;

import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.hc.core5.http.HttpHeaders;
import org.junit.jupiter.api.Test;
import org.wikimedia.discovery.cirrus.updater.common.model.EventDataStreamUtilities;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.TextNode;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.http.Fault;
import com.github.tomakehurst.wiremock.junit5.WireMockRuntimeInfo;
import com.github.tomakehurst.wiremock.stubbing.StubMapping;
import com.google.common.collect.Sets;

@SuppressWarnings("checkstyle:classfanoutcomplexity")
class ConsumerApplicationIT extends AbstractApplicationIT {

    public static final String TEST_INDEX = "testwiki_content";

    public static final Set<String> EQIAD_REQUEST_IDS =
            Sets.newHashSet(
                    "update-page",
                    "rerender-page",
                    "redirect-update-add",
                    "redirect-update-remove",
                    "delete-page",
                    "fetch-failure-simulation",
                    "with-weighted-tags",
                    "trigger-document-not-found-exception",
                    "trigger-index-not-found-exception");

    public static final Set<String> CODFW_REQUEST_IDS = Collections.singleton("update-page");

    public static final Set<String> EQIAD_LEGACY_REQUEST_IDS =
            Collections.singleton("update-page-legacy");

    // The mock events happen to use omega for all rows, but we could test multiple cluster groups
    private static final String CLUSTER_GROUP = "omega";

    private static UnaryOperator<JsonNode> allEventsBefore(Instant now, Duration offset) {
        final AtomicInteger dtOffset = new AtomicInteger();
        return (JsonNode eventNode) -> {
            if (eventNode instanceof ObjectNode) {
                final Instant dt =
                        now.plus(offset.plus(Duration.ofMillis(100L * dtOffset.getAndIncrement())));
                overrideDt((ObjectNode) eventNode, dt);
                final JsonNode metaNode = eventNode.path("meta");
                if (metaNode instanceof ObjectNode) {
                    overrideDt((ObjectNode) metaNode, dt);
                }
            }
            return eventNode;
        };
    }

    private static void overrideDt(ObjectNode node, Instant dt) {
        node.set(DT_FIELD, TextNode.valueOf(dt.toString()));
    }

    private static Stream<JsonNode> getUpdateEventMessages(Set<String> requestIds)
            throws IOException {
        List<JsonNode> filtered =
                getUpdateEventMessages()
                        .filter(e -> requestIds.contains(e.path("meta").path("id").asText()))
                        .collect(Collectors.toList());
        // A bit silly, but the easiest way to verify we have all the requested events is to collect
        // them.
        assertThat(
                        filtered.stream()
                                .map(e -> e.path("meta").path("id").asText())
                                .collect(Collectors.toSet()))
                .containsExactlyElementsOf(requestIds);
        return filtered.stream();
    }

    @Nonnull
    private static Stream<JsonNode> getUpdateEventMessages() throws IOException {
        final Spliterator<JsonNode> eventNodeSpliterator =
                Spliterators.spliteratorUnknownSize(
                        EventDataStreamUtilities.parseJson("/update.events.json").iterator(),
                        Spliterator.ORDERED);

        return StreamSupport.stream(eventNodeSpliterator, false);
    }

    @Test
    void test(WireMockRuntimeInfo runtimeInfo) throws Exception {
        final Instant now = Instant.now().truncatedTo(ChronoUnit.SECONDS);

        createStandardTopics();

        List<Instant> inputMessageTimestamps =
                produceUpdateRecords(
                        getUpdateEventMessages(EQIAD_REQUEST_IDS),
                        EQIAD_UPDATE_TOPIC_NAME,
                        allEventsBefore(now, Duration.ofSeconds(-1)));

        produceUpdateRecords(
                getUpdateEventMessages(EQIAD_LEGACY_REQUEST_IDS),
                EQIAD_LEGACY_UPDATE_TOPIC_NAME,
                allEventsBefore(now, Duration.ofSeconds(-1)));

        createSearchIndex(TEST_INDEX);

        CompletableFuture.delayedExecutor(20, TimeUnit.SECONDS)
                .execute(
                        () -> {
                            try {
                                produceUpdateRecords(
                                        getUpdateEventMessages(CODFW_REQUEST_IDS),
                                        CODFW_UPDATE_TOPIC_NAME,
                                        allEventsBefore(Instant.now().truncatedTo(ChronoUnit.SECONDS), Duration.ZERO));
                            } catch (ExecutionException | InterruptedException | IOException e) {
                                throw new RuntimeException("Failed to produce later events", e);
                            }
                        });

        final StubMapping elasticsearchBulkMapping =
                wireMockExtension.getSingleStubMapping(
                        UUID.fromString("fd14a29d-70d5-4b5f-be09-e8c1ab03075b"));

        assertThat(elasticsearchBulkMapping.getName()).isEqualTo("elasticsearch__bulk");

        final String mediawikiAuthToken = "example auth token";
        wireMockExtension.stubFor(
                WireMock.get(
                                WireMock.urlEqualTo(
                                        "/w/api.php?action=cirrus-config-dump&prop=replicagroup%7Cnamespacemap&format=json&formatversion=2"))
                        // All requests should have the header, but seemed repetitive to keep asserting
                        .withHeader(HttpHeaders.AUTHORIZATION, equalTo("NetworkSession " + mediawikiAuthToken))
                        .willReturn(mockJsonResponse("testwiki-namespace-map.json")));
        wireMockExtension.stubFor(
                WireMock.get(WireMock.urlMatching("/w/api.php.*revids=551141.*"))
                        .willReturn(mockJsonResponse("revision.551141.json")));

        wireMockExtension.stubFor(
                WireMock.get(WireMock.urlMatching("/w/api.php.*pageids=147498.*"))
                        .willReturn(mockJsonResponse("revision.551141.json")));

        wireMockExtension.stubFor(
                WireMock.get(WireMock.urlMatching("/w/api.php.*revids=551144.*"))
                        .inScenario("551144")
                        .willReturn(mockJsonResponse("revision.missing.json"))
                        .willSetStateTo("recover"));

        wireMockExtension.stubFor(
                WireMock.get(WireMock.urlMatching("/w/api.php.*revids=551144.*"))
                        .inScenario("551144")
                        .whenScenarioStateIs("recover")
                        .willReturn(mockJsonResponse("revision.551144.json")));

        wireMockExtension.stubFor(
                WireMock.get(WireMock.urlMatching("/w/api.php.*revids=551147.*"))
                        .inScenario("551147")
                        .willReturn(WireMock.serverError().withFixedDelay(1500))
                        .willSetStateTo("server error"));

        wireMockExtension.stubFor(
                WireMock.get(WireMock.urlMatching("/w/api.php.*revids=551147.*"))
                        .inScenario("551147")
                        .whenScenarioStateIs("server error")
                        .willReturn(WireMock.serverError().withFixedDelay(500))
                        .willSetStateTo("connection reset"));

        wireMockExtension.stubFor(
                WireMock.get(WireMock.urlMatching("/w/api.php.*revids=551147.*"))
                        .inScenario("551147")
                        .whenScenarioStateIs("connection reset")
                        .willReturn(WireMock.serverError().withFault(Fault.CONNECTION_RESET_BY_PEER)));

        wireMockExtension.stubFor(
                WireMock.get(WireMock.urlMatching("/w/api.php.*revids=2153599188.*"))
                        .willReturn(mockJsonResponse("revision.missing.json")));

        // event from the public legacy stream
        wireMockExtension.stubFor(
                WireMock.get(WireMock.urlMatching("/w/api.php.*revids=551142.*"))
                        .willReturn(mockJsonResponse("revision.missing.json")));

        ConsumerApplication.main(
                Stream.concat(
                                baseApplicationArgs(),
                                Stream.of(
                                        "--topic-prefix-filter",
                                        "eqiad",
                                        "--kafka-source-start-time",
                                        inputMessageTimestamps.stream()
                                                .min(Instant::compareTo)
                                                .map(Instant::toString)
                                                .orElseThrow(),
                                        "--kafka-source-end-offset." + EQIAD_UPDATE_TOPIC_NAME,
                                        Integer.toString(EQIAD_REQUEST_IDS.size()),
                                        "--kafka-source-end-offset." + CODFW_UPDATE_TOPIC_NAME,
                                        Integer.toString(CODFW_REQUEST_IDS.size()),
                                        "--elasticsearch-urls",
                                        CLUSTER_GROUP + ":\"" + getElasticsearchUrl() + '"',
                                        "--http-routes.00",
                                        "test\\.wikipedia\\.org=" + runtimeInfo.getHttpBaseUrl(),
                                        "--mediawiki-auth-token=" + mediawikiAuthToken,
                                        "--saneitize-max-runtime",
                                        "1"))
                        .toArray(String[]::new));

        assertThat(collectFailedRecords())
                .extracting(
                        failure -> {
                            JsonNode node = OBJECT_MAPPER.readTree(failure.value());
                            return Tuple3.of(
                                    node.path("meta").path("id").asText(),
                                    node.path("error_type").asText(),
                                    node.path("message").asText());
                        })
                .containsExactlyInAnyOrder(
                        Tuple3.of(
                                "fetch-failure-simulation", "java.io.IOException", "Connection reset by peer"),
                        // Simulating weighted tags update against a page that was since deleted. Ensures an
                        // event with weighted tags is round tripped through the transcoder.
                        Tuple3.of(
                                "with-weighted-tags",
                                "org.wikimedia.discovery.cirrus.updater.common.RevisionNotFoundException",
                                "Revision 2153599188 is missing"));

        verifyElasticsearch();
    }
}
