package org.wikimedia.discovery.cirrus.updater.consumer.graph;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.stream.Stream;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.wikimedia.discovery.cirrus.updater.common.model.ChangeType;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateEvent;

import com.fasterxml.jackson.databind.JsonNode;

@ExtendWith(MockitoExtension.class)
class CombinedCirrusDocEndpointTest {

    @Mock RevisionCirrusDocEndpoint revBasedEndpoint;
    @Mock RerenderCirrusDocEndpoint rerenderCirrusDocEndpoint;

    @Mock JsonNode response;

    @InjectMocks CombinedCirrusDocEndpoint combinedCirrusDocEndpoint;

    @Test
    void buildURI() {
        final UpdateEvent revBasedUpdate = createEvent(ChangeType.REV_BASED_UPDATE);
        combinedCirrusDocEndpoint.buildURI(revBasedUpdate);
        verify(revBasedEndpoint).buildURI(revBasedUpdate);
        verify(rerenderCirrusDocEndpoint, never()).buildURI(revBasedUpdate);

        reset(revBasedEndpoint, rerenderCirrusDocEndpoint);

        final UpdateEvent rerenderUpdate = createEvent(ChangeType.PAGE_RERENDER);
        combinedCirrusDocEndpoint.buildURI(rerenderUpdate);
        verify(rerenderCirrusDocEndpoint).buildURI(rerenderUpdate);
        verify(revBasedEndpoint, never()).buildURI(rerenderUpdate);

        reset(revBasedEndpoint, rerenderCirrusDocEndpoint);

        Assertions.assertThatExceptionOfType(IllegalArgumentException.class)
                .isThrownBy(
                        () -> {
                            combinedCirrusDocEndpoint.buildURI(createEvent(ChangeType.PAGE_DELETE));
                        });
    }

    @Test
    void extractAndAugment() {
        final UpdateEvent revBasedUpdate = createEvent(ChangeType.REV_BASED_UPDATE);
        combinedCirrusDocEndpoint.extractAndAugment(revBasedUpdate, response);
        verify(revBasedEndpoint).extractAndAugment(revBasedUpdate, response);
        verify(rerenderCirrusDocEndpoint, never()).extractAndAugment(revBasedUpdate, response);

        Stream.of(ChangeType.PAGE_RERENDER, ChangeType.PAGE_RERENDER_UPSERT)
                .forEach(
                        changeType -> {
                            reset(revBasedEndpoint, rerenderCirrusDocEndpoint);

                            final UpdateEvent rerenderUpdate = createEvent(changeType);
                            combinedCirrusDocEndpoint.extractAndAugment(rerenderUpdate, response);
                            verify(rerenderCirrusDocEndpoint).extractAndAugment(rerenderUpdate, response);
                            verify(revBasedEndpoint, never()).extractAndAugment(rerenderUpdate, response);
                        });

        reset(revBasedEndpoint, rerenderCirrusDocEndpoint);

        Assertions.assertThatExceptionOfType(IllegalArgumentException.class)
                .isThrownBy(
                        () -> {
                            combinedCirrusDocEndpoint.extractAndAugment(
                                    createEvent(ChangeType.PAGE_DELETE), response);
                        });
    }

    private static UpdateEvent createEvent(ChangeType changeType) {
        final UpdateEvent mock = mock(UpdateEvent.class);
        when(mock.getChangeType()).thenReturn(changeType);
        return mock;
    }
}
