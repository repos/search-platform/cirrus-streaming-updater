package org.wikimedia.discovery.cirrus.updater.consumer;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Deque;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.NavigableMap;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Random;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.apache.commons.io.FileUtils;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * A simulation to narrow down likely well-performing config options for the async fetch operator.
 */
@Slf4j
@Disabled("For running local only")
public class ThroughputSimulationTest {

    private static final String TARGET_DIRECTORY_PATH =
            Objects.requireNonNull(
                                    ThroughputSimulationTest.class.getResource("/"),
                                    "unable to obtain target directory")
                            .toExternalForm()
                    + "..";

    /** A tick represents 1s. */
    private static final int MAX_TICKS = 200;

    /**
     * Indicates if order of records must be kept.
     *
     * @see org.apache.flink.streaming.api.datastream.AsyncDataStream.OutputMode
     */
    private static final boolean ORDERED = true;

    /**
     * Expected input rate. 300 records/s of which 15% are processed in order and 85% out of order.
     */
    private static final int SOURCE_RECORDS_PER_TICK = (int) (300 * (ORDERED ? 0.15f : 0.85f));

    private static final int MAX_RECORDS = MAX_TICKS * SOURCE_RECORDS_PER_TICK;

    /** Max. number of retries per record. */
    private static final int MAX_ATTEMPTS = 2;

    /**
     * If we assume 1 tick equals 1s and a 50% percentile finishes in under 5s, then {@code 5 ticks *
     * 0.2 progress/tick = 1 progress ~ done}.
     */
    private static final float MIN_PROGRESS = 0.2f;

    /** We see 0.1 ops/s of {@value SOURCE_RECORDS_PER_TICK} do get retries. */
    private static final float RETRY_PROBABILITY = 0.1f / SOURCE_RECORDS_PER_TICK;

    /** Repeat every simulation to get better approximations. */
    private static final int MAX_ITERATIONS_PER_SIMULATION = 3;

    /** Output simulation results. Very verbose! */
    private static final boolean VERBOSE = false;

    private static final Map<String, List<Integer>> results = new ConcurrentHashMap<>();

    public static void main(String[] args) throws IOException {
        final ThroughputSimulationTest test = new ThroughputSimulationTest();
        simulations()
                .parallel()
                .forEach(
                        arguments -> test.simulate((Integer) arguments.get()[0], (Integer) arguments.get()[1]));
        logResults();
    }

    /**
     * Logs the results in descending order. Parameters leading to the same result are sorted
     * ascending. So the value logged first has the lowest parameters and the highest result.
     */
    @AfterAll
    static void logResults() throws IOException {
        final Stream<String> stringStream =
                results.entrySet().stream()
                        .map(
                                entry -> {
                                    final float avg =
                                            (float)
                                                    entry.getValue().stream()
                                                            .mapToDouble(d -> d)
                                                            .average()
                                                            .orElseThrow(
                                                                    () ->
                                                                            new NoSuchElementException(
                                                                                    "No values for " + entry.getKey()));
                                    return Map.entry(entry.getKey(), Math.round(100 * (avg / MAX_RECORDS)));
                                })
                        .sorted(
                                Comparator.comparing(Entry<String, Integer>::getValue)
                                        .reversed()
                                        .thenComparing(
                                                (Entry<String, Integer> entry) ->
                                                        Integer.valueOf(entry.getKey().split(":")[0]))
                                        .thenComparing(
                                                (Entry<String, Integer> entry) ->
                                                        Integer.valueOf(entry.getKey().split(":")[1])))
                        .filter(result -> result.getValue() > 50)
                        .map(
                                result ->
                                        String.format(
                                                Locale.ENGLISH, "%s -> %d%%", result.getKey(), result.getValue()));

        final List<String> lines = stringStream.collect(Collectors.toList());
        final File outputfile =
                new File(
                        new File(URI.create(TARGET_DIRECTORY_PATH)),
                        AsyncElasticsearchSinkIT.class.getName()
                                + "-results-"
                                + SOURCE_RECORDS_PER_TICK
                                + ".txt");
        FileUtils.writeLines(outputfile, lines);
        log.info(
                "Simulated {}/tick for {} ticks = {} records, here are the to 10 results:\n{}",
                SOURCE_RECORDS_PER_TICK,
                MAX_TICKS,
                MAX_RECORDS,
                String.join("\n", lines.subList(0, 10)));
        log.info("For more result see {}", outputfile);
    }

    static Stream<Arguments> simulations() {
        final List<Integer> poolSizes =
                IntStream.range(SOURCE_RECORDS_PER_TICK, 10 * SOURCE_RECORDS_PER_TICK)
                        .filter(poolSize -> poolSize % 10 == 0)
                        .boxed()
                        .collect(Collectors.toList());
        final List<Integer> capacities =
                IntStream.range(SOURCE_RECORDS_PER_TICK, 10 * SOURCE_RECORDS_PER_TICK)
                        .filter(capacity -> capacity % 10 == 0)
                        .boxed()
                        .collect(Collectors.toList());

        List<Arguments> arguments = new LinkedList<>();

        capacities.forEach(
                capacity -> poolSizes.forEach(poolSize -> arguments.add(Arguments.of(capacity, poolSize))));

        return arguments.stream();
    }

    @ParameterizedTest
    @MethodSource("simulations")
    void simulate(int operatorCapacity, int poolSize) {
        for (int i = 0; i < MAX_ITERATIONS_PER_SIMULATION; ++i) {
            final Simulation simulation = new Simulation(operatorCapacity, poolSize, i);
            simulation.run();
            results
                    .computeIfAbsent(
                            operatorCapacity + ":" + poolSize,
                            key -> new ArrayList<>(MAX_ITERATIONS_PER_SIMULATION))
                    .add(simulation.output.size());
            if (VERBOSE) {
                log(simulation);
            }
        }
    }

    private void log(Simulation simulation) {
        final String inputs =
                simulation.input.stream()
                        .map(SimulationRecord::getId)
                        .map(Objects::toString)
                        .collect(Collectors.joining(","));
        final String queued =
                simulation.operator.keySet().stream()
                        .map(Objects::toString)
                        .collect(Collectors.joining(","));
        final String pooled =
                simulation.pool.keySet().stream().map(Objects::toString).collect(Collectors.joining(","));
        final String outputs =
                simulation.output.stream()
                        .map(SimulationRecord::getId)
                        .map(Objects::toString)
                        .collect(Collectors.joining(","));

        log.debug(
                "Results for {}:{}:\n{}\n{}\n{}\n{}",
                simulation.operatorCapacity,
                simulation.poolSize,
                String.format(Locale.ENGLISH, "inputs: %s%n", inputs),
                String.format(Locale.ENGLISH, "queued: %s%n", queued),
                String.format(Locale.ENGLISH, "pooled: %s%n", pooled),
                String.format(Locale.ENGLISH, "outputs: %s%n", outputs));
    }

    static class Simulation implements Runnable, Comparable<Simulation> {
        final Deque<SimulationRecord> input = new LinkedList<>();
        final NavigableMap<Integer, SimulationRecord> operator = new TreeMap<>();
        final Map<Integer, SimulationRecord> pool = new HashMap<>();

        final Deque<SimulationRecord> output = new LinkedList<>();

        final Random random;

        final int operatorCapacity;
        final int poolSize;

        Simulation(int operatorCapacity, int poolSize, long seed) {
            this.operatorCapacity = operatorCapacity;
            this.poolSize = poolSize;
            this.random = new Random(seed);
        }

        @Override
        public int compareTo(@NotNull ThroughputSimulationTest.Simulation o) {
            return Integer.compare(output.size(), o.output.size());
        }

        @Override
        public void run() {
            for (int id = 0; id < MAX_RECORDS; ++id) {
                input.addFirst(new SimulationRecord(id));
                if (id % SOURCE_RECORDS_PER_TICK == 0) {
                    flush();
                    fill();
                }
            }
        }

        private void flush() {
            final Iterator<SimulationRecord> poolRecords = pool.values().iterator();
            while (poolRecords.hasNext()) {
                final SimulationRecord poolRecord = poolRecords.next();
                final boolean shouldContinue = simulateProgress(poolRecord);
                if (!shouldContinue || poolRecord.isDone()) {
                    poolRecords.remove();
                }
            }

            for (Iterator<Integer> operatorKeys = operator.keySet().iterator();
                    operatorKeys.hasNext(); ) {
                Integer id = operatorKeys.next();
                final SimulationRecord operatorRecord = operator.get(id);
                final Entry<Integer, SimulationRecord> predecessor = operator.lowerEntry(id);
                if (operatorRecord.isDone()
                        && (!ORDERED || (predecessor == null || predecessor.getValue().isDone()))) {
                    output.addFirst(operator.get(id));
                    operatorKeys.remove();
                } else {
                    break;
                }
            }
        }

        private void fill() {
            while (operator.size() < operatorCapacity && !input.isEmpty()) {
                final SimulationRecord inputRecord = input.removeLast();
                operator.put(inputRecord.id, inputRecord);
            }

            final Iterator<SimulationRecord> operatorRecords = operator.values().iterator();
            while (pool.size() < poolSize && operatorRecords.hasNext()) {
                final SimulationRecord operatorRecord = operatorRecords.next();
                if (!operatorRecord.isDone() && operatorRecord.penalty == 0) {
                    pool.put(operatorRecord.id, operatorRecord);
                } else {
                    operatorRecord.penalty = Math.max(0, operatorRecord.penalty - 1);
                }
            }
        }

        private boolean simulateProgress(SimulationRecord poolRecord) {
            float increment = 0;
            if (poolRecord.attempt < MAX_ATTEMPTS) {
                final float retryProbability = random.nextFloat();
                if (retryProbability <= RETRY_PROBABILITY) {
                    ++poolRecord.attempt;
                    poolRecord.progress = 0;
                    ++poolRecord.penalty;
                    return VERBOSE;
                } else {
                    increment += MIN_PROGRESS + random.nextFloat();
                }
            } else {
                increment = 1;
            }
            poolRecord.progress(increment);
            return true;
        }
    }

    @Data
    @RequiredArgsConstructor
    static class SimulationRecord implements Comparable<SimulationRecord> {
        final int id;

        @EqualsAndHashCode.Exclude float progress;

        @EqualsAndHashCode.Exclude int attempt;

        @EqualsAndHashCode.Exclude int penalty;

        @Override
        public int compareTo(SimulationRecord o) {
            return Integer.compare(id, o.id);
        }

        void progress(float increment) {
            progress = Math.min(1, progress + increment);
        }

        boolean isDone() {
            return progress >= 1;
        }
    }
}
