package org.wikimedia.discovery.cirrus.updater.consumer.graph;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.wikimedia.discovery.cirrus.updater.common.wiremock.WireMockExtensionUtilities.getWireMockExtension;

import java.io.IOException;
import java.time.Duration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import org.apache.flink.api.connector.sink2.Sink;
import org.apache.flink.api.connector.sink2.Sink.InitContext;
import org.apache.flink.connector.base.sink.writer.config.AsyncSinkWriterConfiguration;
import org.apache.flink.connector.base.sink.writer.config.AsyncSinkWriterConfiguration.AsyncSinkWriterConfigurationBuilder;
import org.apache.flink.metrics.Gauge;
import org.apache.flink.metrics.groups.SinkWriterMetricGroup;
import org.apache.flink.streaming.runtime.tasks.TestProcessingTimeService;
import org.apache.http.HttpHost;
import org.elasticsearch.ElasticsearchStatusException;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Answers;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.wikimedia.discovery.cirrus.updater.common.model.EventDataStreamUtilities;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateEvent;
import org.wikimedia.discovery.cirrus.updater.consumer.graph.AsyncElasticsearchBulkRequestBridge.AsyncElasticsearchBulkRequestBridgeFactory;

import com.github.tomakehurst.wiremock.client.ResponseDefinitionBuilder;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.junit5.WireMockExtension;
import com.github.tomakehurst.wiremock.stubbing.StubMapping;

@ExtendWith(MockitoExtension.class)
class AsyncElasticsearchWriterTest {

    @RegisterExtension static final WireMockExtension wireMockExtension = getWireMockExtension();
    static final Set<String> EVENT_IDS =
            Set.of(
                    "update-page",
                    "rerender-page",
                    "redirect-update-add",
                    "redirect-update-remove",
                    "delete-page",
                    "fetch-failure-simulation",
                    "with-weighted-tags",
                    "trigger-document-not-found-exception",
                    "trigger-index-not-found-exception");

    static final String ELASTICSEARCH_PREFIX = "/elasticsearch";

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    Sink.InitContext context;

    TestProcessingTimeService testProcessingTimeService = new TestProcessingTimeService();
    Map<String, Gauge<Long>> gauges = new HashMap<>();

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    SinkWriterMetricGroup sinkWriterMetricGroup;

    @BeforeEach
    public void initMock() {
        when(context.getProcessingTimeService()).thenReturn(testProcessingTimeService);
        when(context.metricGroup()).thenReturn(sinkWriterMetricGroup);
        when(sinkWriterMetricGroup.gauge(any(), any()))
                .thenAnswer(
                        i -> {
                            gauges.put(i.getArgument(0), i.getArgument(1));
                            return i.getArgument(1);
                        });
    }

    @Test
    void getSizeInBytes() throws IOException {
        try (TestWriter writer = createWriter()) {
            AsyncElasticsearchBulkRequestConverterTest.actions()
                    .forEach(
                            action -> {
                                final long sizeInBytes = writer.getSizeInBytes(action);
                                assertThat(sizeInBytes).isPositive();
                            });
        }
    }

    @Test
    void timeout() throws IOException {
        try (TestWriter writer = createWriter()) {

            final StubMapping elasticsearchBulkMapping =
                    wireMockExtension.getSingleStubMapping(
                            UUID.fromString("fd14a29d-70d5-4b5f-be09-e8c1ab03075b"));

            assertThat(elasticsearchBulkMapping.getName()).isEqualTo("elasticsearch__bulk");

            wireMockExtension.stubFor(
                    WireMock.post(WireMock.urlPathEqualTo(ELASTICSEARCH_PREFIX + "/_bulk"))
                            .inScenario("elasticsearch-state")
                            .whenScenarioStateIs("red")
                            .willReturn(
                                    ResponseDefinitionBuilder.like(elasticsearchBulkMapping.getResponse())
                                            .withFixedDelay(5000))
                            .willSetStateTo("green"));

            wireMockExtension.stubFor(
                    WireMock.post(WireMock.urlPathEqualTo(ELASTICSEARCH_PREFIX + "/_bulk"))
                            .inScenario("elasticsearch-state")
                            .whenScenarioStateIs("green")
                            .willReturn(
                                    ResponseDefinitionBuilder.like(elasticsearchBulkMapping.getResponse())
                                            .withFixedDelay(0)));

            wireMockExtension.setScenarioState("elasticsearch-state", "red");

            final List<AsyncElasticsearchBulkRequest> mappedEvents = getMappedEvents();
            writer.submitRequestEntries(mappedEvents);

            assertThat(writer.outcome).succeedsWithin(Duration.ofSeconds(15)).isEqualTo(List.of());
            verify(writer.retry, times(1)).accept(mappedEvents);
        }
    }

    @Test
    void metrics() throws Exception {
        try (TestWriter writer = createWriter()) {
            assertThat(gauges.get("lastBatchMinLag").getValue()).isEqualTo(0L);
            assertThat(gauges.get("lastBatchMaxLag").getValue()).isEqualTo(0L);
            assertThat(gauges.get("lastBatchMedianLag").getValue()).isEqualTo(0L);
            final StubMapping elasticsearchBulkMapping =
                    wireMockExtension.getSingleStubMapping(
                            UUID.fromString("fd14a29d-70d5-4b5f-be09-e8c1ab03075b"));

            wireMockExtension.stubFor(
                    WireMock.post(WireMock.urlPathEqualTo(ELASTICSEARCH_PREFIX + "/_bulk"))
                            .willReturn(
                                    ResponseDefinitionBuilder.like(elasticsearchBulkMapping.getResponse())
                                            .withFixedDelay(0)));

            final List<AsyncElasticsearchBulkRequest> mappedEvents = getMappedEvents();
            writer.submitRequestEntries(mappedEvents);

            assertThat(writer.outcome).succeedsWithin(Duration.ofSeconds(15)).isEqualTo(List.of());
            testProcessingTimeService.setCurrentTime(1700655863813L);
            assertThat(gauges.get("lastBatchMinLag").getValue()).isEqualTo(0L);
            assertThat(gauges.get("lastBatchMaxLag").getValue()).isEqualTo(31025266465L);
            assertThat(gauges.get("lastBatchMedianLag").getValue()).isEqualTo(12500663813L);
        }
    }

    @ParameterizedTest
    @ValueSource(booleans = {true, false})
    void failsAfterMaxRetries(boolean withFixedDelay) throws IOException {
        try (TestWriter writer = createWriter()) {
            final ResponseDefinitionBuilder responseDefinitionBuilder = WireMock.serviceUnavailable();
            if (withFixedDelay) {
                responseDefinitionBuilder.withFixedDelay(2000);
            }
            wireMockExtension.stubFor(
                    WireMock.post(WireMock.urlPathEqualTo(ELASTICSEARCH_PREFIX + "/_bulk"))
                            .willReturn(responseDefinitionBuilder));

            final List<AsyncElasticsearchBulkRequest> mappedEvents = getMappedEvents();
            writer.submitRequestEntries(mappedEvents);

            assertThat(writer.outcome).failsWithin(Duration.ofSeconds(15));
            assertThat(mappedEvents)
                    .describedAs(() -> "Unexpected number of retries")
                    .allMatch(mappedEvent -> mappedEvent.getAttempt() == 3);
            verify(writer.retry, times(2)).accept(mappedEvents);
        }
    }

    @Test
    void failsWithoutRetry() throws IOException {
        try (TestWriter writer = createWriter()) {
            wireMockExtension.stubFor(
                    WireMock.post(WireMock.urlPathEqualTo(ELASTICSEARCH_PREFIX + "/_bulk"))
                            .willReturn(WireMock.serverError()));

            final List<AsyncElasticsearchBulkRequest> mappedEvents = getMappedEvents();
            writer.submitRequestEntries(mappedEvents);

            assertThat(writer.outcome)
                    .failsWithin(Duration.ofSeconds(1))
                    .withThrowableOfType(ExecutionException.class)
                    .withCauseInstanceOf(ElasticsearchStatusException.class);

            assertThat(mappedEvents).allMatch(mappedEvent -> mappedEvent.getAttempt() == 0);
            verify(writer.retry, never()).accept(mappedEvents);
        }
    }

    List<AsyncElasticsearchBulkRequest> getMappedEvents() throws IOException {
        return AsyncElasticsearchBulkRequestConverterTest.actions()
                .filter(updateEvent -> EVENT_IDS.contains(updateEvent.getId()))
                .collect(Collectors.toList());
    }

    TestWriter createWriter() {
        final AsyncSinkWriterConfigurationBuilder writerConfigBuilder =
                new AsyncSinkWriterConfigurationBuilder();
        writerConfigBuilder.setMaxRecordSizeInBytes(4096 * 1024);
        writerConfigBuilder.setMaxBatchSizeInBytes(4096 * 1024);
        writerConfigBuilder.setMaxBatchSize(100);
        writerConfigBuilder.setMaxInFlightRequests(1);
        writerConfigBuilder.setMaxBufferedRequests(1000);
        writerConfigBuilder.setMaxTimeInBufferMS(300000);

        return new TestWriter(context, writerConfigBuilder.build(), this::createBridge);
    }

    CirrusBulkRequestBridge createBridge(
            AsyncElasticsearchBulkRequestBridgeFactory.InitContext initContext) {
        final AsyncElasticsearchClientConfig clientConfig =
                AsyncElasticsearchClientConfig.builder()
                        .hosts(List.of(HttpHost.create(wireMockExtension.baseUrl())))
                        .connectionPathPrefix(ELASTICSEARCH_PREFIX)
                        .connectionRequestTimeout(Duration.ofSeconds(1))
                        .connectionTimeout(Duration.ofSeconds(1))
                        .socketTimeout(Duration.ofSeconds(1))
                        .build();
        return new CirrusBulkRequestBridge(clientConfig, initContext.metricGroup().addGroup("bulk")) {

            @Override
            protected void onResponse(
                    List<AsyncElasticsearchBulkRequest> batch,
                    BulkRequest bulkRequest,
                    BulkResponse bulkResponse,
                    BatchFuture batchFuture) {
                batchFuture.complete();
            }

            @Override
            protected void onFailure(
                    List<AsyncElasticsearchBulkRequest> batch,
                    BulkRequest bulkRequest,
                    Exception exception,
                    BatchFuture batchFuture) {
                if (CirrusBulkRequestBridge.RETRYABLE_EXCEPTIONS.values().stream()
                        .anyMatch(predicate -> predicate.test(exception))) {
                    final long attempt =
                            batch.stream()
                                    .mapToInt(AsyncElasticsearchBulkRequest::incrementAndGetAttempt)
                                    .max()
                                    .orElse(0);

                    if (attempt <= 2) {
                        batchFuture.completeAndRetry(batch, Duration.ZERO);
                        return;
                    }
                }

                batchFuture.completeExceptionally(exception);
            }
        };
    }

    static class TestWriter extends AsyncElasticsearchWriter<UpdateEvent> {

        final CompletableFuture<List<AsyncElasticsearchBulkRequest>> outcome =
                new CompletableFuture<>();
        final Consumer<List<AsyncElasticsearchBulkRequest>> retry =
                spy(
                        new Consumer<List<AsyncElasticsearchBulkRequest>>() {
                            @Override
                            public void accept(List<AsyncElasticsearchBulkRequest> toBeRetried) {
                                if (toBeRetried.isEmpty()) {
                                    outcome.complete(toBeRetried);
                                }
                                submitRequestEntries(toBeRetried, this);
                            }
                        });

        TestWriter(
                InitContext context,
                AsyncSinkWriterConfiguration writerConfiguration,
                AsyncElasticsearchBulkRequestBridgeFactory
                        bridgeAsyncElasticsearchBulkRequestBridgeFactory) {
            super(
                    new AsyncElasticsearchBulkRequestConverter(
                            EventDataStreamUtilities.buildUpdateTypeInfo(),
                            EventDataStreamUtilities.OBJECT_MAPPER),
                    context,
                    writerConfiguration,
                    bridgeAsyncElasticsearchBulkRequestBridgeFactory,
                    List.of());
        }

        public void submitRequestEntries(List<AsyncElasticsearchBulkRequest> list) {
            super.submitRequestEntries(list, retry);
        }

        @Override
        protected Consumer<Exception> getFatalExceptionCons() {
            return outcome::completeExceptionally;
        }
    }
}
