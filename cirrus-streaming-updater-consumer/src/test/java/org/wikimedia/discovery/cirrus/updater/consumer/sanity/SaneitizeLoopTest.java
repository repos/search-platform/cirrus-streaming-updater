package org.wikimedia.discovery.cirrus.updater.consumer.sanity;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.within;
import static org.assertj.core.api.Assertions.withinPercentage;

import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Queue;
import java.util.Set;

import org.apache.flink.metrics.Counter;
import org.apache.flink.metrics.Gauge;
import org.apache.flink.metrics.testutils.MetricListener;
import org.apache.flink.runtime.metrics.NoOpMetricRegistry;
import org.apache.flink.runtime.metrics.groups.MetricGroupTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.wikimedia.discovery.cirrus.updater.common.model.SanityCheck;
import org.wikimedia.discovery.cirrus.updater.consumer.sanity.SaneitizeLoop.Metrics;

@ExtendWith(MockitoExtension.class)
class SaneitizeLoopTest {
    static final String DOMAIN = "junitwiki";
    final Instant now = Instant.parse("2024-01-01T00:00:00Z");
    final Instant farFuture = Instant.parse("2030-01-01T00:00:00Z");
    final Duration loopDuration = Duration.ofDays(1);
    // By using maxPageId = loopDuration each page represents one second. A batch size
    // of 60 represents one minute. But keep in mind we run 1% faster.
    final long maxPageId = loopDuration.toSeconds();
    final int batchSize = 60;

    @Test
    void basicOperation() {
        LoopState initialLoopState = new LoopState(DOMAIN, maxPageId, now);
        SaneitizeLoop loop =
                new SaneitizeLoop(
                        List.of(initialLoopState),
                        new MetricGroupTest.DummyAbstractMetricGroup(new NoOpMetricRegistry()),
                        () -> Optional.of(Collections.singleton(DOMAIN)),
                        x -> Optional.empty(),
                        loopDuration,
                        batchSize,
                        null);
        loop.setNextWikiReconciliation(farFuture);

        List<SanitySourceRecord> seen = new ArrayList<>();
        // First run through should trigger the wiki
        Instant initialInvokeAt = loop.nextInvokeAt();
        boolean completed = loop.next(initialInvokeAt, seen::add);
        assertThat(completed).isFalse();
        LoopState expected = initialLoopState.emitNextBatch(initialInvokeAt, batchSize);
        assertThat(seen)
                .containsExactly(
                        SanitySourceRecord.forNextState(
                                expected, new SanityCheck(DOMAIN, 0, batchSize, initialInvokeAt, 0)));
        Instant sleepUntil = loop.nextInvokeAt();
        assertThat(sleepUntil).isCloseTo("2024-01-01T00:01:00.00Z", within(1, ChronoUnit.SECONDS));

        // Re-running the same instant should do nothing
        seen.clear();
        loop.next(initialInvokeAt, seen::add);
        assertThat(seen).isEmpty();

        // Running just before the declared time should do nothing
        loop.next(sleepUntil.minusSeconds(1), seen::add);
        assertThat(seen).isEmpty();

        // Running at the declared time should emit the next batch
        loop.next(sleepUntil, seen::add);
        expected = expected.emitNextBatch(sleepUntil, batchSize);
        assertThat(seen)
                .containsExactly(
                        SanitySourceRecord.forNextState(
                                expected, new SanityCheck(DOMAIN, batchSize, batchSize, sleepUntil, 0)));
    }

    @Test
    void testLongDelay() {
        LoopState initialLoopState = new LoopState(DOMAIN, maxPageId, now);
        SaneitizeLoop loop =
                new SaneitizeLoop(
                        List.of(initialLoopState),
                        new MetricGroupTest.DummyAbstractMetricGroup(new NoOpMetricRegistry()),
                        () -> Optional.of(Collections.singleton(DOMAIN)),
                        x -> Optional.empty(),
                        loopDuration,
                        batchSize,
                        null);
        loop.setNextWikiReconciliation(farFuture);

        // Running long after the next expected time should emit a standard batch.
        Instant nextInvokeAt = loop.nextInvokeAt().plusSeconds(180);
        List<SanitySourceRecord> seen = new ArrayList<>();
        loop.next(nextInvokeAt, seen::add);
        LoopState expected = initialLoopState.emitNextBatch(nextInvokeAt, batchSize);
        assertThat(seen)
                .hasSize(1)
                .contains(
                        SanitySourceRecord.forNextState(
                                expected, new SanityCheck(DOMAIN, 0, batchSize, nextInvokeAt, 0)));
        assertThat(loop.nextInvokeAt())
                .isCloseTo("2024-01-01T00:04:00Z", within(1, ChronoUnit.SECONDS));
    }

    @Test
    void testWikiReconciliationFailure() {
        Queue<Optional<Set<String>>> fetchResponses = new LinkedList<>();
        fetchResponses.add(Optional.empty());
        fetchResponses.add(Optional.of(Collections.singleton(DOMAIN)));

        SaneitizeLoop loop =
                new SaneitizeLoop(
                        Collections.emptyList(),
                        new MetricGroupTest.DummyAbstractMetricGroup(new NoOpMetricRegistry()),
                        fetchResponses::poll,
                        x -> Optional.of(maxPageId),
                        loopDuration,
                        batchSize,
                        null);

        // First run through, wiki fetch fails and no wikis are added.
        // No exceptions are thrown and we proceed as normal.
        List<SanitySourceRecord> seen = new ArrayList<>();
        loop.next(now, seen::add);
        assertThat(seen).isEmpty();
        Instant nextAfterFailure = now.plus(SaneitizeLoop.RECONCILIATION_FREQUENCY_ON_ERROR);
        assertThat(loop.nextInvokeAt()).isEqualTo(nextAfterFailure);
        // Second time through fetch works and domains are added to state
        loop.next(nextAfterFailure, seen::add);
        LoopState initialLoopState = new LoopState(DOMAIN, maxPageId, nextAfterFailure);
        assertThat(seen)
                .containsExactly(
                        // New wiki gets added
                        SanitySourceRecord.forNextState(initialLoopState, null),
                        // First event generation of new wiki
                        SanitySourceRecord.forNextState(
                                initialLoopState.emitNextBatch(nextAfterFailure, batchSize),
                                new SanityCheck(DOMAIN, 0, batchSize, nextAfterFailure, 0)));
    }

    @Test
    void testRemoveWiki() {
        final String keepDomain = "keepme.local";
        SaneitizeLoop loop =
                new SaneitizeLoop(
                        List.of(
                                new LoopState(DOMAIN, maxPageId, now), new LoopState(keepDomain, maxPageId, now)),
                        new MetricGroupTest.DummyAbstractMetricGroup(new NoOpMetricRegistry()),
                        () -> Optional.of(Collections.singleton(keepDomain)),
                        x -> Optional.of(maxPageId),
                        loopDuration,
                        batchSize,
                        null);

        List<SanitySourceRecord> seen = new ArrayList<>();
        loop.next(now, seen::add);
        // It also contains a SanityCheck event generation for keepDomain, but it's not important
        assertThat(seen).contains(SanitySourceRecord.removeFromState(DOMAIN));
    }

    @Test
    void testLoopReset() {
        LoopState finished = (new LoopState(DOMAIN, maxPageId, now)).emitNextBatch(now, maxPageId + 1);
        assertThat(finished.isFinished()).isTrue();
        SaneitizeLoop loop =
                new SaneitizeLoop(
                        Collections.singletonList(finished),
                        new MetricGroupTest.DummyAbstractMetricGroup(new NoOpMetricRegistry()),
                        () -> Optional.of(Collections.singleton(DOMAIN)),
                        x -> Optional.of(maxPageId),
                        loopDuration,
                        batchSize,
                        null);
        loop.setNextWikiReconciliation(farFuture);

        Instant nextInvokeAt = loop.nextInvokeAt();
        assertThat(nextInvokeAt).isEqualTo(now.plus(loopDuration));
        List<SanitySourceRecord> seen = new ArrayList<>();
        loop.next(nextInvokeAt, seen::add);
        assertThat(seen)
                .containsExactly(
                        SanitySourceRecord.forNextState(
                                finished.resetForNextLoop(nextInvokeAt).emitNextBatch(nextInvokeAt, batchSize),
                                new SanityCheck(DOMAIN, 0, batchSize, nextInvokeAt, 1)));
    }

    @Test
    void testNextInvokeAtSubSecondPrecision() {
        final long maxPageId = 1_000_000_000;
        LoopState initialLoopState = new LoopState(DOMAIN, maxPageId, now);
        SaneitizeLoop loop =
                new SaneitizeLoop(
                        List.of(initialLoopState),
                        new MetricGroupTest.DummyAbstractMetricGroup(new NoOpMetricRegistry()),
                        () -> Optional.of(Collections.singleton(DOMAIN)),
                        x -> Optional.of(maxPageId),
                        Duration.ofDays(14),
                        batchSize,
                        null);
        loop.setNextWikiReconciliation(farFuture);

        assertThat(Duration.between(now, loop.nextInvokeAt())).isLessThan(Duration.ofMillis(100));
    }

    @Test
    void testMaxPageIdRefresh() {
        // Force a refresh by setting last refresh to Instant.MIN
        LoopState initialLoopState =
                new LoopState(DOMAIN, maxPageId, now).refreshMaxPageId(Instant.MIN, maxPageId);
        final long updatedMaxPageId = maxPageId + 10_000;

        SaneitizeLoop loop =
                new SaneitizeLoop(
                        Collections.singletonList(initialLoopState),
                        new MetricGroupTest.DummyAbstractMetricGroup(new NoOpMetricRegistry()),
                        () -> Optional.of(Collections.singleton(DOMAIN)),
                        x -> Optional.of(updatedMaxPageId),
                        loopDuration,
                        batchSize,
                        null);
        loop.setNextWikiReconciliation(farFuture);

        List<SanitySourceRecord> seen = new ArrayList<>();
        Instant nextInvokeAt = loop.nextInvokeAt();
        loop.next(nextInvokeAt, seen::add);
        assertThat(seen)
                .containsExactly(
                        SanitySourceRecord.forNextState(
                                initialLoopState
                                        .refreshMaxPageId(nextInvokeAt, updatedMaxPageId)
                                        .emitNextBatch(nextInvokeAt, batchSize),
                                new SanityCheck(DOMAIN, 0, batchSize, nextInvokeAt, 0)));
    }

    @Test
    void testMaxPageIdRefreshFailure() {
        // Set last refresh to MIN to force a refresh on next event
        LoopState initialLoopState =
                new LoopState(DOMAIN, maxPageId, now).refreshMaxPageId(Instant.MIN, maxPageId);
        final long updatedMaxPageId = maxPageId + 200;

        Queue<Optional<Long>> lookupResults = new LinkedList<>();
        lookupResults.add(Optional.empty());
        lookupResults.add(Optional.of(updatedMaxPageId));
        SaneitizeLoop loop =
                new SaneitizeLoop(
                        Collections.singletonList(initialLoopState),
                        new MetricGroupTest.DummyAbstractMetricGroup(new NoOpMetricRegistry()),
                        () -> Optional.of(Collections.singleton(DOMAIN)),
                        x -> lookupResults.poll(),
                        loopDuration,
                        batchSize,
                        null);
        loop.setNextWikiReconciliation(farFuture);

        List<SanitySourceRecord> seen = new ArrayList<>();
        Instant nextInvokeAt = loop.nextInvokeAt();
        assertThat(nextInvokeAt).isNotEqualTo(Instant.MIN);

        // Lookup fails, normal event generation continues.
        loop.next(nextInvokeAt, seen::add);
        assertThat(lookupResults).hasSize(1);
        assertThat(seen).hasSize(1);
        LoopState nextLoopState = initialLoopState.emitNextBatch(nextInvokeAt, batchSize);
        assertThat(seen)
                .containsExactly(
                        SanitySourceRecord.forNextState(
                                nextLoopState, new SanityCheck(DOMAIN, 0, batchSize, nextInvokeAt, 0)));

        // max page id is only refreshed during normal event generation. Step forward until we expect
        // the refresh. This must not poll for max page id on each event generation.
        Instant nextExpectedMaxPageIdRefresh =
                now.plus(SaneitizeLoop.RECONCILIATION_FREQUENCY_ON_ERROR);
        nextInvokeAt = loop.nextInvokeAt();
        while (nextInvokeAt.isBefore(nextExpectedMaxPageIdRefresh)) {
            loop.next(nextInvokeAt, x -> {});
            nextLoopState = nextLoopState.emitNextBatch(nextInvokeAt, batchSize);
            assertThat(lookupResults).hasSize(1);
            nextInvokeAt = loop.nextInvokeAt();
        }

        // Successfull fetch
        seen.clear();
        loop.next(nextInvokeAt, seen::add);
        assertThat(lookupResults).isEmpty();
        assertThat(seen)
                .containsExactly(
                        SanitySourceRecord.forNextState(
                                nextLoopState
                                        .refreshMaxPageId(nextInvokeAt, updatedMaxPageId)
                                        .emitNextBatch(nextInvokeAt, batchSize),
                                new SanityCheck(
                                        DOMAIN, nextLoopState.getLastEmittedPageId() + 1, batchSize, nextInvokeAt, 0)));
    }

    @Test
    void metricsArePlausible() {
        LoopState loopState = new LoopState(DOMAIN, maxPageId, now);
        MetricListener metricListener = new MetricListener();
        SaneitizeLoop loop =
                new SaneitizeLoop(
                        Collections.singletonList(loopState),
                        metricListener.getMetricGroup(),
                        () -> Optional.of(Collections.singleton(DOMAIN)),
                        x -> Optional.of(maxPageId),
                        Duration.ofDays(14),
                        batchSize,
                        null);
        loop.setNextWikiReconciliation(farFuture);

        Optional<Counter> numEmitted =
                metricListener.getCounter("domain", DOMAIN, Metrics.NUM_EMITTED_EVENTS_METRIC_GROUP);
        assertThat(numEmitted).isPresent();
        Optional<Gauge<Long>> lateness =
                metricListener.getGauge("domain", DOMAIN, Metrics.EVENT_LATENESS_METRIC_GROUP);
        assertThat(lateness).isPresent();
        Optional<Gauge<Float>> completion =
                metricListener.getGauge("domain", DOMAIN, Metrics.LOOP_COMPLETION_METRIC_GROUP);
        assertThat(completion).isPresent();

        assertThat(numEmitted.get().getCount()).isZero();
        assertThat(lateness.get().getValue()).isNull();
        assertThat(completion.get().getValue()).isZero();

        loop.next(loop.nextInvokeAt(), x -> {});

        assertThat(numEmitted.get().getCount()).isEqualTo(1L);
        assertThat(lateness.get().getValue()).isZero();
        float percentagePerBatch = batchSize / (float) maxPageId;
        // Not entirely sure why these are ~1.5% off. Expected an epsilon but not this large.
        assertThat(completion.get().getValue()).isCloseTo(percentagePerBatch, withinPercentage(2));

        // As expected the lateness declines gradually over time due to the 1% speedup.
        final long expectedSpeedup = -8L;
        loop.next(loop.nextInvokeAt(), x -> {});
        assertThat(numEmitted.get().getCount()).isEqualTo(2L);
        assertThat(lateness.get().getValue()).isEqualTo(expectedSpeedup);
        assertThat(completion.get().getValue()).isCloseTo(2 * percentagePerBatch, withinPercentage(2));

        // If event generation is delayed, lateness increases to match. The speedup effect is constant.
        final long delay = 600;
        loop.next(loop.nextInvokeAt().plusSeconds(delay), x -> {});
        assertThat(numEmitted.get().getCount()).isEqualTo(3L);
        assertThat(lateness.get().getValue()).isEqualTo(delay + 2 * expectedSpeedup);
        assertThat(completion.get().getValue()).isCloseTo(3 * percentagePerBatch, withinPercentage(2));
    }
}
