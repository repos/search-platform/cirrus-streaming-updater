package org.wikimedia.discovery.cirrus.updater.consumer.graph;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.Assertions.assertThatNoException;

import org.apache.flink.connector.base.sink.writer.strategy.RequestInfo;
import org.apache.flink.connector.base.sink.writer.strategy.ResultInfo;
import org.junit.jupiter.api.Test;
import org.wikimedia.discovery.cirrus.updater.consumer.graph.AsyncElasticsearchSink.MaxInFlightRequestsRateLimitingStrategy;

import lombok.AllArgsConstructor;
import lombok.Getter;

class MaxInFlightRequestsRateLimitingStrategyTest {

    @Test
    void lifecycle() {
        final MaxInFlightRequestsRateLimitingStrategy strategy =
                new MaxInFlightRequestsRateLimitingStrategy(2, 1);

        assertThat(strategy.getMaxBatchSize()).isEqualTo(2);

        final TestRequestInfo requestInfo = new TestRequestInfo(2);

        assertThat(strategy.shouldBlock(requestInfo)).isFalse();
        strategy.registerInFlightRequest(requestInfo);
        assertThat(strategy.shouldBlock(requestInfo)).isTrue();
        assertThatExceptionOfType(IllegalStateException.class)
                .isThrownBy(() -> strategy.registerInFlightRequest(requestInfo));
        strategy.registerCompletedRequest(new TestResultInfo(0, 2));
        assertThatNoException().isThrownBy(() -> strategy.registerInFlightRequest(requestInfo));
    }

    @Getter
    @AllArgsConstructor
    static class TestRequestInfo implements RequestInfo {

        final int batchSize;
    }

    @Getter
    @AllArgsConstructor
    static class TestResultInfo implements ResultInfo {
        int failedMessages;
        int batchSize;
    }
}
