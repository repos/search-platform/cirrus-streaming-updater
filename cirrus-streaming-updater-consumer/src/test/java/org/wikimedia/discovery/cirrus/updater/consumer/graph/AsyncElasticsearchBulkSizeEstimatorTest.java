package org.wikimedia.discovery.cirrus.updater.consumer.graph;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Map;

import javax.annotation.Nonnull;

import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.common.bytes.BytesReference;
import org.elasticsearch.script.Script;
import org.elasticsearch.script.ScriptType;
import org.junit.jupiter.api.Test;

class AsyncElasticsearchBulkSizeEstimatorTest {

    @Test
    void estimate() {
        final int sizeInBytes = 1024 * 200; // 200kb

        final IndexRequest indexRequest = mockIndexRequest(sizeInBytes);
        assertThat(AsyncElasticsearchBulkSizeEstimator.estimate(indexRequest))
                .isGreaterThan(sizeInBytes);

        final Map<String, Object> params =
                Map.of(
                        "handlers",
                        Map.of("redirect", "set"),
                        "source",
                        Map.of("wikitext", "_".repeat(sizeInBytes)));

        final UpdateRequest updateRequest = mockUpdateRequest(sizeInBytes, params);
        assertThat(AsyncElasticsearchBulkSizeEstimator.estimate(updateRequest))
                .isGreaterThan(sizeInBytes);

        assertThat(AsyncElasticsearchBulkSizeEstimator.estimate(mockDeleteRequest())).isPositive();
    }

    @Nonnull
    private static IndexRequest mockIndexRequest(int sourceSizeInBytes) {
        final IndexRequest indexRequest = mock(IndexRequest.class);
        final BytesReference source = mock(BytesReference.class);
        when(source.length()).thenReturn(sourceSizeInBytes);
        when(indexRequest.source()).thenReturn(source);
        return indexRequest;
    }

    @Nonnull
    private static UpdateRequest mockUpdateRequest(
            int sourceSizeInBytes, Map<String, Object> scriptParams) {
        final UpdateRequest updateRequest = mock(UpdateRequest.class);
        when(updateRequest.doc()).thenReturn(null);
        when(updateRequest.script())
                .thenReturn(
                        new Script(ScriptType.INLINE, "super_detect_noop", "super_detect_noop", scriptParams));
        final IndexRequest upsertRequest = mockIndexRequest(sourceSizeInBytes);
        when(updateRequest.upsertRequest()).thenReturn(upsertRequest);
        return updateRequest;
    }

    @Nonnull
    private static DeleteRequest mockDeleteRequest() {
        return mock(DeleteRequest.class);
    }
}
