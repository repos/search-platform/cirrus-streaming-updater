package org.wikimedia.discovery.cirrus.updater.consumer.sanity;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.time.Duration;
import java.time.Instant;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.flink.api.common.accumulators.ListAccumulator;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.sink.RichSinkFunction;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.condition.DisabledIfEnvironmentVariable;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.wikimedia.discovery.cirrus.updater.common.model.SanityCheck;
import org.wikimedia.discovery.cirrus.updater.common.model.SanityCheckTypeInfoFactory;
import org.wikimedia.discovery.cirrus.updater.consumer.config.ConsumerConfig;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;

@ExtendWith(MockitoExtension.class)
@Slf4j
class SanitySourceIT {
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    @Test
    void basicIntegration() throws Exception {
        final String domain = "testwiki.local";
        final int batchSize = 100;
        final Duration loopDuration = Duration.ofDays(1);
        // one batch per second
        final long maxPageId = loopDuration.toSeconds() * batchSize;
        final SanitySource source =
                new SanitySource(
                        (metricGroup) -> () -> Optional.of(Collections.singleton(domain)),
                        (metricGroup) -> (wiki) -> Optional.of(maxPageId),
                        Instant::now,
                        loopDuration,
                        batchSize,
                        // Maybe a better bound than realtime. Maybe end-after-loop and a very short loop?
                        Instant.now().plusSeconds(10),
                        false);

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);
        DataStream<SanityCheck> stream =
                env.fromSource(
                        source,
                        WatermarkStrategy.noWatermarks(),
                        "sanity-source-it",
                        SanityCheckTypeInfoFactory.create());
        List<SanityCheck> result = execute(env, stream);
        assertThat(result).isNotEmpty();
        assertThat(result.get(0))
                .isEqualTo(new SanityCheck(domain, 0, batchSize, result.get(0).getEventTime(), 0));
    }

    @Test
    void doesNothingWhenPaused() throws Exception {
        final SanitySource source =
                new SanitySource(
                        (metricgroup) ->
                                () -> {
                                    throw new RuntimeException("Must not query wikis");
                                },
                        (metricGroup) ->
                                (wiki) -> {
                                    throw new RuntimeException("Must not query max page id");
                                },
                        Instant::now,
                        Duration.ofDays(1),
                        100,
                        Instant.now().plusSeconds(2),
                        true);

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(2);
        DataStream<SanityCheck> stream =
                env.fromSource(
                        source,
                        WatermarkStrategy.noWatermarks(),
                        "sanity-source-it",
                        SanityCheckTypeInfoFactory.create());
        List<SanityCheck> result = execute(env, stream);
        assertThat(result).isEmpty();
    }

    @Test
    @DisabledIfEnvironmentVariable(
            named = "CI",
            matches = "true",
            disabledReason = "intended for local debugging")
    void verifyOperationWithProdMaxPageId() throws Exception {
        Map<String, Long> maxPageIds =
                read("prod-max-page-ids").properties().stream()
                        .collect(Collectors.toMap(Map.Entry::getKey, entry -> entry.getValue().asLong()));

        final SanitySource source =
                new SanitySource(
                        (metricGroup) -> () -> Optional.of(maxPageIds.keySet()),
                        (metricGroup) -> domain -> Optional.of(maxPageIds.get(domain)),
                        Instant::now,
                        ConsumerConfig.PARAM_SANEITIZER_LOOP_DURATION.defaultValue(),
                        ConsumerConfig.PARAM_SANEITIZER_BATCH_SIZE.defaultValue(),
                        Instant.now().plusSeconds(20),
                        false);

        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.setParallelism(1);
        DataStream<SanityCheck> stream =
                env.fromSource(
                        source,
                        WatermarkStrategy.noWatermarks(),
                        "something",
                        SanityCheckTypeInfoFactory.create());
        List<SanityCheck> result = execute(env, stream);
        assertThat(result).isNotEmpty();
    }

    private List<SanityCheck> execute(StreamExecutionEnvironment env, DataStream<SanityCheck> stream)
            throws Exception {
        stream.addSink(
                new RichSinkFunction<>() {
                    @Override
                    public void open(Configuration parameters) {
                        getRuntimeContext().addAccumulator("result", new ListAccumulator<SanityCheck>());
                    }

                    @Override
                    public void invoke(SanityCheck value, Context context) {
                        log.info(value.toString());
                        getRuntimeContext().getAccumulator("result").add(value);
                    }
                });

        return env.execute().getAccumulatorResult("result");
    }

    private JsonNode read(String resourceSuffix) throws IOException {
        return OBJECT_MAPPER
                .reader()
                .readTree(
                        this.getClass()
                                .getResourceAsStream(
                                        "/" + this.getClass().getSimpleName() + "." + resourceSuffix + ".json"));
    }
}
