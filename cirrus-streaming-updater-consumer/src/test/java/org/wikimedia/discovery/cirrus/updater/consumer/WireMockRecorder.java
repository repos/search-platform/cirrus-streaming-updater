package org.wikimedia.discovery.cirrus.updater.consumer;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.post;
import static com.github.tomakehurst.wiremock.client.WireMock.urlMatching;
import static org.wikimedia.discovery.cirrus.updater.common.wiremock.WireMockExtensionUtilities.getWireMockConfiguration;

import java.nio.charset.StandardCharsets;
import java.util.Scanner;
import java.util.logging.Logger;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.recording.SnapshotRecordResult;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import lombok.experimental.UtilityClass;

/**
 * A recorder for interactions with elasticsearch.
 *
 * <p>Assumes an instance of elastic search is running and is listening a {@code localhost:9200}.
 *
 * <h1>Usage</h1>
 *
 * <ol>
 *   <li>Run {@link WireMockRecorder#main(String[])}
 *   <li>Copy port of the proxy URL shown in terminal
 *   <li>In {@code LocalSettings.php}, add an entry to {@code $wgCirrusSearchServers} with {@code
 *       host} pointing to the docker host, e.g. {@code host.docker.internal} (OSX) and set {@code
 *       port} to the port copied above.
 *   <li>Run {@code docker-compose up} to launch MW (including elasticsearch) and interact with it
 *   <li>Hit any key to or {@code CTRL + C} to top the recorder.
 * </ol>
 *
 * <p>Stub definitions are stored under {@code target/test-classes/wiremock/mappings}.
 */
@UtilityClass
@SuppressWarnings("HideUtilityClassConstructor")
@SuppressFBWarnings(
        value = "HideUtilityClassConstructor",
        justification = "lombok takes care of that")
public class WireMockRecorder {

    private static final Logger LOGGER = Logger.getLogger(WireMockRecorder.class.getName());

    public static void main(String[] args) {
        final String elasticsearchHost = "localhost";
        final int elasticsearchPort = 9200;
        final String elasticsearchProxyPath = "";

        final WireMockServer wireMockServer = new WireMockServer(getWireMockConfiguration());

        Runtime.getRuntime()
                .addShutdownHook(
                        new Thread(
                                () -> {
                                    stop(wireMockServer);
                                }));

        wireMockServer.start();
        LOGGER.info(
                "Launched elasticsearch proxy at " + wireMockServer.baseUrl() + elasticsearchProxyPath);
        try (Scanner scanner = new Scanner(System.in, StandardCharsets.UTF_8)) {
            wireMockServer.stubFor(
                    post(urlMatching(elasticsearchProxyPath + ".*"))
                            .willReturn(
                                    aResponse()
                                            .proxiedFrom("http://" + elasticsearchHost + ":" + elasticsearchPort)
                                            .withProxyUrlPrefixToRemove(elasticsearchProxyPath)));
            scanner.nextLine();
        } finally {
            stop(wireMockServer);
        }
    }

    private static void stop(WireMockServer wireMockServer) {
        if (wireMockServer.isRunning()) {
            final SnapshotRecordResult snapshotRecordResult = wireMockServer.snapshotRecord();
            LOGGER.info("Recorded " + snapshotRecordResult.getStubMappings().size() + " mapping(s)");
            wireMockServer.stop();
        }
    }
}
