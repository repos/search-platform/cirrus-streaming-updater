package org.wikimedia.discovery.cirrus.updater.consumer.sanity;

import static org.assertj.core.api.Assertions.assertThat;
import static org.wikimedia.discovery.cirrus.updater.common.wiremock.WireMockExtensionUtilities.getWireMockExtension;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Optional;

import org.apache.flink.metrics.MetricGroup;
import org.apache.flink.runtime.metrics.NoOpMetricRegistry;
import org.apache.flink.runtime.metrics.groups.MetricGroupTest;
import org.apache.hc.core5.http.HttpHost;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.wikimedia.discovery.cirrus.updater.common.http.CustomRoutePlanner;
import org.wikimedia.discovery.cirrus.updater.common.http.HttpConfig;
import org.wikimedia.discovery.cirrus.updater.common.http.HttpHostMapping;
import org.wikimedia.discovery.cirrus.updater.common.http.MediaWikiHttpClient;
import org.wikimedia.discovery.cirrus.updater.common.util.function.SerializableFunction;

import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.junit5.WireMockExtension;
import com.github.tomakehurst.wiremock.junit5.WireMockRuntimeInfo;

class SiteInfoMaxPageIdLookupTest {
    static final String DOMAIN = "cirrustestwiki.local";
    static final int TIMEOUT_MILLIS = 1000;

    @RegisterExtension static WireMockExtension wireMockExtension = getWireMockExtension();
    private SiteInfoMaxPageIdLookup lookup;

    @BeforeEach
    void createLookup(WireMockRuntimeInfo info) throws Exception {
        MetricGroup metricGroup =
                new MetricGroupTest.DummyAbstractMetricGroup(new NoOpMetricRegistry());
        Duration requestTimeout = Duration.of(TIMEOUT_MILLIS, ChronoUnit.MILLIS);
        Duration connectionTimeout = Duration.of(TIMEOUT_MILLIS / 2, ChronoUnit.MILLIS);
        SerializableFunction<HttpHost, Optional<HttpHost>> httpHostMapper =
                CustomRoutePlanner.createHttpHostMapper(
                        List.of(HttpHostMapping.create(DOMAIN, info.getHttpBaseUrl())));
        lookup =
                new SiteInfoMaxPageIdLookup(
                        MediaWikiHttpClient.factory(
                                        HttpConfig.builder()
                                                .maxConnections(10)
                                                .requestTimeout(requestTimeout)
                                                .connectionTimeout(connectionTimeout)
                                                .httpHostMapper(httpHostMapper)
                                                .userAgent("Test/User Agent")
                                                .rateLimitPerSecond(100)
                                                .build())
                                .apply(metricGroup));
    }

    @Test
    void testHappyPath() {
        wireMockExtension.stubFor(
                WireMock.get(WireMock.urlEqualTo(SiteInfoMaxPageIdLookup.API_PATH))
                        .withHost(WireMock.equalTo(DOMAIN))
                        .willReturn(
                                WireMock.aResponse().withBodyFile("cirrustestwiki-siteinfo.json").withStatus(200)));
        assertThat(lookup.apply(DOMAIN)).contains(1L);
    }

    @Test
    void testFailedResponse() {
        wireMockExtension.stubFor(
                WireMock.get(WireMock.urlEqualTo(SiteInfoMaxPageIdLookup.API_PATH))
                        .withHost(WireMock.equalTo(DOMAIN))
                        .willReturn(WireMock.aResponse().withStatus(500)));
        assertThat(lookup.apply(DOMAIN)).isEmpty();
    }
}
