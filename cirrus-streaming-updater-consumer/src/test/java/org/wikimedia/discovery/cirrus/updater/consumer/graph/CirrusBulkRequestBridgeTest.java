package org.wikimedia.discovery.cirrus.updater.consumer.graph;

import java.net.SocketTimeoutException;
import java.time.Duration;
import java.time.Instant;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

import org.apache.flink.metrics.MetricGroup;
import org.apache.http.HttpHost;
import org.assertj.core.api.Assertions;
import org.elasticsearch.action.bulk.BulkRequest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Answers;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.wikimedia.discovery.cirrus.updater.consumer.graph.AsyncElasticsearchBulkRequestBridge.BatchFuture;

@ExtendWith(MockitoExtension.class)
class CirrusBulkRequestBridgeTest {

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    MetricGroup metricGroup;

    static class CompletableBatchFuture implements BatchFuture {

        private final CompletableFuture<List<AsyncElasticsearchBulkRequest>> outcome =
                new CompletableFuture<>();

        @Override
        public void completeAndRetry(List<AsyncElasticsearchBulkRequest> batch, Duration delay) {
            outcome.completeAsync(
                    () -> batch, CompletableFuture.delayedExecutor(delay.toMillis(), TimeUnit.MILLISECONDS));
        }

        @Override
        public void completeExceptionally(Exception exception) {
            outcome.completeExceptionally(exception);
        }
    }

    @Test
    void timeoutIsRetried() throws Exception {
        final AsyncElasticsearchBulkRequest bulkRequestWrapper;
        final CompletableBatchFuture batchFuture;
        try (CirrusBulkRequestBridge bridge =
                new CirrusBulkRequestBridge(getClientConfig(), metricGroup)) {

            final BulkRequest bulkRequest = new BulkRequest();
            bulkRequestWrapper = new AsyncElasticsearchBulkRequest("test", 0, bulkRequest, Instant.EPOCH);

            batchFuture = new CompletableBatchFuture();
            bridge.onFailure(
                    List.of(bulkRequestWrapper), bulkRequest, new SocketTimeoutException(), batchFuture);
        }

        Assertions.assertThat(batchFuture.outcome)
                .succeedsWithin(Duration.ofSeconds(5))
                .asList()
                .containsExactly(bulkRequestWrapper);
    }

    @Test
    void timeoutFails() throws Exception {
        final CompletableBatchFuture batchFuture;
        try (CirrusBulkRequestBridge inspector =
                new CirrusBulkRequestBridge(getClientConfig(), metricGroup)) {

            final BulkRequest oldBulkRequest = new BulkRequest();
            final AsyncElasticsearchBulkRequest oldBulkRequestWrapper =
                    new AsyncElasticsearchBulkRequest("test", 0, oldBulkRequest, Instant.EPOCH);

            final BulkRequest newBulkRequest = new BulkRequest();
            final AsyncElasticsearchBulkRequest newBulkRequestWrapper =
                    new AsyncElasticsearchBulkRequest("test", 1, newBulkRequest, Instant.EPOCH);

            final SocketTimeoutException passThroughException = new SocketTimeoutException();
            batchFuture = new CompletableBatchFuture();
            inspector.onFailure(
                    List.of(oldBulkRequestWrapper, newBulkRequestWrapper),
                    new BulkRequest(),
                    passThroughException,
                    batchFuture);
        }

        Assertions.assertThat(batchFuture.outcome).failsWithin(Duration.ofSeconds(2));
    }

    private AsyncElasticsearchClientConfig getClientConfig() {
        return AsyncElasticsearchClientConfig.builder()
                .hosts(List.of(HttpHost.create("test")))
                .onFailureMaxRetries(1)
                .onFailureRetryDelay(Duration.ofSeconds(1))
                .build();
    }
}
