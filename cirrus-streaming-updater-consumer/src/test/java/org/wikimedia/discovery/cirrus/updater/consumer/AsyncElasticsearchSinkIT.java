package org.wikimedia.discovery.cirrus.updater.consumer;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.time.Duration;
import java.time.Instant;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import javax.annotation.Nonnull;

import org.apache.commons.io.FileUtils;
import org.apache.flink.api.common.state.ListState;
import org.apache.flink.api.common.state.ListStateDescriptor;
import org.apache.flink.api.common.typeinfo.BasicTypeInfo;
import org.apache.flink.api.connector.sink2.Sink;
import org.apache.flink.configuration.CheckpointingOptions;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.connector.base.sink.writer.BufferedRequestState;
import org.apache.flink.connector.base.sink.writer.ElementConverter;
import org.apache.flink.connector.base.sink.writer.config.AsyncSinkWriterConfiguration;
import org.apache.flink.connector.base.sink.writer.config.AsyncSinkWriterConfiguration.AsyncSinkWriterConfigurationBuilder;
import org.apache.flink.core.execution.JobClient;
import org.apache.flink.core.execution.SavepointFormatType;
import org.apache.flink.runtime.state.FunctionInitializationContext;
import org.apache.flink.runtime.state.FunctionSnapshotContext;
import org.apache.flink.runtime.testutils.MiniClusterResourceConfiguration;
import org.apache.flink.streaming.api.CheckpointingMode;
import org.apache.flink.streaming.api.checkpoint.CheckpointedFunction;
import org.apache.flink.streaming.api.environment.CheckpointConfig.ExternalizedCheckpointCleanup;
import org.apache.flink.streaming.api.environment.ExecutionCheckpointingOptions;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.source.SourceFunction;
import org.apache.flink.test.junit5.MiniClusterExtension;
import org.apache.http.HttpHost;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.condition.DisabledIfEnvironmentVariable;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.wikimedia.discovery.cirrus.updater.common.model.ChangeType;
import org.wikimedia.discovery.cirrus.updater.common.model.EventDataStreamUtilities;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateEvent;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateEvent.Meta;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateEvent.TargetDocument;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateEventTypeInfo;
import org.wikimedia.discovery.cirrus.updater.common.wiremock.WireMockExtensionUtilities;
import org.wikimedia.discovery.cirrus.updater.consumer.graph.AsyncElasticsearchBulkRequest;
import org.wikimedia.discovery.cirrus.updater.consumer.graph.AsyncElasticsearchBulkRequestBridge.AsyncElasticsearchBulkRequestBridgeFactory;
import org.wikimedia.discovery.cirrus.updater.consumer.graph.AsyncElasticsearchBulkRequestBridge.AsyncElasticsearchBulkRequestBridgeFactory.InitContext;
import org.wikimedia.discovery.cirrus.updater.consumer.graph.AsyncElasticsearchBulkRequestConverter;
import org.wikimedia.discovery.cirrus.updater.consumer.graph.AsyncElasticsearchClientConfig;
import org.wikimedia.discovery.cirrus.updater.consumer.graph.AsyncElasticsearchSink;
import org.wikimedia.discovery.cirrus.updater.consumer.graph.AsyncElasticsearchSink.MaxInFlightRequestsRateLimitingStrategy;
import org.wikimedia.discovery.cirrus.updater.consumer.graph.AsyncElasticsearchWriter;
import org.wikimedia.discovery.cirrus.updater.consumer.graph.CirrusBulkRequestBridge;
import org.wikimedia.eventutilities.flink.EventRowTypeInfo;

import com.github.tomakehurst.wiremock.client.CountMatchingStrategy;
import com.github.tomakehurst.wiremock.client.ResponseDefinitionBuilder;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.http.Fault;
import com.github.tomakehurst.wiremock.junit5.WireMockExtension;
import com.github.tomakehurst.wiremock.matching.EqualToPattern;
import com.github.tomakehurst.wiremock.matching.UrlPathPattern;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import lombok.extern.slf4j.Slf4j;

/**
 * Integration test to better understand all how events flow through an {@link
 * AsyncElasticsearchSink}. In particular, it should showcase how checkpoints and savepoints are
 * handled (in regard to the sink's internal buffer).
 */
@Slf4j
@DisabledIfEnvironmentVariable(
        named = "CI",
        matches = "true",
        disabledReason = "intended for local debugging")
@SuppressWarnings({"checkstyle:classfanoutcomplexity"})
class AsyncElasticsearchSinkIT {

    @RegisterExtension
    static WireMockExtension wireMockExtension = WireMockExtensionUtilities.getWireMockExtension();

    @RegisterExtension
    static MiniClusterExtension miniClusterExtension =
            new MiniClusterExtension(
                    new MiniClusterResourceConfiguration.Builder()
                            .setConfiguration(
                                    Configuration.fromMap(
                                            Map.of(
                                                    "metrics.reporters",
                                                    "prometheus",
                                                    "metrics.reporter.prometheus.class",
                                                    "org.apache.flink.metrics.prometheus.PrometheusReporter",
                                                    "metrics.reporter.prometheus.factory.class",
                                                    "org.apache.flink.metrics.prometheus.PrometheusReporterFactory")))
                            .build());

    static final String ES_INDEX_NAME = "testwiki_content";
    static final UrlPathPattern ES_URL_PATTERN = WireMock.urlPathMatching("/elasticsearch/_bulk");

    static final String ERROR_RESET_CONNECTION = "reset-connection";
    static final String ERROR_TIMEOUT = "timeout";
    /**
     * Header that holds a hint what response-error should happen.
     *
     * @see #ERROR_TIMEOUT
     * @see #ERROR_RESET_CONNECTION
     */
    static final String HEADER_TEST_ERROR = "test-error";
    /**
     * Header that holds the number of actions contained in the request body, so we can dynamically
     * respond with the correct number of actions responses.
     */
    static final String HEADER_TEST_BULK_SIZE = "test-bulk-size";

    static final EventRowTypeInfo UPDATE_EVENT_FIELDS_TYPE =
            EventDataStreamUtilities.buildUpdateTypeInfo();
    static final String TARGET_DIRECTORY_PATH =
            Objects.requireNonNull(
                                    AsyncElasticsearchSinkIT.class.getResource("/"),
                                    "unable to obtain target directory")
                            .toExternalForm()
                    + "..";

    static final boolean STOP_WITH_SAVEPOINT = false;

    static final ScheduledExecutorService SCHEDULED_EXECUTOR_SERVICE =
            Executors.newSingleThreadScheduledExecutor(
                    new ThreadFactory() {
                        int counter;

                        @Override
                        public Thread newThread(@Nonnull Runnable runnable) {
                            return new Thread(runnable, "test-scheduler-" + (counter++));
                        }
                    });

    @Test
    void lifecycle() throws Exception {
        final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        configureCheckpointing(env);

        final int numberOfSourceEvents = 50;

        final long startTime = System.currentTimeMillis() - 1000;
        final Duration sourceEventInterval = Duration.ofMillis(100);
        final Duration sourceEventBreak = Duration.ofSeconds(10);

        final int maxBatchSize = 3;
        final int maxInFlightRequests = 1;
        final int maxRecordSizeInBytes = 80;

        // This ends up less atomic than expected since the graph is serialized.
        // Hence, after each failover the batch count starts at 0.
        // However, if state is stored during a checkpoint, it picks up where we left of.
        final AtomicInteger batchCount = new AtomicInteger();

        final Duration elasticsearchSocketTimeout = Duration.ofMillis(1000);

        wireMockExtension.stubFor(
                WireMock.post(ES_URL_PATTERN)
                        .withHeader(HEADER_TEST_ERROR, new EqualToPattern(""))
                        .willReturn(
                                stubElasticsearchBulkResponse()
                                        .withFixedDelay((int) elasticsearchSocketTimeout.dividedBy(2).toMillis())));

        wireMockExtension.stubFor(
                WireMock.post(ES_URL_PATTERN)
                        .withHeader(HEADER_TEST_ERROR, new EqualToPattern(ERROR_RESET_CONNECTION))
                        .willReturn(
                                WireMock.aResponse()
                                        .withFixedDelay((int) elasticsearchSocketTimeout.dividedBy(2).toMillis())
                                        .withFault(Fault.CONNECTION_RESET_BY_PEER)
                                        .withHeader("Content-Type", "application/json")));

        wireMockExtension.stubFor(
                WireMock.post(ES_URL_PATTERN)
                        .withHeader(HEADER_TEST_ERROR, new EqualToPattern(ERROR_TIMEOUT))
                        .willReturn(
                                stubElasticsearchBulkResponse()
                                        .withFixedDelay((int) elasticsearchSocketTimeout.multipliedBy(2).toMillis())));

        final AsyncSinkWriterConfigurationBuilder writerConfigurationBuilder =
                new AsyncSinkWriterConfigurationBuilder();
        writerConfigurationBuilder.setRateLimitingStrategy(
                new MaxInFlightRequestsRateLimitingStrategy(maxBatchSize, maxInFlightRequests));
        writerConfigurationBuilder.setMaxRecordSizeInBytes(maxRecordSizeInBytes);
        writerConfigurationBuilder.setMaxBatchSizeInBytes(maxRecordSizeInBytes * maxBatchSize);
        writerConfigurationBuilder.setMaxTimeInBufferMS(20000);
        writerConfigurationBuilder.setMaxBufferedRequests(10);
        writerConfigurationBuilder.setMaxBatchSize(maxBatchSize);
        writerConfigurationBuilder.setMaxInFlightRequests(maxInFlightRequests);

        final AsyncElasticsearchClientConfig clientConfig =
                AsyncElasticsearchClientConfig.builder()
                        .hosts(
                                List.of(
                                        HttpHost.create(
                                                "localhost:" + wireMockExtension.getRuntimeInfo().getHttpPort())))
                        .connectionPathPrefix("/elasticsearch")
                        .socketTimeout(elasticsearchSocketTimeout)
                        .onFailureMaxRetries(1)
                        .onFailureRetryDelay(Duration.ofSeconds(1))
                        .build();

        env.addSource(
                        new CounterSource(
                                numberOfSourceEvents,
                                sourceEventInterval,
                                sourceEventBreak,
                                numberOfSourceEvents / 2),
                        "long-source",
                        BasicTypeInfo.LONG_TYPE_INFO)
                .map(
                        counter -> asUpdateEvent(counter, startTime, sourceEventInterval),
                        UpdateEventTypeInfo.create(UPDATE_EVENT_FIELDS_TYPE))
                .sinkTo(
                        createSink(
                                context -> new TestBulkRequestBridge(context, clientConfig, batchCount),
                                writerConfigurationBuilder.build()));

        final JobClient jobClient = env.executeAsync();

        fetchMetrics();

        if (STOP_WITH_SAVEPOINT) {
            SCHEDULED_EXECUTOR_SERVICE
                    .schedule(
                            () ->
                                    jobClient
                                            .stopWithSavepoint(
                                                    false, getDirectory("/savepoints"), SavepointFormatType.CANONICAL)
                                            .thenAccept(
                                                    savepointLocation -> log.warn("stored savepoint " + savepointLocation)),
                            2,
                            TimeUnit.SECONDS)
                    .get(1, TimeUnit.MINUTES);
        }

        Assertions.assertThat(jobClient.getJobExecutionResult()).succeedsWithin(Duration.ofMinutes(1));

        wireMockExtension.verify(
                new CountMatchingStrategy(
                        CountMatchingStrategy.GREATER_THAN_OR_EQUAL, numberOfSourceEvents / maxBatchSize),
                WireMock.postRequestedFor(ES_URL_PATTERN));
    }

    static void fetchMetrics() {
        SCHEDULED_EXECUTOR_SERVICE.scheduleAtFixedRate(
                () -> {
                    try {
                        final InputStream inputStream =
                                URI.create("http://localhost:9249").toURL().openConnection().getInputStream();
                        FileUtils.copyToFile(
                                inputStream,
                                new File(
                                        new File(URI.create(TARGET_DIRECTORY_PATH)),
                                        AsyncElasticsearchSinkIT.class.getName()
                                                + "-metrics-"
                                                + System.currentTimeMillis()
                                                + ".txt"));
                    } catch (IOException e) {
                        log.error("Failed to fetch metrics", e);
                    }
                },
                1000,
                5000,
                TimeUnit.MILLISECONDS);
    }

    static UpdateEvent asUpdateEvent(long counter, long startTime, Duration pause) {
        long dt = startTime + counter * pause.toMillis();
        return UpdateEvent.builder()
                .meta(
                        Meta.builder()
                                .domain("test")
                                .id("counter-" + counter)
                                .dt(Instant.ofEpochMilli(dt))
                                .build())
                .changeType(ChangeType.PAGE_DELETE)
                .revId(1L)
                .targetDocument(
                        TargetDocument.builder()
                                .clusterGroup("any")
                                .indexName(ES_INDEX_NAME)
                                .domain("test")
                                .pageNamespace(1L)
                                .pageId(1L)
                                .pageTitle("Test")
                                .build())
                .build();
    }

    static void configureCheckpointing(StreamExecutionEnvironment env) throws IOException {
        // start a checkpoint every 1000 ms
        env.enableCheckpointing(1000);

        // advanced options:

        // set mode to exactly-once (this is the default)
        env.getCheckpointConfig().setCheckpointingMode(CheckpointingMode.AT_LEAST_ONCE);

        // make sure 500 ms of progress happen between checkpoints
        env.getCheckpointConfig().setMinPauseBetweenCheckpoints(500);

        // checkpoints have to complete within one minute, or are discarded
        env.getCheckpointConfig().setCheckpointTimeout(60000);

        // only two consecutive checkpoint failures are tolerated
        env.getCheckpointConfig().setTolerableCheckpointFailureNumber(2);

        // allow only one checkpoint to be in progress at the same time
        env.getCheckpointConfig().setMaxConcurrentCheckpoints(1);

        // enable externalized checkpoints which are retained
        // after job cancellation
        env.getCheckpointConfig()
                .setExternalizedCheckpointCleanup(ExternalizedCheckpointCleanup.RETAIN_ON_CANCELLATION);

        // enables the unaligned checkpoints
        // env.getCheckpointConfig().enableUnalignedCheckpoints();

        // sets the checkpoint storage where checkpoint snapshots will be written
        Configuration config = new Configuration();
        config.set(CheckpointingOptions.CHECKPOINT_STORAGE, "filesystem");

        config.set(CheckpointingOptions.CHECKPOINTS_DIRECTORY, getDirectory("/checkpoints"));
        config.set(ExecutionCheckpointingOptions.ENABLE_CHECKPOINTS_AFTER_TASKS_FINISH, true);

        env.configure(config);
    }

    static String getDirectory(String suffix) throws IOException {
        final String path = TARGET_DIRECTORY_PATH + suffix;

        final File directory = new File(URI.create(path));
        if (directory.exists()) {
            FileUtils.deleteDirectory(directory);
        }
        return path;
    }

    static AsyncElasticsearchSink<UpdateEvent> createSink(
            AsyncElasticsearchBulkRequestBridgeFactory bridgeAsyncElasticsearchBulkRequestBridgeFactory,
            AsyncSinkWriterConfiguration writerConfiguration) {
        return new TestSink(
                new AsyncElasticsearchBulkRequestConverter(
                        UPDATE_EVENT_FIELDS_TYPE, EventDataStreamUtilities.OBJECT_MAPPER),
                bridgeAsyncElasticsearchBulkRequestBridgeFactory,
                writerConfiguration);
    }

    /**
     * This performs templating on the body file, mainly to return the correct number of action
     * responses. In order for this to work, a special header, {@link #HEADER_TEST_BULK_SIZE} must be
     * sent.
     */
    static ResponseDefinitionBuilder stubElasticsearchBulkResponse() {
        return WireMock.aResponse()
                .withHeader("Content-Type", "application/json")
                .withBodyFile("elasticsearch-bulk-response.json.template");
    }

    @SuppressWarnings("serial")
    @SuppressFBWarnings(value = "SE_NO_SERIALVERSIONID")
    static class TestSink extends AsyncElasticsearchSink<UpdateEvent> {

        protected TestSink(
                ElementConverter<UpdateEvent, AsyncElasticsearchBulkRequest> elementConverter,
                AsyncElasticsearchBulkRequestBridgeFactory bridgeAsyncElasticsearchBulkRequestBridgeFactory,
                AsyncSinkWriterConfiguration writerConfig) {
            super(elementConverter, bridgeAsyncElasticsearchBulkRequestBridgeFactory, writerConfig);
        }

        @Override
        public StatefulSinkWriter<UpdateEvent, BufferedRequestState<AsyncElasticsearchBulkRequest>>
                createWriter(InitContext context) throws IOException {
            return new TestWriter(
                    getElementConverter(),
                    context,
                    toWriterConfiguration().build(),
                    bridgeFactory,
                    List.of());
        }

        @Override
        public StatefulSinkWriter<UpdateEvent, BufferedRequestState<AsyncElasticsearchBulkRequest>>
                restoreWriter(
                        InitContext context,
                        Collection<BufferedRequestState<AsyncElasticsearchBulkRequest>> recoveredState)
                        throws IOException {
            log.warn("SINK   - restore writer from " + summarizeBufferedState(recoveredState));
            return new TestWriter(
                    getElementConverter(),
                    context,
                    toWriterConfiguration().build(),
                    bridgeFactory,
                    recoveredState);
        }
    }

    static class TestWriter extends AsyncElasticsearchWriter<UpdateEvent> {

        TestWriter(
                ElementConverter<UpdateEvent, AsyncElasticsearchBulkRequest> elementConverter,
                Sink.InitContext context,
                AsyncSinkWriterConfiguration writerConfiguration,
                AsyncElasticsearchBulkRequestBridgeFactory bridgeAsyncElasticsearchBulkRequestBridgeFactory,
                Collection<BufferedRequestState<AsyncElasticsearchBulkRequest>> bufferedRequestStates) {
            super(
                    elementConverter,
                    context,
                    writerConfiguration,
                    bridgeAsyncElasticsearchBulkRequestBridgeFactory,
                    bufferedRequestStates);
        }

        @Override
        public List<BufferedRequestState<AsyncElasticsearchBulkRequest>> snapshotState(
                long checkpointId) {
            final List<BufferedRequestState<AsyncElasticsearchBulkRequest>> bufferedRequestStates =
                    super.snapshotState(checkpointId);

            log.warn(
                    "WRITER - checkpoint["
                            + checkpointId
                            + "] with "
                            + summarizeBufferedState(bufferedRequestStates));
            return bufferedRequestStates;
        }

        @Override
        public void write(UpdateEvent element, Context context)
                throws IOException, InterruptedException {
            log.warn(
                    "WRITER - write "
                            + element.getMeta().getId()
                            + " | "
                            + summarizeBufferedState(super.snapshotState(-1)));
            super.write(element, context);
        }
    }

    private static String summarizeBufferedState(
            Collection<BufferedRequestState<AsyncElasticsearchBulkRequest>> bufferedRequestStates) {
        final String bufferedIds =
                bufferedRequestStates.stream()
                        .flatMap(s -> s.getBufferedRequestEntries().stream())
                        .map(s -> s.getRequestEntry().getId())
                        .collect(Collectors.joining(", ", "{", "}"));
        return "buffered state size: "
                + bufferedRequestStates.size()
                + ", buffered ids: "
                + bufferedIds;
    }

    @SuppressWarnings("serial")
    @SuppressFBWarnings(value = "SE_NO_SERIALVERSIONID")
    public static class CounterSource implements SourceFunction<Long>, CheckpointedFunction {

        private final Duration interval;
        private final int max;
        private final Duration pause;
        private final int pauseEvery;
        private transient ListState<Long> checkpointedCount;
        private long count;
        private volatile boolean isRunning = true;

        private transient ScheduledExecutorService pauseExecutor;

        CounterSource(int max, Duration interval, Duration pause, int pauseEvery) {
            this.max = max;
            this.interval = interval;
            this.pause = pause;
            this.pauseEvery = pauseEvery;
        }

        public void cancel() {
            isRunning = false;
        }

        public void initializeState(FunctionInitializationContext context) throws Exception {
            this.checkpointedCount =
                    context
                            .getOperatorStateStore()
                            .getListState(new ListStateDescriptor<>("count", Long.class));
            if (context.isRestored()) {
                for (Long count : this.checkpointedCount.get()) {
                    this.count += count;
                }
            }
        }

        public void run(SourceContext<Long> ctx)
                throws InterruptedException, ExecutionException, TimeoutException {
            while (isRunning && count < max) {
                // this synchronized block ensures that state checkpointing,
                // internal state updates and emission of elements are an atomic operation
                synchronized (ctx.getCheckpointLock()) {
                    ctx.collect(count);
                    count++;
                }
                final boolean pause = count % pauseEvery == 0;
                if (pause) {
                    log.warn("SOURCE - pause for " + this.pause + " at counter-" + count);
                    pause(this.pause);
                } else if (!this.interval.isZero()) {
                    pause(this.interval);
                }
            }
        }

        public void snapshotState(FunctionSnapshotContext context) throws Exception {
            this.checkpointedCount.clear();
            this.checkpointedCount.add(count);
        }

        /**
         * Blocks for {@code duration}.
         *
         * <p>As an alternative to {@link Thread#sleep(long)} which is considered a code smell.
         *
         * @param duration length of pause
         */
        private void pause(Duration duration)
                throws InterruptedException, ExecutionException, TimeoutException {
            getPauseExecutor()
                    .schedule(() -> {}, duration.toMillis(), TimeUnit.MILLISECONDS)
                    .get(duration.toMillis() + 10, TimeUnit.MILLISECONDS);
        }

        private ScheduledExecutorService getPauseExecutor() {
            if (pauseExecutor == null) {
                pauseExecutor = Executors.newScheduledThreadPool(1, r -> new Thread(r, "pause"));
            }
            return pauseExecutor;
        }
    }

    static class TestBulkRequestBridge extends CirrusBulkRequestBridge {

        private final AtomicInteger batchCount;

        TestBulkRequestBridge(
                InitContext context,
                AsyncElasticsearchClientConfig clientConfig,
                AtomicInteger batchCount) {
            super(clientConfig, context.metricGroup().addGroup("bulk"));
            this.batchCount = batchCount;
        }

        @Override
        public BulkRequestWithOptions createRequestWithOptions(
                Collection<AsyncElasticsearchBulkRequest> batch) {
            final BulkRequestWithOptions bulkRequestWithOptions = super.createRequestWithOptions(batch);
            final int batchIndex = batchCount.getAndIncrement();
            final String ids =
                    batch.stream()
                            .map(AsyncElasticsearchBulkRequest::getId)
                            .collect(Collectors.joining(", ", " ids: {", "}"));
            log.warn(
                    "BRIDGE - sending batch ["
                            + System.identityHashCode(batchCount)
                            + "] "
                            + batchIndex
                            + " (size: "
                            + bulkRequestWithOptions.getBulkRequest().numberOfActions()
                            + ids
                            + ")");

            final String inflictErrorHeader =
                    batchIndex == 5 ? ERROR_TIMEOUT : (batchIndex == 10 ? ERROR_RESET_CONNECTION : "");

            return new BulkRequestWithOptions(
                    bulkRequestWithOptions.getBulkRequest(),
                    bulkRequestWithOptions.getRequestOptions().toBuilder()
                            .addHeader(
                                    HEADER_TEST_BULK_SIZE,
                                    Integer.toString(bulkRequestWithOptions.getBulkRequest().numberOfActions()))
                            .addHeader(HEADER_TEST_ERROR, inflictErrorHeader)
                            .build());
        }
    }
}
