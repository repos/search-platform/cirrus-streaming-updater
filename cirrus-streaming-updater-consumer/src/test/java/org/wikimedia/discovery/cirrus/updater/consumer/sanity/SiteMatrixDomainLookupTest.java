package org.wikimedia.discovery.cirrus.updater.consumer.sanity;

import static org.assertj.core.api.Assertions.assertThat;
import static org.wikimedia.discovery.cirrus.updater.common.wiremock.WireMockExtensionUtilities.getWireMockExtension;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.apache.flink.metrics.MetricGroup;
import org.apache.flink.runtime.metrics.NoOpMetricRegistry;
import org.apache.flink.runtime.metrics.groups.MetricGroupTest;
import org.apache.hc.core5.http.HttpHost;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.wikimedia.discovery.cirrus.updater.common.graph.WikiFilter;
import org.wikimedia.discovery.cirrus.updater.common.http.CustomRoutePlanner;
import org.wikimedia.discovery.cirrus.updater.common.http.HttpConfig;
import org.wikimedia.discovery.cirrus.updater.common.http.HttpHostMapping;
import org.wikimedia.discovery.cirrus.updater.common.http.MediaWikiHttpClient;
import org.wikimedia.discovery.cirrus.updater.common.util.function.SerializableFunction;
import org.wikimedia.discovery.cirrus.updater.common.util.function.SerializablePredicate;

import com.fasterxml.jackson.databind.JsonNode;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.junit5.WireMockExtension;
import com.github.tomakehurst.wiremock.junit5.WireMockRuntimeInfo;

class SiteMatrixDomainLookupTest {
    static final String DOMAIN = SiteMatrixDomainLookup.DEFAULT_DOMAIN;
    static final int TIMEOUT_MILLIS = 1000;

    @RegisterExtension static WireMockExtension wireMockExtension = getWireMockExtension();

    SiteMatrixDomainLookup createLookup(
            WireMockRuntimeInfo info, SerializablePredicate<JsonNode> filter) throws Exception {
        MetricGroup metricGroup =
                new MetricGroupTest.DummyAbstractMetricGroup(new NoOpMetricRegistry());
        Duration requestTimeout = Duration.of(TIMEOUT_MILLIS, ChronoUnit.MILLIS);
        Duration connectionTimeout = Duration.of(TIMEOUT_MILLIS / 2, ChronoUnit.MILLIS);
        SerializableFunction<HttpHost, Optional<HttpHost>> httpHostMapper =
                CustomRoutePlanner.createHttpHostMapper(
                        List.of(HttpHostMapping.create(DOMAIN, info.getHttpBaseUrl())));
        MediaWikiHttpClient httpClient =
                MediaWikiHttpClient.factory(
                                HttpConfig.builder()
                                        .maxConnections(10)
                                        .requestTimeout(requestTimeout)
                                        .connectionTimeout(connectionTimeout)
                                        .httpHostMapper(httpHostMapper)
                                        .userAgent("Test/User Agent")
                                        .rateLimitPerSecond(100)
                                        .build())
                        .apply(metricGroup);
        return new SiteMatrixDomainLookup(httpClient, filter);
    }

    @Test
    void testHappyPath(WireMockRuntimeInfo info) throws Exception {
        wireMockExtension.stubFor(
                WireMock.get(WireMock.urlEqualTo(SiteMatrixDomainLookup.API_PATH))
                        .withHost(WireMock.equalTo(DOMAIN))
                        .willReturn(WireMock.aResponse().withBodyFile("meta-sitematrix.json").withStatus(200)));

        SiteMatrixDomainLookup lookup = createLookup(info, jsonNode -> true);
        Optional<Set<String>> wikis = lookup.get();
        assertThat(wikis).isPresent();
        assertThat(wikis.get()).hasSize(1017).contains("board.wikimedia.org");
    }

    @Test
    void testFilterPrivateWikis(WireMockRuntimeInfo info) throws Exception {
        wireMockExtension.stubFor(
                WireMock.get(WireMock.urlEqualTo(SiteMatrixDomainLookup.API_PATH))
                        .withHost(WireMock.equalTo(DOMAIN))
                        .willReturn(WireMock.aResponse().withBodyFile("meta-sitematrix.json").withStatus(200)));
        SiteMatrixDomainLookup lookup =
                createLookup(info, SiteMatrixDomainLookup.disallowPrivateWikis());
        Optional<Set<String>> wikis = lookup.get();
        assertThat(wikis).isPresent();
        assertThat(wikis.get()).hasSize(979).doesNotContain("board.wikimedia.org");
    }

    @Test
    void testFilterForWikiIds(WireMockRuntimeInfo info) throws Exception {
        wireMockExtension.stubFor(
                WireMock.get(WireMock.urlEqualTo(SiteMatrixDomainLookup.API_PATH))
                        .withHost(WireMock.equalTo(DOMAIN))
                        .willReturn(WireMock.aResponse().withBodyFile("meta-sitematrix.json").withStatus(200)));
        SiteMatrixDomainLookup lookup =
                createLookup(
                        info,
                        WikiFilter.createFilter(
                                        SiteMatrixDomainLookup::wikiExtractor, Collections.singleton("testwiki"))
                                .orElseThrow(AssertionError::new));
        Optional<Set<String>> wikis = lookup.get();
        assertThat(wikis).isPresent();
        assertThat(wikis.get()).containsExactly("test.wikipedia.org");
    }

    @Test
    void testFailedRequest(WireMockRuntimeInfo info) throws Exception {
        wireMockExtension.stubFor(
                WireMock.get(WireMock.urlEqualTo(SiteMatrixDomainLookup.API_PATH))
                        .withHost(WireMock.equalTo(DOMAIN))
                        .willReturn(WireMock.aResponse().withStatus(500)));
        SiteMatrixDomainLookup lookup = createLookup(info, jsonNode -> true);
        assertThat(lookup.get()).isEmpty();
    }
}
