package org.wikimedia.discovery.cirrus.updater.consumer.config;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.Duration;
import java.time.Instant;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.configuration.ConfigOption;
import org.apache.kafka.common.TopicPartition;
import org.junit.jupiter.api.Test;
import org.wikimedia.discovery.cirrus.updater.common.model.ChangeType;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

class ConsumerConfigTest {

    final List<String> jsonSchemaUrls =
            ImmutableList.of("http://localhost:8080/json-schema1", "http://localhost:8080/json-schema2");
    final Instant kafkaStartTime = Instant.now();
    final Instant kafkaEndTime = Instant.now();
    final Map<TopicPartition, Long> kafkaStartOffsets =
            Map.of(
                    new TopicPartition("topi_a", 1), 100L,
                    new TopicPartition("topi_b", 0), 200L);
    final Map<TopicPartition, Long> kafkaEndOffsets = Map.of(new TopicPartition("topi_b", 0), 300L);
    final String kafkaSourceBootstrapServers = "localhost:4242";
    final String kafkaSourceGroupId = "cirrussearch.update_pipeline.consumer";
    final String streamConfigUrl = "https://streamconfig.local";
    final String publicUpdateStream = "update";
    final String privateUpdateStream = "update.private";
    private final Map<String, String> elasticsearchUrls =
            ImmutableMap.of(
                    "a", "http://localhost:9200",
                    "b", "http://localhost:9300");
    private final Map<String, String> eventStreamHttpRoutes =
            ImmutableMap.of("https://example.org", "http://localhost:1234");

    private final List<String> wikiIds = ImmutableList.of("a_wiki", "anotherwiki");
    private final boolean shouldSaneitize = true;
    private final String saneitizerClusterGroup = "dc1";
    private final Duration saneitizerLoopDuration = Duration.ofDays(1);
    private final int saneitizerBatchSize = 123;
    private final Duration saneitizerRequestTimeout = Duration.ofSeconds(90);
    private final int saneitizerRetryMax = 5;

    private final String[] baseArgs =
            new String[] {
                "--event-stream-config-url",
                streamConfigUrl,
                "--event-stream-json-schema-urls",
                String.join(";", jsonSchemaUrls),
                "--event-stream-http-routes.foo",
                "https://example.org=http://localhost:1234",
                "--kafka-source-config.bootstrap.servers",
                kafkaSourceBootstrapServers,
                "--kafka-source-config.group.id",
                kafkaSourceGroupId,
                "--kafka-source-start-time",
                kafkaStartTime.toString(),
                "--kafka-source-end-time",
                kafkaEndTime.toString(),
                "--kafka-source-start-offset.topi_a:1",
                "100",
                "--kafka-source-start-offset.topi_b",
                "200",
                "--kafka-source-end-offset.topi_b",
                "300",
                "--public-update-stream",
                publicUpdateStream,
                "--private-update-stream",
                privateUpdateStream,
                "--consume-private-updates",
                "true",
                "--wikiids",
                String.join(";", wikiIds),
                "--elasticsearch-urls.a",
                elasticsearchUrls.get("a"),
                "--elasticsearch-urls.b",
                elasticsearchUrls.get("b"),
                "--fetch-error-topic",
                "eqiad.cirrussearch.update_pipeline.fetch_error.v1",
                "--fetch-queue-capacity.default",
                "90",
                "--fetch-queue-capacity.PAGE_DELETE",
                "10",
                "--fetch-connections.default",
                "9",
                "--fetch-connections.TAGS_UPDATE",
                "1",
                "--pipeline.name",
                "eqiad.test.consumer",
                "--dry-run",
                "true"
            };

    @Test
    void fromArgs() {
        final ParameterTool params =
                ParameterTool.fromArgs(
                        Stream.concat(
                                        Arrays.stream(baseArgs),
                                        Stream.of(
                                                param(ConsumerConfig.PARAM_SANEITIZE),
                                                shouldSaneitize ? "true" : "false",
                                                param(ConsumerConfig.PARAM_SANEITIZER_CLUSTER_GROUP),
                                                saneitizerClusterGroup,
                                                param(ConsumerConfig.PARAM_SANEITIZER_LOOP_DURATION),
                                                saneitizerLoopDuration.toHours() + "H",
                                                param(ConsumerConfig.PARAM_SANEITIZER_BATCH_SIZE),
                                                Integer.toString(saneitizerBatchSize),
                                                param(ConsumerConfig.PARAM_SANEITIZER_REQUEST_TIMEOUT),
                                                saneitizerRequestTimeout.toSeconds() + "S",
                                                param(ConsumerConfig.PARAM_SANEITIZER_RETRY_SANITY_CHECK_MAX),
                                                Integer.toString(saneitizerRetryMax)))
                                .toArray(String[]::new));
        final ConsumerConfig config = ConsumerConfig.of(params.getConfiguration());

        assertThat(config.eventStreamConfigUrl()).isEqualTo(streamConfigUrl);
        assertThat(config.eventStreamJsonSchemaUrls()).isEqualTo(jsonSchemaUrls);
        assertThat(config.kafkaSourceBootstrapServers()).isEqualTo(kafkaSourceBootstrapServers);
        assertThat(config.kafkaSourceGroupId()).isEqualTo(kafkaSourceGroupId);
        assertThat(config.kafkaSourceStartTime()).isEqualTo(kafkaStartTime);
        assertThat(config.kafkaSourceEndTime()).isEqualTo(kafkaEndTime);

        assertThat(config.kafkaSourceStartOffsets()).hasSize(2);
        assertThat(config.kafkaSourceStartOffsets()).containsAllEntriesOf(kafkaStartOffsets);
        assertThat(config.kafkaSourceEndOffsets()).hasSize(1);
        assertThat(config.kafkaSourceEndOffsets()).containsAllEntriesOf(kafkaEndOffsets);

        assertThat(config.publicUpdateStream()).isEqualTo(publicUpdateStream);
        assertThat(config.privateUpdateStream()).isEqualTo(privateUpdateStream);
        assertThat(config.consumePrivateUpdates()).isEqualTo(true);
        assertThat(config.elasticSearchUrls()).isEqualTo(elasticsearchUrls);
        assertThat(config.wikiIds()).isEqualTo(wikiIds);
        assertThat(config.dryRun()).isTrue();
        assertThat(config.eventStreamHttpRoutes()).isEqualTo(eventStreamHttpRoutes);

        assertThat(config.fetchQueueCapacity()).containsEntry(ChangeType.REV_BASED_UPDATE, 90);
        assertThat(config.fetchQueueCapacity()).containsEntry(ChangeType.PAGE_DELETE, 10);

        assertThat(config.fetchConnections()).containsEntry(ChangeType.REV_BASED_UPDATE, 9);
        assertThat(config.fetchConnections()).containsEntry(ChangeType.TAGS_UPDATE, 1);
        assertThat(config.shouldSaneitize()).isEqualTo(shouldSaneitize);
        assertThat(config.saneitizerClusterGroup()).isEqualTo(saneitizerClusterGroup);
        assertThat(config.saneitizerLoopDuration()).isEqualTo(saneitizerLoopDuration);
        assertThat(config.saneitizerBatchSize()).isEqualTo(saneitizerBatchSize);
        assertThat(config.saneitizerRequestTimeout()).isEqualTo(saneitizerRequestTimeout);
        assertThat(config.saneitizerRetryMax()).isEqualTo(saneitizerRetryMax);

        assertThat(config.fetchErrorTopic())
                .isEqualTo("eqiad.cirrussearch.update_pipeline.fetch_error.v1");
    }

    @Test
    void saneitizerOptionsAreOptional() {
        final ParameterTool params = ParameterTool.fromArgs(baseArgs);
        final ConsumerConfig config = ConsumerConfig.of(params.getConfiguration());
        assertThat(config.shouldSaneitize()).isFalse();
    }

    private String param(ConfigOption<?> option) {
        return "--" + option.key();
    }
}
