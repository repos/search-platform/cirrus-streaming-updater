package org.wikimedia.discovery.cirrus.updater.consumer.graph;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.io.IOException;
import java.net.URI;
import java.time.Instant;
import java.util.List;
import java.util.Locale;
import java.util.UUID;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.wikimedia.discovery.cirrus.updater.common.InvalidMWApiResponseException;
import org.wikimedia.discovery.cirrus.updater.common.model.ChangeType;
import org.wikimedia.discovery.cirrus.updater.common.model.SanityCheck;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateEvent;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

class CirrusSanityCheckEndpointTest {
    private static final String CLUSTER = "dc1";
    private static final String WIKIID = "cirrustestwiki";
    private static final String DOMAIN = "cirrustestwiki.local";
    private static final String CONTENT_INDEX = "cirrustestwiki_content";
    private static final String GENERAL_INDEX = "cirrustestwiki_general";
    private static final Instant NOW = Instant.parse("2024-01-01T00:00:00Z");
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    private int numUuidGenerated;

    CirrusSanityCheckEndpoint endpoint =
            new CirrusSanityCheckEndpoint(CLUSTER, () -> NOW, this::generateUuid, 16);

    private UUID generateUuid() {
        return generateUuid(++numUuidGenerated);
    }

    private static UUID generateUuid(int n) {
        return UUID.fromString("00000000-0000-0000-0000-" + String.format(Locale.ENGLISH, "%012d", n));
    }

    @Test
    void testBuildUri() {
        SanityCheck sanityCheck = new SanityCheck(DOMAIN, 0, 100, NOW, 0);
        assertThat(endpoint.buildURI(sanityCheck))
                .isEqualTo(
                        URI.create(
                                "https://cirrustestwiki.local/w/api.php"
                                        + "?action=cirrus-check-sanity&format=json&formatversion=2&cluster=dc1"
                                        + "&from=0&limit=100&sequenceid=0&rerenderfrequency=16"));
    }

    private static UpdateEvent.UpdateEventBuilder expectedEventBuilder(int uuidSequence) {
        return UpdateEvent.builder()
                .meta(
                        UpdateEvent.Meta.builder()
                                .id(generateUuid(uuidSequence).toString())
                                .requestId(generateUuid(uuidSequence).toString())
                                .dt(NOW)
                                .stream(CirrusSanityCheckEndpoint.PSEUDO_STREAM_NAME)
                                .domain(DOMAIN)
                                .build())
                .ingestionTime(NOW)
                .eventTime(NOW);
    }

    static Stream<Arguments> provideExtractAndAugmentTestCases() {
        // TODO: error responses
        return Stream.of(
                Arguments.of("no-problems", List.of()),
                Arguments.of(
                        "redirect-in-index",
                        List.of(
                                expectedEventBuilder(1)
                                        .changeType(ChangeType.PAGE_DELETE)
                                        .targetDocument(
                                                new UpdateEvent.TargetDocument(DOMAIN, WIKIID, 0L, 1L)
                                                        .completeWith("alpha", CONTENT_INDEX))
                                        .build(),
                                expectedEventBuilder(2)
                                        .changeType(ChangeType.PAGE_RERENDER)
                                        .targetDocument(
                                                new UpdateEvent.TargetDocument(DOMAIN, WIKIID, 0L, 2L)
                                                        .completeWith("alpha", CONTENT_INDEX))
                                        .build())),
                Arguments.of(
                        "page-not-in-index",
                        List.of(
                                expectedEventBuilder(1)
                                        .changeType(ChangeType.PAGE_RERENDER_UPSERT)
                                        .targetDocument(
                                                new UpdateEvent.TargetDocument(DOMAIN, WIKIID, 0L, 1L)
                                                        .completeWith("alpha", CONTENT_INDEX))
                                        .build())),
                Arguments.of(
                        "old-document",
                        List.of(
                                expectedEventBuilder(1)
                                        .changeType(ChangeType.PAGE_RERENDER)
                                        .targetDocument(
                                                new UpdateEvent.TargetDocument(DOMAIN, WIKIID, 0L, 1L)
                                                        .completeWith("alpha", CONTENT_INDEX))
                                        .build())),
                Arguments.of(
                        "ghost-page-in-index",
                        List.of(
                                expectedEventBuilder(1)
                                        .changeType(ChangeType.PAGE_DELETE)
                                        .targetDocument(
                                                new UpdateEvent.TargetDocument(DOMAIN, WIKIID, 0L, 1L)
                                                        .completeWith("alpha", CONTENT_INDEX))
                                        .build())),
                Arguments.of(
                        "page-in-wrong-index",
                        List.of(
                                expectedEventBuilder(1)
                                        .changeType(ChangeType.PAGE_DELETE)
                                        .targetDocument(
                                                new UpdateEvent.TargetDocument(DOMAIN, WIKIID, 0L, 1L)
                                                        .completeWith("alpha", GENERAL_INDEX))
                                        .build(),
                                expectedEventBuilder(2)
                                        .changeType(ChangeType.PAGE_RERENDER_UPSERT)
                                        .targetDocument(
                                                new UpdateEvent.TargetDocument(DOMAIN, WIKIID, 0L, 1L)
                                                        .completeWith("alpha", CONTENT_INDEX))
                                        .build())),
                Arguments.of(
                        "old-version-in-index",
                        List.of(
                                expectedEventBuilder(1)
                                        .changeType(ChangeType.PAGE_RERENDER)
                                        .targetDocument(
                                                new UpdateEvent.TargetDocument(DOMAIN, WIKIID, 0L, 1L)
                                                        .completeWith("alpha", CONTENT_INDEX))
                                        .build())));
    }

    @ParameterizedTest
    @MethodSource("provideExtractAndAugmentTestCases")
    void extractAndAugment(String resourceSuffix, List<UpdateEvent> expected) throws IOException {
        JsonNode apiResponse = sanityCheckOutput(resourceSuffix);
        SanityCheck sanityCheck = new SanityCheck(DOMAIN, 0, 100, NOW, 0);
        List<UpdateEvent> result = endpoint.extractAndAugment(sanityCheck, apiResponse);
        assertThat(result).containsExactlyElementsOf(expected);
    }

    @Test
    void missingProblems() throws IOException {
        JsonNode apiResponse = OBJECT_MAPPER.createObjectNode();
        SanityCheck sanityCheck = new SanityCheck(DOMAIN, 0, 100, NOW, 0);
        assertThatThrownBy(() -> endpoint.extractAndAugment(sanityCheck, apiResponse))
                .isInstanceOf(InvalidMWApiResponseException.class);
    }

    private JsonNode sanityCheckOutput(String resourceSuffix) throws IOException {
        return OBJECT_MAPPER
                .reader()
                .readTree(
                        this.getClass()
                                .getResourceAsStream(
                                        "/"
                                                + this.getClass().getSimpleName()
                                                + ".sanitycheck."
                                                + resourceSuffix
                                                + ".json"));
    }
}
