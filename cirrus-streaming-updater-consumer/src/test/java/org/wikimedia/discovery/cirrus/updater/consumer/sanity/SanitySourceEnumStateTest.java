package org.wikimedia.discovery.cirrus.updater.consumer.sanity;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

import java.io.IOException;
import java.time.Instant;
import java.util.List;

import org.junit.jupiter.api.Test;

class SanitySourceEnumStateTest {
    private final SanitySourceEnumState.Serializer serializer =
            new SanitySourceEnumState.Serializer(
                    new SanitySourceSplit.Serializer(new LoopState.Serializer()));

    @Test
    void equality() {
        assertThat(new SanitySourceEnumState(List.of(new SanitySourceSplit("0"))))
                .isEqualTo(new SanitySourceEnumState(List.of(new SanitySourceSplit("0"))));
        assertThat(new SanitySourceEnumState(List.of(new SanitySourceSplit("0"))))
                .isNotEqualTo(new SanitySourceEnumState(List.of(new SanitySourceSplit("1"))));
    }

    @Test
    void roundTripEmptyState() throws IOException {
        assertRoundTrip(new SanitySourceEnumState(List.of(new SanitySourceSplit("0"))));
    }

    @Test
    void roundTripSomeState() throws IOException {
        Instant now = Instant.parse("2024-01-01T00:00:00Z");
        SanitySourceSplitState splitState = new SanitySourceSplitState(new SanitySourceSplit("0"));
        splitState.put(new LoopState("example.com", 1, now));

        assertRoundTrip(new SanitySourceEnumState(List.of(splitState.toSanitySourceSplit())));
    }

    private void assertRoundTrip(SanitySourceEnumState enumState) throws IOException {
        assertThat(enumState)
                .isEqualTo(
                        serializer.deserialize(serializer.getVersion(), serializer.serialize(enumState)));
    }
}
