#!/usr/bin/env bash

if [[ "$1" == "" || "$2" == "" ]]; then
  echo "Usage: $0 <path to flink/lib> <path to maven module>"
  exit 1
fi

flink_lib_dir=$1

flink_app_jar=$2/target/cirrus-streaming-updater-producer-0.1.0-SNAPSHOT-jar-with-dependencies.jar

output_path=$2/target

flink_app_class_list=$output_path/flink-app-classlist.txt
flink_runtime_class_list=$output_path/flink-runtime-classlist.txt

rm $flink_runtime_class_list

jar tf $flink_app_jar | grep .class > $flink_app_class_list

find $flink_lib_dir -type f -name "*.jar" -print0 | xargs -0 -I {} jar tf {} | grep .class >> $flink_runtime_class_list

sorted_flink_app_class_list=$(sort $flink_app_class_list)
sorted_flink_runtime_class_list=$(sort $flink_runtime_class_list)

duplicates=$(comm -12 <(echo "$sorted_flink_app_class_list") <(echo "$sorted_flink_runtime_class_list"))

# Print the duplicates
if [ -z "$duplicates" ]; then
  echo "No duplicate lines found."
else
  echo "Found duplicate lines."
  echo "$duplicates" > $output_path/duplicate-classlist.txt
fi


