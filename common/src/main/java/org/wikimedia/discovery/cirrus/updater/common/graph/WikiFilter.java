package org.wikimedia.discovery.cirrus.updater.common.graph;

import java.util.Collection;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.Nullable;

import org.apache.flink.api.java.tuple.Tuple2;
import org.wikimedia.discovery.cirrus.updater.common.util.function.SerializableFunction;
import org.wikimedia.discovery.cirrus.updater.common.util.function.SerializablePredicate;

import lombok.experimental.UtilityClass;

@UtilityClass
@SuppressWarnings("HideUtilityClassConstructor")
public class WikiFilter {
    public static <E> Optional<SerializablePredicate<E>> createFilter(
            SerializableFunction<E, String> wikiIdSupplier, @Nullable Collection<String> maybeWikis) {
        return Optional.ofNullable(maybeWikis)
                .filter(wikis -> !wikis.isEmpty())
                .map(
                        set -> {
                            Set<String> include =
                                    set.stream().filter(s -> !s.startsWith("-")).collect(Collectors.toSet());
                            Set<String> exclude =
                                    set.stream()
                                            .filter(s -> s.startsWith("-"))
                                            .map(s -> s.substring(1))
                                            .collect(Collectors.toSet());
                            if (!include.isEmpty() && !exclude.isEmpty()) {
                                // It would work, but seems like a misconfiguration?
                                throw new IllegalArgumentException("Can only include or exclude, not both.");
                            }
                            return Tuple2.of(include, exclude);
                        })
                .map(
                        tuple ->
                                (input -> {
                                    Set<String> include = tuple.f0;
                                    Set<String> exclude = tuple.f1;
                                    String wikiId = wikiIdSupplier.apply(input);
                                    return !exclude.contains(wikiId)
                                            && (include.isEmpty() || include.contains(wikiId));
                                }));
    }
}
