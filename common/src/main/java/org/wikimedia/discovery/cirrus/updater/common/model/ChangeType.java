package org.wikimedia.discovery.cirrus.updater.common.model;

/**
 * Kind of change the pipeline supports.
 *
 * <p>Always add to the end, you might need to update InputEventTest after adding a new ChangeType
 */
public enum ChangeType {
    REV_BASED_UPDATE,
    PAGE_DELETE,
    TAGS_UPDATE,
    REDIRECT_UPDATE,
    PAGE_RERENDER,
    PAGE_RERENDER_UPSERT,
}
