package org.wikimedia.discovery.cirrus.updater.common.http;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.zip.DataFormatException;

import org.apache.hc.core5.concurrent.FutureCallback;
import org.apache.hc.core5.http.EntityDetails;
import org.apache.hc.core5.http.Header;
import org.apache.hc.core5.http.HttpException;
import org.apache.hc.core5.http.HttpResponse;
import org.apache.hc.core5.http.nio.AsyncDataConsumer;
import org.apache.hc.core5.http.nio.AsyncResponseConsumer;
import org.apache.hc.core5.http.nio.CapacityChannel;
import org.apache.hc.core5.http.protocol.HttpContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.wikimedia.discovery.cirrus.updater.common.util.GzipInflater;

public class GzipJsonAsyncResponseConsumer<
                T, C extends AsyncResponseConsumer<T> & AsyncDataConsumer>
        implements AsyncResponseConsumer<T>, AsyncDataConsumer {

    private static final Logger LOGGER = LoggerFactory.getLogger("test");
    private final C delegate;

    private final int inflaterBufferSize;

    private GzipInflater inflater;
    /**
     * buffer meant to keep track of what's left to consume from a previous consume call. Reason is
     * that GzipInflater might not consume everything that it's given
     */
    private ByteBuffer buffer;

    public GzipJsonAsyncResponseConsumer(C delegate, int bufferSize) {
        this.delegate = delegate;
        // pre-allocate the buffer but mark it as "empty"
        this.buffer = ByteBuffer.allocate(bufferSize);
        buffer.position(buffer.limit());
        // we expect bufferSize of input chunks, the compression ratio is expected to be 2:1
        inflaterBufferSize = bufferSize * 2;
    }

    @Override
    public void releaseResources() {
        delegate.releaseResources();

        if (inflater != null) {
            inflater.close();
            inflater = null;
        }
    }

    @Override
    public void consumeResponse(
            HttpResponse response,
            EntityDetails entityDetails,
            HttpContext context,
            FutureCallback<T> resultCallback)
            throws HttpException, IOException {
        if ("gzip".equals(entityDetails.getContentEncoding())) {
            inflater = new GzipInflater(inflaterBufferSize);
        }
        delegate.consumeResponse(response, entityDetails, context, resultCallback);
    }

    @Override
    public void informationResponse(HttpResponse response, HttpContext context)
            throws HttpException, IOException {
        delegate.informationResponse(response, context);
    }

    @Override
    public void failed(Exception cause) {
        delegate.failed(cause);
    }

    @Override
    public void updateCapacity(CapacityChannel capacityChannel) throws IOException {
        delegate.updateCapacity(capacityChannel);
    }

    /**
     * Save the unconsumed bytes after a consumer call. If reading from the http client ByteBuffer we
     * need to copy any leftovers bytes that might need to be prepended on the next call.
     */
    private void saveUnconsumedBytes(ByteBuffer src) {
        // only save unconsumed bytes if they're not already in our own buffer
        int unconsumed = src.remaining();
        if (unconsumed > 0 && src != buffer) {
            if (buffer.capacity() < src.remaining()) {
                buffer = ByteBuffer.allocate(src.remaining());
            }
            src.get(buffer.array(), buffer.arrayOffset(), unconsumed);
            buffer.position(0);
            buffer.limit(unconsumed);
        }
    }

    /**
     * "prepend" possible bytes that were not fully read by GzipInflater in a previous call to
     * consume.
     */
    private ByteBuffer prependWithUnconsumedBytes(ByteBuffer src) {
        int unconsumed = this.buffer.remaining();
        if (unconsumed == 0) {
            // no remaining bytes to prepend use the source ByteBuffer no need to do extra copy
            return src;
        }

        int total = unconsumed + src.remaining();
        if (total < 0) {
            throw new OutOfMemoryError();
        }

        if (total > this.buffer.capacity()) {
            // we need to allocate more data
            ByteBuffer newBuf = ByteBuffer.allocate(total);
            buffer.get(newBuf.array(), newBuf.arrayOffset(), unconsumed);
            this.buffer = newBuf;
        } else {
            // move the unconsumed bytes to the beginning
            buffer.get(buffer.array(), buffer.arrayOffset(), unconsumed);
        }
        // copy the new data at the end
        src.get(buffer.array(), buffer.arrayOffset() + unconsumed, src.remaining());
        buffer.position(0);
        buffer.limit(total);
        return buffer;
    }

    @Override
    public void consume(ByteBuffer source) {
        if (inflater != null) {
            try {
                ByteBuffer sourceWithLeftOvers = prependWithUnconsumedBytes(source);
                inflater.inflate(sourceWithLeftOvers, this::consumeSafely);
                saveUnconsumedBytes(sourceWithLeftOvers);
            } catch (DataFormatException e) {
                failed(new IOException("Failed to inflate compressed buffer", e));
            }
        } else {
            // no need to do extra buffering here, the delegate should consume everything in all calls
            consumeSafely(source);
        }
    }

    @Override
    public void streamEnd(List<? extends Header> trailers) throws HttpException, IOException {
        delegate.streamEnd(trailers);
    }

    private void consumeSafely(ByteBuffer src) {
        try {
            delegate.consume(src);
        } catch (IOException e) {
            failed(e);
        }
    }
}
