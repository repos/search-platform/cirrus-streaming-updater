package org.wikimedia.discovery.cirrus.updater.common.graph;

import java.io.IOException;
import java.time.Duration;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.function.Predicate;

import org.apache.flink.streaming.api.functions.async.AsyncRetryPredicate;
import org.apache.flink.streaming.api.functions.async.AsyncRetryStrategy;
import org.apache.flink.types.Either;
import org.wikimedia.discovery.cirrus.updater.common.InvalidMWApiResponseException;
import org.wikimedia.discovery.cirrus.updater.common.PageIdMismatchException;
import org.wikimedia.discovery.cirrus.updater.common.RevisionNotFoundException;
import org.wikimedia.discovery.cirrus.updater.common.model.FetchFailure;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateEvent;
import org.wikimedia.eventutilities.core.SerializableClock;

public final class LagAwareRetryStrategy
        implements AsyncRetryStrategy<Either<UpdateEvent, FetchFailure<UpdateEvent>>>,
                AsyncRetryPredicate<Either<UpdateEvent, FetchFailure<UpdateEvent>>> {

    /**
     * List of retryable exceptions with a flag indicating if they are bound to lag.
     *
     * <p>If the flag is {@code true}, the exception is retried ignoring the event time lag. If the
     * flag is {@code false}, the exception is retried only if the event time lag is below {@link
     * #maxLag}.
     */
    private static final List<Entry<Class<? extends Throwable>, Boolean>> RETRYABLE_EXCEPTIONS =
            List.of(
                    Map.entry(IOException.class, Boolean.TRUE),
                    Map.entry(InvalidMWApiResponseException.class, Boolean.TRUE),
                    Map.entry(RevisionNotFoundException.class, Boolean.FALSE),
                    Map.entry(PageIdMismatchException.class, Boolean.FALSE));

    private final SerializableClock clock;
    private final int maxAttempts;
    private final Duration maxLag;

    public LagAwareRetryStrategy(int maxAttempts, Duration maxLag, SerializableClock clock) {
        this.maxAttempts = maxAttempts;
        this.maxLag = maxLag;
        this.clock = clock;
    }

    @Override
    public boolean canRetry(int currentAttempts) {
        return currentAttempts < maxAttempts;
    }

    @Override
    public Optional<Predicate<Throwable>> exceptionPredicate() {
        return Optional.empty();
    }

    @Override
    public long getBackoffTimeMillis(int currentAttempts) {
        return currentAttempts * 1000L;
    }

    @Override
    public AsyncRetryPredicate<Either<UpdateEvent, FetchFailure<UpdateEvent>>> getRetryPredicate() {
        return this;
    }

    private boolean mapRetryMode(Either<UpdateEvent, FetchFailure<UpdateEvent>> result) {
        return result.isRight()
                && mapRetryMode(result.right().getFailure())
                        .map(
                                retryMode ->
                                        retryMode
                                                || result
                                                        .right()
                                                        .getEvent()
                                                        .getEventTime()
                                                        .plus(maxLag)
                                                        .isAfter(clock.get()))
                        .orElse(Boolean.FALSE);
    }

    private Optional<Boolean> mapRetryMode(Throwable error) {
        return mapRetryMode(error.getClass());
    }

    private Optional<Boolean> mapRetryMode(Class<?> errorType) {
        return RETRYABLE_EXCEPTIONS.stream()
                .filter(entry -> entry.getKey().isAssignableFrom(errorType))
                .findFirst()
                .map(Entry::getValue);
    }

    @Override
    public Optional<Predicate<Collection<Either<UpdateEvent, FetchFailure<UpdateEvent>>>>>
            resultPredicate() {
        return Optional.of(fetchResults -> fetchResults.stream().anyMatch(this::mapRetryMode));
    }
}
