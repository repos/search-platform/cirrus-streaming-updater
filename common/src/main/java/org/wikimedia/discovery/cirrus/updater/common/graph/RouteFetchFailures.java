package org.wikimedia.discovery.cirrus.updater.common.graph;

import static org.wikimedia.discovery.cirrus.updater.common.logging.LoggingContext.log;

import org.apache.flink.streaming.api.functions.ProcessFunction;
import org.apache.flink.types.Either;
import org.apache.flink.types.Row;
import org.apache.flink.util.Collector;
import org.apache.flink.util.OutputTag;
import org.wikimedia.discovery.cirrus.updater.common.model.FetchFailure;
import org.wikimedia.discovery.cirrus.updater.common.model.FetchFailureEncoder;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateEvent;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class RouteFetchFailures
        extends ProcessFunction<Either<UpdateEvent, FetchFailure<UpdateEvent>>, UpdateEvent> {
    private final String jobName;
    private final OutputTag<Row> failureOutputTag;
    private final FetchFailureEncoder failureEncoder;

    public RouteFetchFailures(
            String jobName, OutputTag<Row> failureOutputTag, FetchFailureEncoder failureEncoder) {
        this.jobName = jobName;
        this.failureOutputTag = failureOutputTag;
        this.failureEncoder = failureEncoder;
    }

    @Override
    public void processElement(
            Either<UpdateEvent, FetchFailure<UpdateEvent>> result,
            ProcessFunction<Either<UpdateEvent, FetchFailure<UpdateEvent>>, UpdateEvent>.Context context,
            Collector<UpdateEvent> collector) {
        if (result.isRight()) {
            log(log::atWarn, result.right()::getFailureMessage, result.right().getEvent());
            context.output(
                    failureOutputTag,
                    failureEncoder.encode(result.right(), jobName + "." + getRuntimeContext().getJobId()));
        } else {
            collector.collect(result.left());
        }
    }
}
