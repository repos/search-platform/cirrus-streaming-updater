package org.wikimedia.discovery.cirrus.updater.common.util.function;

import java.io.Serializable;
import java.util.function.Supplier;

public interface SerializableSupplier<T> extends Supplier<T>, Serializable {}
