package org.wikimedia.discovery.cirrus.updater.common.http;

import java.util.Arrays;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.hc.client5.http.classic.HttpClient;
import org.apache.hc.core5.http.HttpRequest;
import org.apache.http.HttpHeaders;

import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.httpclient5.HttpClientMetricNameStrategy;

/** A {@link HttpClientMetricNameStrategy} that encodes key-values-pairs into the name. */
public class TagNamingStrategy implements HttpClientMetricNameStrategy {

    public static Entry<String, Stream<Entry<String, String>>> decodeTags(String suffix) {
        final int tagsStart = suffix.indexOf('{');
        if (tagsStart > 0 && suffix.charAt(suffix.length() - 1) == '}') {
            return Map.entry(
                    suffix.substring(0, tagsStart),
                    Arrays.stream(suffix.substring(tagsStart + 1, suffix.length() - 1).split(";"))
                            .map(String::trim)
                            .map(
                                    (entry) -> {
                                        final String[] tuple = entry.split("=");
                                        return Map.entry(tuple[0].trim(), tuple[1].trim());
                                    }));
        }
        return Map.entry(suffix, Stream.empty());
    }

    public static String encodeTags(Map<String, String> tags) {
        return tags.entrySet().stream()
                .map(e -> e.getKey() + "=" + e.getValue())
                .collect(Collectors.joining(";", "{", "}"));
    }

    @Override
    public String getNameFor(String name, Exception exception) {
        return name(name, "errors")
                + encodeTags(Map.of("exception", exception.getClass().getSimpleName()));
    }

    @Override
    public String getNameFor(String name, HttpRequest httpRequest) {
        final String metricName = "request_duration";
        return name(name, metricName) + encodeRequest(httpRequest);
    }

    public static String encodeRequest(org.apache.http.HttpRequest httpRequest) {
        return encodeTags(
                Map.of(
                        "authority", httpRequest.getFirstHeader(HttpHeaders.HOST).getValue(),
                        "method", httpRequest.getRequestLine().getMethod(),
                        "path", getPath(httpRequest)));
    }

    public static String encodeRequest(HttpRequest httpRequest) {
        return encodeTags(
                Map.of(
                        "authority", httpRequest.getAuthority().toString(),
                        "method", httpRequest.getMethod(),
                        "path", getPath(httpRequest)));
    }

    public static String name(String... names) {
        return MetricRegistry.name(HttpClient.class, names);
    }

    /**
     * Extract the URI path without parsing the URI. The original {@link
     * org.apache.hc.client5.http.async.methods.SimpleHttpRequest} holds the query as part of its
     * path, hence we have to trim down the path again. Otherwise, a new metric is created for every
     * request, since the query contains the revision ID.
     *
     * @param request representation of the original request
     * @return the path segment of the {@code request} URI
     */
    private static String getPath(String path) {
        final int queryOffset = path.indexOf('?');
        return queryOffset > 0 ? path.substring(0, queryOffset) : path;
    }

    private static String getPath(HttpRequest request) {
        return getPath(request.getPath());
    }

    private static String getPath(org.apache.http.HttpRequest httpRequest) {
        return getPath(httpRequest.getRequestLine().getUri());
    }
}
