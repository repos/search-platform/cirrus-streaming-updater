package org.wikimedia.discovery.cirrus.updater.common.graph;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.apache.flink.api.common.ExecutionConfig;
import org.apache.flink.api.common.functions.FilterFunction;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.connector.kafka.sink.KafkaSinkBuilder;
import org.apache.flink.connector.kafka.source.KafkaSourceBuilder;
import org.apache.flink.connector.kafka.source.enumerator.initializer.OffsetsInitializer;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.types.Row;
import org.apache.hc.core5.http.HttpHost;
import org.wikimedia.discovery.cirrus.updater.common.config.CommonConfig;
import org.wikimedia.discovery.cirrus.updater.common.http.CustomRoutePlanner;
import org.wikimedia.discovery.cirrus.updater.common.http.HttpConfig;
import org.wikimedia.discovery.cirrus.updater.common.http.HttpConfig.HttpConfigBuilder;
import org.wikimedia.discovery.cirrus.updater.common.http.HttpHostMapping;
import org.wikimedia.discovery.cirrus.updater.common.http.MediaWikiHttpClient;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateEvent;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateEventTypeInfo;
import org.wikimedia.discovery.cirrus.updater.common.util.function.SerializableFunction;
import org.wikimedia.eventutilities.core.event.EventStream;
import org.wikimedia.eventutilities.core.event.EventStreamFactory;
import org.wikimedia.eventutilities.flink.EventRowTypeInfo;
import org.wikimedia.eventutilities.flink.formats.json.KafkaRecordTimestampStrategy;
import org.wikimedia.eventutilities.flink.stream.EventDataStreamFactory;
import org.wikimedia.eventutilities.flink.stream.EventDataStreamFactory.Builder;

public abstract class CommonGraphFactory<C extends CommonConfig> implements AutoCloseable {

    protected final StreamExecutionEnvironment env;

    protected final C config;

    protected final EventDataStreamFactory eventDataStreamFactory;

    private final transient Map<String, EventStream> streams = new HashMap<>();
    private transient EventRowTypeInfo updateRowTypeInfo;
    private transient TypeInformation<UpdateEvent> updateTypeInfo;

    private transient SerializableFunction<HttpHost, Optional<HttpHost>> httpHostMapper;

    protected CommonGraphFactory(C config) {
        this(
                createStreamExecutionEnvironment(),
                createEventDataStreamFactoryBuilder(config).build(),
                config);
    }

    protected static Builder createEventDataStreamFactoryBuilder(CommonConfig config) {
        EventStreamFactory.Builder eventStreamBuilder =
                EventStreamFactory.builder()
                        .setEventStreamConfig(config.eventStreamConfigUrl())
                        .setEventSchemaLoader(config.eventStreamJsonSchemaUrls());
        Optional.ofNullable(config.eventStreamHttpRoutes())
                .ifPresent(eventStreamBuilder::setHttpRoutes);

        return EventDataStreamFactory.builder().eventStreamFactory(eventStreamBuilder.build());
    }

    protected CommonGraphFactory(
            StreamExecutionEnvironment streamExecutionEnvironment,
            EventDataStreamFactory eventDataStreamFactory,
            C config) {
        this.env = streamExecutionEnvironment;
        this.config = config;
        this.eventDataStreamFactory = eventDataStreamFactory;
    }

    @Override
    public void close() throws Exception {
        this.env.close();
    }

    public StreamExecutionEnvironment prepareEnvironment() {
        createStreamGraph();
        return env;
    }

    protected abstract void createStreamGraph();

    @Nonnull
    protected KafkaSourceBuilder<Row> createKafkaSourceBuilder(String streamName) {
        KafkaSourceBuilder<Row> sourceBuilder =
                eventDataStreamFactory.kafkaSourceBuilder(
                        streamName, config.kafkaSourceBootstrapServers(), config.kafkaSourceGroupId());
        setKafkaOffsetBounds(sourceBuilder);
        sourceBuilder.setProperties(config.kafkaSourceProperties());
        sourceBuilder.setClientIdPrefix(config.jobName() + ":" + streamName);
        return sourceBuilder;
    }

    @Nonnull
    protected KafkaSinkBuilder<Row> createKafkaSinkBuilder(
            String streamName, String topic, KafkaRecordTimestampStrategy timestampStrategy) {
        final EventStream eventStream = getOrCreateStream(streamName);
        KafkaSinkBuilder<Row> sinkBuilder =
                eventDataStreamFactory.kafkaSinkBuilder(
                        eventStream.streamName(),
                        eventStream.latestSchemaVersion(),
                        config.kafkaSinkBootstrapServers(),
                        topic,
                        timestampStrategy);
        sinkBuilder.setKafkaProducerConfig(config.kafkaSinkProperties());
        return sinkBuilder;
    }

    protected void setKafkaOffsetBounds(KafkaSourceBuilder<Row> sourceBuilder) {
        if (config.kafkaSourceStartOffsets() != null) {
            sourceBuilder.setStartingOffsets(
                    OffsetsInitializer.offsets(config.kafkaSourceStartOffsets()));
        } else if (config.kafkaSourceStartTime() != null) {
            sourceBuilder.setStartingOffsets(
                    OffsetsInitializer.timestamp(config.kafkaSourceStartTime().toEpochMilli()));
        } else {
            sourceBuilder.setStartingOffsets(getDefaultKafkaStartingOffsets());
        }

        if (config.kafkaSourceEndOffsets() != null) {
            sourceBuilder.setBounded(OffsetsInitializer.offsets(config.kafkaSourceEndOffsets()));
        } else if (config.kafkaSourceEndTime() != null) {
            sourceBuilder.setBounded(
                    OffsetsInitializer.timestamp(config.kafkaSourceEndTime().toEpochMilli()));
        }
    }

    @Nonnull
    protected abstract OffsetsInitializer getDefaultKafkaStartingOffsets();

    @Nonnull
    protected SerializableFunction<HttpHost, Optional<HttpHost>> getOrCreateHttpHostMapper() {
        if (httpHostMapper == null) {
            final List<HttpHostMapping> httpRoutes = config.httpRoutes();
            if (httpRoutes == null || httpRoutes.isEmpty()) {
                httpHostMapper = (httpHost) -> Optional.empty();
            } else {
                httpHostMapper = CustomRoutePlanner.createHttpHostMapper(httpRoutes);
            }
        }
        return httpHostMapper;
    }

    @Nonnull
    protected EventStream getOrCreateStream(String streamName) {
        streams.computeIfAbsent(
                streamName, key -> eventDataStreamFactory.getEventStreamFactory().createEventStream(key));
        return streams.get(streamName);
    }

    @Nonnull
    protected EventRowTypeInfo getOrCreateUpdateRowTypeInfo() {
        if (updateRowTypeInfo == null) {
            updateRowTypeInfo = getEventRowTypeInfo(getOrCreateStream(config.publicUpdateStream()));
        }
        return updateRowTypeInfo;
    }

    protected EventRowTypeInfo getEventRowTypeInfo(EventStream eventStream) {
        return eventDataStreamFactory.rowTypeInfo(
                eventStream.streamName(), eventStream.latestSchemaVersion());
    }

    @Nonnull
    protected TypeInformation<UpdateEvent> getOrCreateUpdateTypeInfo() {
        if (updateTypeInfo == null) {
            updateTypeInfo = UpdateEventTypeInfo.create(getOrCreateUpdateRowTypeInfo());
        }
        return updateTypeInfo;
    }

    @Nonnull
    protected static StreamExecutionEnvironment createStreamExecutionEnvironment() {
        final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        // Per flink production readiness checklist max parallelism should be hard coded
        env.setMaxParallelism(CommonConfig.MAX_OPERATOR_PARALLELISM);

        // Generic types do use Kryo as a fallback, since kryo has multiple drawbacks:
        // - perf is generally worse:
        // https://flink.apache.org/2020/04/15/flink-serialization-tuning-vol.-1-choosing-your-serializer-if-you-can/
        // - schema evolutions are not supported:
        // https://nightlies.apache.org/flink/flink-docs-release-1.17/docs/dev/datastream/fault-tolerance/serialization/schema_evolution/
        // we prefer to avoid it.
        // Since it's easy to forget to declare the proper TypeInformation when chaining operators we
        // tell flink to explicitly fail whenever it detects that Kryo is going to be used.
        final ExecutionConfig config = env.getConfig();

        config.disableGenericTypes();

        config.disableAutoGeneratedUIDs();

        return env;
    }

    protected static boolean canaryEventFilter(UpdateEvent updateEvent) {
        UpdateEvent.TargetDocument target = updateEvent.getTargetDocument();
        return target.getDomain() != null
                && !target.getDomain().equals("canary")
                && target.getWikiId() != null
                && !target.getWikiId().equals("canary");
    }

    protected static FilterFunction<UpdateEvent> createFilter(@Nullable List<String> wikiIds) {
        return WikiFilter.createFilter(UpdateEvent.WIKIID_EXTRACTOR, wikiIds)
                .map(
                        filter ->
                                (FilterFunction<UpdateEvent>)
                                        (event -> CommonGraphFactory.canaryEventFilter(event) && filter.test(event)))
                .orElse(CommonGraphFactory::canaryEventFilter);
    }

    protected MediaWikiHttpClient.Factory createMediWikiHttpClientFactory(int maxConnection) {
        final HttpConfigBuilder httpConfigBuilder = createMediaWikiHttpConfigBuilder(maxConnection);
        return MediaWikiHttpClient.factory(httpConfigBuilder.build());
    }

    protected HttpConfigBuilder createMediaWikiHttpConfigBuilder(int maxConnection) {
        Duration connectionTimeout =
                config
                        .httpRequestTimeout()
                        .dividedBy(config.httpRequestConnectionTimeoutDivisor())
                        .truncatedTo(ChronoUnit.MILLIS);
        return HttpConfig.builder()
                .maxConnections(maxConnection)
                .requestTimeout(config.httpRequestTimeout())
                .connectionTimeout(connectionTimeout)
                .httpHostMapper(getOrCreateHttpHostMapper())
                .rateLimitMaxRetries(config.httpRateLimitMaxRetries())
                .rateLimitRetryInterval(config.httpRateLimitRetryInterval())
                .rateLimitPerSecond(
                        config.httpRateLimitPerSecond() < 0
                                ? config.httpRateLimitPerSecond()
                                : config.httpRateLimitPerSecond() / env.getParallelism())
                .authToken(config.mediawikiAuthToken())
                .userAgent(config.httpUserAgent());
    }
}
