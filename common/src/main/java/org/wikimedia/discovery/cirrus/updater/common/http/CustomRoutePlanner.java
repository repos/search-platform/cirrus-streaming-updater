package org.wikimedia.discovery.cirrus.updater.common.http;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;

import javax.annotation.Nonnull;

import org.apache.hc.client5.http.HttpRoute;
import org.apache.hc.client5.http.routing.HttpRoutePlanner;
import org.apache.hc.core5.http.HttpException;
import org.apache.hc.core5.http.HttpHost;
import org.apache.hc.core5.http.protocol.HttpContext;
import org.wikimedia.discovery.cirrus.updater.common.util.function.SerializableFunction;

/**
 * wmf-jvm-utils provides http-client-utils which is based on http client v4 sadly this does not
 * match the http client v5 that we need for this project. Put this route planner in here for now
 * but it might be worthwhile to consider putting it in wmf-jvm-utils.
 */
public class CustomRoutePlanner implements HttpRoutePlanner {

    private final HttpRoutePlanner defaultRoutePlanner;

    private final Function<HttpHost, Optional<HttpHost>> mapper;

    public CustomRoutePlanner(
            Function<HttpHost, Optional<HttpHost>> mapper, HttpRoutePlanner defaultRoutePlanner) {
        this.mapper = mapper;
        this.defaultRoutePlanner = defaultRoutePlanner;
    }

    @Override
    public HttpRoute determineRoute(HttpHost httpHost, HttpContext httpContext) throws HttpException {
        final Optional<HttpRoute> replacedRoute = mapper.apply(httpHost).map(HttpRoute::new);

        return replacedRoute.isPresent()
                ? replacedRoute.get()
                : defaultRoutePlanner.determineRoute(httpHost, httpContext);
    }

    @Nonnull
    public static SerializableFunction<HttpHost, Optional<HttpHost>> createHttpHostMapper(
            List<HttpHostMapping> httpRoutes) {
        return (httpHost) ->
                httpRoutes.stream()
                        .filter(mapping -> mapping.matches(httpHost))
                        .map(mapping -> mapping.mergeTarget(httpHost))
                        .findFirst();
    }
}
