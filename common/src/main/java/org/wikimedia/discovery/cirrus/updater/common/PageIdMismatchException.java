package org.wikimedia.discovery.cirrus.updater.common;

public class PageIdMismatchException extends CirrusDocFetchException {
    public PageIdMismatchException(String message) {
        super(message);
    }
}
