package org.wikimedia.discovery.cirrus.updater.common;

public class RevisionNotFoundException extends CirrusDocFetchException {
    public RevisionNotFoundException(String message) {
        super(message);
    }
}
