package org.wikimedia.discovery.cirrus.updater.common.util;

import java.nio.ByteBuffer;
import java.util.function.Consumer;
import java.util.zip.CRC32;
import java.util.zip.Checksum;
import java.util.zip.DataFormatException;
import java.util.zip.Inflater;

/**
 * Wrapper around {@link Inflater} for decoding gzipped data.
 *
 * <p>Based on <a
 * href="https://github.com/netty/netty/blob/4.1/codec/src/main/java/io/netty/handler/codec/compression/JdkZlibDecoder.java">JdkZlibDecoder</a>.
 */
public class GzipInflater implements AutoCloseable {

    private static final int METHOD_DEFLATED = 8;
    private static final int FHCRC = 0x02;
    private static final int FEXTRA = 0x04;
    private static final int FNAME = 0x08;
    private static final int FCOMMENT = 0x10;
    private static final int FRESERVED = 0xE0;

    private final Checksum crc = new CRC32();
    private final int inflaterBufferSize;
    private final Inflater inflater;
    private ByteBuffer inflaterBuffer;

    private int flags;
    private int xlen;

    private GzipState gzipState = GzipState.HEADER_START;

    private volatile boolean finished;

    public GzipInflater(int inflaterBufferSize) {
        this.inflater = new Inflater(true);
        this.inflaterBufferSize = inflaterBufferSize;
    }

    @Override
    public void close() {
        inflater.end();
        inflaterBuffer = null;
    }

    public void inflate(ByteBuffer in, Consumer<ByteBuffer> onInflated) throws DataFormatException {
        if (finished) {
            // Skip data received after finished.
            in.position(in.limit());
            return;
        }

        if (in.remaining() == 0) {
            return;
        }

        if (gzipState != GzipState.HEADER_END) {
            if (gzipState == GzipState.FOOTER_START) {
                if (!handleGzipFooter(in)) {
                    // Either there was not enough data or the input is finished.
                    return;
                }
                // If we consumed the footer we will start with the header again.
                assert gzipState == GzipState.HEADER_START;
            }
            if (!readHeader(in)) {
                // There was not enough data readable to read the GZIP header.
                return;
            }
            // do not attempt to inflate any more bytes if after reading the headers there's nothing left
            if (in.remaining() == 0) {
                return;
            }
        }

        inflatePayload(in, onInflated);
    }

    private void inflatePayload(ByteBuffer in, Consumer<ByteBuffer> onInflated)
            throws DataFormatException {
        if (inflater.needsInput()) {
            inflater.setInput(in);
        }

        if (inflaterBuffer == null) {
            inflaterBuffer = ByteBuffer.allocate(inflaterBufferSize);
        }

        while (!(inflater.needsInput() || inflater.finished())) {
            int decompressed = inflater.inflate(inflaterBuffer);

            if (decompressed == 0 && inflater.needsDictionary()) {
                throw new UnsupportedOperationException("Dictionaries are not supported");
            }

            inflaterBuffer.rewind();
            inflaterBuffer.limit(decompressed);

            crc.update(inflaterBuffer.array(), 0, decompressed);

            onInflated.accept(inflaterBuffer);

            if (inflater.finished()) {
                gzipState = GzipState.FOOTER_START;
                handleGzipFooter(in);
            }

            inflaterBuffer.clear();
        }
    }

    @SuppressWarnings("checkstyle:CyclomaticComplexity")
    private boolean readHeader(ByteBuffer in) {
        switch (gzipState) {
            case HEADER_START:
                if (in.remaining() < 10) {
                    return false;
                }
                // read magic numbers
                int magic0 = unsigned(in.get());
                int magic1 = unsigned(in.get());

                if (magic0 != 31) {
                    throw new IllegalStateException("Input is not in the GZIP format");
                }
                crc.update(magic0);
                crc.update(magic1);

                int method = unsigned(in.get());
                if (method != METHOD_DEFLATED) {
                    throw new UnsupportedOperationException(
                            "Unsupported compression method " + method + " in the GZIP header");
                }
                crc.update(method);

                flags = unsigned(in.get());
                crc.update(flags);

                if ((flags & FRESERVED) != 0) {
                    throw new IllegalStateException("Reserved flags are set in the GZIP header");
                }

                // mtime (int)
                crc.update(in.array(), in.position(), 4);
                in.position(in.position() + 4);

                crc.update(unsigned(in.get())); // extra flags
                crc.update(unsigned(in.get())); // operating system

                gzipState = GzipState.FLG_READ;
                // fall through
            case FLG_READ:
                if ((flags & FEXTRA) != 0) {
                    if (in.remaining() < 2) {
                        return false;
                    }
                    int xlen1 = unsigned(in.get());
                    int xlen2 = unsigned(in.get());
                    crc.update(xlen1);
                    crc.update(xlen2);

                    xlen |= xlen1 << 8 | xlen2;
                }
                gzipState = GzipState.XLEN_READ;
                // fall through
            case XLEN_READ:
                if (xlen != -1) {
                    if (in.remaining() < xlen) {
                        return false;
                    }
                    crc.update(in.array(), in.position(), xlen);
                    in.position(in.position() + xlen);
                }
                gzipState = GzipState.SKIP_FNAME;
                // fall through
            case SKIP_FNAME:
                if (!skipIfNeeded(in, FNAME)) {
                    return false;
                }
                gzipState = GzipState.SKIP_COMMENT;
                // fall through
            case SKIP_COMMENT:
                if (!skipIfNeeded(in, FCOMMENT)) {
                    return false;
                }
                gzipState = GzipState.PROCESS_FHCRC;
                // fall through
            case PROCESS_FHCRC:
                if ((flags & FHCRC) != 0 && !verifyCrc16(in)) {
                    return false;
                }
                crc.reset();
                gzipState = GzipState.HEADER_END;
                // fall through
            case HEADER_END:
                return true;
            default:
                throw new IllegalStateException();
        }
    }

    /**
     * Skip bytes in the input if needed until we find the end marker {@code 0x00}.
     *
     * @param in the input
     * @param flagMask the mask that should be present in the {@code flags} when we need to skip
     *     bytes.
     * @return {@code true} if the operation is complete and we can move to the next state, {@code
     *     false} if we need the retry again once we have more readable bytes.
     */
    private boolean skipIfNeeded(ByteBuffer in, int flagMask) {
        if ((flags & flagMask) != 0) {
            for (; ; ) {
                if (!in.hasRemaining()) {
                    // We didn't find the end yet, need to retry again once more data is readable
                    return false;
                }
                int b = unsigned(in.get());
                crc.update(b);
                if (b == 0x00) {
                    break;
                }
            }
        }
        // Skip is handled, we can move to the next processing state.
        return true;
    }

    private boolean handleGzipFooter(ByteBuffer in) {
        if (readFooter(in)) {
            finished = true;

            if (!finished) {
                inflater.reset();
                crc.reset();
                gzipState = GzipState.HEADER_START;
                return true;
            }
        }
        return false;
    }

    /**
     * Read the GZIP footer.
     *
     * @param in the input.
     * @return {@code true} if the footer could be read, {@code false} if the read could not be
     *     performed as the input {@link ByteBuffer} doesn't have enough readable bytes (8 bytes).
     */
    private boolean readFooter(ByteBuffer in) {
        if (in.remaining() < 8) {
            return false;
        }

        boolean enoughData = verifyCrc(in);
        assert enoughData;

        // read ISIZE and verify
        int dataLength = 0;
        for (int i = 0; i < 4; ++i) {
            dataLength |= unsigned(in.get()) << i * 8;
        }
        int readLength = inflater.getTotalOut();
        if (dataLength != readLength) {
            throw new IllegalStateException(
                    "Number of bytes mismatch. Expected: " + dataLength + ", Got: " + readLength);
        }
        return true;
    }

    /**
     * Verifies body CRC.
     *
     * @param in the input.
     * @return {@code true} if verification could be performed, {@code false} if verification could
     *     not be performed as the input {@link ByteBuffer} doesn't have enough readable bytes (4
     *     bytes).
     */
    private boolean verifyCrc(ByteBuffer in) {
        if (in.remaining() < 4) {
            return false;
        }
        long crcValue = 0;
        for (int i = 0; i < 4; ++i) {
            crcValue |= (long) unsigned(in.get()) << i * 8;
        }
        long readCrc = crc.getValue();
        if (crcValue != readCrc) {
            throw new IllegalStateException(
                    "CRC value mismatch. Expected: " + crcValue + ", Got: " + readCrc);
        }
        return true;
    }

    /**
     * Verifies header CRC.
     *
     * @param in the input.
     * @return {@code true} if verification could be performed, {@code false} if verification could
     *     not be performed as the input {@link ByteBuffer} doesn't have enough readable bytes (2
     *     bytes).
     */
    private boolean verifyCrc16(ByteBuffer in) {
        if (in.remaining() < 2) {
            return false;
        }
        long readCrc32 = crc.getValue();
        long crc16Value = 0;
        long readCrc16 = 0; // the two least significant bytes from the CRC32
        for (int i = 0; i < 2; ++i) {
            crc16Value |= (long) unsigned(in.get()) << (i * 8);
            readCrc16 |= ((readCrc32 >> (i * 8)) & 0xff) << (i * 8);
        }

        if (crc16Value != readCrc16) {
            throw new IllegalStateException(
                    "CRC16 value mismatch. Expected: " + crc16Value + ", Got: " + readCrc16);
        }
        return true;
    }

    /** @return the byte value converted to an unsigned int value */
    private static int unsigned(byte b) {
        return b & 0xFF;
    }

    private enum GzipState {
        HEADER_START,
        HEADER_END,
        FLG_READ,
        XLEN_READ,
        SKIP_FNAME,
        SKIP_COMMENT,
        PROCESS_FHCRC,
        FOOTER_START;

        GzipState() {}
    }
}
