package org.wikimedia.discovery.cirrus.updater.common.model;

import static org.apache.flink.api.common.typeinfo.BasicTypeInfo.INSTANT_TYPE_INFO;
import static org.apache.flink.api.common.typeinfo.BasicTypeInfo.INT_TYPE_INFO;
import static org.apache.flink.api.common.typeinfo.BasicTypeInfo.LONG_TYPE_INFO;
import static org.apache.flink.api.common.typeinfo.BasicTypeInfo.STRING_TYPE_INFO;

import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;

import org.apache.flink.api.common.typeinfo.TypeInfoFactory;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.api.java.typeutils.PojoField;
import org.apache.flink.api.java.typeutils.PojoTypeInfo;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

@SuppressFBWarnings("FCCD_FIND_CLASS_CIRCULAR_DEPENDENCY")
public class SanityCheckTypeInfoFactory extends TypeInfoFactory<SanityCheck> {
    public static TypeInformation<SanityCheck> create() {
        return new PojoTypeInfo<>(SanityCheck.class, getFields());
    }

    private static List<PojoField> getFields() {
        try {
            final Field domain = SanityCheck.class.getDeclaredField("domain");
            final Field startPageId = SanityCheck.class.getDeclaredField("startPageId");
            final Field batchSize = SanityCheck.class.getDeclaredField("batchSize");
            final Field eventTime = SanityCheck.class.getDeclaredField("eventTime");
            final Field loopId = SanityCheck.class.getDeclaredField("loopId");

            return List.of(
                    new PojoField(domain, STRING_TYPE_INFO),
                    new PojoField(startPageId, LONG_TYPE_INFO),
                    new PojoField(batchSize, INT_TYPE_INFO),
                    new PojoField(eventTime, INSTANT_TYPE_INFO),
                    new PojoField(loopId, INT_TYPE_INFO));
        } catch (NoSuchFieldException e) {
            throw new IllegalStateException(
                    "Field to build type information for " + SanityCheck.class, e);
        }
    }

    public TypeInformation<SanityCheck> createTypeInfo(
            Type t, Map<String, TypeInformation<?>> genericParameters) {
        return create();
    }
}
