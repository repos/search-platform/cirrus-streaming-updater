package org.wikimedia.discovery.cirrus.updater.common.model;

import java.io.Serializable;

import org.apache.flink.types.Row;
import org.jetbrains.annotations.NotNull;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateEvent.Meta;
import org.wikimedia.discovery.cirrus.updater.common.util.function.SerializableSupplier;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

@SuppressFBWarnings(value = "CE_CLASS_ENVY", justification = "false positive")
public final class MetaRowTranscoder implements Serializable {

    public static final String DEFAULT_META_FIELD = "meta";

    public static final String META_DT = "dt";
    public static final String META_ID = "id";
    public static final String META_URI = "uri";
    public static final String META_DOMAIN = "domain";
    public static final String META_STREAM = "stream";
    public static final String META_REQUEST_ID = "request_id";
    private final SerializableSupplier<Row> rowFactory;

    private MetaRowTranscoder() {
        this(
                () -> {
                    throw new UnsupportedOperationException(
                            "Encoding is only supported with type information");
                });
    }

    private MetaRowTranscoder(SerializableSupplier<Row> rowFactory) {
        this.rowFactory = rowFactory;
    }

    public static MetaRowTranscoder encoder(SerializableSupplier<Row> rowFactory) {
        return new MetaRowTranscoder(rowFactory);
    }

    public static MetaRowTranscoder decoder() {
        return new MetaRowTranscoder();
    }

    @NotNull
    public Row encode(Meta meta) {
        final Row metaRow = rowFactory.get();
        if (meta != null) {
            metaRow.setField(META_DT, meta.getDt());
            metaRow.setField(META_ID, meta.getId());
            metaRow.setField(META_URI, meta.getUri());
            metaRow.setField(META_DOMAIN, meta.getDomain());
            metaRow.setField(META_STREAM, meta.getStream());
            metaRow.setField(META_REQUEST_ID, meta.getRequestId());
        }
        return metaRow;
    }

    public Meta decode(Row metaRow) {
        return Meta.builder()
                .dt(metaRow.getFieldAs(META_DT))
                .id(metaRow.getFieldAs(META_ID))
                .uri(metaRow.getFieldAs(META_URI))
                .domain(metaRow.getFieldAs(META_DOMAIN))
                .stream(metaRow.getFieldAs(META_STREAM))
                .requestId(metaRow.getFieldAs(META_REQUEST_ID))
                .build();
    }
}
