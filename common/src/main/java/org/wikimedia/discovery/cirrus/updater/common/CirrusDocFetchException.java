package org.wikimedia.discovery.cirrus.updater.common;

/** Exception thrown while fetching the cirrus doc out of the MW API. */
public abstract class CirrusDocFetchException extends RuntimeException {
    protected CirrusDocFetchException(String s) {
        super(s);
    }
}
