package org.wikimedia.discovery.cirrus.updater.common.config;

import lombok.experimental.UtilityClass;

@UtilityClass
@SuppressWarnings("HideUtilityClassConstructor")
public class EventStreamConstants {

    public static final String UPDATER_PUBLIC_OUTPUT_STREAM_NAME =
            "cirrussearch.update_pipeline.update.v1";
    public static final String UPDATER_PRIVATE_OUTPUT_STREAM_NAME =
            "cirrussearch.update_pipeline.update.private.v1";
    public static final String UPDATER_OUTPUT_SCHEMA_VERSION = "1.0.0";
    public static final String FETCH_ERROR_STREAM = "cirrussearch.update_pipeline.fetch_error.v1";
    public static final String FETCH_ERROR_SCHEMA_VERSION = "1.0.0";
}
