package org.wikimedia.discovery.cirrus.updater.common.model;

import java.lang.reflect.Field;
import java.util.List;

import org.apache.flink.api.common.typeinfo.BasicTypeInfo;
import org.apache.flink.api.common.typeinfo.TypeHint;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.api.java.typeutils.PojoField;
import org.apache.flink.api.java.typeutils.PojoTypeInfo;

public final class FetchFailureTypeInformation {

    private FetchFailureTypeInformation() {}

    public static <I> PojoTypeInfo<FetchFailure<I>> create(TypeInformation<I> inputTypeInfo) {
        return new PojoTypeInfo<>(
                new TypeHint<FetchFailure<I>>() {}.getTypeInfo().getTypeClass(), getFields(inputTypeInfo));
    }

    private static <I> List<PojoField> getFields(TypeInformation<I> inputTypeInfo) {
        try {
            final Field input = FetchFailure.class.getDeclaredField("event");
            final Field errorType = FetchFailure.class.getDeclaredField("failureType");
            final Field errorMessage = FetchFailure.class.getDeclaredField("failureMessage");
            final Field errorStack = FetchFailure.class.getDeclaredField("failureStack");
            return List.of(
                    new PojoField(input, inputTypeInfo),
                    new PojoField(errorType, BasicTypeInfo.STRING_TYPE_INFO),
                    new PojoField(errorMessage, BasicTypeInfo.STRING_TYPE_INFO),
                    new PojoField(errorStack, BasicTypeInfo.STRING_TYPE_INFO));
        } catch (NoSuchFieldException e) {
            throw new IllegalStateException(
                    "Failed to build type information for " + FetchFailure.class, e);
        }
    }
}
