package org.wikimedia.discovery.cirrus.updater.common.model;

import com.fasterxml.jackson.databind.JsonNode;

import lombok.experimental.UtilityClass;

@UtilityClass
@SuppressWarnings("HideUtilityClassConstructor")
public class JsonPathUtils {
    public static JsonNode getRequiredNode(JsonNode parentNode, String path) {
        final JsonNode node = parentNode.at(path);
        if (node.isMissingNode()) {
            throw new IllegalArgumentException("Missing property '" + path + "'");
        }
        return node;
    }
}
