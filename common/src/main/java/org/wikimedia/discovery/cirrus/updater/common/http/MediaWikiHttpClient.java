package org.wikimedia.discovery.cirrus.updater.common.http;

import java.io.IOException;
import java.util.function.Function;

import javax.annotation.Nonnull;

import org.apache.flink.metrics.MetricGroup;
import org.apache.hc.client5.http.impl.classic.CloseableHttpClient;
import org.apache.hc.core5.http.ClassicHttpRequest;
import org.wikimedia.discovery.cirrus.updater.common.InvalidMWApiResponseException;
import org.wikimedia.discovery.cirrus.updater.common.util.function.SerializableFunction;

import com.codahale.metrics.MetricRegistry;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.SneakyThrows;

public final class MediaWikiHttpClient implements AutoCloseable {
    private static final int ATTEMPTS = 5;
    /** tune the delay between retries: 0.1 should give 0.1, 0.3, 0.7 and 1.5 seconds. */
    private static final double BACKOFF_FACTOR = 0.1;

    private final ObjectMapper objectMapper = new ObjectMapper();
    private final CloseableHttpClient client;

    private MediaWikiHttpClient(HttpConfig httpConfig, MetricGroup metricGroup) {
        this.client = makeClient(httpConfig, metricGroup);
    }

    public interface Factory extends SerializableFunction<MetricGroup, MediaWikiHttpClient> {}

    public static Factory factory(HttpConfig httpConfig) {
        return (metricGroup) -> new MediaWikiHttpClient(httpConfig, metricGroup);
    }

    @Override
    public void close() throws Exception {
        if (this.client != null) {
            this.client.close();
        }
    }

    private static CloseableHttpClient makeClient(HttpConfig httpConfig, MetricGroup metricGroup) {
        final MetricRegistry metricRegistry = new MetricRegistry();
        metricRegistry.addListener(new MetricGroupMetricRegistryListener(metricGroup.addGroup("http")));
        return HttpClientFactory.buildSyncClient(httpConfig, metricRegistry);
    }

    @SneakyThrows(InterruptedException.class)
    @Nonnull
    public <T> T load(ClassicHttpRequest request, Function<JsonNode, T> responseHandler) {
        Throwable thrown = null;
        for (int attempt = 0; attempt < ATTEMPTS; attempt++) {
            try {
                long sleep = (long) backoffDelayMs(attempt);
                if (attempt > 0) { // NOSONAR (silence FP java:S2583)
                    Thread.sleep(sleep);
                }
                return fetch(request, responseHandler);
            } catch (IOException | InvalidMWApiResponseException e) {
                thrown = e;
            }
        }
        throw new IllegalStateException(
                "Failed to invoke mw api at " + request.getRequestUri(), thrown);
    }

    private <T> T fetch(ClassicHttpRequest request, Function<JsonNode, T> responseHandler)
            throws IOException {
        return client.execute(
                request,
                (response) -> {
                    if (response.getCode() != 200) {
                        throw new InvalidMWApiResponseException(response.getReasonPhrase());
                    }
                    try (JsonParser parser = objectMapper.createParser(response.getEntity().getContent())) {
                        JsonNode jsonResponse = parser.readValueAsTree();
                        // Do not trust a 200 and double the presence of an error entry
                        if (jsonResponse.has("error")) {
                            throw new InvalidMWApiResponseException(jsonResponse.get("error").asText());
                        }
                        return responseHandler.apply(jsonResponse);
                    }
                });
    }

    private double backoffDelayMs(int attempt) {
        return 1000D * (BACKOFF_FACTOR * (Math.pow(2D, attempt) - 1D));
    }
}
