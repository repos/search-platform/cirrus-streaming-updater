package org.wikimedia.discovery.cirrus.updater.common.graph;

import java.io.Serializable;
import java.net.URI;

import com.fasterxml.jackson.databind.JsonNode;

/**
 * Define an endpoint to fetch from CirrusSearch.
 *
 * @param <I> The type of input for this endpoint
 * @param <O> The type of output emitted form this endpoint
 */
public interface CirrusEndpoint<I, O> extends Serializable {

    URI buildURI(I event);

    O extractAndAugment(I event, JsonNode response);
}
