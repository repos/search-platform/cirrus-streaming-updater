package org.wikimedia.discovery.cirrus.updater.common.graph;

import java.io.Closeable;
import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.flink.api.common.functions.RuntimeContext;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.metrics.Counter;
import org.apache.flink.metrics.MetricGroup;
import org.apache.flink.streaming.api.functions.async.ResultFuture;
import org.apache.flink.streaming.api.functions.async.RichAsyncFunction;
import org.apache.flink.types.Either;
import org.wikimedia.discovery.cirrus.updater.common.model.ChangeType;
import org.wikimedia.discovery.cirrus.updater.common.model.FetchFailure;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateEvent;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

/**
 * Wrapper around {@link CirrusFetcher} that bypasses fetching revisions under certain conditions.
 *
 * <p>Allows skipping {@link ChangeType#PAGE_DELETE}s while maintaining the order of events from
 * flink's perspective.
 */
@SuppressFBWarnings(value = "SE_NO_SERIALVERSIONID")
public class BypassingCirrusDocFetcher
        extends RichAsyncFunction<UpdateEvent, Either<UpdateEvent, FetchFailure<UpdateEvent>>>
        implements Closeable {

    private static final String COUNTER_SUFFIX_ATTEMPTS = "attempts";
    private static final String COUNTER_SUFFIX_RETRIED = "retried";
    private final CirrusFetcher<UpdateEvent, UpdateEvent> delegate;

    private transient MetricGroup retryMetricGroup;

    private transient Map<String, MetricGroup> authorityMetricGroups;
    private transient Map<String, Counter> retryCounters;

    public BypassingCirrusDocFetcher(CirrusFetcher<UpdateEvent, UpdateEvent> delegate) {
        this.delegate = delegate;
    }

    public static boolean fetchRequired(UpdateEvent updateEvent) {
        return updateEvent.getChangeType() == ChangeType.REV_BASED_UPDATE
                || updateEvent.getChangeType() == ChangeType.PAGE_RERENDER
                || updateEvent.getChangeType() == ChangeType.PAGE_RERENDER_UPSERT;
    }

    @Override
    public void open(Configuration parameters) throws Exception {
        super.open(parameters);
        delegate.open(parameters);
    }

    @Override
    public void setRuntimeContext(RuntimeContext runtimeContext) {
        super.setRuntimeContext(runtimeContext);
        delegate.setRuntimeContext(runtimeContext);

        retryMetricGroup = runtimeContext.getMetricGroup().addGroup("retry");
        retryCounters = new ConcurrentHashMap<>();

        authorityMetricGroups = new ConcurrentHashMap<>();
    }

    @Override
    public void asyncInvoke(
            UpdateEvent event,
            ResultFuture<Either<UpdateEvent, FetchFailure<UpdateEvent>>> resultFuture) {
        if (fetchRequired(event)) {
            if (event.hasFetchedFields()) {
                // fail since we fetch late and expect only lean events without fields
                resultFuture.completeExceptionally(
                        new IllegalStateException("Fields already set for " + event));
            }
            final String domain = event.getTargetDocument().getDomain();
            if (event.getFetchAttempt() == null) {
                event.setFetchAttempt(0);
            } else {
                final MetricGroup authorityMetricGroup =
                        authorityMetricGroups.computeIfAbsent(
                                domain, key -> retryMetricGroup.addGroup("authority", key));
                if (event.getFetchAttempt() == 0) {
                    retryCounters
                            .computeIfAbsent(
                                    COUNTER_SUFFIX_RETRIED + "_" + domain,
                                    (key) -> authorityMetricGroup.counter(COUNTER_SUFFIX_RETRIED))
                            .inc();
                }
                retryCounters
                        .computeIfAbsent(
                                COUNTER_SUFFIX_ATTEMPTS + "_" + domain,
                                (key) -> authorityMetricGroup.counter(COUNTER_SUFFIX_ATTEMPTS))
                        .inc();
                event.setFetchAttempt(event.getFetchAttempt() + 1);
            }
            delegate.asyncInvoke(
                    event,
                    new ResultFuture<>() {
                        @Override
                        public void complete(
                                Collection<Either<UpdateEvent, FetchFailure<UpdateEvent>>> result) {
                            resultFuture.complete(result);
                        }

                        @Override
                        public void completeExceptionally(Throwable error) {
                            resultFuture.completeExceptionally(error);
                        }
                    });
        } else {
            resultFuture.complete(List.of(Either.Left(event)));
        }
    }

    @Override
    public void timeout(
            UpdateEvent input, ResultFuture<Either<UpdateEvent, FetchFailure<UpdateEvent>>> resultFuture)
            throws Exception {
        delegate.timeout(input, resultFuture);
    }

    @Override
    public void close() throws IOException {
        delegate.close();
    }
}
