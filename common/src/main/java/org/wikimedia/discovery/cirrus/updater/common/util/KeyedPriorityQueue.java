package org.wikimedia.discovery.cirrus.updater.common.util;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.PriorityQueue;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Predicate;

/**
 * Provides minimal portion of the PriorityQueue api along with keyed access to values. Allows
 * metrics to efficiently reach into the priority queue. Assumes all stored values are immutable.
 */
public class KeyedPriorityQueue<K, V> {
    private final Function<V, K> keyExtractor;
    private final Map<K, V> map = new HashMap<>();
    private final PriorityQueue<V> pq;

    public KeyedPriorityQueue(Function<V, K> keyExtractor, Comparator<? super V> comparator) {
        this.keyExtractor = keyExtractor;
        pq = new PriorityQueue<>(comparator);
    }

    public void addAll(Collection<V> c) {
        pq.addAll(c);
        c.forEach(v -> map.put(keyExtractor.apply(v), v));
    }

    public void add(V v) {
        pq.add(v);
        map.put(keyExtractor.apply(v), v);
    }

    public V poll() {
        V v = pq.poll();
        if (v != null) {
            map.remove(keyExtractor.apply(v));
        }
        return v;
    }

    public Optional<V> get(K k) {
        return Optional.ofNullable(map.get(k));
    }

    public boolean isEmpty() {
        return pq.isEmpty();
    }

    public V peek() {
        return pq.peek();
    }

    public Set<K> keySet() {
        return Collections.unmodifiableSet(map.keySet());
    }

    public void removeIf(Predicate<V> predicate) {
        pq.removeIf(predicate);
        map.values().removeIf(predicate);
    }
}
