package org.wikimedia.discovery.cirrus.updater.common.model;

import java.io.Serializable;
import java.time.Instant;
import java.util.Arrays;
import java.util.Collection;
import java.util.Optional;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.Nonnull;

import org.apache.flink.types.Row;
import org.jetbrains.annotations.NotNull;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateEvent.Meta;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateEvent.TargetDocument;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateEvent.TargetDocument.TargetDocumentBuilder;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateEvent.Update;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateEvent.Update.UpdateBuilder;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateEvent.UpdateEventBuilder;
import org.wikimedia.discovery.cirrus.updater.common.util.function.SerializableFunction;
import org.wikimedia.eventutilities.flink.EventRowTypeInfo;

import com.google.common.base.Preconditions;

public final class UpdateRowTranscoder implements Serializable {

    private final SerializableFunction<String, Row> rowFactory;
    private final MetaRowTranscoder metaRowTranscoder;

    private UpdateRowTranscoder(
            SerializableFunction<String, Row> rowFactory, MetaRowTranscoder metaRowTranscoder) {
        this.rowFactory = rowFactory;
        this.metaRowTranscoder = metaRowTranscoder;
    }

    private static TargetDocument decodeRedirectTargetDocument(Row row) {
        return TargetDocument.builder()
                .pageNamespace(row.getFieldAs(UpdateFields.REDIRECT_NAMESPACE))
                .pageTitle(row.getFieldAs(UpdateFields.REDIRECT_TITLE))
                .build();
    }

    public static UpdateRowTranscoder decoder() {
        return new UpdateRowTranscoder(
                (field) -> {
                    throw new UnsupportedOperationException(
                            "Encoding is only supported with type information");
                },
                MetaRowTranscoder.decoder());
    }

    public static UpdateRowTranscoder encoder(EventRowTypeInfo typeInfo) {
        return new UpdateRowTranscoder(
                (fieldPath) ->
                        fieldPath == null || fieldPath.isEmpty()
                                ? typeInfo.createEmptyRow()
                                : typeInfo.createEmptySubRow(fieldPath),
                MetaRowTranscoder.encoder(
                        () -> typeInfo.createEmptySubRow(MetaRowTranscoder.DEFAULT_META_FIELD)));
    }

    @Nonnull
    public UpdateEvent decode(Row row) {
        final UpdateEventBuilder updateEventBuilder = UpdateEvent.builder();

        final ChangeType changeType = ChangeType.valueOf(row.getFieldAs(UpdateFields.CHANGE_TYPE));

        updateEventBuilder.changeType(changeType);

        updateEventBuilder.eventTime(row.getFieldAs(UpdateFields.DT));

        Row metaRow = row.getFieldAs(MetaRowTranscoder.DEFAULT_META_FIELD);

        final Meta meta = metaRowTranscoder.decode(metaRow);
        updateEventBuilder.meta(meta);

        final TargetDocumentBuilder targetDocumentBuilder =
                new TargetDocument()
                        .toBuilder()
                                .domain(meta.getDomain())
                                .wikiId(row.getFieldAs(UpdateFields.WIKI_ID))
                                .indexName(row.getFieldAs(UpdateFields.INDEX_NAME))
                                .pageId(row.getFieldAs(UpdateFields.PAGE_ID))
                                .pageTitle(row.getFieldAs(UpdateFields.PAGE_TITLE))
                                .pageNamespace(row.getFieldAs(UpdateFields.NAMESPACE_ID))
                                .clusterGroup(row.getFieldAs(UpdateFields.CLUSTER_GROUP));
        final TargetDocument targetDocument = targetDocumentBuilder.build();
        updateEventBuilder.targetDocument(targetDocument);

        updateEventBuilder.revId(row.getFieldAs(UpdateFields.REV_ID));

        if (changeType != ChangeType.PAGE_DELETE) {
            final Row rawFields =
                    Optional.ofNullable(row.<Row>getFieldAs(UpdateFields.FIELDS)).map(Row::copy).orElse(null);
            final Row noopFields = row.getFieldAs(UpdateFields.NOOP_FIELDS);

            final UpdateBuilder updateBuilder =
                    Update.builder()
                            .rawFields(rawFields)
                            .rawFieldsFetchedAt(row.getFieldAs(UpdateFields.FETCHED_AT))
                            .noopHints(row.getFieldAs(UpdateFields.NOOP_HINTS));

            if (rawFields != null) {
                final String[] weightedTags = rawFields.getFieldAs(UpdateFields.WEIGHTED_TAGS);
                if (weightedTags != null) {
                    rawFields.setField(UpdateFields.WEIGHTED_TAGS, null);
                    updateBuilder.weightedTags(Arrays.asList(weightedTags));
                }
            }

            if (noopFields != null) {
                final Row redirectNoopFields = noopFields.getFieldAs(UpdateFields.REDIRECT);
                if (redirectNoopFields != null) {
                    Optional.ofNullable(redirectNoopFields.<Row[]>getFieldAs(UpdateFields.NOOP_HINTS_SET_ADD))
                            .map(Stream::of)
                            .map(
                                    stream ->
                                            stream
                                                    .map(UpdateRowTranscoder::decodeRedirectTargetDocument)
                                                    .collect(Collectors.toList()))
                            .ifPresent(updateBuilder::redirectAdd);

                    Optional.ofNullable(
                                    redirectNoopFields.<Row[]>getFieldAs(UpdateFields.NOOP_HINTS_SET_REMOVE))
                            .map(Stream::of)
                            .map(
                                    stream ->
                                            stream
                                                    .map(UpdateRowTranscoder::decodeRedirectTargetDocument)
                                                    .collect(Collectors.toList()))
                            .ifPresent(updateBuilder::redirectRemove);
                }
            }

            updateEventBuilder.update(updateBuilder.build());
        }

        return updateEventBuilder.build();
    }

    @Nonnull
    public Row encode(UpdateEvent updateEvent) {
        try {
            return encodeWithException(updateEvent);
        } catch (IllegalArgumentException | IllegalStateException e) {
            throw new RuntimeException("Failed to encode " + updateEvent, e);
        }
    }

    @NotNull
    private Row encodeWithException(UpdateEvent updateEvent) {
        final TargetDocument target = updateEvent.getTargetDocument();
        if (!target.isComplete()) {
            throw new IllegalArgumentException("TargetDocument incomplete");
        }

        final Row row = rowFactory.apply("");
        row.setField(UpdateFields.DT, updateEvent.getEventTime());

        row.setField(
                MetaRowTranscoder.DEFAULT_META_FIELD, metaRowTranscoder.encode(updateEvent.getMeta()));

        row.setField(UpdateFields.CHANGE_TYPE, updateEvent.getChangeType().name());
        row.setField(UpdateFields.WIKI_ID, target.getWikiId());

        row.setField(UpdateFields.NAMESPACE_ID, target.getPageNamespace());
        row.setField(UpdateFields.PAGE_ID, target.getPageId());
        row.setField(UpdateFields.REV_ID, updateEvent.getRevId());

        row.setField(UpdateFields.CLUSTER_GROUP, target.getClusterGroup());
        row.setField(UpdateFields.INDEX_NAME, target.getIndexName());

        Update update = updateEvent.getUpdate();

        final LazyRowSupplier fieldsRowSupplier;

        if (update == null) {
            if (updateEvent.getChangeType() == ChangeType.PAGE_DELETE) {
                return row;
            }
            fieldsRowSupplier = new LazyRowSupplier(() -> rowFactory.apply(UpdateFields.FIELDS));
        } else {
            fieldsRowSupplier =
                    new LazyRowSupplier(() -> rowFactory.apply(UpdateFields.FIELDS), update.getRawFields());

            encodeWeightedTags(updateEvent.getUpdate(), fieldsRowSupplier);

            if (update.getNoopHints() != null) {
                row.setField(UpdateFields.NOOP_HINTS, update.getNoopHints());
            }

            final LazyRowSupplier noopFieldsRowSupplier =
                    new LazyRowSupplier(() -> rowFactory.apply(UpdateFields.NOOP_FIELDS));

            encodeRedirectUpdate(update, noopFieldsRowSupplier);

            noopFieldsRowSupplier
                    .getRow()
                    .ifPresent(noopFields -> row.setField(UpdateFields.NOOP_FIELDS, noopFields));

            final Instant rawFieldsFetchedAt = update.getRawFieldsFetchedAt();
            if (rawFieldsFetchedAt != null) {
                row.setField(UpdateFields.FETCHED_AT, rawFieldsFetchedAt);
            }
        }

        fieldsRowSupplier.getRow().ifPresent(fields -> row.setField(UpdateFields.FIELDS, fields));

        return row;
    }

    private void encodeWeightedTags(Update update, Supplier<Row> fieldsSupplier) {
        Preconditions.checkArgument(
                update.getRawFields() == null
                        || update.getRawFields().getField(UpdateFields.WEIGHTED_TAGS) == null,
                "Weighted tags must only be provided through the dedicated class property");
        if (update.getWeightedTags() != null) {
            Row fields = fieldsSupplier.get();
            // we don't attempt to merge with an existing value as this should never
            // be provided by cirrusdoc.
            fields.setField(UpdateFields.WEIGHTED_TAGS, update.getWeightedTags().toArray(new String[0]));
        }
    }

    private void encodeRedirectUpdate(Update update, Supplier<Row> noopFieldsRowSupplier) {
        final String noopFieldsRedirectPath =
                fieldPath(UpdateFields.NOOP_FIELDS, UpdateFields.REDIRECT);
        final Row noopFieldsRedirect = rowFactory.apply(noopFieldsRedirectPath);
        Optional.ofNullable(update.getRedirectAdd())
                .filter(add -> !add.isEmpty())
                .map(
                        add ->
                                encodeRedirects(
                                        add, fieldPath(noopFieldsRedirectPath, UpdateFields.NOOP_HINTS_SET_ADD)))
                .ifPresent(
                        add -> {
                            noopFieldsRedirect.setField(UpdateFields.NOOP_HINTS_SET_ADD, add);
                            noopFieldsRowSupplier.get().setField(UpdateFields.REDIRECT, noopFieldsRedirect);
                        });
        Optional.ofNullable(update.getRedirectRemove())
                .filter(remove -> !remove.isEmpty())
                .map(
                        remove ->
                                encodeRedirects(
                                        remove, fieldPath(noopFieldsRedirectPath, UpdateFields.NOOP_HINTS_SET_REMOVE)))
                .ifPresent(
                        remove -> {
                            noopFieldsRedirect.setField(UpdateFields.NOOP_HINTS_SET_REMOVE, remove);
                            noopFieldsRowSupplier.get().setField(UpdateFields.REDIRECT, noopFieldsRedirect);
                        });
    }

    private Row[] encodeRedirects(Collection<TargetDocument> redirects, String addSubRowPath) {
        return redirects.stream()
                .map(redirectTarget -> encodeRedirect(redirectTarget, addSubRowPath))
                .distinct()
                .toArray(Row[]::new);
    }

    private Row encodeRedirect(TargetDocument redirectTarget, String subRowPath) {
        final Row redirect = rowFactory.apply(subRowPath);
        redirect.setField(UpdateFields.REDIRECT_NAMESPACE, redirectTarget.getPageNamespace());
        redirect.setField(UpdateFields.REDIRECT_TITLE, redirectTarget.getPageTitle());
        return redirect;
    }

    private static String fieldPath(String... fields) {
        return String.join(".", fields);
    }

    /**
     * Supplies a {@code noop_fields} row on demand.
     *
     * <p>Allows for sparse updates.
     */
    private static class LazyRowSupplier implements Supplier<Row> {

        private final Supplier<Row> rowFactory;

        private Row row;

        private final Row source;

        LazyRowSupplier(Supplier<Row> rowFactory) {
            this(rowFactory, null);
        }

        LazyRowSupplier(Supplier<Row> rowFactory, Row row) {
            this.source = row;
            this.rowFactory = rowFactory;
        }

        @Override
        public Row get() {
            if (row == null) {
                if (source == null) {
                    row = rowFactory.get();
                } else {
                    row = Row.copy(source);
                }
            }
            return row;
        }

        public Optional<Row> getRow() {
            return Optional.ofNullable(row).or(() -> Optional.ofNullable(source));
        }
    }
}
