package org.wikimedia.discovery.cirrus.updater.common.json;

import java.io.EOFException;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.function.Consumer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.core.async.ByteBufferFeeder;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.util.TokenBuffer;

/**
 * Asynchronous/incremental JSON parser.
 *
 * <p>Based on <a href="https://github.com/mmimica/async-jackson/tree/master">async-jackson</a> and
 * <a href="https://github.com/spring-projects/spring-framework/blob/main/spring-
 * web/src/main/java/org/springframework/http/codec/json/Jackson2Tokenizer.java">spring-web</a>.
 */
public class AsyncJsonParser implements AutoCloseable {

    private final ObjectMapper mapper;
    private final JsonParser parser;
    private final TokenBuffer tokenBuffer;

    private int objectDepth;
    private int arrayDepth;

    private final Consumer<JsonNode> completedCallback;

    public AsyncJsonParser(ObjectMapper mapper, Consumer<JsonNode> completedCallback)
            throws IOException {
        this.mapper = mapper;
        this.parser = mapper.getFactory().createNonBlockingByteBufferParser();
        this.tokenBuffer = new TokenBuffer(parser, mapper.getDeserializationContext());
        this.completedCallback = completedCallback;
    }

    @Override
    public void close() throws IOException {
        tokenBuffer.close();
        parser.close();
    }

    public void endOfInput() throws IOException {
        parser.getNonBlockingInputFeeder().endOfInput();
        if (this.objectDepth != 0 || this.arrayDepth != 0) {
            throw new EOFException(
                    "Prematurely reached end of input (incomplete object(s): "
                            + this.objectDepth
                            + ", incomplete array(s): "
                            + this.arrayDepth);
        }
        completedCallback.accept(mapper.readTree(tokenBuffer.asParser()));
    }

    public void feedInput(ByteBuffer buffer) throws IOException {
        ((ByteBufferFeeder) parser.getNonBlockingInputFeeder()).feedInput(buffer);

        while (parser.nextToken() != JsonToken.NOT_AVAILABLE) {
            updateDepth(parser.currentToken());
            tokenBuffer.copyCurrentEvent(parser);
        }
    }

    private void updateDepth(JsonToken token) {
        switch (token) {
            case START_OBJECT:
                this.objectDepth++;
                break;
            case END_OBJECT:
                this.objectDepth--;
                break;
            case START_ARRAY:
                this.arrayDepth++;
                break;
            case END_ARRAY:
                this.arrayDepth--;
                break;
            default:
        }
    }
}
