package org.wikimedia.discovery.cirrus.updater.common.model;

import static org.apache.flink.api.common.typeinfo.BasicTypeInfo.BOOLEAN_TYPE_INFO;
import static org.apache.flink.api.common.typeinfo.BasicTypeInfo.INSTANT_TYPE_INFO;
import static org.apache.flink.api.common.typeinfo.BasicTypeInfo.INT_TYPE_INFO;
import static org.apache.flink.api.common.typeinfo.BasicTypeInfo.LONG_TYPE_INFO;
import static org.apache.flink.api.common.typeinfo.BasicTypeInfo.STRING_TYPE_INFO;

import java.lang.reflect.Field;
import java.util.List;

import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.api.java.typeutils.ListTypeInfo;
import org.apache.flink.api.java.typeutils.MapTypeInfo;
import org.apache.flink.api.java.typeutils.PojoField;
import org.apache.flink.api.java.typeutils.PojoTypeInfo;
import org.apache.flink.types.Row;
import org.jetbrains.annotations.NotNull;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateEvent.Meta;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateEvent.TargetDocument;
import org.wikimedia.eventutilities.flink.EventRowTypeInfo;

import com.google.common.base.Verify;
import com.google.common.collect.ImmutableList;

import lombok.experimental.UtilityClass;

@UtilityClass
@SuppressWarnings("HideUtilityClassConstructor")
public class UpdateEventTypeInfo {

    public static TypeInformation<UpdateEvent> create(EventRowTypeInfo updateRowTypeInfo) {
        return new PojoTypeInfo<>(
                UpdateEvent.class, getFields(updateRowTypeInfo.getTypeAt(UpdateFields.FIELDS)));
    }

    private static List<PojoField> getFields(TypeInformation<Row> fieldsTypeInfo) {
        try {
            final Field meta = UpdateEvent.class.getDeclaredField("meta");
            final Field changeType = UpdateEvent.class.getDeclaredField("changeType");
            final Field eventTime = UpdateEvent.class.getDeclaredField("eventTime");
            final Field revId = UpdateEvent.class.getDeclaredField("revId");
            final Field ingestionTime = UpdateEvent.class.getDeclaredField("ingestionTime");
            final Field targetDocument = UpdateEvent.class.getDeclaredField("targetDocument");
            final Field movedFrom = UpdateEvent.class.getDeclaredField("movedFrom");
            final Field update = UpdateEvent.class.getDeclaredField("update");
            final Field fetchAttempt = UpdateEvent.class.getDeclaredField("fetchAttempt");
            final Field bypassDeduplication = UpdateEvent.class.getDeclaredField("bypassDeduplication");
            final Field fromPrivateStream = UpdateEvent.class.getDeclaredField("fromPrivateStream");
            return List.of(
                    new PojoField(meta, getPojoTypeInformation(Meta.class)),
                    new PojoField(changeType, TypeInformation.of(ChangeType.class)),
                    new PojoField(eventTime, INSTANT_TYPE_INFO),
                    new PojoField(revId, LONG_TYPE_INFO),
                    new PojoField(ingestionTime, INSTANT_TYPE_INFO),
                    new PojoField(targetDocument, getPojoTypeInformation(TargetDocument.class)),
                    new PojoField(movedFrom, getPojoTypeInformation(TargetDocument.class)),
                    new PojoField(update, getUpdateTypeInfo(fieldsTypeInfo)),
                    new PojoField(fetchAttempt, INT_TYPE_INFO),
                    new PojoField(bypassDeduplication, BOOLEAN_TYPE_INFO),
                    new PojoField(fromPrivateStream, BOOLEAN_TYPE_INFO));
        } catch (NoSuchFieldException e) {
            throw new IllegalStateException(
                    "Failed to build type information for " + UpdateEvent.class, e);
        }
    }

    private static TypeInformation<UpdateEvent.Update> getUpdateTypeInfo(
            TypeInformation<Row> fieldsType) throws NoSuchFieldException {
        final Field noopHints = UpdateEvent.Update.class.getDeclaredField("noopHints");
        final Field rawFields = UpdateEvent.Update.class.getDeclaredField("rawFields");
        final Field rawFieldsFetchedAt =
                UpdateEvent.Update.class.getDeclaredField("rawFieldsFetchedAt");
        final Field weightedTags = UpdateEvent.Update.class.getDeclaredField("weightedTags");
        final Field redirectAdd = UpdateEvent.Update.class.getDeclaredField("redirectAdd");
        final Field redirectRemove = UpdateEvent.Update.class.getDeclaredField("redirectRemove");
        final TypeInformation<TargetDocument> targetDocumentTypeInformation =
                getPojoTypeInformation(TargetDocument.class);
        return new PojoTypeInfo<>(
                UpdateEvent.Update.class,
                ImmutableList.of(
                        new PojoField(noopHints, new MapTypeInfo<>(STRING_TYPE_INFO, STRING_TYPE_INFO)),
                        new PojoField(rawFields, fieldsType),
                        new PojoField(rawFieldsFetchedAt, INSTANT_TYPE_INFO),
                        new PojoField(weightedTags, new ListTypeInfo<>(STRING_TYPE_INFO)),
                        new PojoField(redirectAdd, new ListTypeInfo<>(targetDocumentTypeInformation)),
                        new PojoField(redirectRemove, new ListTypeInfo<>(targetDocumentTypeInformation))));
    }

    @NotNull
    private static <T> TypeInformation<T> getPojoTypeInformation(Class<T> typeClass) {
        TypeInformation<T> targetDocumentTypeInformation = TypeInformation.of(typeClass);
        Verify.verify(
                targetDocumentTypeInformation instanceof PojoTypeInfo,
                typeClass + "must be treated as a PojoTypeInfo, got " + targetDocumentTypeInformation);
        return targetDocumentTypeInformation;
    }
}
