package org.wikimedia.discovery.cirrus.updater.common.logging;

import java.util.Map;
import java.util.function.Function;
import java.util.function.Supplier;

import org.apache.logging.log4j.CloseableThreadContext;
import org.apache.logging.log4j.CloseableThreadContext.Instance;
import org.apache.logging.log4j.LogBuilder;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateEvent;

import lombok.experimental.UtilityClass;

@UtilityClass
@SuppressWarnings("checkstyle:HideUtilityClassConstructor")
public class LoggingContext {

    static Map<String, Function<UpdateEvent, String>> extractors =
            Map.of(
                    "http.request.headers.x_request_id",
                    UpdateEvent::getRequestId,
                    "url.domain",
                    (updateEvent) -> updateEvent.getTargetDocument().getDomain(),
                    "labels.wiki",
                    (updateEvent) -> updateEvent.getTargetDocument().getWikiId());

    public static void log(
            Supplier<LogBuilder> logBuilderSupplier,
            Supplier<String> messageSupplier,
            UpdateEvent updateEvent) {

        try (Instance threadContext = CloseableThreadContext.putAll(Map.of())) {
            extractors.forEach(
                    (key, extractor) -> {
                        final String value = extractor.apply(updateEvent);
                        if (value != null) {
                            threadContext.put(key, value);
                        }
                    });
            logBuilderSupplier.get().log(messageSupplier.get());
        }
    }
}
