package org.wikimedia.discovery.cirrus.updater.common.config;

import static org.wikimedia.discovery.cirrus.updater.common.config.ReadableConfigReader.getRequired;
import static org.wikimedia.discovery.cirrus.updater.common.config.ReadableConfigReader.getRequiredMapEntry;

import java.time.Duration;
import java.time.Instant;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Optional;
import java.util.Properties;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.configuration.ConfigOption;
import org.apache.flink.configuration.ConfigOptions;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.configuration.PipelineOptions;
import org.apache.flink.configuration.description.Description;
import org.apache.flink.configuration.description.LinkElement;
import org.apache.flink.configuration.description.TextElement;
import org.apache.kafka.common.TopicPartition;
import org.wikimedia.discovery.cirrus.updater.common.http.HttpHostMapping;
import org.wikimedia.eventutilities.core.SerializableClock;

import com.google.common.base.Preconditions;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.experimental.Accessors;
import lombok.experimental.FieldDefaults;
import lombok.experimental.SuperBuilder;

@Getter
@SuperBuilder
@Accessors(fluent = true)
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
public abstract class CommonConfig {

    public static final int MAX_OPERATOR_PARALLELISM = 1024;
    private static final String BOOTSTRAP_SERVERS_CONFIG = "bootstrap.servers";
    private static final String GROUP_ID_CONFIG = "group.id";

    public static final ConfigOption<String> PARAM_PIPELINE_NAME = PipelineOptions.NAME;

    public static final ConfigOption<Boolean> PARAM_DRY_RUN =
            ConfigOptions.key("dry-run").booleanType().defaultValue(Boolean.FALSE);
    public static final ConfigOption<String> PARAM_EVENT_STREAM_CONFIG_URL =
            ConfigOptions.key("event-stream-config-url").stringType().noDefaultValue();
    public static final ConfigOption<List<String>> PARAM_EVENT_STREAM_JSON_SCHEMA_URLS =
            ConfigOptions.key("event-stream-json-schema-urls").stringType().asList().noDefaultValue();
    public static final ConfigOption<Duration> PARAM_HTTP_REQUEST_TIMEOUT =
            ConfigOptions.key("http-request-timeout")
                    .durationType()
                    .defaultValue(Duration.ofSeconds(10))
                    .withDescription("Timeout for HTTP requests");
    public static final ConfigOption<Long> PARAM_HTTP_REQUEST_CONNECTION_TIMEOUT_DIVISOR =
            ConfigOptions.key("http-request-connection-timeout-divisor")
                    .longType()
                    .defaultValue(10L)
                    .withDescription(
                            Description.builder()
                                    .text(
                                            "Portion of the overall %s after establishing a connection times out",
                                            LinkElement.link(PARAM_HTTP_REQUEST_TIMEOUT.key()))
                                    .build());

    public static final ConfigOption<Map<String, String>> PARAM_EVENT_STREAM_HTTP_ROUTES =
            ConfigOptions.key("event-stream-http-routes")
                    .mapType()
                    .noDefaultValue()
                    .withDescription(
                            "A map of 'source_host: dest_host' used by event streams for schema loading.");

    public static final ConfigOption<Map<String, String>> PARAM_HTTP_ROUTES =
            ConfigOptions.key("http-routes")
                    .mapType()
                    .noDefaultValue()
                    .withDescription(
                            "A map of '00_sorting_key: original_uri_regular_expression=alternate_uri'");

    public static final ConfigOption<String> PARAM_HTTP_USER_AGENT =
            ConfigOptions.key("http-user-agent")
                    .stringType()
                    .defaultValue("WMF/cirrus-streaming-updater")
                    .withDescription("User agent to provide with all http requests");

    public static final ConfigOption<Duration> PARAM_HTTP_RATE_LIMIT_RETRY_INTERVAL =
            ConfigOptions.key("http-rate-limit-retry-interval")
                    .durationType()
                    .defaultValue(Duration.ofMillis(500L)) // avg. response time is ~400ms
                    .withDescription(
                            "Retry interval in case of a 429 (rate-limited, too many requests) response");

    public static final ConfigOption<Integer> PARAM_HTTP_RATE_LIMIT_MAX_RETRIES =
            ConfigOptions.key("http-rate-limit-max-retries")
                    .intType()
                    .defaultValue(-1)
                    .withDescription(
                            Description.builder()
                                    .text(
                                            "Maximum number of retries attempted in case of a 429  (rate-limited, too many requests) response")
                                    .linebreak()
                                    .text("A value < 0 means unbounded retries")
                                    .build());

    public static final ConfigOption<Integer> PARAM_HTTP_RATE_LIMIT_PER_SECOND =
            ConfigOptions.key("http-rate-limit-per-second")
                    .intType()
                    .defaultValue(1000)
                    .withDescription(
                            Description.builder()
                                    .text(
                                            "Maximum number of requests per second permitted per %s",
                                            LinkElement.link(PARAM_HTTP_USER_AGENT.key()))
                                    .linebreak()
                                    .text("A value < 0 means no client-side rate-limiting")
                                    .build());

    public static final ConfigOption<String> PARAM_MEDIAWIKI_AUTH_TOKEN =
            ConfigOptions.key("mediawiki-auth-token")
                    .stringType()
                    .noDefaultValue()
                    .withDescription("NetworkSession token to provide to mediawiki to access private wikis");

    public static final ConfigOption<Map<String, String>> PARAM_KAFKA_SOURCE_CONFIG =
            ConfigOptions.key("kafka-source-config")
                    .mapType()
                    .noDefaultValue()
                    .withDescription(
                            Description.builder()
                                    .text("Map of kafka source configuration properties")
                                    .linebreak()
                                    .text("Minimal configuration:")
                                    .list(
                                            TextElement.code(BOOTSTRAP_SERVERS_CONFIG), TextElement.code(GROUP_ID_CONFIG))
                                    .build());

    public static final ConfigOption<Map<String, String>> PARAM_KAFKA_SINK_CONFIG =
            ConfigOptions.key("kafka-sink-config")
                    .mapType()
                    .noDefaultValue()
                    .withDescription(
                            Description.builder()
                                    .text(
                                            "Map of kafka sink configuration properties, defaults to %s",
                                            LinkElement.link(PARAM_KAFKA_SOURCE_CONFIG.key()))
                                    .build());

    public static final ConfigOption<Map<String, String>> PARAM_KAFKA_SOURCE_END_OFFSETS =
            ConfigOptions.key("kafka-source-end-offset")
                    .mapType()
                    .noDefaultValue()
                    .withDescription(
                            Description.builder()
                                    .text(
                                            "Source start offsets, format %s",
                                            TextElement.code("topic[:partition]=offset"))
                                    .build());
    public static final ConfigOption<String> PARAM_KAFKA_SOURCE_END_TIME =
            ConfigOptions.key("kafka-source-end-time")
                    .stringType()
                    .noDefaultValue()
                    .withDescription(
                            Description.builder()
                                    .text(
                                            "Source end instant (ISO format), used to determine last offset before that time")
                                    .linebreak()
                                    .text(
                                            "Alternatively, specify %s",
                                            LinkElement.link(PARAM_KAFKA_SOURCE_END_OFFSETS.key()))
                                    .build());

    public static final ConfigOption<Map<String, String>> PARAM_KAFKA_SOURCE_START_OFFSETS =
            ConfigOptions.key("kafka-source-start-offset")
                    .mapType()
                    .noDefaultValue()
                    .withDescription(
                            Description.builder()
                                    .text(
                                            "Source end offsets, format %s", TextElement.code("topic[:partition]=offset"))
                                    .build());

    public static final ConfigOption<String> PARAM_KAFKA_SOURCE_START_TIME =
            ConfigOptions.key("kafka-source-start-time")
                    .stringType()
                    .noDefaultValue()
                    .withDescription(
                            Description.builder()
                                    .text(
                                            "Source start instant (ISO format), used to determine first offset after that time")
                                    .linebreak()
                                    .text(
                                            "Alternatively, specify %s",
                                            LinkElement.link(PARAM_KAFKA_SOURCE_START_OFFSETS.key()))
                                    .build());

    public static final ConfigOption<String> PARAM_PUBLIC_UPDATE_STREAM =
            ConfigOptions.key("public-update-stream")
                    .stringType()
                    .defaultValue(EventStreamConstants.UPDATER_PUBLIC_OUTPUT_STREAM_NAME)
                    .withDescription(
                            Description.builder()
                                    .text(
                                            "Bridging stream for public wikis between producer (aggregator) and consumer (indexer)")
                                    .linebreak()
                                    .text(
                                            "This is used to fetch the event schema "
                                                    + "as well as a list of kafka topics from stream configuration, see %s",
                                            LinkElement.link(PARAM_EVENT_STREAM_CONFIG_URL.key()))
                                    .build());

    public static final ConfigOption<String> PARAM_PRIVATE_UPDATE_STREAM =
            ConfigOptions.key("private-update-stream")
                    .stringType()
                    .defaultValue(EventStreamConstants.UPDATER_PRIVATE_OUTPUT_STREAM_NAME)
                    .withDescription(
                            Description.builder()
                                    .text(
                                            "Bridging stream for private wikis between producer (aggregator) and consumer (indexer)")
                                    .linebreak()
                                    .text(
                                            "This is used to fetch the event schema "
                                                    + "as well as a list of kafka topics from stream configuration, see %s",
                                            LinkElement.link(PARAM_EVENT_STREAM_CONFIG_URL.key()))
                                    .build());

    public static final ConfigOption<List<String>> PARAM_WIKI_IDS =
            ConfigOptions.key("wikiids")
                    .stringType()
                    .asList()
                    .noDefaultValue()
                    .withDescription(
                            "List of wiki database names we will process, when not provided all wikis are processed.");

    @Builder.Default SerializableClock clock = Instant::now;
    Configuration configuration;
    @Builder.Default boolean dryRun = PARAM_DRY_RUN.defaultValue();
    String eventStreamConfigUrl;
    List<String> eventStreamJsonSchemaUrls;

    @Builder.Default Duration httpRequestTimeout = PARAM_HTTP_REQUEST_TIMEOUT.defaultValue();

    @Builder.Default
    long httpRequestConnectionTimeoutDivisor =
            PARAM_HTTP_REQUEST_CONNECTION_TIMEOUT_DIVISOR.defaultValue();

    @Nullable List<HttpHostMapping> httpRoutes;
    @Nullable Map<String, String> eventStreamHttpRoutes;
    String httpUserAgent;

    @Builder.Default
    Duration httpRateLimitRetryInterval = PARAM_HTTP_RATE_LIMIT_RETRY_INTERVAL.defaultValue();

    @Builder.Default int httpRateLimitMaxRetries = PARAM_HTTP_RATE_LIMIT_MAX_RETRIES.defaultValue();
    @Builder.Default int httpRateLimitPerSecond = PARAM_HTTP_RATE_LIMIT_PER_SECOND.defaultValue();

    @Nullable String mediawikiAuthToken;

    String kafkaSinkBootstrapServers;
    Map<String, String> kafkaSinkConfig;
    String kafkaSourceBootstrapServers;
    Map<String, String> kafkaSourceConfig;
    @Nullable Instant kafkaSourceEndTime;
    String kafkaSourceGroupId;
    @Nullable Instant kafkaSourceStartTime;

    @Nullable Map<TopicPartition, Long> kafkaSourceStartOffsets;
    @Nullable Map<TopicPartition, Long> kafkaSourceEndOffsets;

    @Builder.Default String publicUpdateStream = PARAM_PUBLIC_UPDATE_STREAM.defaultValue();

    @Builder.Default String privateUpdateStream = PARAM_PRIVATE_UPDATE_STREAM.defaultValue();

    @Nullable List<String> wikiIds;

    protected static <B extends CommonConfigBuilder<?, ?>> B superBuilder(
            Configuration configuration, Supplier<B> builderFactory) {
        final B builder = builderFactory.get();

        final String kafkaSourceBootstrapServers =
                getRequiredMapEntry(configuration, PARAM_KAFKA_SOURCE_CONFIG, BOOTSTRAP_SERVERS_CONFIG);

        builder
                .configuration(configuration)
                .eventStreamConfigUrl(getRequired(configuration, PARAM_EVENT_STREAM_CONFIG_URL))
                .eventStreamJsonSchemaUrls(getRequired(configuration, PARAM_EVENT_STREAM_JSON_SCHEMA_URLS))
                .httpUserAgent(getRequired(configuration, PARAM_HTTP_USER_AGENT))
                .kafkaSourceConfig(getRequired(configuration, PARAM_KAFKA_SOURCE_CONFIG))
                .kafkaSourceBootstrapServers(kafkaSourceBootstrapServers)
                .kafkaSourceGroupId(
                        getRequiredMapEntry(configuration, PARAM_KAFKA_SOURCE_CONFIG, GROUP_ID_CONFIG))
                .kafkaSourceStartTime(
                        ReadableConfigReader.getOptionalOrNull(
                                configuration, PARAM_KAFKA_SOURCE_START_TIME, Instant::parse))
                .kafkaSourceStartOffsets(
                        ReadableConfigReader.getOptionalOrNull(
                                configuration, PARAM_KAFKA_SOURCE_START_OFFSETS, CommonConfig::mapPartitionOffsets))
                .kafkaSourceEndTime(
                        ReadableConfigReader.getOptionalOrNull(
                                configuration, PARAM_KAFKA_SOURCE_END_TIME, Instant::parse))
                .kafkaSourceEndOffsets(
                        ReadableConfigReader.getOptionalOrNull(
                                configuration, PARAM_KAFKA_SOURCE_END_OFFSETS, CommonConfig::mapPartitionOffsets))
                .kafkaSinkConfig(
                        configuration.getOptional(PARAM_KAFKA_SINK_CONFIG).orElse(Collections.emptyMap()))
                .kafkaSinkBootstrapServers(
                        ReadableConfigReader.getOptionalMapEntry(
                                        configuration, PARAM_KAFKA_SINK_CONFIG, BOOTSTRAP_SERVERS_CONFIG)
                                .orElse(kafkaSourceBootstrapServers))
                .wikiIds(configuration.get(PARAM_WIKI_IDS));

        configuration
                .getOptional(PARAM_HTTP_ROUTES)
                .map(
                        httpRoutes ->
                                httpRoutes.entrySet().stream()
                                        .sorted(Map.Entry.comparingByKey())
                                        .map(Map.Entry::getValue)
                                        .map(HttpHostMapping::parse)
                                        .collect(Collectors.toList()))
                .ifPresent(builder::httpRoutes);

        configuration
                .getOptional(PARAM_EVENT_STREAM_HTTP_ROUTES)
                // This is a URL->URL mapping, but we can't fit http urls into config
                // keys, the syntax doesn't match. Instead we ignore the initial key
                // and reform the map using the values.
                .map(
                        httpRoutes ->
                                httpRoutes.entrySet().stream()
                                        .sorted(Map.Entry.comparingByKey())
                                        .map(Map.Entry::getValue)
                                        .map(CommonConfig::parseEventStreamRoute)
                                        .collect(Collectors.toMap(tuple -> tuple.f0, tuple -> tuple.f1)))
                .ifPresent(builder::eventStreamHttpRoutes);

        configuration.getOptional(PARAM_MEDIAWIKI_AUTH_TOKEN).ifPresent(builder::mediawikiAuthToken);

        builder.httpRequestTimeout(getRequired(configuration, PARAM_HTTP_REQUEST_TIMEOUT));
        builder.httpRequestConnectionTimeoutDivisor(
                getRequired(configuration, PARAM_HTTP_REQUEST_CONNECTION_TIMEOUT_DIVISOR));

        builder.httpRateLimitRetryInterval(
                getRequired(configuration, PARAM_HTTP_RATE_LIMIT_RETRY_INTERVAL));
        builder.httpRateLimitMaxRetries(getRequired(configuration, PARAM_HTTP_RATE_LIMIT_MAX_RETRIES));
        builder.httpRateLimitPerSecond(getRequired(configuration, PARAM_HTTP_RATE_LIMIT_PER_SECOND));

        configuration.getOptional(PARAM_PUBLIC_UPDATE_STREAM).ifPresent(builder::publicUpdateStream);
        configuration.getOptional(PARAM_PRIVATE_UPDATE_STREAM).ifPresent(builder::privateUpdateStream);
        configuration.getOptional(PARAM_DRY_RUN).ifPresent(builder::dryRun);

        return builder;
    }

    private static Tuple2<String, String> parseEventStreamRoute(String source) {
        String[] pair = source.split("=");
        Preconditions.checkArgument(
                pair.length == 2, "Event stream http route value must contain a single url=url mapping.");
        return Tuple2.of(pair[0], pair[1]);
    }

    @Nonnull
    public Properties kafkaSinkProperties() {
        return asProperties(kafkaSinkConfig);
    }

    @Nonnull
    public Properties kafkaSourceProperties() {
        return asProperties(kafkaSourceConfig);
    }

    public <T> Optional<T> optional(ConfigOption<T> option) {
        return configuration.getOptional(option).or(() -> Optional.ofNullable(option.defaultValue()));
    }

    public <T> T required(ConfigOption<T> option) {
        return optional(option).orElseThrow(() -> missing(option));
    }

    public <T> NoSuchElementException missing(ConfigOption<T> option) {
        return new NoSuchElementException("Missing parameter for " + option.key());
    }

    public String jobName() {
        return getRequired(configuration, PARAM_PIPELINE_NAME);
    }

    @Nonnull
    private static Properties asProperties(Map<String, String> config) {
        final Properties properties = new Properties();
        properties.putAll(config);
        return properties;
    }

    private static Map<TopicPartition, Long> mapPartitionOffsets(Map<String, String> offsets) {
        return offsets.entrySet().stream()
                .map(
                        entry -> Map.entry(parseTopicPartition(entry.getKey()), Long.valueOf(entry.getValue())))
                .collect(Collectors.toMap(Entry::getKey, Entry::getValue));
    }

    private static TopicPartition parseTopicPartition(String offset) {
        final String[] parts = Objects.requireNonNull(offset, "offset must not be null").split(":");
        if (parts.length == 1) {
            return new TopicPartition(parts[0], 0);
        } else if (parts.length == 2) {
            return new TopicPartition(parts[0], Integer.parseInt(parts[1]));
        }
        throw new IllegalArgumentException(
                "Unexpected offset '" + offset + "', expected [partition:]offset");
    }
}
