package org.wikimedia.discovery.cirrus.updater.common.http;

import java.io.Serializable;
import java.net.URISyntaxException;
import java.util.regex.Pattern;

import org.apache.hc.core5.http.HttpHost;
import org.apache.hc.core5.net.NamedEndpoint;

import lombok.Value;

@Value
public class HttpHostMapping implements Serializable {

    private static final String SEPARATOR = "=";
    Pattern pattern;
    HttpHost target;

    public static HttpHostMapping create(String pattern, String target) throws URISyntaxException {
        return new HttpHostMapping(Pattern.compile(pattern), HttpHost.create(target));
    }

    public static HttpHostMapping parse(String mapping) {
        final String[] tuple = mapping.split(SEPARATOR);
        if (tuple.length == 2) {
            try {
                return create(tuple[0].trim(), tuple[1].trim());
            } catch (URISyntaxException e) {
                throw new IllegalArgumentException(
                        "Unexpected target '" + tuple[1] + "', expected valid URI", e);
            }
        }
        throw new IllegalArgumentException("Malformed mapping '" + mapping + "', expected PATTERN=URI");
    }

    public boolean matches(HttpHost source) {
        return pattern.matcher(source.toURI()).find();
    }

    public HttpHost mergeTarget(NamedEndpoint source) {
        return new HttpHost(
                target.getSchemeName(),
                target.getHostName(),
                // If the dest port was not set, then assume we want to use the same one as
                // the request url.
                target.getPort() != -1 ? target.getPort() : source.getPort());
    }

    public String toString() {
        return pattern.pattern() + SEPARATOR + target.toURI();
    }
}
