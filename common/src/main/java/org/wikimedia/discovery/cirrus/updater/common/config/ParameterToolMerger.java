package org.wikimedia.discovery.cirrus.updater.common.config;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.flink.api.java.utils.ParameterTool;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.SafeConstructor;
import org.yaml.snakeyaml.nodes.Tag;

import com.google.common.base.Verify;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import lombok.experimental.UtilityClass;

@UtilityClass
// Lombok takes care of hiding the constructor, but checkstyle works with source, not bytecode
@SuppressWarnings("HideUtilityClassConstructor")
@SuppressFBWarnings(
        value = {"PATH_TRAVERSAL_IN", "URLCONNECTION_SSRF_FD"},
        justification = "We trust configuration files.")
public class ParameterToolMerger {

    public static ParameterTool fromDefaultsWithOverrides(String[] args) throws IOException {
        return fromDefaultsWithOverrides("/cirrus-streaming-updater-producer.properties", args);
    }

    public static ParameterTool fromDefaultsWithOverrides(String defaultsResourceName, String[] args)
            throws IOException {
        final List<ParameterTool> sources = new ArrayList<>(3);
        if (defaultsResourceName != null) {
            try (InputStream inputStream =
                    ParameterToolMerger.class.getResourceAsStream(defaultsResourceName)) {
                if (inputStream == null) {
                    throw new IllegalArgumentException("Cannot find " + defaultsResourceName);
                }
                sources.add(ParameterTool.fromPropertiesFile(inputStream));
            }
        }
        if (args.length > 0) {
            if (!args[0].startsWith("--")) {
                if (args[0].contains(":/")) {
                    URI uri = URI.create(args[0]);
                    try (InputStream is = uri.toURL().openStream()) {
                        sources.add(
                                uri.getPath().endsWith(".yaml")
                                        ? fromYamlFile(is)
                                        : ParameterTool.fromPropertiesFile(is));
                    }
                } else {
                    final File path = new File(args[0]);
                    if (!path.exists()) {
                        throw new IllegalArgumentException("Cannot find " + path);
                    }
                    sources.add(
                            path.getName().endsWith(".yaml")
                                    ? fromYamlFile(path)
                                    : ParameterTool.fromPropertiesFile(path));
                }
                args = Arrays.copyOfRange(args, 1, args.length);
            }
            sources.add(ParameterTool.fromArgs(args));
        }
        return sources.stream()
                .reduce(ParameterTool.fromMap(Collections.emptyMap()), ParameterTool::mergeWith);
    }

    private static ParameterTool fromYamlFile(File path) throws IOException {
        return fromYamlFile(path.toPath());
    }

    private static ParameterTool fromYamlFile(Path path) throws IOException {
        try (InputStream fis = Files.newInputStream(path)) {
            return fromYamlFile(fis);
        }
    }

    private static ParameterTool fromYamlFile(InputStream is) {
        Yaml yaml = new Yaml(new MostlyStringsConstructor());
        Map<Object, Object> raw = yaml.load(is);
        Map<String, String> config =
                raw.entrySet().stream()
                        .peek(
                                entry ->
                                        Verify.verify(
                                                entry.getValue() instanceof String
                                                        || entry.getValue() instanceof Number
                                                        || entry.getValue() instanceof Boolean,
                                                "invalid config map entry: %s",
                                                entry))
                        .collect(
                                Collectors.toMap(
                                        entry -> String.valueOf(entry.getKey()),
                                        entry -> String.valueOf(entry.getValue())));
        return ParameterTool.fromMap(config);
    }

    /**
     * Keep timestamps as strings, similar to how they would have come out of the Properties parser.
     * Later steps will do the Instant conversion.
     */
    private static class MostlyStringsConstructor extends SafeConstructor {
        MostlyStringsConstructor() {
            super();
            this.yamlConstructors.put(Tag.TIMESTAMP, new ConstructYamlStr());
        }
    }
}
