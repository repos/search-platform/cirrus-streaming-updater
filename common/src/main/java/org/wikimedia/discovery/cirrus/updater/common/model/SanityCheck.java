package org.wikimedia.discovery.cirrus.updater.common.model;

import java.io.Serializable;
import java.time.Instant;

import org.apache.flink.api.common.typeinfo.TypeInfo;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

/** Represents a request to invoke the sanity check api for specific pages. Immutable. */
@AllArgsConstructor
@EqualsAndHashCode
@Getter
@ToString
@TypeInfo(SanityCheckTypeInfoFactory.class)
public class SanityCheck implements Serializable {
    private String domain;
    private long startPageId;
    private int batchSize;
    private Instant eventTime;
    private int loopId;

    public SanityCheck() {
        // Visible for PojoSerializer
    }
}
