package org.wikimedia.discovery.cirrus.updater.common.util.function;

import java.io.Serializable;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

/**
 * Serializable version of the {@link Consumer} functional interface. Useful in operators serialized
 * by flink.
 */
public interface SerializableBiConsumer<T, U> extends BiConsumer<T, U>, Serializable {}
