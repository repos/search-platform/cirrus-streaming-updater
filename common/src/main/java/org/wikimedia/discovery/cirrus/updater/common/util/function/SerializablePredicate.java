package org.wikimedia.discovery.cirrus.updater.common.util.function;

import java.io.Serializable;
import java.util.function.Predicate;

public interface SerializablePredicate<T> extends Predicate<T>, Serializable {
    default SerializablePredicate<T> and(SerializablePredicate<T> other) {
        return t -> this.test(t) && other.test(t);
    }
}
