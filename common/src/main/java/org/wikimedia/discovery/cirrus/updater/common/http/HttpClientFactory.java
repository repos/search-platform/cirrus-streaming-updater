package org.wikimedia.discovery.cirrus.updater.common.http;

import static org.apache.http.protocol.HttpCoreContext.HTTP_REQUEST;

import java.io.IOException;
import java.time.Duration;
import java.util.Map;
import java.util.Optional;
import java.util.SortedMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.function.BiConsumer;
import java.util.function.Function;

import org.apache.flink.api.connector.source.util.ratelimit.GuavaRateLimiter;
import org.apache.flink.api.connector.source.util.ratelimit.NoOpRateLimiter;
import org.apache.flink.api.connector.source.util.ratelimit.RateLimiter;
import org.apache.flink.util.Preconditions;
import org.apache.hc.client5.http.HttpRequestRetryStrategy;
import org.apache.hc.client5.http.classic.ExecChain;
import org.apache.hc.client5.http.classic.ExecChain.Scope;
import org.apache.hc.client5.http.classic.ExecChainHandler;
import org.apache.hc.client5.http.config.ConnectionConfig;
import org.apache.hc.client5.http.config.RequestConfig;
import org.apache.hc.client5.http.cookie.StandardCookieSpec;
import org.apache.hc.client5.http.impl.ChainElement;
import org.apache.hc.client5.http.impl.DefaultSchemePortResolver;
import org.apache.hc.client5.http.impl.async.CloseableHttpAsyncClient;
import org.apache.hc.client5.http.impl.async.HttpAsyncClientBuilder;
import org.apache.hc.client5.http.impl.classic.CloseableHttpClient;
import org.apache.hc.client5.http.impl.classic.HttpClientBuilder;
import org.apache.hc.client5.http.impl.io.PoolingHttpClientConnectionManager;
import org.apache.hc.client5.http.impl.routing.DefaultRoutePlanner;
import org.apache.hc.client5.http.routing.HttpRoutePlanner;
import org.apache.hc.client5.http.ssl.ClientTlsStrategyBuilder;
import org.apache.hc.core5.http.ClassicHttpRequest;
import org.apache.hc.core5.http.ClassicHttpResponse;
import org.apache.hc.core5.http.ContentType;
import org.apache.hc.core5.http.EntityDetails;
import org.apache.hc.core5.http.HttpException;
import org.apache.hc.core5.http.HttpHeaders;
import org.apache.hc.core5.http.HttpHost;
import org.apache.hc.core5.http.HttpRequest;
import org.apache.hc.core5.http.HttpResponse;
import org.apache.hc.core5.http.HttpResponseInterceptor;
import org.apache.hc.core5.http.config.Http1Config;
import org.apache.hc.core5.http.nio.ssl.TlsStrategy;
import org.apache.hc.core5.http.protocol.HttpContext;
import org.apache.hc.core5.http.ssl.TLS;
import org.apache.hc.core5.pool.PoolConcurrencyPolicy;
import org.apache.hc.core5.pool.PoolReusePolicy;
import org.apache.hc.core5.ssl.SSLContexts;
import org.apache.hc.core5.util.Args;
import org.apache.hc.core5.util.TimeValue;
import org.apache.hc.core5.util.Timeout;

import com.codahale.metrics.Histogram;
import com.codahale.metrics.MetricFilter;
import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.httpclient5.InstrumentedAsyncClientConnectionManager;
import com.codahale.metrics.httpclient5.InstrumentedHttpAsyncClients;
import com.codahale.metrics.httpclient5.InstrumentedHttpClientConnectionManager;
import com.codahale.metrics.httpclient5.InstrumentedHttpRequestExecutor;

import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;

@UtilityClass
@SuppressWarnings({"HideUtilityClassConstructor", "checkstyle:classfanoutcomplexity"})
public class HttpClientFactory {

    public static final int ENVOY_OVERHEAD_MS = 100;
    public static final String MEDIAWIKI_AUTH_SCHEME = "NetworkSession";

    private static final Map<String, RateLimiter> RATE_LIMITERS = new ConcurrentHashMap<>();

    private static final TlsStrategy TLS_STRATEGY =
            ClientTlsStrategyBuilder.create()
                    .setSslContext(SSLContexts.createSystemDefault())
                    .setTlsVersions(TLS.V_1_3, TLS.V_1_2)
                    .build();

    public static CloseableHttpAsyncClient buildAsyncClient(
            HttpConfig httpConfig, MetricRegistry metrics) {

        final InstrumentedAsyncClientConnectionManager connectionManager =
                InstrumentedAsyncClientConnectionManager.builder(metrics)
                        .tlsStrategyLookup((s) -> TLS_STRATEGY)
                        .poolConcurrencyPolicy(PoolConcurrencyPolicy.STRICT)
                        .poolReusePolicy(PoolReusePolicy.LIFO)
                        .timeToLive(TimeValue.ofMinutes(1L))
                        .build();

        connectionManager.setDefaultConnectionConfig(
                createConnectionConfig(httpConfig.getRequestTimeout(), httpConfig.getConnectionTimeout()));

        connectionManager.setMaxTotal(httpConfig.getMaxConnections());
        // pool.lease() is called with the mapped route,
        // so there is only one route since all requests are routed through envoy
        connectionManager.setDefaultMaxPerRoute(httpConfig.getMaxConnections());

        final HttpAsyncClientBuilder builder =
                InstrumentedHttpAsyncClients.custom(metrics, new TagNamingStrategy(), connectionManager)
                        .setRoutePlanner(createHttpRoutePlanner(httpConfig.getHttpHostMapper()))
                        .setDefaultRequestConfig(createRequestConfig(httpConfig.getRequestTimeout()))
                        .addRequestInterceptorFirst(
                                (request, entity, context) -> {
                                    addEnvoyHeaders(httpConfig.getRequestTimeout(), request::addHeader);
                                    if (httpConfig.isAcceptGzip()) {
                                        request.addHeader(HttpHeaders.ACCEPT_ENCODING, "gzip");
                                    }
                                    if (httpConfig.getAuthToken() != null) {
                                        request.addHeader(
                                                HttpHeaders.AUTHORIZATION,
                                                MEDIAWIKI_AUTH_SCHEME + " " + httpConfig.getAuthToken());
                                    }
                                    request.addHeader(HttpHeaders.ACCEPT, ContentType.APPLICATION_JSON.toString());
                                })
                        .addResponseInterceptorLast(new ContentLengthMetricResponseInterceptor(metrics))
                        .setUserAgent(httpConfig.getUserAgent());

        deriveRetryStrategy(httpConfig)
                .ifPresentOrElse(builder::setRetryStrategy, builder::disableAutomaticRetries);

        if (httpConfig.getHttp1DefaultBufferSize() > 0) {
            builder.setHttp1Config(
                    Http1Config.custom().setBufferSize(httpConfig.getHttp1DefaultBufferSize()).build());
        }

        if (httpConfig.getRateLimitPerSecond() > 0) {
            builder.addExecInterceptorBefore(
                    ChainElement.CONNECT.name(),
                    "RATE_LIMIT",
                    (request, entityProducer, scope, chain, asyncExecCallback) ->
                            getRateLimiter(httpConfig)
                                    .acquire()
                                    .thenRun(
                                            () -> {
                                                try {
                                                    chain.proceed(request, entityProducer, scope, asyncExecCallback);
                                                } catch (HttpException | IOException e) {
                                                    asyncExecCallback.failed(e);
                                                }
                                            }));
        }

        return builder.build();
    }

    private static RateLimiter getRateLimiter(HttpConfig httpConfig) {
        final String userAgent = httpConfig.getUserAgent();
        return RATE_LIMITERS.computeIfAbsent(
                userAgent == null ? "default" : userAgent,
                key ->
                        httpConfig.getRateLimitPerSecond() > 0
                                ? new GuavaRateLimiter(httpConfig.getRateLimitPerSecond())
                                : new NoOpRateLimiter());
    }

    private static Optional<RateLimitHttpRequestRetryStrategy> deriveRetryStrategy(
            HttpConfig httpConfig) {

        if (httpConfig.getRateLimitRetryInterval() == null
                || httpConfig.getRateLimitMaxRetries() == 0) {
            return Optional.empty();
        }

        return Optional.of(
                new RateLimitHttpRequestRetryStrategy(
                        TimeValue.of(httpConfig.getRateLimitRetryInterval()),
                        httpConfig.getRateLimitMaxRetries()));
    }

    public static CloseableHttpClient buildSyncClient(
            HttpConfig httpConfig, MetricRegistry metricRegistry) {

        final PoolingHttpClientConnectionManager connectionManager =
                InstrumentedHttpClientConnectionManager.builder(metricRegistry)
                        .poolConcurrencyPolicy(PoolConcurrencyPolicy.STRICT)
                        .poolReusePolicy(PoolReusePolicy.LIFO)
                        .timeToLive(TimeValue.ofMinutes(1L))
                        .build();

        connectionManager.setDefaultConnectionConfig(
                createConnectionConfig(httpConfig.getRequestTimeout(), httpConfig.getConnectionTimeout()));

        connectionManager.setMaxTotal(httpConfig.getMaxConnections());
        // pool.lease() is called with the mapped route,
        // so there is only one route since all requests are routed through envoy
        connectionManager.setDefaultMaxPerRoute(httpConfig.getMaxConnections());

        final HttpClientBuilder builder =
                HttpClientBuilder.create()
                        .setRequestExecutor(
                                new InstrumentedHttpRequestExecutor(metricRegistry, new TagNamingStrategy()))
                        .setConnectionManager(connectionManager)
                        .setRoutePlanner(createHttpRoutePlanner(httpConfig.getHttpHostMapper()))
                        .setDefaultRequestConfig(createRequestConfig(httpConfig.getRequestTimeout()))
                        .setUserAgent(httpConfig.getUserAgent());

        deriveRetryStrategy(httpConfig)
                .ifPresentOrElse(builder::setRetryStrategy, builder::disableAutomaticRetries);

        if (!httpConfig.isAcceptGzip()) {
            builder.disableContentCompression();
        }

        if (httpConfig.getRateLimitPerSecond() > 0) {
            builder.addExecInterceptorBefore(
                    ChainElement.CONNECT.name(),
                    "RATE_LIMIT",
                    new ExecChainHandler() {
                        final Duration rateLimitPermitTimeout = Duration.ofSeconds(5);

                        @Override
                        @SneakyThrows
                        public ClassicHttpResponse execute(
                                ClassicHttpRequest request, Scope scope, ExecChain chain)
                                throws IOException, HttpException {
                            getRateLimiter(httpConfig)
                                    .acquire()
                                    .toCompletableFuture()
                                    .get(rateLimitPermitTimeout.toMillis(), TimeUnit.MILLISECONDS);
                            return chain.proceed(request, scope);
                        }
                    });
        }

        if (httpConfig.getAuthToken() != null) {
            // The auth token is mediawiki specific, we depend on calling code
            // to only provide this in appropriate contexts.
            builder.addRequestInterceptorFirst(
                    (request, entity, context) -> {
                        request.addHeader(
                                HttpHeaders.AUTHORIZATION, MEDIAWIKI_AUTH_SCHEME + " " + httpConfig.getAuthToken());
                    });
        }

        return builder.build();
    }

    public static void addEnvoyHeaders(Duration socketTimeout, BiConsumer<String, String> addHeader) {

        // prevent envoy from retrying, to prevent timeouts inflicted by upstream retries, see
        // https://www.envoyproxy.io/docs/envoy/latest/configuration/http/http_filters/router_filter#x-envoy-max-retries
        addHeader.accept("x-envoy-max-retries", Integer.toString(0));
        addHeader.accept(
                "x-envoy-upstream-rq-timeout-ms",
                Long.toString(socketTimeout.toMillis() - ENVOY_OVERHEAD_MS));
        // envoy only respects the above headers for requests considered internal, which in
        // turn is derived from the following header being present
        addHeader.accept("x-forwarded-for", "127.0.0.1");
    }

    public static HttpRoutePlanner createHttpRoutePlanner(
            Function<HttpHost, Optional<HttpHost>> httpHostMapper) {
        HttpRoutePlanner routePlanner = new DefaultRoutePlanner(DefaultSchemePortResolver.INSTANCE);
        if (httpHostMapper != null) {
            return new CustomRoutePlanner(httpHostMapper, routePlanner);
        }
        return routePlanner;
    }

    private static ConnectionConfig createConnectionConfig(
            Duration requestTimeout, Duration connectionTimeout) {

        Args.check(
                requestTimeout.compareTo(connectionTimeout) > 0,
                "connectionTimeout must be less than overall requestTimeout");

        return ConnectionConfig.custom()
                .setConnectTimeout(Timeout.of(connectionTimeout))
                .setSocketTimeout(Timeout.of(requestTimeout.minus(connectionTimeout)))
                .build();
    }

    private static RequestConfig createRequestConfig(Duration requestTimeout) {
        return RequestConfig.custom()
                .setResponseTimeout(Timeout.of(requestTimeout))
                .setCookieSpec(StandardCookieSpec.IGNORE)
                .build();
    }

    private static class ContentLengthMetricResponseInterceptor implements HttpResponseInterceptor {

        private final MetricRegistry metrics;

        ContentLengthMetricResponseInterceptor(MetricRegistry metrics) {
            this.metrics = metrics;
        }

        @Override
        public void process(HttpResponse response, EntityDetails entity, HttpContext context) {
            if (response.getCode() == 200) {
                if (entity.getContentLength() > 0) {
                    HttpRequest request = (HttpRequest) context.getAttribute(HTTP_REQUEST);
                    String name =
                            TagNamingStrategy.name("response_content_length")
                                    + TagNamingStrategy.encodeRequest(request);
                    final SortedMap<String, Histogram> histograms =
                            metrics.getHistograms(MetricFilter.contains(name));
                    final Histogram histogram =
                            histograms.containsKey(name) ? histograms.get(name) : metrics.histogram(name);
                    histogram.update(entity.getContentLength());
                }
            }
        }
    }

    static class RateLimitHttpRequestRetryStrategy implements HttpRequestRetryStrategy {

        private final TimeValue fixedInterval;
        private final int maxAttempts;

        RateLimitHttpRequestRetryStrategy(TimeValue fixedInterval, int maxAttempts) {
            Preconditions.checkArgument(fixedInterval != null, "fixedInterval must not be null");

            this.fixedInterval = fixedInterval;
            this.maxAttempts = maxAttempts;
        }

        @Override
        public boolean retryRequest(
                HttpRequest request, IOException exception, int execCount, HttpContext context) {
            return false;
        }

        @Override
        public boolean retryRequest(HttpResponse response, int execCount, HttpContext context) {
            return response.getCode() == 429 && (maxAttempts < 0 || execCount < maxAttempts);
        }

        @Override
        public TimeValue getRetryInterval(HttpResponse response, int execCount, HttpContext context) {
            return fixedInterval;
        }
    }
}
