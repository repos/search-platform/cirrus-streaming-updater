package org.wikimedia.discovery.cirrus.updater.common.model;

import java.util.Objects;

import org.apache.flink.util.ExceptionUtils;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public final class FetchFailure<I> {
    I event;

    /**
     * Transient reference to the failure wrapped by this {@code FetchFailure}.
     *
     * <p>This reference is only kept until the wrapping instance is serialized; it will not be
     * deserialized. We keep it only to provide code evaluating this result, for example an {@link
     * org.apache.flink.streaming.api.functions.async.AsyncRetryStrategy} with all the information
     * available.
     */
    transient Exception failure;

    String failureType;
    String failureMessage;
    String failureStack;

    public FetchFailure(I event, Exception failure) {
        this.event = Objects.requireNonNull(event, "source event must not be null");
        this.failure = Objects.requireNonNull(failure, "failure must not be null");
        this.failureType = extractFailureType(failure);
        this.failureMessage = failure.getMessage();
        this.failureStack = ExceptionUtils.stringifyException(failure);
    }

    public static String extractFailureType(Exception errorType) {
        return errorType.getClass().getCanonicalName();
    }
}
