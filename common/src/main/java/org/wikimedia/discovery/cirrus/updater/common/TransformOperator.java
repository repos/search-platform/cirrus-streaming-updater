package org.wikimedia.discovery.cirrus.updater.common;

import org.apache.flink.api.common.functions.FilterFunction;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.streaming.api.operators.OneInputStreamOperatorFactory;

import lombok.Value;

/**
 * A value object holding parameters to construct an {@link
 * org.apache.flink.streaming.api.operators.async.AsyncWaitOperatorFactory}.
 *
 * @param <I> input type
 * @param <O> output type
 */
@Value
public class TransformOperator<I, O> {

    public String operatorName;

    public String nameAndUuidPrefix;

    public FilterFunction<I> filterFunction;

    public TypeInformation<O> outTypeInformation;

    public OneInputStreamOperatorFactory<I, O> operatorFactory;
}
