package org.wikimedia.discovery.cirrus.updater.common;

public class InvalidMWApiResponseException extends CirrusDocFetchException {
    public InvalidMWApiResponseException(String s) {
        super(s);
    }
}
