package org.wikimedia.discovery.cirrus.updater.common.model;

import static org.wikimedia.discovery.cirrus.updater.common.model.JsonPathUtils.getRequiredNode;

import java.time.Instant;
import java.util.Map;

import org.apache.flink.types.Row;
import org.wikimedia.eventutilities.flink.formats.json.JsonRowDeserializationSchema;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.log4j.Log4j2;

@AllArgsConstructor
@Log4j2
public class CirrusDocJsonHelper {
    @Getter private final JsonRowDeserializationSchema fieldsSchema;
    private final ObjectMapper objectMapper;
    @Getter private final JsonNode cirrusDoc;
    private final JsonNode cirrusDocMetadata;
    @Getter private final Instant fetchedAt;

    public CirrusDocJsonHelper(
            JsonRowDeserializationSchema fieldsSchema,
            ObjectMapper mapper,
            JsonNode root,
            Instant fetchedAt) {
        this(
                fieldsSchema,
                mapper,
                getRequiredNode(root, "/cirrusbuilddoc"),
                getRequiredNode(root, "/cirrusbuilddoc_metadata"),
                fetchedAt);
    }

    public long getRevId() {
        return getRequiredNode(cirrusDoc, "/version").asLong();
    }

    public long getPageId() {
        return getRequiredNode(cirrusDoc, "/page_id").asLong();
    }

    public long getNamespace() {
        return getRequiredNode(cirrusDoc, "/namespace").asLong();
    }

    public String getClusterGroup() {
        return getRequiredNode(cirrusDocMetadata, "/cluster_group").asText();
    }

    public String getIndexName() {
        return getRequiredNode(cirrusDocMetadata, "/index_name").asText();
    }

    public Map<String, String> getNoopHints() {
        return objectMapper.convertValue(
                getRequiredNode(cirrusDocMetadata, "/noop_hints"),
                new TypeReference<Map<String, String>>() {});
    }

    public Row getRawFields() {
        return fieldsSchema.convert(cirrusDoc);
    }
}
