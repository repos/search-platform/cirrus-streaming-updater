package org.wikimedia.discovery.cirrus.updater.common.graph;

import org.apache.flink.api.common.functions.MapFunction;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateEvent;
import org.wikimedia.eventutilities.core.SerializableClock;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class IngestionTimeAssigner implements MapFunction<UpdateEvent, UpdateEvent> {
    private final SerializableClock clock;

    @Override
    public UpdateEvent map(UpdateEvent updateEvent) {
        updateEvent.setIngestionTime(clock.get());
        return updateEvent;
    }
}
