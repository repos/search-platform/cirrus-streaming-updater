package org.wikimedia.discovery.cirrus.updater.common.http;

import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Stream;

import javax.annotation.Nonnull;

import org.apache.flink.dropwizard.metrics.DropwizardHistogramWrapper;
import org.apache.flink.dropwizard.metrics.DropwizardMeterWrapper;
import org.apache.flink.metrics.HistogramStatistics;
import org.apache.flink.metrics.MetricGroup;

import com.codahale.metrics.Counter;
import com.codahale.metrics.Counting;
import com.codahale.metrics.Gauge;
import com.codahale.metrics.Histogram;
import com.codahale.metrics.Meter;
import com.codahale.metrics.MetricRegistryListener.Base;
import com.codahale.metrics.Sampling;
import com.codahale.metrics.Snapshot;
import com.codahale.metrics.Timer;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class MetricGroupMetricRegistryListener extends Base {

    private final MetricGroup metricGroup;

    public MetricGroupMetricRegistryListener(MetricGroup metricGroup) {
        this.metricGroup = metricGroup;
    }

    @Override
    public void onGaugeAdded(String name, Gauge<?> gauge) {
        final Entry<String, MetricGroup> targetGroup = routeMetric(name);
        log.debug("Mapped gauge {} -> {}", name, targetGroup.getKey());
        targetGroup.getValue().gauge(targetGroup.getKey(), gauge::getValue);
    }

    @Override
    public void onCounterAdded(String name, Counter counter) {
        final Entry<String, MetricGroup> targetGroup = routeMetric(name);
        log.debug("Mapped counter {} -> {}", name, targetGroup.getKey());
        targetGroup.getValue().counter(targetGroup.getKey(), new ReadOnlyCounter(counter));
    }

    @Override
    public void onMeterAdded(String name, Meter meter) {
        final Entry<String, MetricGroup> targetGroup = routeMetric(name);
        log.debug("Mapped meter {} -> {}", name, targetGroup.getKey());
        targetGroup.getValue().meter(targetGroup.getKey(), new DropwizardMeterWrapper(meter));
    }

    @Override
    public void onHistogramAdded(String name, Histogram histogram) {
        final Entry<String, MetricGroup> targetGroup = routeMetric(name);
        log.debug("Mapped histogram {} -> {}", name, targetGroup.getKey());
        targetGroup
                .getValue()
                .histogram(targetGroup.getKey(), new DropwizardHistogramWrapper(histogram));
    }

    @Override
    public void onTimerAdded(String name, Timer timer) {
        final Entry<String, MetricGroup> targetGroup = routeMetric(name);
        log.debug("Mapped timer (as histogram) {} -> {}", name, targetGroup.getKey());
        targetGroup.getValue().histogram(targetGroup.getKey(), new ReadOnlyHistogram<>(timer));
    }

    /**
     * Maps a {@code codahale} metric name to a flink metric name.
     *
     * <p>By default {@link com.codahale.metrics.MetricRegistry#name(Class, String...)} produces names
     * like {@code fully.qualified.name.of.SurroundingClass.metric-name}. If {@link TagNamingStrategy}
     * is used, the name may also encode additional properties.
     *
     * @param name the name as created by {@code codahale}
     * @return a tuple of metric name and tagged metric group
     */
    @Nonnull
    private Entry<String, MetricGroup> routeMetric(String name) {
        final Entry<String, Stream<Entry<String, String>>> nameWithTags =
                TagNamingStrategy.decodeTags(name);
        final MetricGroup taggedGroup =
                nameWithTags
                        .getValue()
                        .reduce(
                                metricGroup,
                                (parentGroup, entry) -> parentGroup.addGroup(entry.getKey(), entry.getValue()),
                                (a, b) -> {
                                    throw new UnsupportedOperationException();
                                });
        name = nameWithTags.getKey();
        final int suffixOffset = name.lastIndexOf('.') + 1;
        if (suffixOffset > 0) {
            String suffix = name.substring(suffixOffset);
            return Map.entry(suffix, taggedGroup);
        }

        return Map.entry(nameWithTags.getKey(), taggedGroup);
    }

    private static class ReadOnlyCounter implements org.apache.flink.metrics.Counter {

        private final Counter counter;

        ReadOnlyCounter(Counter counter) {
            this.counter = counter;
        }

        @Override
        public void inc() {
            throw new UnsupportedOperationException();
        }

        @Override
        public void inc(long n) {
            throw new UnsupportedOperationException();
        }

        @Override
        public void dec() {
            throw new UnsupportedOperationException();
        }

        @Override
        public void dec(long n) {
            throw new UnsupportedOperationException();
        }

        @Override
        public long getCount() {
            return counter.getCount();
        }
    }

    private static class ReadOnlyHistogram<T extends Sampling & Counting>
            implements org.apache.flink.metrics.Histogram {

        private final T delegate;

        ReadOnlyHistogram(T delegate) {
            this.delegate = delegate;
        }

        @Override
        public void update(long value) {
            throw new UnsupportedOperationException();
        }

        @Override
        public long getCount() {
            return delegate.getCount();
        }

        @Override
        public HistogramStatistics getStatistics() {
            final Snapshot snapshot = delegate.getSnapshot();
            return new SnapshotHistogramStatistics(snapshot);
        }
    }

    private static class SnapshotHistogramStatistics extends HistogramStatistics {

        private final Snapshot snapshot;

        SnapshotHistogramStatistics(Snapshot snapshot) {
            this.snapshot = snapshot;
        }

        @Override
        public double getQuantile(double quantile) {
            return snapshot.getValue(quantile);
        }

        @Override
        public long[] getValues() {
            return snapshot.getValues();
        }

        @Override
        public int size() {
            return snapshot.size();
        }

        @Override
        public double getMean() {
            return snapshot.getMean();
        }

        @Override
        public double getStdDev() {
            return snapshot.getStdDev();
        }

        @Override
        public long getMax() {
            return snapshot.getMax();
        }

        @Override
        public long getMin() {
            return snapshot.getMin();
        }
    }
}
