package org.wikimedia.discovery.cirrus.updater.common;

public class RevisionHiddenException extends CirrusDocFetchException {
    public RevisionHiddenException(String s) {
        super(s);
    }
}
