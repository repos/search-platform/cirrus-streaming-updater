package org.wikimedia.discovery.cirrus.updater.common.graph;

import static org.wikimedia.discovery.cirrus.updater.common.logging.LoggingContext.log;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.concurrent.NotThreadSafe;

import org.apache.flink.metrics.Counter;
import org.apache.flink.metrics.MetricGroup;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateEvent;

import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;

@NotThreadSafe
@Log4j2
public class WeightedTagMonitor {
    public static final int MAX_MONITORED_TAG_PREFIX = 200;
    public static final String TOO_MANY_COUNTERS_TAG_PREFIX = "e_too_many";
    private final MetricGroup metricGroup;
    private final Map<String, TagCounters> tagCountersMap = new HashMap<>();
    private TagCounters tooManyCountersCounter;

    public WeightedTagMonitor(MetricGroup metricGroup) {
        this.metricGroup = metricGroup;
    }

    private static final Pattern EXTRACTOR =
            Pattern.compile(
                    "^(?<prefix>[^/]+)/(?<del>\\Q" + UpdateEvent.WEIGHTED_TAGS_DELETE_GROUPING + "\\E$)?");

    public void report(UpdateEvent event) {
        Optional.ofNullable(event.getUpdate()).map(UpdateEvent.Update::getWeightedTags).stream()
                .flatMap(List::stream)
                .map(this::decode)
                .flatMap(Optional::stream)
                // dedup per tag prefix so that the count reflects the number of docs
                .distinct()
                .map(e -> this.getCounter(e, event))
                .forEach(Counter::inc);
    }

    /** Key is the tag prefix, value is true for clear, false for set operations. */
    private Optional<Map.Entry<String, Boolean>> decode(String encodedTag) {
        Matcher matcher = EXTRACTOR.matcher(encodedTag);
        if (matcher.find()) {
            return Optional.of(Map.entry(matcher.group("prefix"), matcher.group("del") != null));
        }
        return Optional.empty();
    }

    private Counter getCounter(Map.Entry<String, Boolean> counterDefinition, UpdateEvent event) {
        TagCounters tagCounters = getTagCounters(counterDefinition.getKey(), event);
        return counterDefinition.getValue() ? tagCounters.clearCounter : tagCounters.setCounter;
    }

    private TagCounters getTagCounters(String tagPrefix, UpdateEvent event) {
        if (tagCountersMap.size() >= MAX_MONITORED_TAG_PREFIX) {
            return Optional.ofNullable(tagCountersMap.get(tagPrefix))
                    .orElseGet(() -> getTooManyCountersCounter(tagPrefix, event));
        }
        return tagCountersMap.computeIfAbsent(
                tagPrefix, p -> TagCounters.forMetricGroup(metricGroup.addGroup("tag_prefix", p)));
    }

    private TagCounters getTooManyCountersCounter(String prefix, UpdateEvent event) {
        if (tooManyCountersCounter == null) {
            tooManyCountersCounter =
                    TagCounters.forMetricGroup(
                            metricGroup.addGroup("tag_prefix", TOO_MANY_COUNTERS_TAG_PREFIX));
        }
        log(
                log::atWarn,
                () ->
                        String.format(
                                Locale.ENGLISH,
                                "Too many counters already registered, reporting [%s] as [%s]",
                                prefix,
                                TOO_MANY_COUNTERS_TAG_PREFIX),
                event);
        return tooManyCountersCounter;
    }

    @AllArgsConstructor
    private static final class TagCounters {
        static TagCounters forMetricGroup(MetricGroup group) {
            return new TagCounters(group.counter("set"), group.counter("clear"));
        }

        Counter setCounter;
        Counter clearCounter;
    }
}
