package org.wikimedia.discovery.cirrus.updater.common.model;

import java.io.Serializable;
import java.nio.charset.StandardCharsets;

import org.apache.flink.types.Row;
import org.wikimedia.discovery.cirrus.updater.common.util.function.SerializableFunction;
import org.wikimedia.eventutilities.core.event.EventStream;
import org.wikimedia.eventutilities.flink.EventRowTypeInfo;
import org.wikimedia.eventutilities.flink.formats.json.JsonRowSerializationSchema;
import org.wikimedia.eventutilities.flink.stream.EventDataStreamFactory;

public final class FetchFailureEncoder implements Serializable {

    private final MetaRowTranscoder metaRowTranscoder;
    private final SerializableFunction<UpdateEvent, String> rawEventEncoder;
    private final EventRowTypeInfo fetchErrorRowTypeInfo;

    private FetchFailureEncoder(
            EventRowTypeInfo fetchErrorRowTypeInfo,
            MetaRowTranscoder metaRowTranscoder,
            SerializableFunction<UpdateEvent, String> rawEventEncoder) {
        this.fetchErrorRowTypeInfo = fetchErrorRowTypeInfo;
        this.metaRowTranscoder = metaRowTranscoder;
        this.rawEventEncoder = rawEventEncoder;
    }

    public static FetchFailureEncoder encoder(
            EventRowTypeInfo fetchErrorRowTypeInfo,
            EventDataStreamFactory eventDataStreamFactory,
            EventStream rawEventStream,
            EventRowTypeInfo rawEventRowTypeInfo) {
        final JsonRowSerializationSchema serializer =
                eventDataStreamFactory.serializer(
                        rawEventStream.streamName(), rawEventStream.latestSchemaVersion());

        final UpdateRowTranscoder rawEventEncoder = UpdateRowTranscoder.encoder(rawEventRowTypeInfo);
        return new FetchFailureEncoder(
                fetchErrorRowTypeInfo,
                MetaRowTranscoder.encoder(() -> fetchErrorRowTypeInfo.createEmptySubRow("meta")),
                event ->
                        new String(
                                serializer.serialize(rawEventEncoder.encode(event)), StandardCharsets.UTF_8));
    }

    public Row encode(FetchFailure<UpdateEvent> result, String emitterId) {
        UpdateEvent event = result.getEvent();
        Row fetchErrorRow = fetchErrorRowTypeInfo.createEmptyRow();
        fetchErrorRow.setField("meta", metaRowTranscoder.encode(event.getMeta()));

        fetchErrorRow.setField("emitter_id", emitterId);
        fetchErrorRow.setField("wiki_id", event.getTargetDocument().getWikiId());
        fetchErrorRow.setField("page_id", event.getTargetDocument().getPageId());
        fetchErrorRow.setField("page_title", event.getTargetDocument().getPageTitle());
        fetchErrorRow.setField("rev_id", event.getRevId());
        fetchErrorRow.setField("namespace_id", event.getTargetDocument().getPageNamespace());
        fetchErrorRow.setField("original_event_stream", event.getEventStream());
        fetchErrorRow.setField("original_event_time", event.getEventTime());
        fetchErrorRow.setField("original_ingestion_time", event.getIngestionTime());

        fetchErrorRow.setField("error_type", result.getFailureType());
        fetchErrorRow.setField("message", result.getFailureMessage());
        fetchErrorRow.setField("stack", result.getFailureStack());

        fetchErrorRow.setField("raw_event", rawEventEncoder.apply(event));
        return fetchErrorRow;
    }
}
