package org.wikimedia.discovery.cirrus.updater.common.util.function;

import java.io.Serializable;
import java.util.function.BiFunction;

/**
 * Serializable version of the {@link BiFunction} functional interface. Useful in operators
 * serialized by flink.
 */
public interface SerializableBiFunction<T, U, R> extends BiFunction<T, U, R>, Serializable {}
