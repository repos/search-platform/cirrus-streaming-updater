package org.wikimedia.discovery.cirrus.updater.common.http;

import java.io.Serializable;
import java.time.Duration;
import java.util.Optional;

import javax.annotation.Nullable;

import org.apache.hc.core5.http.HttpHost;
import org.wikimedia.discovery.cirrus.updater.common.util.function.SerializableFunction;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;

@Value
@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class HttpConfig implements Serializable {

    public static final int UNSPECIFIED_INT = -1;

    int maxConnections;
    Duration requestTimeout;
    Duration connectionTimeout;
    SerializableFunction<HttpHost, Optional<HttpHost>> httpHostMapper;
    @Builder.Default boolean acceptGzip = true;
    @Builder.Default int http1DefaultBufferSize = UNSPECIFIED_INT;

    @Nullable String userAgent;
    @Nullable String authToken;

    Duration rateLimitRetryInterval;
    @Builder.Default int rateLimitMaxRetries = UNSPECIFIED_INT;
    @Builder.Default int rateLimitPerSecond = UNSPECIFIED_INT;
}
