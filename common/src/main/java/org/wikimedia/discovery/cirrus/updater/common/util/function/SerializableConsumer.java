package org.wikimedia.discovery.cirrus.updater.common.util.function;

import java.io.Serializable;
import java.util.function.Consumer;

public interface SerializableConsumer<T> extends Consumer<T>, Serializable {}
