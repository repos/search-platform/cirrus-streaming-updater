package org.wikimedia.discovery.cirrus.updater.common.graph;

import java.io.Closeable;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.CancellationException;
import java.util.concurrent.TimeoutException;
import java.util.function.Function;

import org.apache.flink.streaming.api.functions.async.ResultFuture;
import org.apache.flink.streaming.api.functions.async.RichAsyncFunction;
import org.apache.flink.streaming.api.operators.async.AsyncWaitOperator;
import org.apache.flink.types.Either;
import org.apache.hc.client5.http.async.methods.SimpleHttpRequest;
import org.apache.hc.client5.http.async.methods.SimpleRequestProducer;
import org.apache.hc.client5.http.impl.DefaultHttpRequestRetryStrategy;
import org.apache.hc.client5.http.impl.async.CloseableHttpAsyncClient;
import org.apache.hc.core5.concurrent.FutureCallback;
import org.apache.hc.core5.http.HttpResponse;
import org.apache.hc.core5.http.Message;
import org.apache.hc.core5.http.Method;
import org.wikimedia.discovery.cirrus.updater.common.CirrusDocFetchException;
import org.wikimedia.discovery.cirrus.updater.common.InvalidMWApiResponseException;
import org.wikimedia.discovery.cirrus.updater.common.http.GzipJsonAsyncResponseConsumer;
import org.wikimedia.discovery.cirrus.updater.common.http.HttpClientFactory;
import org.wikimedia.discovery.cirrus.updater.common.http.HttpConfig;
import org.wikimedia.discovery.cirrus.updater.common.http.MetricGroupMetricRegistryListener;
import org.wikimedia.discovery.cirrus.updater.common.model.FetchFailure;

import com.codahale.metrics.MetricRegistry;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ok2c.hc5.json.http.JsonResponseConsumers;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import lombok.extern.slf4j.Slf4j;

/**
 * A {@link Function function} fetching revision data via HTTP.
 *
 * <p>This function only retries HTTP requests if protocol implies it, for example by responding
 * with a code of <a href="https://developer.mozilla.org/en-US/docs/Web/HTTP/Status/429">429</a>,
 * see {@link DefaultHttpRequestRetryStrategy}. Retries implied by the HTTP payload must be
 * implemented at a higher level, for example via {@link AsyncWaitOperator}. Otherwise, we would
 * have to tell the content-agnostic part of the HTTP stack how to interpret content.
 *
 * @param <I> the type of input
 * @param <O> the type of output
 */
@Slf4j
@SuppressFBWarnings("SE_NO_SERIALVERSIONID")
public class CirrusFetcher<I, O> extends RichAsyncFunction<I, Either<O, FetchFailure<I>>>
        implements Closeable {
    private static final int DEFAULT_HTTP1_BUFFER_SIZE = 8192;
    private final HttpConfig httpConfig;

    private final CirrusEndpoint<I, O> endpoint;

    private transient CloseableHttpAsyncClient httpClient;

    private transient ObjectMapper objectMapper;

    public CirrusFetcher(CirrusEndpoint<I, O> endpoint, HttpConfig httpConfig) {
        this.endpoint = endpoint;
        this.httpConfig =
                httpConfig.getHttp1DefaultBufferSize() < 0
                        ? httpConfig.toBuilder().http1DefaultBufferSize(DEFAULT_HTTP1_BUFFER_SIZE).build()
                        : httpConfig;
    }

    @Override
    public void asyncInvoke(I event, ResultFuture<Either<O, FetchFailure<I>>> result) {
        getClient()
                .execute(
                        SimpleRequestProducer.create(
                                SimpleHttpRequest.create(Method.GET, endpoint.buildURI(event))),
                        new GzipJsonAsyncResponseConsumer<>(
                                JsonResponseConsumers.create(getObjectMapper().getFactory()),
                                DEFAULT_HTTP1_BUFFER_SIZE),
                        new FutureCallback<>() {
                            @Override
                            @SuppressWarnings("checkstyle:IllegalCatch")
                            public void completed(Message<HttpResponse, JsonNode> message) {
                                try {
                                    final JsonNode body = message.getBody();
                                    if (body == null) {
                                        failed(new InvalidMWApiResponseException("Empty body: " + message.getHead()));
                                    }
                                    result.complete(List.of(Either.Left(endpoint.extractAndAugment(event, body))));
                                } catch (Exception e) {
                                    failed(e);
                                }
                            }

                            @Override
                            public void failed(Exception e) {
                                if (e instanceof IOException || e instanceof CirrusDocFetchException) {
                                    result.complete(List.of(Either.Right(new FetchFailure<>(event, e))));
                                } else {
                                    result.completeExceptionally(e);
                                }
                            }

                            @Override
                            public void cancelled() {
                                failed(new CancellationException());
                            }
                        });
    }

    /**
     * Called if the async operation has timed out without yielding an acceptable result.
     *
     * <p>Since we calculate the operation timeout upfront to accommodate N retries, each with its own
     * request timeout, plus a buffer, we should never run into this.
     *
     * @param input element coming from an upstream task
     * @param resultFuture to be completed with the result data
     */
    @Override
    public void timeout(I input, ResultFuture<Either<O, FetchFailure<I>>> resultFuture) {
        resultFuture.complete(
                List.of(
                        Either.Right(
                                new FetchFailure<I>(
                                        input, new TimeoutException("Timed out while (re-)trying to fetch")))));
    }

    @Override
    public void close() throws IOException {
        if (httpClient != null) {
            httpClient.close();
        }
    }

    private CloseableHttpAsyncClient getClient() {
        if (httpClient == null) {
            final MetricRegistry metricRegistry = new MetricRegistry();
            try {
                metricRegistry.addListener(
                        new MetricGroupMetricRegistryListener(
                                getRuntimeContext().getMetricGroup().addGroup("http")));
            } catch (IllegalStateException e) {
                // If there is no runtime, skip the metric bridge
                log.warn("No runtime, unable to initialize metric bridge", e);
            }
            httpClient = HttpClientFactory.buildAsyncClient(httpConfig, metricRegistry);
            httpClient.start();
        }

        return httpClient;
    }

    private ObjectMapper getObjectMapper() {
        if (objectMapper == null) {
            objectMapper = new ObjectMapper();
        }
        return objectMapper;
    }
}
