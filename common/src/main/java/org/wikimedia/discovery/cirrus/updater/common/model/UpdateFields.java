package org.wikimedia.discovery.cirrus.updater.common.model;

import lombok.experimental.UtilityClass;

@UtilityClass
@SuppressWarnings("HideUtilityClassConstructor")
public class UpdateFields {

    public static final String DT = "dt";

    public static final String PAGE_ID = "page_id";
    public static final String PAGE_TITLE = "page_title";
    public static final String REV_ID = "rev_id";
    public static final String FIELDS = "fields";
    public static final String CHANGE_TYPE = "change_type";
    public static final String NAMESPACE_ID = "namespace_id";
    public static final String NOOP_HINTS = "cirrussearch_noop_hints";
    public static final String NOOP_FIELDS = "cirrussearch_noop_fields";
    public static final String INDEX_NAME = "cirrussearch_index_name";
    public static final String CLUSTER_GROUP = "cirrussearch_cluster_group";
    public static final String FETCHED_AT = "cirrussearch_fetched_at";
    public static final String WEIGHTED_TAGS = "weighted_tags";
    public static final String REDIRECT_NAMESPACE = "namespace";
    public static final String REDIRECT_TITLE = "title";
    public static final String REDIRECT = "redirect";
    public static final String NOOP_HINTS_SET_ADD = "add";
    public static final String NOOP_HINTS_SET_REMOVE = "remove";
    public static final String NOOP_HINTS_SET_MAX_SIZE = "max_size";
    public static final String WIKI_ID = "wiki_id";
}
