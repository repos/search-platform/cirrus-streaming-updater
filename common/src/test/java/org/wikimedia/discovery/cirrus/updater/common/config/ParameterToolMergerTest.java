package org.wikimedia.discovery.cirrus.updater.common.config;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.util.stream.Stream;

import org.apache.flink.api.java.utils.ParameterTool;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import com.google.common.base.VerifyException;

class ParameterToolMergerTest {

    private static final String DEFAULTS_LOCATION =
            "/org/wikimedia/discovery/cirrus/updater/common/config/ParameterToolMergerTest/defaults.properties";

    @Test
    void with_nothing() throws IOException {
        final ParameterTool parameterTool =
                ParameterToolMerger.fromDefaultsWithOverrides(null, new String[] {});
        assertThat(parameterTool.toMap()).isEmpty();
    }

    @Test
    void with_defaults() throws IOException {
        final ParameterTool parameterTool =
                ParameterToolMerger.fromDefaultsWithOverrides(DEFAULTS_LOCATION, new String[] {});
        assertThat(parameterTool.toMap()).hasSize(3);
        assertThat(parameterTool.getRequired("a")).isEqualTo("42");
        assertThat(parameterTool.getRequired("b")).isEqualTo("test");
        assertThat(parameterTool.getRequired("c")).isEqualTo("default");
    }

    @Test
    void with_defaults_and_file_overrides() throws IOException, URISyntaxException {
        final ParameterTool parameterTool =
                ParameterToolMerger.fromDefaultsWithOverrides(
                        DEFAULTS_LOCATION, new String[] {resourcePath("overrides.properties")});
        assertThat(parameterTool.toMap()).hasSize(3);
        assertThat(parameterTool.getRequired("a")).isEqualTo("84");
        assertThat(parameterTool.getRequired("b")).isEqualTo("test");
        assertThat(parameterTool.getRequired("c")).isEqualTo("default");
    }

    @Test
    void with_defaults_and_arg_overrides() throws IOException {
        final ParameterTool parameterTool =
                ParameterToolMerger.fromDefaultsWithOverrides(
                        DEFAULTS_LOCATION, new String[] {"--c", "overridden"});
        assertThat(parameterTool.toMap()).hasSize(3);
        assertThat(parameterTool.getRequired("a")).isEqualTo("42");
        assertThat(parameterTool.getRequired("b")).isEqualTo("test");
        assertThat(parameterTool.getRequired("c")).isEqualTo("overridden");
    }

    @Test
    void with_defaults_and_file_overrides_and_arg_overrides() throws IOException, URISyntaxException {
        final ParameterTool parameterTool =
                ParameterToolMerger.fromDefaultsWithOverrides(
                        DEFAULTS_LOCATION,
                        new String[] {resourcePath("overrides.properties"), "--c", "overridden"});
        assertThat(parameterTool.toMap()).hasSize(3);
        assertThat(parameterTool.getRequired("a")).isEqualTo("84");
        assertThat(parameterTool.getRequired("b")).isEqualTo("test");
        assertThat(parameterTool.getRequired("c")).isEqualTo("overridden");
    }

    @Test
    void fails_if_defaults_file_do_not_exist() {
        assertThatThrownBy(
                        () -> {
                            ParameterToolMerger.fromDefaultsWithOverrides("/missing.properties", new String[] {});
                        })
                .isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    void fails_if_overrides_file_do_not_exist() {
        assertThatThrownBy(
                        () -> {
                            ParameterToolMerger.fromDefaultsWithOverrides(
                                    null, new String[] {"/missing.properties"});
                        })
                .isInstanceOf(IllegalArgumentException.class);
    }

    @Test
    void with_yaml() throws IOException, URISyntaxException {
        final ParameterTool parameterTool =
                ParameterToolMerger.fromDefaultsWithOverrides(
                        DEFAULTS_LOCATION, new String[] {resourcePath("valid.yaml")});
        assertThat(parameterTool.toMap()).hasSize(7);
        assertThat(parameterTool.getRequired("a")).isEqualTo("84");
        assertThat(parameterTool.getRequired("b")).isEqualTo("test");
        assertThat(parameterTool.getRequired("c")).isEqualTo("overridden");
        assertThat(parameterTool.getRequired("d")).isEqualTo("multi line example content");
        assertThat(parameterTool.getRequired("5")).isEqualTo("numeric keys as strings");
        assertThat(parameterTool.getRequired("f")).isEqualTo("true");
        // Should be a string, not an instant
        assertThat(parameterTool.getRequired("g")).isEqualTo("2020-01-01T00:00:00");
    }

    static Stream<Arguments> invalidYamls() {
        return Stream.of(Arguments.of("nested_list.yaml"), Arguments.of("nested_map.yaml"));
    }

    @ParameterizedTest
    @MethodSource("invalidYamls")
    void reject_invalid_yamls(String path) throws IOException, URISyntaxException {
        assertThatThrownBy(
                        () -> {
                            ParameterToolMerger.fromDefaultsWithOverrides(
                                    null, new String[] {resourcePath(path)});
                        })
                .isInstanceOf(VerifyException.class);
    }

    private String resourcePath(String name) throws URISyntaxException {
        return Paths.get(getClass().getResource(getClass().getSimpleName() + "/" + name).toURI())
                .toAbsolutePath()
                .toString();
    }
}
