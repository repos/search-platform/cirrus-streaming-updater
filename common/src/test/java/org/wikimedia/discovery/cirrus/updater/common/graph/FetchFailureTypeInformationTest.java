package org.wikimedia.discovery.cirrus.updater.common.graph;

import static org.apache.flink.api.common.typeinfo.BasicTypeInfo.INT_TYPE_INFO;
import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;

import org.apache.flink.api.common.ExecutionConfig;
import org.apache.flink.api.common.typeutils.TypeSerializer;
import org.apache.flink.api.java.typeutils.PojoTypeInfo;
import org.apache.flink.api.java.typeutils.runtime.TestDataOutputSerializer;
import org.apache.flink.core.memory.DataInputDeserializer;
import org.junit.jupiter.api.Test;
import org.wikimedia.discovery.cirrus.updater.common.model.FetchFailure;
import org.wikimedia.discovery.cirrus.updater.common.model.FetchFailureTypeInformation;

class FetchFailureTypeInformationTest {

    @Test
    void serde() throws IOException {
        FetchFailure<Integer> result = new FetchFailure<>(1, new Exception("boom"));
        final PojoTypeInfo<FetchFailure<Integer>> typeInformation =
                FetchFailureTypeInformation.create(INT_TYPE_INFO);

        ExecutionConfig config = new ExecutionConfig();
        config.disableGenericTypes();
        final TypeSerializer<FetchFailure<Integer>> serializer =
                typeInformation.createSerializer(config);

        final TestDataOutputSerializer target = new TestDataOutputSerializer(4096);

        serializer.serialize(result, target);

        final FetchFailure<Integer> restoredContext =
                serializer.deserialize(new DataInputDeserializer(target.copyByteBuffer()));

        assertThat(result.getEvent()).isEqualTo(restoredContext.getEvent());
        assertThat(result.getFailureType()).isEqualTo(restoredContext.getFailureType());
        assertThat(result.getFailureMessage()).isEqualTo(restoredContext.getFailureMessage());
        assertThat(result.getFailureStack()).isEqualTo(restoredContext.getFailureStack());
    }
}
