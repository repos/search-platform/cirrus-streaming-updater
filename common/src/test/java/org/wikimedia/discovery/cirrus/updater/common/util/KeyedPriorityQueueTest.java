package org.wikimedia.discovery.cirrus.updater.common.util;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

class KeyedPriorityQueueTest {
    @Test
    void removeIf() {
        KeyedPriorityQueue<String, Integer> pq =
                new KeyedPriorityQueue<>(i -> Integer.toString(i), Integer::compare);
        assertThat(pq.isEmpty()).isTrue();
        assertThat(pq.keySet()).isEmpty();
        assertThat(pq.get("1")).isNotPresent();

        pq.add(1);
        assertThat(pq.isEmpty()).isFalse();
        assertThat(pq.keySet()).containsExactly("1");
        assertThat(pq.get("1")).get().isEqualTo(1);
        assertThat(pq.peek()).isEqualTo(1);

        pq.removeIf(i -> true);
        assertThat(pq.isEmpty()).isTrue();
        assertThat(pq.peek()).isNull();
        assertThat(pq.keySet()).isEmpty();
        assertThat(pq.get("1")).isNotPresent();
    }
}
