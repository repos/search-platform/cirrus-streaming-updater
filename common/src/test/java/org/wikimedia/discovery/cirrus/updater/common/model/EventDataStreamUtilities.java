package org.wikimedia.discovery.cirrus.updater.common.model;

import static java.util.Collections.singletonList;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Optional;

import javax.annotation.Nullable;

import org.wikimedia.eventutilities.flink.EventRowTypeInfo;
import org.wikimedia.eventutilities.flink.stream.EventDataStreamFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import lombok.experimental.UtilityClass;

@UtilityClass
@SuppressWarnings("HideUtilityClassConstructor")
@SuppressFBWarnings(
        value = "HideUtilityClassConstructor",
        justification = "lombok takes care of that")
public class EventDataStreamUtilities {

    public static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
    public static final ObjectReader JSON_READER = OBJECT_MAPPER.reader();

    public static final String SCHEMA_REPO =
            Optional.ofNullable(getResource("/schema_repo")).map(URL::toString).get();
    public static final String STREAM_CONFIG =
            Optional.ofNullable(getResource("/event-stream-config.json")).map(URL::toString).get();

    public static final EventDataStreamFactory FACTORY =
            EventDataStreamFactory.from(singletonList(SCHEMA_REPO), STREAM_CONFIG);

    private static final EventRowTypeInfo FETCH_ERROR_ROW_TYPE_INFO = buildFetchErrorTypeInfo();
    private static final EventRowTypeInfo RAW_EVENT_ROW_TYPE_INFO = buildUpdateTypeInfo();

    public static EventRowTypeInfo buildTypeInfo(String resourceName, String version) {
        return FACTORY.rowTypeInfo(resourceName, version);
    }

    public static EventRowTypeInfo buildUpdateTypeInfo() {
        return FACTORY.rowTypeInfo("cirrussearch.update_pipeline.update.rc0", "1.0.0");
    }

    public static EventRowTypeInfo buildFetchErrorTypeInfo() {
        return FACTORY.rowTypeInfo("cirrussearch.update_pipeline.fetch_error.v1", "1.0.0");
    }

    public static FetchFailureEncoder createFetchErrorEncoder() {
        return FetchFailureEncoder.encoder(
                FETCH_ERROR_ROW_TYPE_INFO,
                FACTORY,
                FACTORY
                        .getEventStreamFactory()
                        .createEventStream("cirrussearch.update_pipeline.update.rc0"),
                RAW_EVENT_ROW_TYPE_INFO);
    }

    @Nullable
    public static URL getResource(String name) {
        return EventDataStreamUtilities.class.getResource(name);
    }

    @Nullable
    public static InputStream getResourceAsStream(String name) {
        return EventDataStreamUtilities.class.getResourceAsStream(name);
    }

    public static JsonNode parseJson(String resourceName) throws IOException {
        return JSON_READER.readTree(getResourceAsStream(resourceName));
    }
}
