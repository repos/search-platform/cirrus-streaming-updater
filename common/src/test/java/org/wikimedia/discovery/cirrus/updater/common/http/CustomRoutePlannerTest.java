package org.wikimedia.discovery.cirrus.updater.common.http;

import static org.assertj.core.api.Assertions.assertThat;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import org.apache.hc.client5.http.HttpRoute;
import org.apache.hc.client5.http.impl.DefaultSchemePortResolver;
import org.apache.hc.client5.http.impl.routing.DefaultRoutePlanner;
import org.apache.hc.client5.http.routing.HttpRoutePlanner;
import org.apache.hc.core5.http.HttpException;
import org.apache.hc.core5.http.HttpHost;
import org.apache.hc.core5.http.protocol.BasicHttpContext;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class CustomRoutePlannerTest {

    List<HttpHostMapping> httpRoutes = new ArrayList<>(2);
    HttpRoutePlanner defaultRoutePlanner =
            new DefaultRoutePlanner(DefaultSchemePortResolver.INSTANCE);

    @BeforeEach
    public void setUp() throws URISyntaxException {
        httpRoutes.add(HttpHostMapping.create("managed\\.test", "someScheme://route.test:9999"));
        httpRoutes.add(HttpHostMapping.create("proxy\\.test", "diffScheme://route.test:8080"));
    }

    @Test
    void shouldNotGetSameHost() throws HttpException {
        CustomRoutePlanner customRoutePlanner =
                new CustomRoutePlanner(
                        CustomRoutePlanner.createHttpHostMapper(List.of()), defaultRoutePlanner);
        HttpHost testHost = new HttpHost("someScheme", "wikidata.org", 9999);
        HttpRoute actualRoute = customRoutePlanner.determineRoute(testHost, new BasicHttpContext());
        assertThat(actualRoute.getTargetHost()).isEqualTo(testHost);
    }

    @Test
    void shouldGetCustomRoute() throws HttpException {
        CustomRoutePlanner customRoutePlanner =
                new CustomRoutePlanner(
                        CustomRoutePlanner.createHttpHostMapper(httpRoutes), defaultRoutePlanner);
        HttpHost testHost = new HttpHost("someScheme", "managed.test", 9999);
        HttpHost expectedHost = new HttpHost("someScheme", "route.test", 9999);
        HttpRoute actualRoute = customRoutePlanner.determineRoute(testHost, new BasicHttpContext());
        assertThat(actualRoute.getTargetHost()).isEqualTo(expectedHost);
    }

    @Test
    void shouldUseProxyRoute() throws HttpException {
        CustomRoutePlanner customRoutePlanner =
                new CustomRoutePlanner(
                        CustomRoutePlanner.createHttpHostMapper(httpRoutes), defaultRoutePlanner);
        HttpHost testHost = new HttpHost("someScheme", "proxy.test", 9999);
        HttpHost expectedHost = new HttpHost("diffScheme", "route.test", 8080);
        HttpRoute actualRoute = customRoutePlanner.determineRoute(testHost, new BasicHttpContext());
        assertThat(actualRoute.getTargetHost()).isEqualTo(expectedHost);
    }
}
