package org.wikimedia.discovery.cirrus.updater.common.http;

import static org.assertj.core.api.Assertions.assertThat;

import java.net.URISyntaxException;
import java.util.stream.Stream;

import org.apache.hc.core5.http.HttpHost;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

class HttpHostMappingTest {

    @ParameterizedTest
    @MethodSource("arguments")
    void test(String sourceTarget, HttpHostMapping mapping, String expectedTarget)
            throws URISyntaxException {
        final HttpHost sourceHost = HttpHost.create(sourceTarget);
        final String actualTarget =
                mapping.matches(sourceHost) ? mapping.mergeTarget(sourceHost).toURI() : sourceTarget;

        assertThat(actualTarget).isEqualTo(expectedTarget);
    }

    static Stream<Arguments> arguments() {
        return Stream.of(
                Arguments.of(
                        "https://en.wikipedia.org",
                        HttpHostMapping.parse("en\\.wikipedia\\.org=http://localhost:9090"),
                        "http://localhost:9090"),
                Arguments.of(
                        "https://en.wikipedia.org:8443",
                        HttpHostMapping.parse("en\\.wikipedia\\.org=http://localhost"),
                        "http://localhost:8443"));
    }
}
