package org.wikimedia.discovery.cirrus.updater.common.model;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.time.Instant;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.apache.commons.compress.utils.Lists;
import org.apache.flink.types.Row;
import org.junit.jupiter.api.Test;
import org.wikimedia.eventutilities.flink.EventRowTypeInfo;
import org.wikimedia.eventutilities.flink.formats.json.JsonRowDeserializationSchema;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

class CirrusDocJsonHelperTest {
    static final EventRowTypeInfo UPDATE_TYPE_INFO = EventDataStreamUtilities.buildUpdateTypeInfo();
    static final ObjectMapper MAPPER = new ObjectMapper();

    static final String TEST_RESOURCE_PATH =
            "/" + CirrusDocJsonHelperTest.class.getCanonicalName().replace(".", "/");
    static final Instant NOW = Instant.now();

    @Test
    void test() throws IOException {
        // Obtained with curl
        // 'https://en.wikipedia.org/w/api.php?action=query&format=json&prop=cirrusbuilddoc&pageids=42548&formatversion=2&cbbuilders=content%7Clinks'
        // | jq .query.pages[0] >
        // common/src/test/resources/cirrus_doc_gulf_guinea_content_and_redirects.json
        JsonNode pageNode =
                MAPPER
                        .reader()
                        .readTree(
                                this.getClass()
                                        .getResourceAsStream(
                                                TEST_RESOURCE_PATH + "/cirrus_doc_gulf_guinea_content_and_redirects.json"));
        CirrusDocJsonHelper helper =
                new CirrusDocJsonHelper(
                        new JsonRowDeserializationSchema.Builder(
                                        UPDATE_TYPE_INFO.getTypeAt(UpdateFields.FIELDS))
                                .build(),
                        MAPPER,
                        pageNode,
                        NOW);
        assertThat(helper.getNamespace()).isEqualTo(0);
        assertThat(helper.getPageId()).isEqualTo(42548);
        assertThat(helper.getRevId()).isEqualTo(1161931219);
        assertThat(helper.getIndexName()).isEqualTo("enwiki_content");
        assertThat(helper.getNoopHints())
                .isEqualTo(Collections.singletonMap("version", "documentVersion"));
        Row rowFields = helper.getRawFields();
        assertThat(rowFields.<Long>getFieldAs("namespace")).isEqualTo(0);
        assertThat(rowFields.<Long>getFieldAs("page_id")).isEqualTo(42548);
        assertThat(rowFields.<Long>getFieldAs("version")).isEqualTo(1161931219);
        assertThat(rowFields.<Row[]>getFieldAs("coordinates")).hasSize(4);
        List<String> keys = Lists.newArrayList(pageNode.get("cirrusbuilddoc").fieldNames());
        assertThat(rowFields.getFieldNames(true)).containsAll(keys);
        for (Map.Entry<String, JsonNode> elem :
                Lists.newArrayList(pageNode.get("cirrusbuilddoc").fields())) {
            assertThat(rowFields.getField(elem.getKey()))
                    .matches(e -> elem.getValue().isNull() == (e == null));
        }
    }
}
