package org.wikimedia.discovery.cirrus.updater.common.util;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatException;
import static org.assertj.core.api.Assertions.assertThatNoException;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.Random;
import java.util.zip.DataFormatException;
import java.util.zip.GZIPOutputStream;

import org.apache.commons.io.IOUtils;
import org.apache.commons.text.RandomStringGenerator;
import org.apache.commons.text.RandomStringGenerator.Builder;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

class GzipInflaterTest {

    static final Random RANDOM = new Random(1L);
    static final RandomStringGenerator RANDOM_STRING_GENERATOR =
            new Builder().usingRandom(RANDOM::nextInt).build();

    @ParameterizedTest
    @ValueSource(ints = {16, 32, 64, 128})
    void inflate(int chunkSize) throws DataFormatException, IOException {
        final byte[] textBytes = generateRandomText(512).getBytes(StandardCharsets.UTF_8);
        final byte[] deflatedTextBytes = deflate(textBytes);

        final ByteBuffer inflatedBuffer = ByteBuffer.allocate(textBytes.length);

        try (GzipInflater inflater = new GzipInflater(chunkSize * 2)) {
            for (int i = 0; i < deflatedTextBytes.length; i += chunkSize) {
                final int length = Math.min(deflatedTextBytes.length, i + chunkSize) - i;
                final ByteBuffer inputBuffer = ByteBuffer.wrap(deflatedTextBytes, i, length);
                inflater.inflate(inputBuffer, inflatedBuffer::put);
            }
        }

        assertThat(inflatedBuffer.limit()).isEqualTo(textBytes.length);
        assertThat(inflatedBuffer.array()).isEqualTo(textBytes);
    }

    @ParameterizedTest
    @ValueSource(ints = {16, 32, 64, 128})
    void checksum(int chunkSize) throws IOException {
        final byte[] textBytes = generateRandomText(512).getBytes(StandardCharsets.UTF_8);
        final byte[] deflatedTextBytes = deflate(textBytes);

        deflatedTextBytes[deflatedTextBytes.length - 5] =
                (byte) (deflatedTextBytes[deflatedTextBytes.length - 5] + 1);

        final ByteBuffer inflatedBuffer = ByteBuffer.allocate(textBytes.length);

        try (GzipInflater inflater = new GzipInflater(chunkSize * 2)) {
            for (int i = 0; i < deflatedTextBytes.length; i += chunkSize) {
                final int length = Math.min(deflatedTextBytes.length, i + chunkSize) - i;
                final ByteBuffer inputBuffer = ByteBuffer.wrap(deflatedTextBytes, i, length);

                if (deflatedTextBytes.length - i <= chunkSize) {
                    assertThatException()
                            .isThrownBy(
                                    () -> {
                                        inflater.inflate(inputBuffer, inflatedBuffer::put);
                                    })
                            .isInstanceOf(IllegalStateException.class);
                } else {
                    assertThatNoException()
                            .isThrownBy(
                                    () -> {
                                        inflater.inflate(inputBuffer, inflatedBuffer::put);
                                    });
                }
            }
        }
    }

    /**
     * Test that a potentially encoded filename is skipped properly.
     *
     * <p>The file used was created by {@code gzip --name --keep -9 file.txt}
     */
    @Test
    void skipFileName() throws DataFormatException, IOException {
        final byte[] deflatedFileBytes =
                IOUtils.readFully(
                        GzipInflaterTest.class.getResourceAsStream(
                                GzipInflaterTest.class.getSimpleName() + "/file.txt.gz"),
                        670);

        try (GzipInflater inflater = new GzipInflater(1024)) {
            final StringBuilder fileContentBuilder = new StringBuilder();
            inflater.inflate(
                    ByteBuffer.wrap(deflatedFileBytes),
                    (inflatedBuffer) -> {
                        fileContentBuilder.append(new String(inflatedBuffer.array(), StandardCharsets.UTF_8));
                    });

            assertThat(fileContentBuilder).startsWith("start-of-file-");
            assertThat(fileContentBuilder).endsWith("-end-of-file");
        }
    }

    static byte[] deflate(byte[] textBytes) throws IOException {
        final ByteArrayOutputStream deflatedOutputStream =
                new ByteArrayOutputStream(textBytes.length / 2);
        final GZIPOutputStream deflatingOutputStream = new GZIPOutputStream(deflatedOutputStream);
        deflatingOutputStream.write(textBytes);
        deflatingOutputStream.flush();
        deflatingOutputStream.close();
        return deflatedOutputStream.toByteArray();
    }

    static String generateRandomText(int length) {
        return RANDOM_STRING_GENERATOR.generate(length);
    }
}
