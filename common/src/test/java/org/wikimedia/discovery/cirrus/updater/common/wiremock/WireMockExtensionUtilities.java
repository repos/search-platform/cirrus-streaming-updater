package org.wikimedia.discovery.cirrus.updater.common.wiremock;

import java.net.URISyntaxException;
import java.util.Collections;
import java.util.MissingResourceException;

import org.apache.hc.core5.http.ContentType;
import org.apache.hc.core5.http.HttpHeaders;

import com.github.jknack.handlebars.Handlebars;
import com.github.jknack.handlebars.Helper;
import com.github.tomakehurst.wiremock.client.ResponseDefinitionBuilder;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.common.Slf4jNotifier;
import com.github.tomakehurst.wiremock.core.Options.ChunkedEncodingPolicy;
import com.github.tomakehurst.wiremock.core.WireMockConfiguration;
import com.github.tomakehurst.wiremock.extension.responsetemplating.ResponseTemplateTransformer;
import com.github.tomakehurst.wiremock.junit5.WireMockExtension;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import lombok.experimental.UtilityClass;

@UtilityClass
@SuppressWarnings("HideUtilityClassConstructor")
@SuppressFBWarnings(
        value = "HideUtilityClassConstructor",
        justification = "lombok takes care of that")
public class WireMockExtensionUtilities {

    private static final String ROOT_DIRECTORY;

    static {
        try {
            ROOT_DIRECTORY = WireMockExtensionUtilities.class.getResource("/wiremock").toURI().getPath();
        } catch (URISyntaxException e) {
            throw new MissingResourceException(
                    "Unable to resolve wiremock root directory",
                    WireMockExtensionUtilities.class.getCanonicalName(),
                    "/wiremock");
        }
    }

    private static final Handlebars HANDLEBARS =
            new Handlebars()
                    .registerHelper(
                            "compare",
                            (Helper<Number>)
                                    (left, options) -> {
                                        String operator = (String) options.params[0];
                                        Number right = (Number) options.params[1];
                                        switch (operator) {
                                            case "<":
                                                return left.doubleValue() < right.doubleValue();
                                            case "<=":
                                                return left.doubleValue() <= right.doubleValue();
                                            case ">":
                                                return left.doubleValue() > right.doubleValue();
                                            case ">=":
                                                return left.doubleValue() >= right.doubleValue();
                                            default:
                                                throw new IllegalArgumentException("Unsupported operator: " + operator);
                                        }
                                    });

    static {
        // override default ({{) so handlebars does not get confused with wiki text expressions.
        // Note that wiremock integration doesn't support custom delimiters. For proper operation
        // the custom delimiter must start with {{.
        HANDLEBARS.setStartDelimiter("{{$");
    }

    public static WireMockExtension getWireMockExtension() {
        return WireMockExtension.newInstance().options(getWireMockConfiguration()).build();
    }

    public static WireMockConfiguration getWireMockConfiguration() {
        return WireMockConfiguration.options()
                .withRootDirectory(ROOT_DIRECTORY)
                .useChunkedTransferEncoding(ChunkedEncodingPolicy.NEVER)
                .notifier(new Slf4jNotifier(true))
                .dynamicPort()
                .extensions(
                        new ResponseTemplateTransformer(true, HANDLEBARS, Collections.emptyMap(), null, null),
                        new UnorderedLines());
    }

    public static ResponseDefinitionBuilder mockJsonResponse(String fileName) {
        return WireMock.aResponse()
                .withHeader(HttpHeaders.CONTENT_TYPE, ContentType.APPLICATION_JSON.toString())
                .withBodyFile(fileName);
    }
}
