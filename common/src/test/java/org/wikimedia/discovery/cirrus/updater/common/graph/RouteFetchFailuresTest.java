package org.wikimedia.discovery.cirrus.updater.common.graph;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.time.Instant;
import java.util.List;
import java.util.UUID;
import java.util.stream.Stream;

import org.apache.flink.streaming.runtime.streamrecord.StreamRecord;
import org.apache.flink.streaming.util.OneInputStreamOperatorTestHarness;
import org.apache.flink.streaming.util.ProcessFunctionTestHarnesses;
import org.apache.flink.types.Either;
import org.apache.flink.types.Row;
import org.apache.flink.util.OutputTag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.wikimedia.discovery.cirrus.updater.common.InvalidMWApiResponseException;
import org.wikimedia.discovery.cirrus.updater.common.RevisionNotFoundException;
import org.wikimedia.discovery.cirrus.updater.common.config.EventStreamConstants;
import org.wikimedia.discovery.cirrus.updater.common.model.ChangeType;
import org.wikimedia.discovery.cirrus.updater.common.model.EventDataStreamUtilities;
import org.wikimedia.discovery.cirrus.updater.common.model.FetchFailure;
import org.wikimedia.discovery.cirrus.updater.common.model.FetchFailureEncoder;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateEvent;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateEvent.Meta;

class RouteFetchFailuresTest {

    private static final String JOB_NAME = "my_pipeline";
    private final OutputTag<Row> tag =
            new OutputTag<>(
                    "errors",
                    EventDataStreamUtilities.buildTypeInfo(
                            EventStreamConstants.FETCH_ERROR_STREAM,
                            EventStreamConstants.FETCH_ERROR_SCHEMA_VERSION));
    private final FetchFailureEncoder encoder = EventDataStreamUtilities.createFetchErrorEncoder();
    private final RouteFetchFailures failureRouter = new RouteFetchFailures(JOB_NAME, tag, encoder);

    public static Stream<Arguments> provide_exceptions_for_test_errors() {
        return Stream.of(
                Arguments.arguments(new InvalidMWApiResponseException("bad response"), "MW_ERROR"),
                Arguments.arguments(new RevisionNotFoundException("not found"), "NOT_FOUND"),
                Arguments.arguments(new IOException("IO problem"), "NETWORK_ERROR"),
                Arguments.arguments(
                        new UncheckedIOException(new IOException("IO problem")), "NETWORK_ERROR"));
    }

    @ParameterizedTest
    @MethodSource("provide_exceptions_for_test_errors")
    void test_errors(Exception e) throws Exception {
        try (OneInputStreamOperatorTestHarness<
                        Either<UpdateEvent, FetchFailure<UpdateEvent>>, UpdateEvent>
                testHarness = ProcessFunctionTestHarnesses.forProcessFunction(failureRouter)) {
            UpdateEvent event = new UpdateEvent();
            event.setChangeType(ChangeType.REV_BASED_UPDATE);
            final String domain = "my_domain";
            event.setMeta(Meta.builder().domain(domain).requestId(UUID.randomUUID().toString()).build());
            event.setEventTime(Instant.now());
            event.setIngestionTime(Instant.now());
            UpdateEvent.TargetDocument target = new UpdateEvent.TargetDocument(domain, "my_db", 2L, 123L);
            target.setPageTitle("My_Title");
            target.completeWith("test", "test");
            event.setTargetDocument(target);
            event.setRevId(234L);
            final FetchFailure<UpdateEvent> failure = new FetchFailure<>(event, e);
            testHarness.processElement(Either.Right(failure), 1L);
            StreamRecord<Row> error = testHarness.getSideOutput(tag).poll();
            assertThat(error).isNotNull();
            assertThat(error.getTimestamp()).isEqualTo(1L);
        }
    }

    @Test
    void test_events_are_propagated() throws Exception {
        try (OneInputStreamOperatorTestHarness<
                        Either<UpdateEvent, FetchFailure<UpdateEvent>>, UpdateEvent>
                testHarness = ProcessFunctionTestHarnesses.forProcessFunction(failureRouter)) {
            UpdateEvent event = new UpdateEvent();
            event.setMeta(Meta.builder().requestId(UUID.randomUUID().toString()).build());
            testHarness.processElement(Either.Left(event), 1L);
            List<StreamRecord<? extends UpdateEvent>> streamRecords =
                    testHarness.extractOutputStreamRecords();
            assertThat(streamRecords).isNotEmpty();
            StreamRecord<? extends UpdateEvent> streamRecord = streamRecords.get(0);
            assertThat(streamRecord.getTimestamp()).isEqualTo(1);
            assertThat(streamRecord.getValue()).isEqualTo(event);
        }
    }
}
