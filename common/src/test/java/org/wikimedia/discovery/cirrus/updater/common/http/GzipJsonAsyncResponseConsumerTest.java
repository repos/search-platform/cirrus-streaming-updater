package org.wikimedia.discovery.cirrus.updater.common.http;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatNoException;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.URI;
import java.time.Duration;
import java.util.Map;
import java.util.Optional;
import java.util.Random;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.IntStream;
import java.util.zip.GZIPInputStream;

import javax.annotation.Nonnull;

import org.apache.commons.text.RandomStringGenerator;
import org.apache.commons.text.RandomStringGenerator.Builder;
import org.apache.hc.client5.http.async.methods.SimpleHttpRequest;
import org.apache.hc.client5.http.async.methods.SimpleHttpResponse;
import org.apache.hc.client5.http.async.methods.SimpleRequestProducer;
import org.apache.hc.client5.http.impl.async.CloseableHttpAsyncClient;
import org.apache.hc.core5.concurrent.FutureCallback;
import org.apache.hc.core5.http.ContentType;
import org.apache.hc.core5.http.HttpHeaders;
import org.apache.hc.core5.http.HttpResponse;
import org.apache.hc.core5.http.Message;
import org.apache.hc.core5.http.Method;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;
import org.junit.jupiter.api.condition.EnabledIfEnvironmentVariable;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.wikimedia.discovery.cirrus.updater.common.wiremock.WireMockExtensionUtilities;

import com.codahale.metrics.MetricRegistry;
import com.fasterxml.jackson.core.io.JsonEOFException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.tomakehurst.wiremock.client.ResponseDefinitionBuilder;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.junit5.WireMockExtension;
import com.ok2c.hc5.json.http.JsonResponseConsumers;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@SuppressWarnings({"checkstyle:classfanoutcomplexity"})
class GzipJsonAsyncResponseConsumerTest {
    // test with small buffer size to force worst case buffering scenario
    private static final int TEST_BUFFER_SIZE = 3;

    static final @RegisterExtension WireMockExtension wireMockExtension =
            WireMockExtensionUtilities.getWireMockExtension();

    static final String TEXT_SUFFIX = "-end-of-string";
    static final RandomStringGenerator RANDOM_STRING_GENERATOR =
            new Builder().withinRange(97, 123).usingRandom(new Random(1L)::nextInt).build();
    static final String TEXT = generateRandomString(500 * 1024, TEXT_SUFFIX);

    static final int MAX_REQUESTS = 2000;

    static final int PERIOD_MS = 10;
    static final int MAX_CONNECTIONS = 10;

    final ScheduledExecutorService scheduledExecutorService =
            Executors.newScheduledThreadPool(
                    1,
                    new ThreadFactory() {

                        int counter;

                        @Override
                        public Thread newThread(@Nonnull Runnable r) {
                            return new Thread(r, "request-scheduler-" + counter++);
                        }
                    });

    final ObjectMapper objectMapper = new ObjectMapper();

    CloseableHttpAsyncClient client;

    @BeforeEach
    void startClient(TestInfo testInfo) {
        boolean acceptGzip = !testInfo.getDisplayName().equals("acceptGzip=false");
        client =
                HttpClientFactory.buildAsyncClient(
                        HttpConfig.builder()
                                .maxConnections(MAX_CONNECTIONS)
                                .requestTimeout(Duration.ofMillis(3000))
                                .connectionTimeout(Duration.ofMillis(250))
                                .httpHostMapper(host -> Optional.empty())
                                .acceptGzip(acceptGzip)
                                .http1DefaultBufferSize(TEST_BUFFER_SIZE)
                                .userAgent("Test/User Agent")
                                .build(),
                        new MetricRegistry());
        client.start();
    }

    @AfterEach
    void stopClient() throws IOException {
        if (client != null) {
            client.close();
        }

        scheduledExecutorService.shutdownNow();
    }

    @ParameterizedTest(name = "nonBlocking={arguments}")
    @ValueSource(booleans = {true, false})
    @EnabledIfEnvironmentVariable(
            named = "PROFILE",
            matches = "1",
            disabledReason = "Only enabled on demand")
    void profile(boolean nonBlocking) {
        wireMockExtension.stubFor(
                WireMock.get(WireMock.urlMatching("/largeResponse.*"))
                        .willReturn(
                                mockLargeJsonResponse("GzipJsonAsyncResponseConsumerTest.largeResponse.json")));

        final Map<Integer, Boolean> completedRequests = new ConcurrentHashMap<>(MAX_REQUESTS);

        final AtomicInteger scheduledRequests = new AtomicInteger(0);

        scheduledExecutorService.scheduleAtFixedRate(
                () -> {
                    if (scheduledRequests.get() < MAX_REQUESTS) {
                        final int id = scheduledRequests.getAndIncrement();
                        executeRequest("/largeResponse?id=" + id, nonBlocking)
                                .whenComplete(
                                        (extractedId, error) -> {
                                            if (error != null) {
                                                log.error("ID {} failed", id, error);
                                            }
                                            completedRequests.put(id, extractedId == id);
                                            if (completedRequests.size() == MAX_REQUESTS) {
                                                scheduledExecutorService.shutdown();
                                            }
                                        });
                    }
                },
                0,
                PERIOD_MS,
                TimeUnit.MILLISECONDS);

        assertThatNoException()
                .isThrownBy(
                        () -> {
                            final int terminationTimeoutMs = MAX_REQUESTS * (PERIOD_MS + 100);
                            assertThat(
                                            scheduledExecutorService.awaitTermination(
                                                    terminationTimeoutMs, TimeUnit.MILLISECONDS))
                                    .isTrue();
                        });

        IntStream.range(0, MAX_REQUESTS)
                .forEach(i -> assertThat(completedRequests).containsEntry(i, true));
    }

    @ParameterizedTest(name = "acceptGzip={arguments}")
    @ValueSource(booleans = {true, false})
    void largeResponse(boolean acceptGzip) {
        wireMockExtension.stubFor(
                WireMock.get(WireMock.urlMatching("/largeResponse.*"))
                        .willReturn(
                                mockLargeJsonResponse("GzipJsonAsyncResponseConsumerTest.largeResponse.json")));

        final int id = 42;

        final CompletableFuture<Integer> future = executeRequest("/largeResponse?id=" + id, true);

        assertThat(future).succeedsWithin(Duration.ofSeconds(5));
        assertThat(future.getNow(-1)).isEqualTo(id);
    }

    @Test
    void largeIncompleteResponse() {
        wireMockExtension.stubFor(
                WireMock.get(WireMock.urlMatching("/largeIncompleteResponse"))
                        .willReturn(
                                mockLargeJsonResponse(
                                        "GzipJsonAsyncResponseConsumerTest.largeIncompleteResponse.json")));

        final CompletableFuture<Integer> future = executeRequest("/largeIncompleteResponse", true);

        assertThat(future)
                .failsWithin(Duration.ofSeconds(5))
                .withThrowableOfType(ExecutionException.class)
                .withCauseInstanceOf(JsonEOFException.class);
    }

    private static ResponseDefinitionBuilder mockLargeJsonResponse(String fileName) {
        return WireMock.aResponse()
                .withHeader(HttpHeaders.CONTENT_TYPE, ContentType.APPLICATION_JSON.toString())
                .withTransformer("response-template", "text", TEXT)
                .withBodyFile(fileName);
    }

    @Nonnull
    private CompletableFuture<Integer> executeRequest(String path, boolean nonBlocking) {
        final CompletableFuture<Integer> future = new CompletableFuture<>();
        final URI uri = URI.create(wireMockExtension.baseUrl() + path);
        final SimpleHttpRequest request = SimpleHttpRequest.create(Method.GET, uri);
        final FutureCallback<Message<HttpResponse, JsonNode>> futureCallback =
                createFutureCallback(future);

        if (nonBlocking) {
            client.execute(
                    SimpleRequestProducer.create(request),
                    new GzipJsonAsyncResponseConsumer<>(
                            JsonResponseConsumers.create(objectMapper.getFactory()), TEST_BUFFER_SIZE),
                    futureCallback);
        } else {
            client.execute(request, createSimpleCallback(future));
        }

        return future;
    }

    private static int validateJson(JsonNode node) {
        assertThat(node.at("/nested/text").asText()).endsWith(TEXT_SUFFIX);
        return node.at("/nested/meta/id").asInt();
    }

    @Nonnull
    private static FutureCallback<Message<HttpResponse, JsonNode>> createFutureCallback(
            CompletableFuture<Integer> future) {
        return new FutureCallback<>() {
            @Override
            public void cancelled() {
                future.cancel(true);
            }

            @Override
            public void completed(Message<HttpResponse, JsonNode> result) {
                future.complete(validateJson(result.getBody()));
            }

            @Override
            public void failed(Exception ex) {
                future.completeExceptionally(ex);
            }
        };
    }

    @Nonnull
    private FutureCallback<SimpleHttpResponse> createSimpleCallback(
            CompletableFuture<Integer> future) {
        return new FutureCallback<>() {
            @Override
            public void cancelled() {
                future.cancel(true);
            }

            @Override
            public void completed(SimpleHttpResponse result) {
                try {
                    final GZIPInputStream gzipInputStream =
                            new GZIPInputStream(new ByteArrayInputStream(result.getBodyBytes()));
                    final JsonNode jsonNode = objectMapper.readTree(gzipInputStream);
                    // this closes the stream prematurely,
                    future.complete(validateJson(jsonNode));
                } catch (IOException e) {
                    future.completeExceptionally(e);
                }
            }

            @Override
            public void failed(Exception ex) {
                future.completeExceptionally(ex);
            }
        };
    }

    static String generateRandomString(int size, String suffix) {
        return RANDOM_STRING_GENERATOR.generate(size - suffix.length()) + suffix;
    }
}
