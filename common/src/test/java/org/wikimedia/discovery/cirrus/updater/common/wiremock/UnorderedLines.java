package org.wikimedia.discovery.cirrus.updater.common.wiremock;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.github.tomakehurst.wiremock.extension.Parameters;
import com.github.tomakehurst.wiremock.http.Request;
import com.github.tomakehurst.wiremock.matching.MatchResult;
import com.github.tomakehurst.wiremock.matching.RequestMatcherExtension;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class UnorderedLines extends RequestMatcherExtension {
    public static final String NAME = "unordered-lines";
    public static final String PARAM_LINES = "lines";

    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public MatchResult match(Request request, Parameters parameters) {
        String body = request.getBodyAsString();
        List<JsonNode> actual =
                Stream.of(body.split("\n")).map(UnorderedLines::toJson).collect(Collectors.toList());
        try {
            List<JsonNode> expected =
                    parse(parameters.getList(PARAM_LINES)).stream()
                            .map(UnorderedLines::toJson)
                            .collect(Collectors.toList());
            final boolean isMatch = expected.size() == actual.size() && actual.containsAll(expected);
            if (!isMatch) {
                log.warn("NO-MATCH {}/{}", actual.size(), expected.size());
            }
            return MatchResult.of(isMatch);
        } catch (JsonProcessingException e) {
            log.error("NO-MATCH", e);
            return MatchResult.noMatch();
        }
    }

    private List<String> parse(List<?> lines) throws JsonProcessingException {
        List<String> result = new ArrayList<>(lines.size());
        for (Object line : lines) {
            if (line instanceof String) {
                result.add((String) line);
            } else {
                result.add(OBJECT_MAPPER.writeValueAsString(line));
            }
        }
        return result;
    }

    public static JsonNode toJson(ArrayNode lines) {
        ObjectNode matcher = OBJECT_MAPPER.createObjectNode();
        matcher.set("name", OBJECT_MAPPER.valueToTree(UnorderedLines.NAME));
        matcher.set(
                "parameters", OBJECT_MAPPER.createObjectNode().set(UnorderedLines.PARAM_LINES, lines));
        return matcher;
    }

    public static JsonNode toJson(String line) {
        try {
            return OBJECT_MAPPER.readTree(line);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
}
