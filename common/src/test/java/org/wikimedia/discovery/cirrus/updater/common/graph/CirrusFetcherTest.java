package org.wikimedia.discovery.cirrus.updater.common.graph;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatNoException;
import static org.wikimedia.discovery.cirrus.updater.common.wiremock.WireMockExtensionUtilities.getWireMockExtension;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.Duration;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.apache.flink.streaming.api.functions.async.ResultFuture;
import org.apache.flink.types.Either;
import org.apache.hc.core5.http.ContentType;
import org.apache.hc.core5.http.HttpHeaders;
import org.apache.hc.core5.http.HttpStatus;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.mockito.junit.jupiter.MockitoExtension;
import org.wikimedia.discovery.cirrus.updater.common.InvalidMWApiResponseException;
import org.wikimedia.discovery.cirrus.updater.common.http.CustomRoutePlanner;
import org.wikimedia.discovery.cirrus.updater.common.http.HttpConfig;
import org.wikimedia.discovery.cirrus.updater.common.http.HttpHostMapping;
import org.wikimedia.discovery.cirrus.updater.common.model.FetchFailure;

import com.fasterxml.jackson.databind.JsonNode;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.http.Fault;
import com.github.tomakehurst.wiremock.junit5.WireMockExtension;
import com.github.tomakehurst.wiremock.junit5.WireMockRuntimeInfo;

@ExtendWith(MockitoExtension.class)
class CirrusFetcherTest {
    static final CirrusEndpoint<Long, Long> FAKE_ENDPOINT =
            new CirrusEndpoint<>() {
                @Override
                public URI buildURI(Long event) {
                    return URI.create(
                            "https://"
                                    + DOMAIN
                                    + "/w/api.php?action=query&format=json&cbbuilders=content"
                                    + "&prop=cirrusbuilddoc&formatversion=2&format=json&revids="
                                    + event);
                }

                @Override
                public Long extractAndAugment(Long event, JsonNode response) {
                    return response.at("/query/pages/0/cirrusbuilddoc/version").asLong();
                }
            };

    @RegisterExtension static WireMockExtension wireMockExtension = getWireMockExtension();

    static final String DOMAIN = "mydomain.local";

    CirrusFetcher<Long, Long> revisionFetcher;

    @BeforeEach
    void createFetcher(WireMockRuntimeInfo info) throws URISyntaxException {
        revisionFetcher =
                new CirrusFetcher<>(
                        FAKE_ENDPOINT,
                        HttpConfig.builder()
                                .maxConnections(1)
                                .requestTimeout(Duration.ofMillis(250))
                                .connectionTimeout(Duration.ofMillis(100))
                                .httpHostMapper(
                                        CustomRoutePlanner.createHttpHostMapper(
                                                List.of(HttpHostMapping.create(DOMAIN, info.getHttpBaseUrl()))))
                                .userAgent("Test/User Agent")
                                .rateLimitRetryInterval(Duration.ofMillis(100))
                                .rateLimitMaxRetries(10)
                                .build());
    }

    @AfterEach
    void closeFetcher() throws IOException {
        if (revisionFetcher != null) {
            revisionFetcher.close();
        }
    }

    @Test
    void properResponses() {
        final long revId = 551141L;

        wireMockExtension.stubFor(
                WireMock.get(WireMock.urlMatching("/w/api.php.*revids=" + revId + ".*"))
                        .inScenario(Long.toString(revId))
                        .willReturn(
                                WireMock.aResponse()
                                        .withHeader(HttpHeaders.CONTENT_TYPE, ContentType.APPLICATION_JSON.toString())
                                        .withBodyFile("cirrus_build_doc_test.revision." + revId + ".json")));

        assertThatNoException()
                .isThrownBy(
                        () -> {
                            assertThat(get(revisionFetcher, revId)).extracting(Either::left).isEqualTo(revId);
                        });
    }

    @Test
    void faultyResponses() throws ExecutionException, InterruptedException, TimeoutException {
        final long revId = 551142L;

        wireMockExtension.stubFor(
                WireMock.get(WireMock.urlMatching("/w/api.php.*revids=" + revId + ".*"))
                        .inScenario(Long.toString(revId))
                        .willReturn(WireMock.serverError().withFixedDelay(1000))
                        .willSetStateTo("too many requests"));

        assertThat(get(revisionFetcher, revId))
                .extracting(Either::right)
                .extracting(FetchFailure::getFailure)
                .isInstanceOf(IOException.class);

        wireMockExtension.stubFor(
                WireMock.get(WireMock.urlMatching("/w/api.php.*revids=" + revId + ".*"))
                        .inScenario(Long.toString(revId))
                        .whenScenarioStateIs("too many requests")
                        .willReturn(
                                WireMock.status(HttpStatus.SC_TOO_MANY_REQUESTS)
                                        .withHeader(HttpHeaders.RETRY_AFTER, "2")
                                        .withFixedDelay(50))
                        .willSetStateTo("server error"));
        // will cause a retry a client level

        wireMockExtension.stubFor(
                WireMock.get(WireMock.urlMatching("/w/api.php.*revids=" + revId + ".*"))
                        .inScenario(Long.toString(revId))
                        .whenScenarioStateIs("server error")
                        .willReturn(WireMock.serverError().withFixedDelay(50))
                        .willSetStateTo("connection reset"));
        // will fail, since CirrusFetcher does not handle retries (only the wrapping
        // AsyncWaitOperatorFactory does)

        assertThat(get(revisionFetcher, revId))
                .extracting(Either::right)
                .extracting(FetchFailure::getFailure)
                .isInstanceOf(InvalidMWApiResponseException.class);

        wireMockExtension.stubFor(
                WireMock.get(WireMock.urlMatching("/w/api.php.*revids=" + revId + ".*"))
                        .inScenario(Long.toString(revId))
                        .whenScenarioStateIs("connection reset")
                        .willReturn(WireMock.serverError().withFault(Fault.CONNECTION_RESET_BY_PEER)));

        assertThat(get(revisionFetcher, revId))
                .extracting(Either::right)
                .extracting(FetchFailure::getFailure)
                .isInstanceOf(IOException.class);
    }

    private static Either<Long, FetchFailure<Long>> get(
            CirrusFetcher<Long, Long> revisionFetcher, Long input)
            throws ExecutionException, InterruptedException, TimeoutException {
        final CompletableResultFuture<Either<Long, FetchFailure<Long>>> result =
                new CompletableResultFuture<>();
        revisionFetcher.asyncInvoke(input, result);
        return get(result);
    }

    private static Either<Long, FetchFailure<Long>> get(
            CompletableResultFuture<Either<Long, FetchFailure<Long>>> result)
            throws InterruptedException, ExecutionException, TimeoutException {
        return result.delegate.get(5, TimeUnit.SECONDS);
    }

    static class CompletableResultFuture<OUT> implements ResultFuture<OUT> {

        final CompletableFuture<OUT> delegate = new CompletableFuture<>();

        @Override
        public void complete(Collection<OUT> result) {
            result.stream().findFirst().ifPresent(delegate::complete);
        }

        @Override
        public void completeExceptionally(Throwable error) {
            delegate.completeExceptionally(error);
        }
    }
}
