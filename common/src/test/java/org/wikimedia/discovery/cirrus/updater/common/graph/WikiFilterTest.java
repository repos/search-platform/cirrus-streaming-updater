package org.wikimedia.discovery.cirrus.updater.common.graph;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

import java.util.Collections;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.testcontainers.shaded.com.google.common.collect.ImmutableList;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateEvent;
import org.wikimedia.discovery.cirrus.updater.common.util.function.SerializableFunction;
import org.wikimedia.discovery.cirrus.updater.common.util.function.SerializablePredicate;

public class WikiFilterTest {
    SerializableFunction<String, String> stringIdentity = e -> e;

    @Test
    void noFilter() {
        Optional<SerializablePredicate<String>> maybeFilter =
                WikiFilter.createFilter(stringIdentity, null);
        assertThat(maybeFilter).isNotPresent();

        maybeFilter = WikiFilter.createFilter(stringIdentity, Collections.emptyList());
        assertThat(maybeFilter).isNotPresent();
    }

    @Test
    void includeOnly() {
        Optional<SerializablePredicate<UpdateEvent>> maybeFilter =
                WikiFilter.createFilter(UpdateEvent.WIKIID_EXTRACTOR, ImmutableList.of("aawiki"));
        assertThat(maybeFilter).isPresent();
        SerializablePredicate<UpdateEvent> filter = maybeFilter.get();

        UpdateEvent event = new UpdateEvent();

        event.setTargetDocument(docForWiki("aawiki"));
        assertThat(filter.test(event)).isTrue();

        event.setTargetDocument(docForWiki("aawikibooks"));
        assertThat(filter.test(event)).isFalse();

        event.setTargetDocument(docForWiki("aaawiki"));
        assertThat(filter.test(event)).isFalse();
    }

    @Test
    void excludeOnly() {
        Optional<SerializablePredicate<UpdateEvent>> maybeFilter =
                WikiFilter.createFilter(UpdateEvent.WIKIID_EXTRACTOR, ImmutableList.of("-aawiki"));
        assertThat(maybeFilter).isPresent();
        SerializablePredicate<UpdateEvent> filter = maybeFilter.get();

        UpdateEvent event = new UpdateEvent();

        event.setTargetDocument(docForWiki("aawiki"));
        assertThat(filter.test(event)).isFalse();

        event.setTargetDocument(docForWiki("aawikibooks"));
        assertThat(filter.test(event)).isTrue();

        event.setTargetDocument(docForWiki("aaawiki"));
        assertThat(filter.test(event)).isTrue();
    }

    @Test
    void includeAndExclude() {
        assertThatExceptionOfType(IllegalArgumentException.class)
                .isThrownBy(
                        () ->
                                WikiFilter.createFilter(
                                        UpdateEvent.WIKIID_EXTRACTOR, ImmutableList.of("-aawiki", "abwiki")));
    }

    private static UpdateEvent.TargetDocument docForWiki(String wikiId) {
        return new UpdateEvent.TargetDocument(null, wikiId, null, null, null, null, null);
    }
}
