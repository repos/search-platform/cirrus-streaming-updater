package org.wikimedia.discovery.cirrus.updater.common.graph;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Collections;

import org.apache.flink.api.common.functions.FilterFunction;
import org.junit.jupiter.api.Test;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateEvent;

import com.google.common.collect.ImmutableList;

class CommonGraphFactoryTest {

    @Test
    void filterCanaryEvents() throws Exception {
        FilterFunction<UpdateEvent> filter = CommonGraphFactory.createFilter(null);
        final UpdateEvent updateEvent = new UpdateEvent();

        updateEvent.setTargetDocument(new UpdateEvent.TargetDocument(null, null, null, null));
        assertThat(filter.filter(updateEvent)).isFalse();

        updateEvent.setTargetDocument(new UpdateEvent.TargetDocument("canary", null, null, null));
        assertThat(filter.filter(updateEvent)).isFalse();

        updateEvent.setTargetDocument(new UpdateEvent.TargetDocument(null, "canary", null, null));
        assertThat(filter.filter(updateEvent)).isFalse();

        updateEvent.setTargetDocument(new UpdateEvent.TargetDocument("other", "test", null, null));
        assertThat(filter.filter(updateEvent)).isTrue();
    }

    @Test
    void filterUnsupportedWikis() throws Exception {
        FilterFunction<UpdateEvent> filter =
                CommonGraphFactory.createFilter(ImmutableList.of("testwiki"));
        final UpdateEvent updateEvent = new UpdateEvent();

        updateEvent.setTargetDocument(new UpdateEvent.TargetDocument("canary", null, null, null));
        assertThat(filter.filter(updateEvent)).isFalse();

        updateEvent.setTargetDocument(
                new UpdateEvent.TargetDocument("test.somewhere.com", "testwiki", null, null));
        assertThat(filter.filter(updateEvent)).isTrue();

        updateEvent.setTargetDocument(
                new UpdateEvent.TargetDocument("other.org", "otherwiki", null, null));
        assertThat(filter.filter(updateEvent)).isFalse();

        filter = CommonGraphFactory.createFilter(null);
        assertThat(filter.filter(updateEvent)).isTrue();

        filter = CommonGraphFactory.createFilter(Collections.emptyList());
        assertThat(filter.filter(updateEvent)).isTrue();
    }
}
