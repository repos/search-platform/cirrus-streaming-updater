package org.wikimedia.discovery.cirrus.updater.common.model;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.time.Instant;
import java.util.UUID;
import java.util.stream.Stream;

import org.apache.flink.types.Row;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.wikimedia.discovery.cirrus.updater.common.InvalidMWApiResponseException;
import org.wikimedia.discovery.cirrus.updater.common.RevisionNotFoundException;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateEvent.Meta;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateEvent.TargetDocument;

class FetchFailureEncoderTest {

    public static Stream<Arguments> provide_exceptions_for_test_errors() {
        return Stream.of(
                Arguments.arguments(new InvalidMWApiResponseException("bad response"), "MW_ERROR"),
                Arguments.arguments(new RevisionNotFoundException("not found"), "NOT_FOUND"),
                Arguments.arguments(new IOException("IO problem"), "NETWORK_ERROR"),
                Arguments.arguments(
                        new UncheckedIOException(new IOException("IO problem")), "NETWORK_ERROR"));
    }

    @ParameterizedTest
    @MethodSource("provide_exceptions_for_test_errors")
    void test_errors(Exception e) {
        final UpdateEvent event = createEvent("my_domain", ChangeType.REV_BASED_UPDATE);
        final TargetDocument target = event.getTargetDocument();

        final FetchFailureEncoder encoder = EventDataStreamUtilities.createFetchErrorEncoder();

        Row r = encoder.encode(new FetchFailure<>(event, e), "test");
        assertThat(r.<Row>getFieldAs("meta").getField("domain")).isEqualTo(target.getDomain());
        assertThat(r.<Row>getFieldAs("meta").getField("request_id")).isEqualTo(event.getRequestId());
        assertThat(r.getField("wiki_id")).isEqualTo(target.getWikiId());
        assertThat(r.getField("namespace_id")).isEqualTo(target.getPageNamespace());
        assertThat(r.getField("page_id")).isEqualTo(target.getPageId());
        assertThat(r.getField("rev_id")).isEqualTo(event.getRevId());
        assertThat(r.getField("page_title")).isEqualTo(target.getPageTitle());
        assertThat(r.getField("original_ingestion_time")).isEqualTo(event.getIngestionTime());
        assertThat(r.getField("original_event_time")).isEqualTo(event.getEventTime());
        assertThat(r.getField("error_type")).isEqualTo(FetchFailure.extractFailureType(e));
        assertThat(r.getField("message")).isEqualTo(e.getMessage());
    }

    @Test
    void test_page_rerender_upsert() {
        final UpdateEvent event = createEvent("my_domain", ChangeType.PAGE_RERENDER_UPSERT);
        final FetchFailureEncoder encoder = EventDataStreamUtilities.createFetchErrorEncoder();
        Row r =
                encoder.encode(new FetchFailure<>(event, new InvalidMWApiResponseException("")), "test");
        assertThat(r.<String>getFieldAs("raw_event")).contains(ChangeType.PAGE_RERENDER_UPSERT.name());
    }

    private TargetDocument createTarget(String domain) {
        TargetDocument target = new TargetDocument(domain, "my_db", 2L, 123L);
        target.completeWith("test", "test");
        target.setPageTitle("My_Title");
        return target;
    }

    private UpdateEvent createEvent(String domain, ChangeType changeType) {
        UpdateEvent event = new UpdateEvent();
        event.setMeta(Meta.builder().domain(domain).requestId(UUID.randomUUID().toString()).build());
        event.setEventTime(Instant.now());
        event.setIngestionTime(Instant.now());
        event.setChangeType(changeType);

        TargetDocument target = createTarget(domain);
        event.setTargetDocument(target);
        event.setRevId(234L);
        return event;
    }
}
