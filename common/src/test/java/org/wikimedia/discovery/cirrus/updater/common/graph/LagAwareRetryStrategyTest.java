package org.wikimedia.discovery.cirrus.updater.common.graph;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.time.Duration;
import java.time.Instant;
import java.util.List;
import java.util.stream.Stream;

import org.apache.flink.types.Either;
import org.junit.jupiter.api.Named;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.wikimedia.discovery.cirrus.updater.common.InvalidMWApiResponseException;
import org.wikimedia.discovery.cirrus.updater.common.RevisionNotFoundException;
import org.wikimedia.discovery.cirrus.updater.common.model.ChangeType;
import org.wikimedia.discovery.cirrus.updater.common.model.FetchFailure;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateEvent;

class LagAwareRetryStrategyTest {

    static final Instant NOW = Instant.parse("2022-01-01T10:00:00.000Z");
    final int maxAttempts = 3;

    private static UpdateEvent buildUpdateEvent(Instant revTimestamp) {
        UpdateEvent updateEvent = new UpdateEvent();
        updateEvent.setChangeType(ChangeType.REV_BASED_UPDATE);
        updateEvent.setEventTime(revTimestamp);
        return updateEvent;
    }

    static Stream<Arguments> parameters() {
        final Instant inTimeRevTimestamp = NOW.minusSeconds(1);
        final Instant lateRevTimestamp = NOW.minusSeconds(15);

        return Stream.of(
                Arguments.of(
                        Named.of("in-time, retryable", true),
                        Named.of("update " + inTimeRevTimestamp, buildUpdateEvent(inTimeRevTimestamp)),
                        Named.of("iteration " + 0, 0),
                        new IOException()),
                Arguments.of(
                        Named.of("in-time, retryable", true),
                        Named.of("update " + inTimeRevTimestamp, buildUpdateEvent(inTimeRevTimestamp)),
                        Named.of("iteration " + 0, 0),
                        new InvalidMWApiResponseException("")),
                Arguments.of(
                        Named.of("in-time, retryable", true),
                        Named.of("update " + inTimeRevTimestamp, buildUpdateEvent(inTimeRevTimestamp)),
                        Named.of("iteration " + 0, 0),
                        new SocketTimeoutException()),
                Arguments.of(
                        Named.of("in-time, retryable", true),
                        Named.of("update " + inTimeRevTimestamp, buildUpdateEvent(inTimeRevTimestamp)),
                        Named.of("iteration " + 0, 0),
                        new RevisionNotFoundException("")),
                Arguments.of(
                        Named.of("in-time, non-retryable", false),
                        Named.of("update " + inTimeRevTimestamp, buildUpdateEvent(inTimeRevTimestamp)),
                        Named.of("iteration " + 1, 1),
                        new IllegalStateException()),
                Arguments.of(
                        Named.of("late, non-retryable", false),
                        Named.of("update " + lateRevTimestamp, buildUpdateEvent(lateRevTimestamp)),
                        Named.of("iteration " + 2, 2),
                        new RevisionNotFoundException("")),
                Arguments.of(
                        Named.of("late > max retries, non-retryable", false),
                        Named.of("update " + lateRevTimestamp, buildUpdateEvent(lateRevTimestamp)),
                        Named.of("iteration " + 3, 3),
                        new RevisionNotFoundException("")));
    }

    private LagAwareRetryStrategy buildPredicate() {
        return new LagAwareRetryStrategy(maxAttempts, Duration.ofSeconds(10), () -> NOW);
    }

    @ParameterizedTest
    @MethodSource("parameters")
    void retryable(boolean expected, UpdateEvent updateEvent, int iteration, Exception exception) {
        LagAwareRetryStrategy predicate = buildPredicate();
        if (iteration > 0) {
            assertThat(predicate.getBackoffTimeMillis(iteration)).isPositive();
        } else {
            assertThat(predicate.getBackoffTimeMillis(iteration)).isZero();
        }

        assertThat(predicate.canRetry(iteration)).isEqualTo(iteration < maxAttempts);
        assertThat(predicate.getRetryPredicate().exceptionPredicate()).isEmpty();

        final boolean isRetryableResult =
                predicate
                        .getRetryPredicate()
                        .resultPredicate()
                        .orElseThrow()
                        .test(List.of(Either.Right(new FetchFailure<UpdateEvent>(updateEvent, exception))));

        assertThat(isRetryableResult).isEqualTo(expected);
    }
}
