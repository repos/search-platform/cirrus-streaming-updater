package org.wikimedia.discovery.cirrus.updater.common.json;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

import org.apache.commons.lang3.mutable.MutableBoolean;
import org.apache.commons.lang3.mutable.MutableInt;
import org.junit.Assert;
import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class AsyncJsonParserTest {
    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor(access = AccessLevel.PACKAGE)
    @FieldDefaults(level = AccessLevel.PRIVATE)
    private static final class Model {
        String stringValue;
        int intValue;

        Model inner;
        Model[] innerArray;

        int[] intArray;
        Double nullable;
        boolean booleanValue;
    }

    private static final ObjectMapper mapper = new ObjectMapper();

    static {
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    private final Model model =
            Model.builder()
                    .stringValue("hahaha")
                    .intValue(2)
                    .inner(Model.builder().stringValue("inner").intArray(new int[] {5, 6, 7}).build())
                    .innerArray(
                            new Model[] {
                                Model.builder().stringValue("innerArray1").build(),
                                Model.builder().stringValue("innerArray2").build()
                            })
                    .intArray(new int[] {2, 3, 4})
                    .nullable(null)
                    .booleanValue(true)
                    .build();

    @Test
    public void testRootObject() throws IOException {
        MutableBoolean parsed = new MutableBoolean(false);
        try (AsyncJsonParser parser =
                new AsyncJsonParser(
                        mapper,
                        root -> {
                            parsed.setValue(true);
                            try {
                                Assert.assertEquals(mapper.treeToValue(root, Model.class), model);
                            } catch (JsonProcessingException e) {
                                Assert.fail(e.getMessage());
                            }
                        })) {

            for (byte b : new ObjectMapper().writeValueAsBytes(model)) {
                parser.feedInput(ByteBuffer.wrap(new byte[] {b}, 0, 1));
            }

            parser.endOfInput();
        }
        Assert.assertTrue(parsed.booleanValue());
    }

    @Test
    public void testChunks() throws IOException {
        MutableBoolean parsed = new MutableBoolean(false);
        try (AsyncJsonParser parser =
                new AsyncJsonParser(
                        mapper,
                        root -> {
                            parsed.setValue(true);
                            try {
                                Assert.assertEquals(mapper.treeToValue(root, Model.class), model);
                            } catch (JsonProcessingException e) {
                                Assert.fail(e.getMessage());
                            }
                        })) {

            final int chunkSize = 20;
            byte[] bytes = mapper.writeValueAsBytes(model);
            for (int i = 0; i < bytes.length; i += chunkSize) {
                byte[] chunk = new byte[20];
                int len = Math.min(chunkSize, bytes.length - i);
                System.arraycopy(bytes, i, chunk, 0, len);
                parser.feedInput(ByteBuffer.wrap(chunk, 0, len));
            }

            parser.endOfInput();
        }
        Assert.assertTrue(parsed.booleanValue());
    }

    @Test
    public void testRootArray() throws IOException {
        MutableInt parsed = new MutableInt(0);

        try (AsyncJsonParser parser =
                new AsyncJsonParser(
                        mapper,
                        root -> {
                            parsed.increment();
                            try {
                                Model[] deserialized = mapper.treeToValue(root, Model[].class);
                                log.debug(Arrays.toString(deserialized));
                                Assert.assertEquals(deserialized[0], model);
                                Assert.assertEquals(deserialized[1], model);
                                Assert.assertEquals(deserialized[2], model);
                            } catch (JsonProcessingException e) {
                                Assert.fail(e.getMessage());
                            }
                        })) {

            byte[] bytes3 = "[".getBytes(StandardCharsets.UTF_8);
            parser.feedInput(ByteBuffer.wrap(bytes3, 0, bytes3.length));
            for (byte b : mapper.writeValueAsBytes(model)) {
                byte[] bytes = new byte[] {b};
                parser.feedInput(ByteBuffer.wrap(bytes, 0, bytes.length));
            }
            byte[] bytes2 = ",".getBytes(StandardCharsets.UTF_8);
            parser.feedInput(ByteBuffer.wrap(bytes2, 0, bytes2.length));
            for (byte b : mapper.writeValueAsBytes(model)) {
                byte[] bytes = new byte[] {b};
                parser.feedInput(ByteBuffer.wrap(bytes, 0, bytes.length));
            }
            byte[] bytes1 = ",".getBytes(StandardCharsets.UTF_8);
            parser.feedInput(ByteBuffer.wrap(bytes1, 0, bytes1.length));
            for (byte b : mapper.writeValueAsBytes(model)) {
                byte[] bytes = new byte[] {b};
                parser.feedInput(ByteBuffer.wrap(bytes, 0, bytes.length));
            }
            byte[] bytes = "]".getBytes(StandardCharsets.UTF_8);
            parser.feedInput(ByteBuffer.wrap(bytes, 0, bytes.length));

            parser.endOfInput();
        }

        Assert.assertEquals(1, parsed.intValue());
    }
}
