package org.wikimedia.discovery.cirrus.updater.common.http;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;

class TagNamingStrategyTest {

    @Test
    void tags() {
        final Map<String, String> originalTags = Map.of("a", "1", "b", "2");
        final String encodeTags = TagNamingStrategy.encodeTags(originalTags);
        final Entry<String, Stream<Entry<String, String>>> nameWithTags =
                TagNamingStrategy.decodeTags("metric-name" + encodeTags);

        assertThat(nameWithTags.getKey()).isEqualTo("metric-name");
        assertThat(nameWithTags.getValue())
                .containsExactlyInAnyOrder(Map.entry("a", "1"), Map.entry("b", "2"));
    }
}
