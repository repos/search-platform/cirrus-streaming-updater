package org.wikimedia.discovery.cirrus.updater.common.model;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.time.Instant;
import java.util.stream.Stream;

import org.apache.flink.api.common.ExecutionConfig;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.api.common.typeutils.TypeSerializer;
import org.apache.flink.core.memory.DataInputDeserializer;
import org.apache.flink.core.memory.DataOutputSerializer;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class SanityCheckTypeInfoFactoryTest {

    static Stream<Arguments> events() {
        return Stream.of(
                Arguments.of(new Object[] {null}),
                Arguments.of(
                        new SanityCheck("test.local", 0, 100, Instant.parse("2024-01-01T00:00:00Z"), 0)));
    }

    @ParameterizedTest
    @MethodSource("events")
    void roundtrip(SanityCheck sourceEvent) throws IOException {
        TypeInformation<SanityCheck> typeInfo = SanityCheckTypeInfoFactory.create();
        TypeSerializer<SanityCheck> serializer = typeInfo.createSerializer(new ExecutionConfig());
        DataOutputSerializer dos = new DataOutputSerializer(128);
        serializer.serialize(sourceEvent, dos);
        DataInputDeserializer did = new DataInputDeserializer(dos.getCopyOfBuffer());
        SanityCheck roundtrip = serializer.deserialize(did);
        assertThat(roundtrip).isEqualTo(sourceEvent);
    }
}
