package org.wikimedia.discovery.cirrus.updater.common.model;

import static com.google.common.collect.Sets.newHashSet;
import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.time.Instant;
import java.util.Collections;
import java.util.stream.Stream;

import javax.annotation.Nonnull;

import org.apache.flink.api.common.typeutils.ComparatorTestBase.TestInputView;
import org.apache.flink.api.common.typeutils.TypeSerializer;
import org.apache.flink.api.java.typeutils.runtime.TestDataOutputSerializer;
import org.apache.flink.types.Row;
import org.assertj.core.api.Assertions;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateEvent.Meta;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateEvent.TargetDocument;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateEvent.Update;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateEvent.UpdateEventBuilder;
import org.wikimedia.eventutilities.flink.EventRowTypeInfo;

import com.google.common.collect.ImmutableList;

@TestMethodOrder(MethodOrderer.MethodName.class)
class UpdateRowTranscoderTest {

    static final EventRowTypeInfo EVENT_ROW_TYPE_INFO =
            EventDataStreamUtilities.buildUpdateTypeInfo();
    final Instant now = Instant.now();
    final UpdateRowTranscoder encoder = UpdateRowTranscoder.encoder(EVENT_ROW_TYPE_INFO);
    final UpdateRowTranscoder decoder = UpdateRowTranscoder.decoder();

    static Stream<Arguments> changTypesWithoutUpdate() {
        return Stream.of(ChangeType.REV_BASED_UPDATE, ChangeType.PAGE_RERENDER, ChangeType.PAGE_DELETE)
                .map(Arguments::of);
    }

    @ParameterizedTest
    @MethodSource("changTypesWithoutUpdate")
    void withoutUpdate(ChangeType changeType) {
        final UpdateEvent updateEvent = createUpdateEvent(now, changeType);
        updateEvent.getTargetDocument().completeWith("clusterGroup", "indexName");

        Assertions.assertThat(transcode(updateEvent))
                .extracting(UpdateEvent::getChangeType)
                .isEqualTo(updateEvent.getChangeType());
    }

    @Test
    void redirectAdd() {
        final UpdateEvent updateEvent = createUpdateEvent(now, ChangeType.REDIRECT_UPDATE);
        final TargetDocument originalRedirect = new TargetDocument("domain", "wikiId", 0L, 1L);
        originalRedirect.setPageTitle("A_Redirect");
        updateEvent.getTargetDocument().completeWith("clusterGroup", "indexName");
        updateEvent.setUpdate(Update.forRedirect(newHashSet(originalRedirect), newHashSet()));

        final UpdateEvent decodedEvent = transcode(updateEvent);

        assertThat(decodedEvent.getUpdate().getRedirectRemove()).isNull();
        assertThat(decodedEvent.getUpdate().getRedirectAdd())
                .allSatisfy(
                        decodedRedirect -> {
                            assertThat(decodedRedirect.getPageNamespace())
                                    .isEqualTo(originalRedirect.getPageNamespace());
                            assertThat(decodedRedirect.getPageTitle()).isEqualTo(originalRedirect.getPageTitle());
                        });
    }

    @Test
    void redirectRemove() {
        final UpdateEvent updateEvent = createUpdateEvent(now, ChangeType.REDIRECT_UPDATE);
        final TargetDocument originalRedirect = new TargetDocument("domain", "wikiId", 0L, 1L);
        originalRedirect.setPageTitle("A_Redirect");
        updateEvent.getTargetDocument().completeWith("clusterGroup", "indexName");
        updateEvent.setUpdate(Update.forRedirect(newHashSet(), newHashSet(originalRedirect)));

        final UpdateEvent decodedEvent = transcode(updateEvent);

        assertThat(decodedEvent.getUpdate().getRedirectAdd()).isNull();
        assertThat(decodedEvent.getUpdate().getRedirectRemove())
                .allSatisfy(
                        decodedRedirect -> {
                            assertThat(decodedRedirect.getPageNamespace())
                                    .isEqualTo(originalRedirect.getPageNamespace());
                            assertThat(decodedRedirect.getPageTitle()).isEqualTo(originalRedirect.getPageTitle());
                        });
    }

    @Test
    void withUpdate() throws IOException {
        final UpdateEvent updateEvent = createUpdateEvent(now, ChangeType.REV_BASED_UPDATE);
        updateEvent.getTargetDocument().completeWith("omega", "testwiki_general");
        Row fields = EVENT_ROW_TYPE_INFO.createEmptySubRow(UpdateFields.FIELDS);
        fields.setField("text", "random text");
        int fieldsHash = fields.hashCode();

        TargetDocument redirAdded =
                TargetDocument.builder().pageNamespace(0L).pageTitle("Added Redirect").build();
        TargetDocument redirDeleted =
                TargetDocument.builder().pageNamespace(0L).pageTitle("Removed Redirect").build();
        updateEvent.setUpdate(
                UpdateEvent.Update.builder()
                        .noopHints(Collections.singletonMap("field", "handler"))
                        .rawFields(fields)
                        .rawFieldsFetchedAt(now)
                        .weightedTags(ImmutableList.of("tag/value1", "tag/value2"))
                        .redirectAdd(Collections.singletonList(redirAdded))
                        .redirectRemove(Collections.singletonList(redirDeleted))
                        .build());

        final Row row = encoder.encode(updateEvent);

        assertThat(fields.hashCode())
                .withFailMessage("encoder must not mutate source fields")
                .isEqualTo(fieldsHash);

        final Row metaRow = row.getFieldAs("meta");
        assertThat(metaRow.getField("domain")).isEqualTo("my.domain.local");

        assertThat(row.getField("dt")).isEqualTo(now);
        assertThat(row.getField("page_id")).isEqualTo(147498L);
        assertThat(row.getField("rev_id")).isEqualTo(551141L);
        assertThat(row.getField("wiki_id")).isEqualTo("mywiki_id");
        assertThat(row.getField("cirrussearch_index_name")).isEqualTo("testwiki_general");
        assertThat(row.getField("cirrussearch_cluster_group")).isEqualTo("omega");

        Row redirSetNoopField =
                row.<Row>getFieldAs(UpdateFields.NOOP_FIELDS).getFieldAs(UpdateFields.REDIRECT);
        Row[] redirAddedRows = redirSetNoopField.getFieldAs(UpdateFields.NOOP_HINTS_SET_ADD);
        Row[] redirDeletedRows = redirSetNoopField.getFieldAs(UpdateFields.NOOP_HINTS_SET_REMOVE);

        assertThat(redirAddedRows).hasSize(1);
        Row redirRow = redirAddedRows[0];
        assertThat(redirRow.<String>getFieldAs(UpdateFields.REDIRECT_TITLE))
                .isEqualTo(redirAdded.getPageTitle());
        assertThat(redirRow.<Long>getFieldAs(UpdateFields.REDIRECT_NAMESPACE))
                .isEqualTo(redirAdded.getPageNamespace());
        redirRow = redirDeletedRows[0];
        assertThat(redirRow.<String>getFieldAs(UpdateFields.REDIRECT_TITLE))
                .isEqualTo(redirDeleted.getPageTitle());
        assertThat(redirRow.<Long>getFieldAs(UpdateFields.REDIRECT_NAMESPACE))
                .isEqualTo(redirDeleted.getPageNamespace());

        final TypeSerializer<Row> rowSerializer = EVENT_ROW_TYPE_INFO.createSerializer(null);
        final TestDataOutputSerializer target = new TestDataOutputSerializer(4096);
        rowSerializer.serialize(row, target);

        assertThat(target.length()).isPositive();

        final Row deserialized = rowSerializer.deserialize(new TestInputView(target.copyByteBuffer()));

        final UpdateEvent decoded = decoder.decode(deserialized);

        assertThat(decoded).isEqualTo(updateEvent);
    }

    @Test
    void failIfTagsAlreadySet() {
        final UpdateEvent updateEvent = createUpdateEvent(now, ChangeType.TAGS_UPDATE);
        updateEvent.getTargetDocument().completeWith("clusterGroup", "indexName");

        Row fields = EVENT_ROW_TYPE_INFO.createEmptySubRow(UpdateFields.FIELDS);
        fields.setField("text", "random text");
        fields.setField(UpdateFields.WEIGHTED_TAGS, ImmutableList.of());
        updateEvent.setUpdate(
                UpdateEvent.Update.builder()
                        .rawFields(fields)
                        .noopHints(Collections.singletonMap("field", "handler"))
                        .weightedTags(ImmutableList.of("tag/value1", "tag/value2"))
                        .build());

        Assertions.assertThatExceptionOfType(RuntimeException.class)
                .isThrownBy(() -> transcode(updateEvent))
                .withCauseInstanceOf(IllegalArgumentException.class);
    }

    @Test
    void failIfTagsInWrongClassProperty() {
        final UpdateEvent updateEvent = createUpdateEvent(now, ChangeType.TAGS_UPDATE);
        updateEvent.getTargetDocument().completeWith("clusterGroup", "indexName");

        Row fields = EVENT_ROW_TYPE_INFO.createEmptySubRow(UpdateFields.FIELDS);
        fields.setField("text", "random text");
        fields.setField(UpdateFields.WEIGHTED_TAGS, ImmutableList.of());
        updateEvent.setUpdate(
                UpdateEvent.Update.builder()
                        .rawFields(fields)
                        .noopHints(Collections.singletonMap("field", "handler"))
                        .build());

        Assertions.assertThatExceptionOfType(RuntimeException.class)
                .isThrownBy(() -> transcode(updateEvent))
                .withCauseInstanceOf(IllegalArgumentException.class);
    }

    @Test
    void failIfTargetIncomplete() {
        final UpdateEvent updateEvent = createUpdateEvent(now, ChangeType.PAGE_DELETE);
        updateEvent.setUpdate(Update.forWeightedTags(ImmutableList.of("a")));

        Assertions.assertThatExceptionOfType(RuntimeException.class)
                .isThrownBy(() -> transcode(updateEvent))
                .withCauseInstanceOf(IllegalArgumentException.class);
    }

    @NotNull
    private UpdateEvent transcode(UpdateEvent updateEvent) {
        return decoder.decode(encoder.encode(updateEvent));
    }

    @Nonnull
    private static UpdateEvent createUpdateEvent(Instant now, ChangeType changeType) {
        final String domain = "my.domain.local";
        final UpdateEventBuilder builder =
                UpdateEvent.builder()
                        .eventTime(now)
                        .changeType(changeType)
                        .meta(Meta.builder().dt(now).domain(domain).build())
                        .targetDocument(new TargetDocument(domain, "mywiki_id", 0L, 147498L))
                        .revId(551141L);
        return builder.build();
    }
}
