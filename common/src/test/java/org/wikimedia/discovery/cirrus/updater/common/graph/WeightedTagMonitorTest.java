package org.wikimedia.discovery.cirrus.updater.common.graph;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.startsWith;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.List;
import java.util.stream.IntStream;

import org.apache.flink.metrics.Counter;
import org.apache.flink.metrics.MetricGroup;
import org.apache.flink.metrics.SimpleCounter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateEvent;

@ExtendWith(MockitoExtension.class)
class WeightedTagMonitorTest {
    @Mock MetricGroup metricGroup;
    @Mock MetricGroup myPrefixMetricGroup;
    @Mock MetricGroup eTooManyPrefixMetricGroup;
    Counter eTooManySetCounter = new SimpleCounter();
    Counter eTooManyClearCounter = new SimpleCounter();
    Counter myPrefixSetCounter = new SimpleCounter();
    Counter myPrefixClearCounter = new SimpleCounter();
    WeightedTagMonitor monitor;

    @BeforeEach
    void setUp() {
        monitor = new WeightedTagMonitor(metricGroup);
    }

    @Test
    void test_happy_path() {
        when(metricGroup.addGroup("tag_prefix", "my_prefix")).thenReturn(myPrefixMetricGroup);
        when(myPrefixMetricGroup.counter("set")).thenReturn(myPrefixSetCounter);
        UpdateEvent event =
                UpdateEvent.builder()
                        .update(UpdateEvent.Update.builder().weightedTags(List.of("my_prefix/tag1")).build())
                        .build();
        monitor.report(event);
        event =
                UpdateEvent.builder()
                        .update(UpdateEvent.Update.builder().weightedTags(List.of("my_prefix/tag2")).build())
                        .build();
        monitor.report(event);
        assertThat(myPrefixClearCounter.getCount()).isZero();
        assertThat(myPrefixSetCounter.getCount()).isEqualTo(2);
        verify(metricGroup, times(1)).addGroup("tag_prefix", "my_prefix");
        verify(myPrefixMetricGroup, times(1)).counter("set");
    }

    @Test
    void test_tag_deduplication() {
        when(metricGroup.addGroup("tag_prefix", "my_prefix")).thenReturn(myPrefixMetricGroup);
        when(myPrefixMetricGroup.counter("clear")).thenReturn(myPrefixClearCounter);
        when(myPrefixMetricGroup.counter("set")).thenReturn(myPrefixSetCounter);
        UpdateEvent event =
                UpdateEvent.builder()
                        .update(
                                UpdateEvent.Update.builder()
                                        .weightedTags(List.of("my_prefix/tag1", "my_prefix/tag2"))
                                        .build())
                        .build();
        monitor.report(event);
        assertThat(myPrefixClearCounter.getCount()).isZero();
        assertThat(myPrefixSetCounter.getCount()).isOne();
        verify(metricGroup, times(1)).addGroup("tag_prefix", "my_prefix");
        verify(myPrefixMetricGroup, times(1)).counter("clear");
        verify(myPrefixMetricGroup, times(1)).counter("set");
    }

    @Test
    void test_clear() {
        when(metricGroup.addGroup("tag_prefix", "my_prefix")).thenReturn(myPrefixMetricGroup);
        when(myPrefixMetricGroup.counter("clear")).thenReturn(myPrefixClearCounter);
        when(myPrefixMetricGroup.counter("set")).thenReturn(myPrefixSetCounter);
        UpdateEvent event =
                UpdateEvent.builder()
                        .update(
                                UpdateEvent.Update.builder()
                                        .weightedTags(List.of("my_prefix/__DELETE_GROUPING__"))
                                        .build())
                        .build();
        monitor.report(event);
        assertThat(myPrefixClearCounter.getCount()).isOne();
        assertThat(myPrefixSetCounter.getCount()).isZero();
        verify(metricGroup, times(1)).addGroup("tag_prefix", "my_prefix");
        verify(myPrefixMetricGroup, times(1)).counter("clear");
        verify(myPrefixMetricGroup, times(1)).counter("set");
    }

    @Test
    void test_no_tags() {
        UpdateEvent event = UpdateEvent.builder().update(UpdateEvent.Update.builder().build()).build();
        monitor.report(event);
        assertThat(myPrefixSetCounter.getCount()).isZero();
        assertThat(myPrefixClearCounter.getCount()).isZero();
    }

    @Test
    void test_broken_tags_are_ignored() {
        UpdateEvent event =
                UpdateEvent.builder()
                        .update(UpdateEvent.Update.builder().weightedTags(List.of("garbage")).build())
                        .build();
        monitor.report(event);
        verify(metricGroup, never()).addGroup(any(), any());
    }

    @Test
    void test_do_not_create_too_many_counters() {
        when(metricGroup.addGroup(eq("tag_prefix"), startsWith("my_prefix_")))
                .thenReturn(myPrefixMetricGroup);
        when(metricGroup.addGroup("tag_prefix", "e_too_many")).thenReturn(eTooManyPrefixMetricGroup);
        when(eTooManyPrefixMetricGroup.counter("set")).thenReturn(eTooManySetCounter);
        when(eTooManyPrefixMetricGroup.counter("clear")).thenReturn(eTooManyClearCounter);
        when(myPrefixMetricGroup.counter("set")).thenReturn(myPrefixSetCounter);
        when(myPrefixMetricGroup.counter("clear")).thenReturn(myPrefixClearCounter);
        IntStream.range(0, 205)
                .mapToObj(
                        cnt ->
                                UpdateEvent.builder()
                                        .targetDocument(UpdateEvent.TargetDocument.builder().wikiId("my_wiki").build())
                                        .meta(UpdateEvent.Meta.builder().requestId("my_request_id").build())
                                        .update(
                                                UpdateEvent.Update.builder()
                                                        .weightedTags(
                                                                List.of(
                                                                        "my_prefix_" + cnt + "/tag1",
                                                                        "my_prefix_" + cnt + "/__DELETE_GROUPING__"))
                                                        .build())
                                        .build())
                .forEach(monitor::report);
        assertThat(myPrefixSetCounter.getCount()).isEqualTo(200);
        assertThat(eTooManySetCounter.getCount()).isEqualTo(5);
        assertThat(myPrefixClearCounter.getCount()).isEqualTo(200);
        assertThat(eTooManyClearCounter.getCount()).isEqualTo(5);
        verify(metricGroup, times(200)).addGroup(eq("tag_prefix"), startsWith("my_prefix_"));
        verify(metricGroup, times(1)).addGroup("tag_prefix", "e_too_many");
        verify(myPrefixMetricGroup, times(200)).counter("set");
        verify(myPrefixMetricGroup, times(200)).counter("clear");
    }
}
