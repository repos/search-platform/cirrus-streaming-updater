package org.wikimedia.discovery.cirrus.updater.common.model;

import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import org.apache.commons.io.IOUtils;
import org.apache.flink.api.common.ExecutionConfig;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.api.common.typeutils.TypeSerializer;
import org.apache.flink.api.common.typeutils.TypeSerializerSnapshot;
import org.apache.flink.api.java.typeutils.PojoTypeInfo;
import org.apache.flink.api.java.typeutils.runtime.TestDataOutputSerializer;
import org.apache.flink.core.memory.DataInputDeserializer;
import org.apache.flink.types.Row;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.wikimedia.discovery.cirrus.updater.common.config.EventStreamConstants;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateEvent.Meta;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateEvent.TargetDocument;
import org.wikimedia.eventutilities.flink.EventRowTypeInfo;

import com.google.common.base.VerifyException;

import lombok.extern.slf4j.Slf4j;

/**
 * Test the behavior of the flink Pojo serializer, since we don't have much experience with add
 * couple assertions to make sure it works as we expect (better safe than sorry).
 */
@Slf4j
class UpdateEventTest {
    static final EventRowTypeInfo UPDATE_EVENT_ROW_TYPEINFO_1_0_0 =
            EventDataStreamUtilities.buildTypeInfo(
                    EventStreamConstants.UPDATER_PUBLIC_OUTPUT_STREAM_NAME, "1.0.0");
    static final EventRowTypeInfo UPDATE_EVENT_ROW_TYPEINFO_LATEST =
            EventDataStreamUtilities.buildTypeInfo(
                    EventStreamConstants.UPDATER_PUBLIC_OUTPUT_STREAM_NAME,
                    EventStreamConstants.UPDATER_OUTPUT_SCHEMA_VERSION);

    static final TypeInformation<UpdateEvent> LATEST_TYPE_INFO =
            UpdateEventTypeInfo.create(UPDATE_EVENT_ROW_TYPEINFO_LATEST);
    private static final String V1_DTO = "UpdateEvent.v1.bytes";
    private static final String V2_DTO = "UpdateEvent.v2.bytes";
    private static final String V3_DTO = "UpdateEvent.v3.bytes";
    private static final String V4_DTO = "UpdateEvent.v4.bytes";
    private static final String V5_DTO = "UpdateEvent.v5.bytes";
    private static final String V6_DTO = "UpdateEvent.v6.bytes";
    private static final String V7_DTO = "UpdateEvent.v7.bytes";
    private static final String V8_DTO = "UpdateEvent.v8.bytes";
    private static final String LATEST_DTO = V8_DTO;

    private static final String LATEST_SNAPSHOT = "UpdateEvent.v8.snapshot.bytes";

    // 23.10.2023 10:00:00.000
    private static final Instant NOW = Instant.ofEpochMilli(1698048000000L);

    @Test
    void test_key_selector() throws Exception {
        UpdateEvent e = new UpdateEvent();
        e.setTargetDocument(new TargetDocument("dom", "mywiki", 0L, 123L));
        PageKey key = UpdateEvent.KEY_SELECTOR.getKey(e);
        assertThat(key.getWiki()).isEqualTo("mywiki");
        assertThat(key.getPageId()).isEqualTo(123L);
    }

    @Test
    void test_is_flink_pojo() {
        assertThat(LATEST_TYPE_INFO).isInstanceOf(PojoTypeInfo.class);
    }

    @Test
    void test_is_current() throws IOException {
        // This test guarantees that whenever we change the shape of UpdateEvent we make sure to re-run
        // the main method here.
        TypeSerializerSnapshot<Object> snapshot =
                deserializeSnapshotConfig(readResource(LATEST_SNAPSHOT));
        TypeSerializerSnapshot<UpdateEvent> actualCurrent =
                LATEST_TYPE_INFO.createSerializer(new ExecutionConfig()).snapshotConfiguration();
        assertThat(snapshot.restoreSerializer())
                .withFailMessage(
                        "The shape of UpdateEvent might have changed please update the test resources.")
                .isEqualTo(actualCurrent.restoreSerializer());
    }

    @ParameterizedTest
    @MethodSource("versions")
    void can_be_deserialized(String resource, UpdateEvent expected) throws IOException {
        final UpdateEvent deserialized = deserialize(readResource(resource));
        assertThat(deserialized).isEqualTo(expected);
    }

    @Test
    void has_fetched_fields() {
        UpdateEvent e = new UpdateEvent();
        assertThat(e.hasFetchedFields()).isFalse();

        e.setUpdate(UpdateEvent.Update.builder().build());
        assertThat(e.hasFetchedFields()).isFalse();

        e.setUpdate(UpdateEvent.Update.builder().rawFieldsFetchedAt(NOW).build());
        assertThat(e.hasFetchedFields()).isTrue();
    }

    @Test
    void unprefixed_title_text_form() {
        assertThat(
                        TargetDocument.builder()
                                .pageNamespace(0L)
                                .pageTitle("Example_Page")
                                .build()
                                .getUnprefixedPageTitleTextForm())
                .isEqualTo("Example Page");

        assertThat(
                        TargetDocument.builder()
                                .pageNamespace(0L)
                                .pageTitle("Not_A_Namespace:Example")
                                .build()
                                .getUnprefixedPageTitleTextForm())
                .isEqualTo("Not A Namespace:Example");

        assertThat(
                        TargetDocument.builder()
                                .pageNamespace(1L)
                                .pageTitle("Talk:Example_Page")
                                .build()
                                .getUnprefixedPageTitleTextForm())
                .isEqualTo("Example Page");

        assertThat(
                        TargetDocument.builder()
                                .pageNamespace(1L)
                                .pageTitle("Talk:Not_A_Namespace:Example")
                                .build()
                                .getUnprefixedPageTitleTextForm())
                .isEqualTo("Not A Namespace:Example");
        assertThatExceptionOfType(VerifyException.class)
                .isThrownBy(
                        () ->
                                TargetDocument.builder()
                                        .pageNamespace(1L)
                                        .pageTitle("Example_Missing_Prefix")
                                        .build()
                                        .getUnprefixedPageTitleTextForm());
    }

    private byte[] readResource(String name) throws IOException {
        return IOUtils.toByteArray(
                getClass().getResourceAsStream(getClass().getSimpleName() + "/" + name));
    }

    static Stream<Arguments> versions() {
        return Stream.of(
                Arguments.of(V1_DTO, v1()),
                Arguments.of(V2_DTO, v2()),
                Arguments.of(V3_DTO, v3()),
                Arguments.of(V4_DTO, v4()),
                Arguments.of(V5_DTO, v5()),
                Arguments.of(V6_DTO, v6()),
                Arguments.of(V7_DTO, v7()),
                Arguments.of(V8_DTO, v8()));
    }

    static UpdateEvent v1() {
        // Do not change this method
        UpdateEvent e = new UpdateEvent();
        e.setMeta(
                Meta.builder().stream("my_stream")
                        .requestId("85e66cc8-0d5c-4bcd-87aa-37c5c818314d")
                        .build());
        e.setRevId(123L);
        e.setChangeType(ChangeType.PAGE_RERENDER);
        e.setIngestionTime(Instant.parse("2022-11-18T16:53:23Z"));
        e.setEventTime(Instant.parse("2022-11-18T16:52:02Z"));
        e.setTargetDocument(new UpdateEvent.TargetDocument("my.domain.local", "my_database", 2L, 123L));
        e.getTargetDocument().setPageTitle("my page title");
        Map<String, String> hints = new HashMap<>();
        hints.put("version", "documentVersion");
        List<String> tags = singletonList("mytag/value|10");
        Row fields = UPDATE_EVENT_ROW_TYPEINFO_1_0_0.createEmptySubRow(UpdateFields.FIELDS);
        fields.setField("text", "some text");
        e.setUpdate(
                UpdateEvent.Update.builder().noopHints(hints).rawFields(fields).weightedTags(tags).build());
        return e;
    }

    static UpdateEvent v2() {
        final UpdateEvent e = v1().toBuilder().build();
        e.getUpdate().setRawFieldsFetchedAt(NOW);
        return e;
    }

    static UpdateEvent v3() {
        final UpdateEvent e = v2().toBuilder().build();
        e.setFetchAttempt(2);
        return e;
    }

    static UpdateEvent v4() {
        final UpdateEvent e = v3().toBuilder().build();
        e.setFetchAttempt(3);
        return e;
    }

    static UpdateEvent v5() {
        final UpdateEvent e = v4().toBuilder().build();
        e.getUpdate().getRawFields().setField("lexical_category", "Q24905");
        return e;
    }

    static UpdateEvent v6() {
        final UpdateEvent e = v5().toBuilder().build();
        e.setMovedFrom(new UpdateEvent.TargetDocument("my.domain.local", "my_database", 3L, 123L));
        return e;
    }

    static UpdateEvent v7() {
        final UpdateEvent e = v6().toBuilder().build();
        e.setBypassDeduplication(true);
        return e;
    }

    static UpdateEvent v8() {
        final UpdateEvent e = v7().toBuilder().build();
        e.setFromPrivateStream(Boolean.FALSE);
        return e;
    }

    static TypeSerializerSnapshot<Object> deserializeSnapshotConfig(byte[] bytes) throws IOException {
        DataInputDeserializer deser = new DataInputDeserializer(bytes);
        return TypeSerializerSnapshot.readVersionedSnapshot(deser, UpdateEvent.class.getClassLoader());
    }

    static byte[] serializeCurrentSnapshot() throws IOException {
        TypeInformation<UpdateEvent> typeInfo =
                UpdateEventTypeInfo.create(UPDATE_EVENT_ROW_TYPEINFO_LATEST);
        TestDataOutputSerializer ser = new TestDataOutputSerializer(4096);
        TypeSerializer<UpdateEvent> typeser = typeInfo.createSerializer(new ExecutionConfig());

        TypeSerializerSnapshot.writeVersionedSnapshot(ser, typeser.snapshotConfiguration());
        return ser.copyByteBuffer();
    }

    static UpdateEvent deserialize(byte[] bytes) throws IOException {
        DataInputDeserializer deser = new DataInputDeserializer(bytes);
        final UpdateEvent deserialized =
                (UpdateEvent)
                        TypeSerializerSnapshot.readVersionedSnapshot(deser, UpdateEvent.class.getClassLoader())
                                .restoreSerializer()
                                .deserialize(deser);
        assertThat(deser.available()).isZero();
        return deserialized;
    }

    static byte[] serialize(UpdateEvent event) throws IOException {
        TypeInformation<UpdateEvent> typeInfo =
                UpdateEventTypeInfo.create(UPDATE_EVENT_ROW_TYPEINFO_LATEST);
        TestDataOutputSerializer ser = new TestDataOutputSerializer(4096);
        TypeSerializer<UpdateEvent> typeSerializer = typeInfo.createSerializer(new ExecutionConfig());

        TypeSerializerSnapshot.writeVersionedSnapshot(ser, typeSerializer.snapshotConfiguration());
        typeSerializer.serialize(event, ser);
        return ser.copyByteBuffer();
    }

    public static void main(String[] args) throws IOException {
        // Generates two payloads:
        // - UpdateEvent.vX.bytes that contains the current snapshot config (pojo serialization data)
        // - UpdateEvent.current.snapshot.bytes the current snapshot config
        //
        // When UpdateEvent changes we make sure that it's backward compatible by
        // - a new Vx_DTO constant refering to new serialized bytes
        // - a new vX() method that generates an UpdateEvent in version X
        // - update this::versions to include the new Vx_DTO and vX references
        // - UpdateEvent.current.snapshot.bytes is updated to ensure that test_is_current passes
        final String directory = System.getProperty("java.io.tmpdir");
        Files.write(Paths.get(directory, LATEST_DTO), serialize(v8()));
        Files.write(Paths.get(directory, LATEST_SNAPSHOT), serializeCurrentSnapshot());

        log.info("Wrote to " + directory);
    }
}
