package org.wikimedia.discovery.cirrus.updater.common.graph;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.List;
import java.util.stream.Stream;

import org.apache.flink.api.common.functions.RuntimeContext;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.metrics.MetricGroup;
import org.apache.flink.streaming.api.functions.async.ResultFuture;
import org.apache.flink.types.Either;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.wikimedia.discovery.cirrus.updater.common.model.ChangeType;
import org.wikimedia.discovery.cirrus.updater.common.model.FetchFailure;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateEvent;

@ExtendWith(MockitoExtension.class)
class BypassingCirrusDocFetcherTest {

    static final String TARGET_DOCUMENT_DOMAIN = "www.example.com";

    @Mock CirrusFetcher<UpdateEvent, UpdateEvent> delegate;

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    UpdateEvent updateEvent;

    @InjectMocks BypassingCirrusDocFetcher bypassingRevisionFetcher;

    @Mock Configuration parameters;

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    RuntimeContext runtimeContext;

    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    MetricGroup retryMetricGroup;

    @BeforeEach
    void initialize() throws Exception {
        when(runtimeContext.getMetricGroup().addGroup("retry")).thenReturn(retryMetricGroup);

        bypassingRevisionFetcher.open(parameters);
        bypassingRevisionFetcher.setRuntimeContext(runtimeContext);
    }

    static Stream<Arguments> provideCases() {
        return Stream.of(
                Arguments.of(ChangeType.TAGS_UPDATE, -1),
                Arguments.of(ChangeType.REDIRECT_UPDATE, -1),
                Arguments.of(ChangeType.TAGS_UPDATE, -1),
                Arguments.of(ChangeType.PAGE_DELETE, -1),
                Arguments.of(ChangeType.PAGE_RERENDER, null),
                Arguments.of(ChangeType.PAGE_RERENDER, 1),
                Arguments.of(ChangeType.REV_BASED_UPDATE, 0));
    }

    @ParameterizedTest
    @MethodSource("provideCases")
    void test(ChangeType type, Integer fetchAttempt) {
        when(updateEvent.getChangeType()).thenReturn(type);
        final ResultFuture<Either<UpdateEvent, FetchFailure<UpdateEvent>>> resultFuture =
                mock(ResultFuture.class);

        if (fetchAttempt == null || fetchAttempt >= 0) {
            when(updateEvent.getTargetDocument().getDomain()).thenReturn(TARGET_DOCUMENT_DOMAIN);
            when(updateEvent.getFetchAttempt()).thenReturn(fetchAttempt);
        }

        bypassingRevisionFetcher.asyncInvoke(updateEvent, resultFuture);

        if (fetchAttempt == null || fetchAttempt >= 0) {
            verify(delegate).asyncInvoke(eq(updateEvent), any(ResultFuture.class));
            if (fetchAttempt != null) {
                verify(retryMetricGroup).addGroup("authority", TARGET_DOCUMENT_DOMAIN);
                if (fetchAttempt == 0) {
                    verify(retryMetricGroup.addGroup("authority", TARGET_DOCUMENT_DOMAIN)).counter("retried");
                    verify(retryMetricGroup.addGroup("authority", TARGET_DOCUMENT_DOMAIN).counter("retried"))
                            .inc();
                } else {
                    verify(retryMetricGroup.addGroup("authority", TARGET_DOCUMENT_DOMAIN), never())
                            .counter("retried");
                    verify(retryMetricGroup.addGroup("authority", TARGET_DOCUMENT_DOMAIN))
                            .counter("attempts");
                    verify(retryMetricGroup.addGroup("authority", TARGET_DOCUMENT_DOMAIN).counter("attempts"))
                            .inc();
                }
            } else {
                verify(retryMetricGroup, never()).addGroup("authority", TARGET_DOCUMENT_DOMAIN);
            }
        } else {
            verify(delegate, never()).asyncInvoke(eq(updateEvent), any(ResultFuture.class));
            verify(resultFuture).complete(eq(List.of(Either.Left(updateEvent))));
        }
    }

    @Test
    void close() throws IOException {
        bypassingRevisionFetcher.close();
        verify(delegate).close();
    }
}
