"""
Collect production inputs and outputs to cirrus-streaming-producer

An occurance while debugging is to find an event that was emitted from the
producer that doesn't match what we expected to happen. This script helps by
querying the event records to attempt to find all the input events that
resulted in the output event. It additionally can write out a .json file that
feeds into DeduplicateAndMergeTest.debugFailureInvestigationEvents and will
attempt to create a reproduction of the failure.

Example usage:

    spark3-submit \
        --master yarn \
        collect_events.py \
        --uri https://www.wikidata.org/wiki/Q57021011 \
        2024-02-01 \
        2024-02-04 \

"""
from argparse import ArgumentParser, ArgumentTypeError
from concurrent.futures import ThreadPoolExecutor
from contextlib import redirect_stdout
from datetime import datetime, timedelta
from functools import reduce, wraps
import json
from pathlib import Path
from pprint import pformat, pprint
from pyspark.sql import functions as F, Column, Row, SparkSession
import sys
from typing import Mapping, Optional, Sequence
import urllib.parse


SUP_UPDATE_EVENTS = 'event.cirrussearch_update_pipeline_update_rc0'
SOURCE_TABLES = [
    SUP_UPDATE_EVENTS,
    'event.cirrussearch_update_pipeline_fetch_error_rc0',
    'event.cirrussearch_update_pipeline_fetch_error_v1',
    'event.mediawiki_page_change_v1',
    'event.mediawiki_cirrussearch_page_rerender_v1',
    'event.mediawiki_page_outlink_topic_prediction_change_v1',
    'event.mediawiki_revision_score_drafttopic',
    'event.mediawiki_revision_recommendation_create',
]


def datetime_arg(s):
    try:
        return datetime.fromisoformat(s)
    except ValueError:
        raise ArgumentTypeError("Invalid datetime format. Use iso8601")


def arg_parser() -> ArgumentParser:
    parser = ArgumentParser()
    parser.add_argument('start', type=datetime_arg,
                        help='iso8601 timestamp to start event collection. Ex: 2024-02-04T11:00:00')
    parser.add_argument('end', type=datetime_arg,
                        help='iso8601 timestamp to end event collection')
    parser.add_argument('--uri',
                        help='URI of page to investigate. Must be URI encoded. Ex: https://fr.wikipedia.org/wiki/Wikip%C3%A9dia')
    parser.add_argument('--extra-tables', nargs='+', default=[],
                        help='Additional event tables to query')
    parser.add_argument('--extra-conditions-path', type=Path,
                        help='Path to json file containing additional conditions to look for. Either this or uri must be provided.')
    parser.add_argument('--output-report-path', type=Path,
                        help='Path to write markdown-ish report to. If not provided it will be printed to stdout.')
    parser.add_argument('--output-fixture-path', type=Path,
                        help='Path to write fixture for further investigation via SUP test suite')
    return parser


def hourly_partition_dt() -> Column:
    """Assemble year/month/day/hour columns into spark timestamp"""
    partition_date = F.concat_ws(
        '-',
        F.col('year'),
        F.lpad(F.col('month'), 2, '0'),
        F.lpad(F.col('day'), 2, '0'))
    partition_dt = F.concat(
        partition_date,
        F.lit(' '),
        F.lpad(F.col('hour'), 2, '0'),
        F.lit(':00:00Z'))
    return F.from_utc_timestamp(partition_dt, 'UTC')


def make_dt_condition(start_time: datetime, end_time: datetime) -> Column:
    # First pass allows filtering of the partitions, so we read only the necessary partitions
    partition_dt = hourly_partition_dt()
    partition_start = start_time.replace(minute=0, second=0, microsecond=0)
    partition_end = end_time.replace(minute=0, second=0, microsecond=0) + timedelta(hours=1)
    partition_cond = (partition_dt >= F.lit(partition_start)) & (partition_dt < F.lit(partition_end))
    # Second pass narrows down to the requested range
    row_dt = F.to_timestamp(F.col('meta.dt'))
    row_cond = (row_dt >= F.lit(start_time)) & (row_dt < F.lit(end_time))

    return partition_cond & row_cond


def listify(fn):
    """Calls list() on return value to resolve generator

    General idea is to make it easy to define a series of
    functions that return 0-n values through yield, combining
    down the call stack, without the awkwardness of single-use
    generators (as perf here is irrelevant).
    """
    @wraps(fn)
    def wrapper(*args, **kwargs):
        return list(fn(*args, **kwargs))
    return wrapper


@listify
def initial_conditions(uri: Optional[str], json_path: Optional[Path]) -> Sequence[dict]:
    if uri is not None:
        yield {'uri': uri}
    if json_path is not None:
        with json_path.open('rt') as f:
            extra = json.load(f)
            if not isinstance(extra, list):
                raise TypeError(f'Extra conditions at {json_path} must be a list of dicts')
            yield from extra


@listify
def namespace_condition(schema, cond_def):
    if 'namespace_id' not in schema.fieldNames():
        return

    yield (
        (F.col('meta.domain') == cond_def['domain']) &
        (F.col('namespace_id') == cond_def['namespace_id']) &
        (F.col('meta.uri').endswith(urllib.parse.quote(cond_def['title']))))


@listify
def page_id_condition(schema, cond_def: dict) -> Sequence[dict]:
    if 'page_id' in schema.fieldNames():
        page_id_col = 'page_id'
    elif 'page' in schema.fieldNames() and 'page_id' in schema['page'].dataType.fieldNames():
        page_id_col = 'page.page_id'
    else:
        return
    yield (
        (F.col('meta.domain') == cond_def['domain']) &
        (F.col(page_id_col) == cond_def['page_id']))


@listify
def make_condition(schema, cond_def: dict) -> Sequence[dict]:
    if 'request_id' in cond_def:
        yield F.col('meta.request_id') == cond_def['request_id']
    elif 'uri' in cond_def:
        yield F.col('meta.uri') == cond_def['uri']
    elif 'page_id' in cond_def:
        yield from page_id_condition(schema, cond_def)
    elif 'namespace_id' in cond_def:
        yield from namespace_condition(schema, cond_def)
    else:
        raise NotImplementedError(f'Unknown condition: {pformat(cond_def)}')


def make_conditions(schema, definitions: Sequence[dict]) -> Column:
    conditions = []
    for cond_def in definitions:
        conditions.extend(make_condition(schema, cond_def))
    if not conditions:
        raise ValueError('No conditions generated')
    return reduce(lambda a, b: a | b, conditions)


@listify
def conditions_from_event(event: dict) -> Sequence[dict]:
    # Recieves an event from the SUP output topic and
    # reports conditions we should look for in input streams
    yield {
        'request_id': event['meta']['request_id'],
    }
    if 'page_id' in event:
        yield {
            'domain': event['meta']['domain'],
            'page_id': event['page_id'],
        }


@listify
def conditions_from_events(events: Sequence[dict]):
    for event in events:
        yield from conditions_from_event(event)


def query_table(spark, table, start, end, conditions):
    df = spark.read.table(table)
    return (
        df
        .where(make_dt_condition(start, end))
        .where(make_conditions(df.schema, conditions))
        .collect()
    )


def recursive_as_dict(value, prune_none: bool = True):
    if isinstance(value, dict):
        return {k: recursive_as_dict(v, prune_none) for k, v in value.items() if prune_none is False or v is not None}
    if isinstance(value, list):
        return [recursive_as_dict(v, prune_none) for v in value if prune_none is False or v is not None]
    if isinstance(value, Row):
        result = recursive_as_dict(value.asDict(), prune_none)
        # Move the 'meta' field to the front if it exists, to make reading easier
        if 'meta' in result:
            meta = result.pop('meta')
            result = {'meta': meta, **result}
        return result
    return value


def print_report(source_events: Sequence[dict], conditions: Sequence[dict], augmented_events: Mapping[str, Sequence[dict]]):
    print('=== Source Events ===\n')
    for event in source_events:
        pprint(event)
    if not source_events:
        print('*** No Events Found ***')

    print('\n=== Final Investigation Conditions ===')
    pprint(conditions)

    print('\n=== Related Events ===')
    for table, events in augmented_events.items():
        print(f'\n==== {table} ====')
        if events:
            for event in events:
                pprint(recursive_as_dict(event))
        else:
            print('  No events')


def write_junit_fixture(destination: Path, events_by_table: dict):
    with destination.open('wt') as f:
        # Write out one event per line to ease manual tweaking of the result when
        # feeding it into the test case
        f.write('[')
        prefix = '\n  '
        for events in events_by_table.values():
            for event in events:
                f.write(prefix)
                json.dump(recursive_as_dict(event, False), f)
                prefix = ',\n  '
        f.write('\n]')


def main(
    start: datetime,
    end: datetime,
    uri: Optional[str],
    extra_tables: Sequence[str],
    extra_conditions_path: Optional[Path],
    output_report_path: Optional[Path],
    output_fixture_path: Optional[Path],
) -> int:
    spark = SparkSession.builder.getOrCreate()

    # Apply provided conditions to the output table
    conditions = initial_conditions(uri, extra_conditions_path)
    source_events = query_table(spark, SUP_UPDATE_EVENTS, start, end, conditions)
    if source_events:
        # Use the found events to expand our conditions and query all the tables
        # Future work could perhaps assemble plausible windows from the events
        augmented_conditions = conditions + conditions_from_events(source_events)
        all_tables = SOURCE_TABLES + extra_tables
        with ThreadPoolExecutor(max_workers=len(all_tables)) as pool:
            mapper = pool.map(
                lambda table: query_table(spark, table, start, end, augmented_conditions),
                all_tables)
            augmented_events = dict(zip(all_tables, mapper))
    else:
        augmented_conditions = conditions
        augmented_events = {}

    # Output a report about the events we found
    if output_report_path is None:
        print_report(source_events, augmented_conditions, augmented_events)
    else:
        with output_report_path.open('wt') as f, redirect_stdout(f):
            print_report(source_events, augmented_conditions, augmented_events)
        with output_report_path.open('rt') as f:
            print(f.read())

    # Also output a json file that can be loaded into the SUP test suite
    if source_events and output_fixture_path is not None:
        write_junit_fixture(output_fixture_path, augmented_events)

    return 0 if source_events else 1


if __name__ == "__main__":
    args = arg_parser().parse_args()
    sys.exit(main(**dict(vars(args))))
