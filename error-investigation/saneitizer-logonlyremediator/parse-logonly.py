# Cleans up raw logs from LogOnlyRemediator on mwlog instance into simple json lines
#
# Example usage (mwlog2002.codfw.wmnet):
#   $ grep LogOnlyRemediator /srv/mw-log/CirrusSearch.log | python3 parse-logonly.py
import json
import sys

for line in sys.stdin:
    pieces = line.strip().split(' ')
    context = json.loads(' '.join(pieces[9:]))
    context['dt'] = ' '.join(pieces[0:2])
    context['wiki'] = pieces[4]
    context['error'] = pieces[8]
    context['title'] = context['title']['MediaWiki\\Title\\Title']
    print(json.dumps(context))
