# Reads lines from parse-logonly.py on stdin and filters them based
# on a couple principals:
#
# 1) The error must exist only on cloudelastic. If the error also exists on eqiad
#    assume the error is unrelated to SUP.
# 2) The error must still exist on cloudelastic. If the error fixed itself that
#    suggests it was a timing or data collection issue. Saneitizer is not writing
#    to cloudelastic, so any fix must have come from SUP.
#
# Example usage (mwlog2002.codfw.wmnet):
#   $ grep LogOnlyRemediator /srv/mw-log/CirrusSearch.log | python3 parse-logonly.py | python3 reverify.py
from collections import Counter
from enum import Enum
import json
import multiprocessing
from pprint import pformat
import requests
import sys

CLOUDELASTIC_URL = 'https://cloudelastic.wikimedia.org'
EQIAD_URL = 'https://search.svc.eqiad.wmnet'

WIKIS = {
    'commonswiki': {
        'api': 'https://commons.wikimedia.org/w/api.php',
        'port': 9243,
        'index_suffixes': ['content', 'general', 'file']
    },
    'frwiki': {
        'api': 'https://fr.wikipedia.org/w/api.php',
        'port': 9243,
        'index_suffixes': ['content', 'general'],
    },
    'itwiki': {
        'api': 'https://it.wikipedia.org/w/api.php',
        'port': 9243,
        'index_suffixes': ['content', 'general'],
    },
    'mediawikiwiki': {
        'api': 'https://mediawiki.org/w/api.php',
        'port': 9243,
        'index_suffixes': ['content', 'general'],
    },
    'testwiki': {
        'api': 'https://test.wikipedia.org/w/api.php',
        'port': 9443,
        'index_suffixes': ['content', 'general'],
    },
    'wikidatawiki': {
        'api': 'https://wikidata.org/w/api.php',
        'port': 9243,
        'index_suffixes': ['content', 'general'],
    },
}


class BrokenPageError(Exception):
    pass


class Status(Enum):
    fixed = 1
    broken_everywhere = 2
    broken_on_cloudelastic = 3
    not_implemented = 4


def main(num_processes=12):
    pool = multiprocessing.Pool(processes=num_processes)

    counts = Counter()
    for line, status in pool.imap_unordered(process_line, sys.stdin):
        counts[status] += 1
        if status == Status.broken_on_cloudelastic:
            print(line, end='')
    print('', flush=True)
    for status, count in counts.items():
        print(f'{status}: {count}', file=sys.stderr)


def process_line(line):
    error = json.loads(line)
    try:
        if error['error'] == 'LogOnlyRemediator::oldVersionInIndex':
            is_fixed = old_version(error)
        elif error['error'] == 'LogOnlyRemediator::ghostPageInIndex':
            is_fixed = ghost_page(error)
        elif error['error'] == 'LogOnlyRemediator::pageNotInIndex':
            is_fixed = missing_page(error)
        elif error['error'] == 'LogOnlyRemediator::redirectInIndex':
            is_fixed = redirect_in_index(error)
        elif error['error'] == 'LogOnlyRemediator::pageInWrongIndex':
            is_fixed = wrong_index(error)
        else:
            # We could use Status.not_implemented, but we want these to be louder
            raise NotImplementedError(error['error'])
    except BrokenPageError:
        # The page can't be rendered
        return line, Status.broken_everywhere
    return line, is_fixed


def cirrusbuilddoc(wiki, page_id):
    # https://commons.wikimedia.org/w/api.php?action=query&format=json&prop=cirrusbuilddoc&pageids=54570432&formatversion=2
    res = requests.get(WIKIS[wiki]['api'], params={
        'action': 'query',
        'prop': 'cirrusbuilddoc|info',
        'pageids': str(page_id),
        'format': 'json',
        'formatversion': '2',
    })
    res.raise_for_status()
    try:
        page = res.json()['query']['pages'][0]
    except KeyError:
        # Probably some problem with either the wikitext parser, or html post-processing
        raise BrokenPageError(f'Failed requesting page {page_id} from {wiki}')
    if 'missing' in page:
        return None
    if 'cirrusbuilddoc' not in page:
        # While all occurances here are probably BrokenPageError, only throwing
        # for the ones we know about to learn about other cases.
        if page['lastrevid'] == 0:
            raise BrokenPageError(f'Page is broken, cant be rendered')
    return page


def old_version(error):
    doc_id = error['doc']
    wiki = error['wiki']
    port = WIKIS[wiki]['port']

    doc = cirrusbuilddoc(wiki, doc_id)
    if doc is None:
        # Could also be something like the page has since been deleted, but not important enough
        # to try and figure that out.
        return Status.broken_everywhere
    index = doc['cirrusbuilddoc_metadata']['index_name']

    current_rev_id = doc['cirrusbuilddoc']['version']
    eqiad_doc, cloudelastic_doc = all_elastic_docs(port, index, doc_id)

    if cloudelastic_doc is None:
        # Different problem than expected, but a problem none the less
        print('expected old version, found nothing', file=sys.stderr)
        return Status.broken_on_cloudelastic
    cloudelastic_rev_id = cloudelastic_doc['version']
    eqiad_rev_id = None if eqiad_doc is None else eqiad_doc['version']

    if cloudelastic_rev_id == current_rev_id:
        return Status.fixed

    if cloudelastic_rev_id == eqiad_rev_id:
        return Status.broken_everywhere

    return Status.broken_on_cloudelastic


def ghost_page(error):
    doc_id = error['doc']
    wiki = error['wiki']
    port = WIKIS[wiki]['port']

    doc = cirrusbuilddoc(wiki, doc_id)

    expect_index = None
    if doc is not None:
        expect_index = doc['cirrusbuilddoc_metadata']['index_name']

    status = Status.fixed
    for suffix in WIKIS[wiki]['index_suffixes']:
        index_name = f"{wiki}_{suffix}"
        should_exist = expect_index == index_name

        url_path = f"{index_name}/_doc/{doc_id}"
        res = requests.head(f"{CLOUDELASTIC_URL}:{port}/{url_path}")
        doc_exists = res.status_code == 200
        if doc_exists == should_exist:
            continue

        # Check if the same error occurs in eqiad
        res = requests.head(f"{EQIAD_URL}:{port}/{url_path}")
        doc_exists_on_eqiad = res.status_code == 200

        if doc_exists == doc_exists_on_eqiad:
            # problem in both sides, we aren't (currently) concerened with these
            status = Status.broken_everywhere
            continue
        # Almost certainly a problem
        return Status.broken_on_cloudelastic

    return status


def missing_page(error):
    doc_id = error['page']
    wiki = error['wiki']
    port = WIKIS[wiki]['port']

    doc = cirrusbuilddoc(wiki, doc_id)
    if doc is None:
        return Status.broken_everywhere
    try:
        index = doc['cirrusbuilddoc_metadata']['index_name']
    except KeyError:
        raise KeyError(f"[{wiki}][{doc_id}]: {pformat(doc)}")

    eqiad_doc, cloudelastic_doc = all_elastic_docs(port, index, doc_id)
    eqiad_exists = eqiad_doc is not None
    cloudelastic_exists = cloudelastic_doc is not None

    if cloudelastic_exists:
        # Problem fixed itself since saneitizer complained
        print(f'fixed itself. {index}/{doc_id} exists on cloudelastic.', file=sys.stderr)
        return Status.fixed

    if eqiad_exists == cloudelastic_exists:
        # problem (or fixed) in both sides, we aren't (currently) concerened with these
        print('problem in eqiad and cloudelastic', file=sys.stderr)
        return Status.broken_everywhere

    return Status.broken_on_cloudelastic


def redirect_in_index(error):
    # This page is a redirect and should not directly exist in the index
    doc_id = error['page']
    wiki = error['wiki']
    port = WIKIS[wiki]['port']

    doc = cirrusbuilddoc(wiki, doc_id)
    if doc is None:
        return Status.broken_everywhere
    index = doc['cirrusbuilddoc_metadata']['index_name']
    eqiad_doc, cloudelastic_doc = all_elastic_docs(port, index, doc_id)
    eqiad_exists = eqiad_doc is not None
    cloudelastic_exists = cloudelastic_doc is not None

    if eqiad_exists and cloudelastic_exists:
        return Status.broken_everywhere

    if cloudelastic_exists:
        return Status.broken_on_cloudelastic

    return Status.fixed


def wrong_index(error):
    print('TODO: wrong_index', file=sys.stderr)
    return Status.not_implemented


def all_elastic_docs(port, index, doc_id):
    eqiad = elastic_doc(EQIAD_URL, port, index, doc_id)
    cloudelastic = elastic_doc(CLOUDELASTIC_URL, port, index, doc_id)
    return eqiad, cloudelastic


def elastic_doc(base_url, port, index, doc_id):
    res = requests.get(f"{EQIAD_URL}:{port}/{index}/_doc/{doc_id}")
    if res.status_code == 404:
        return None
    res.raise_for_status()
    return res.json()['_source']


if __name__ == "__main__":
    sys.exit(main())
