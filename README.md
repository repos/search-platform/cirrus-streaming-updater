# Cirrus Streaming Updater

This project defines a combination of two flink applications that process wiki-page-related update events
and map them to elastic search updates.

## Build and Deploy

### Configuration

* [Config options for producer/aggregator](./cirrus-streaming-updater-producer/CONFIG.md)
* [Config options for consumer/indexer](./cirrus-streaming-updater-consumer/CONFIG.md)

#### Generate Documentation

The above documentation is auto-generated.
By default [`ConfigRenderer`](common/src/test/java/org/wikimedia/discovery/cirrus/updater/common/config/ConfigRenderer.java) will only validate that the documentation is up-to-date.
To regenerate, pass `-Drender.config.goal=render` with any build that includes the maven `test` phase.

### Containerization

To containerize the application, we use [blubber](https://github.com/wikimedia/blubber).
You can build the container locally as follows:

* Copy the [shared resources](.pipeline/blubber.yaml) to the module's `.pipeline` directory
* Run `docker build --target test -f .pipeline/blubber.yaml .` inside that module directory
* Execute `docker run --rm <image SHA256 from previous step>`

Flink applications are executed in a flink runtime environment.
Any dependency that is part of that runtime environment should not be shipped with the application
to avoid class loading issues, and to reduce the size of the container.

There's a [helper script](common/assembly/classpath-check.sh) that checks for duplicate classes.
If it yields any, adapt the [assembly descriptor](common/assembly/flink-app.xml) accordingly.

### CI/CD

Both applications are shipped as docker containers. The container images are produced as follows:

1. **\[maven]** Run a regular maven build `./mvnw clean verify`
2. **\[blubber]** Copy the resulting JARs in an image based on [`docker-registry.wikimedia.org/flink`](https://gerrit.wikimedia.org/r/plugins/gitiles/operations/docker-images/production-images/+/refs/heads/master/images/flink/flink)

For deployment, helm is used:

1. **\[helm]** [chart](https://gerrit.wikimedia.org/r/plugins/gitiles/operations/deployment-charts/+/refs/heads/master/charts/flink-app/), 
   see [example](https://gerrit.wikimedia.org/r/plugins/gitiles/operations/deployment-charts/+/refs/heads/master/charts/flink-app/.fixtures/jvm_app.yaml)

