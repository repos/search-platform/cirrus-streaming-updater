#!/bin/sh -x

export LOG4J_LEVEL=INFO

exec /bin/java -classpath 'lib/*:usrlib/*' \
    -Dlog4j.configuration=file:$(pwd)/conf/log4j-console.properties \
    -Dlog4j.configurationFile=file:$(pwd)/conf/log4j-console.properties \
    org.wikimedia.discovery.cirrus.updater.producer.ProducerApplication \
    --event-stream-config-url file:$(pwd)/resources/event-stream-config.json \
    --event-stream-json-schema-urls file:$(pwd)/resources/schema_repo \
    --kafka-source-config.bootstrap.servers PLAINTEXT://localhost:9092 \
    --kafka-source-config.group.id test \
    --public-update-topic eqiad.cirrussearch.update_pipeline.update.rc0 \
    --private-update-topic eqiad.cirrussearch.update_pipeline.update.private.rc0 \
    --kafka-sink-config.bootstrap.servers PLAINTEXT://localhost:9092 \
    --kafka-sink-config.batch.size 100 \
    --kafka-sink-config.compression.type snappy \
    --kafka-sink-config.linger.ms 2000 \
    --kafka-sink-config.max.request.size 5242880 \
    --kafka-sink-config.request.timeout.ms 5000 \
    --public-page-change-stream mediawiki.page_change.v1 \
    --private-page-change-stream mediawiki.page_change.private.v1 \
    --public-page-rerender-stream mediawiki.cirrussearch.page_rerender.v1 \
    --private-page-rerender-stream mediawiki.cirrussearch.page_rerender.private.v1 \
    --page-weighted-tags-change-stream mediawiki.cirrussearch.page_weighted_tags_change.rc0 \
    --article-topic-stream mediawiki.revision_score_articletopic \
    --draft-topic-stream mediawiki.revision_score_drafttopic \
    --recommendation-create-stream mediawiki.revision-recommendation-create \
    --pipeline.name eqiad.test.producer \
    --dry-run true
