# Configuration parameters for ProducerConfig

```
--article-topic-stream                     
                                   String  
--bypass-optimization-wikiids              List of wiki database names, that shall bypass optimization (deduplication).
                                           When not provided all wikiids are subject to optimization.
                                 String[]  default: []

--draft-topic-stream                       
                                   String  
--page-weighted-tags-change-legacy-stream  
                                   String  
--page-weighted-tags-change-stream         
                                   String  
--private-page-change-stream               
                                   String  
--private-page-rerender-stream             
                                   String  
--private-update-topic                     
                                   String  
--public-page-change-stream                
                                   String  
--public-page-rerender-stream              
                                   String  
--public-update-topic                      
                                   String  
--recommendation-create-stream             
                                   String  
--topic-prefix-filter                      If given, only stream-derived-topics with the specified prefix are subscribed
                                   String  
--upsert-rerender-reasons                  List of reasons for which a rerender event might enable an upsert
                                 String[]  default: [cirrus-force-refresh]

--window-size                              
                                 Duration  default: PT5M

```

# Configuration parameters for CommonConfig

```
--dry-run                                  
                                  Boolean  default: false

--event-stream-config-url                  
                                   String  
--event-stream-http-routes                 A map of 'source_host: dest_host' used by event streams for schema loading.
                                      Map  
--event-stream-json-schema-urls            
                                 String[]  
--http-rate-limit-max-retries              Maximum number of retries attempted in case of a 429  (rate-limited, too many requests) response
                                           A value < 0 means unbounded retries
                                  Integer  default: -1

--http-rate-limit-per-second               Maximum number of requests per second permitted per http-user-agent
                                           A value < 0 means no client-side rate-limiting
                                  Integer  default: 1000

--http-rate-limit-retry-interval           Retry interval in case of a 429 (rate-limited, too many requests) response
                                 Duration  default: PT0.5S

--http-request-connection-timeout-divisor  Portion of the overall http-request-timeout after establishing a connection times out
                                     Long  default: 10

--http-request-timeout                     Timeout for HTTP requests
                                 Duration  default: PT10S

--http-routes                              A map of '00_sorting_key: original_uri_regular_expression=alternate_uri'
                                      Map  
--http-user-agent                          User agent to provide with all http requests
                                   String  default: WMF/cirrus-streaming-updater

--kafka-sink-config                        Map of kafka sink configuration properties, defaults to kafka-source-config
                                      Map  
--kafka-source-config                      Map of kafka source configuration properties
                                           Minimal configuration:
                                            * bootstrap.servers
                                            * group.id
                                      Map  
--kafka-source-end-offset                  Source start offsets, format topic[:partition]=offset
                                      Map  
--kafka-source-end-time                    Source end instant (ISO format), used to determine last offset before that time
                                           Alternatively, specify kafka-source-end-offset
                                   String  
--kafka-source-start-offset                Source end offsets, format topic[:partition]=offset
                                      Map  
--kafka-source-start-time                  Source start instant (ISO format), used to determine first offset after that time
                                           Alternatively, specify kafka-source-start-offset
                                   String  
--mediawiki-auth-token                     NetworkSession token to provide to mediawiki to access private wikis
                                   String  
--pipeline.name                            The job name used for printing and logging.
                                   String  
--private-update-stream                    Bridging stream for private wikis between producer (aggregator) and consumer (indexer)
                                           This is used to fetch the event schema as well as a list of kafka topics from stream configuration, see event-stream-config-url
                                   String  default: cirrussearch.update_pipeline.update.private.v1

--public-update-stream                     Bridging stream for public wikis between producer (aggregator) and consumer (indexer)
                                           This is used to fetch the event schema as well as a list of kafka topics from stream configuration, see event-stream-config-url
                                   String  default: cirrussearch.update_pipeline.update.v1

--wikiids                                  List of wiki database names we will process, when not provided all wikis are processed.
                                 String[]  
```

This file has been auto-generated by `org.wikimedia.discovery.cirrus.updater.common.config.ConfigRenderer`.
