package org.wikimedia.discovery.cirrus.updater.producer.graph;

import org.apache.flink.streaming.api.functions.ProcessFunction;
import org.apache.flink.util.Collector;
import org.wikimedia.discovery.cirrus.updater.common.model.ChangeType;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateEvent;

import com.google.common.annotations.VisibleForTesting;

public class DeleteCrossIndexMovedPage extends ProcessFunction<UpdateEvent, UpdateEvent> {
    @Override
    public void processElement(UpdateEvent source, Context ctx, Collector<UpdateEvent> out) {
        out.collect(source);

        if (isMovedFromAnotherIndex(source)) {
            UpdateEvent event = source.createDerivedEvent();
            event.setChangeType(ChangeType.PAGE_DELETE);
            event.setTargetDocument(source.getMovedFrom().toBuilder().build());
            out.collect(event);
        }
    }

    /** Test if this event carries out a page move between two different indices. */
    @VisibleForTesting
    boolean isMovedFromAnotherIndex(UpdateEvent event) {
        if (event.getTargetDocument() == null || !event.getTargetDocument().isComplete()) {
            return false;
        }
        if (event.getMovedFrom() == null || !event.getMovedFrom().isComplete()) {
            return false;
        }
        return !event.getTargetDocument().getIndexName().equals(event.getMovedFrom().getIndexName());
    }
}
