/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.wikimedia.discovery.cirrus.updater.producer;

import org.apache.flink.configuration.Configuration;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.slf4j.LoggerFactory;
import org.wikimedia.discovery.cirrus.updater.common.config.ParameterToolMerger;
import org.wikimedia.discovery.cirrus.updater.producer.config.ProducerConfig;
import org.wikimedia.discovery.cirrus.updater.producer.graph.ProducerGraphFactory;

import lombok.experimental.UtilityClass;

@UtilityClass
@SuppressWarnings("checkstyle:HideUtilityClassConstructor")
public class ProducerApplication {

    public static void main(String[] args) throws Exception {
        final Configuration configuration =
                ParameterToolMerger.fromDefaultsWithOverrides(
                                "/cirrus-streaming-updater-producer.properties", args)
                        .getConfiguration();

        final ProducerConfig config = ProducerConfig.of(configuration);
        try (ProducerGraphFactory factory = new ProducerGraphFactory(config)) {
            final StreamExecutionEnvironment env = factory.prepareEnvironment();
            if (config.dryRun()) {
                LoggerFactory.getLogger(ProducerApplication.class).info("DRY RUN DONE");
                return;
            }
            env.execute(config.jobName());
        }
    }
}
