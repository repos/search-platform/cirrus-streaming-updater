package org.wikimedia.discovery.cirrus.updater.producer.graph;

import org.apache.flink.streaming.api.functions.KeyedProcessFunction;
import org.apache.flink.util.Collector;
import org.apache.flink.util.OutputTag;
import org.wikimedia.discovery.cirrus.updater.common.model.PageKey;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateEvent;

/**
 * Filter late events and route them to a side-output.
 *
 * <p>Workaround for FLINK-22425.
 */
public class LateEventProcessFunction
        extends KeyedProcessFunction<PageKey, UpdateEvent, UpdateEvent> {
    private final OutputTag<UpdateEvent> sideoutput;

    public LateEventProcessFunction(OutputTag<UpdateEvent> sideoutput) {
        this.sideoutput = sideoutput;
    }

    @Override
    public void processElement(
            UpdateEvent value,
            KeyedProcessFunction<PageKey, UpdateEvent, UpdateEvent>.Context ctx,
            Collector<UpdateEvent> out) {
        if (ctx.timestamp() <= ctx.timerService().currentWatermark()) {
            // this event is late, route it to the side output
            ctx.output(sideoutput, value);
        } else {
            out.collect(value);
        }
    }
}
