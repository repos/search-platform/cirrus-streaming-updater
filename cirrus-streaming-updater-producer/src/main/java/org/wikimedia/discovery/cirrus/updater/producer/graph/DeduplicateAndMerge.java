package org.wikimedia.discovery.cirrus.updater.producer.graph;

import org.apache.flink.configuration.Configuration;
import org.apache.flink.metrics.Counter;
import org.apache.flink.streaming.api.functions.windowing.ProcessWindowFunction;
import org.apache.flink.streaming.api.windowing.windows.Window;
import org.apache.flink.util.Collector;
import org.wikimedia.discovery.cirrus.updater.common.model.PageKey;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateEvent;

import com.google.common.annotations.VisibleForTesting;

import lombok.AccessLevel;
import lombok.Getter;

/**
 * Deduplicates and merges events from multiple sources.
 *
 * <p>The general goal here is to reduce indexing load on the downstream elasticsearch instances by
 * reducing the set of updates we emit to the fewest possible. In most cases we should be able to
 * emit a single update event per window.
 */
public class DeduplicateAndMerge<W extends Window>
        extends ProcessWindowFunction<UpdateEvent, UpdateEvent, PageKey, W> {

    private static final long serialVersionUID = -6737513534227247997L;

    @Getter(
            value = AccessLevel.PACKAGE,
            onMethod_ = {@VisibleForTesting})
    private transient Counter deduplicated;

    @Getter(
            value = AccessLevel.PACKAGE,
            onMethod_ = {@VisibleForTesting})
    private transient Counter merged;

    @Override
    public void open(Configuration parameters) throws Exception {
        super.open(parameters);
        deduplicated = getRuntimeContext().getMetricGroup().counter("deduplicated");
        merged = getRuntimeContext().getMetricGroup().counter("merged");
    }

    @Override
    public void process(
            PageKey key,
            ProcessWindowFunction<UpdateEvent, UpdateEvent, PageKey, W>.Context context,
            Iterable<UpdateEvent> in,
            Collector<UpdateEvent> out) {
        final UpdateEventMerger merger = UpdateEventMerger.create(in);
        deduplicated.inc(merger.getDuplicateCount());
        merged.inc(merger.getMergedCount());
        merger.get().forEach(out::collect);
    }
}
