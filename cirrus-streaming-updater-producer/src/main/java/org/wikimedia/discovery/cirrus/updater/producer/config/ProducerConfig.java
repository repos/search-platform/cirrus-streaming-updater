package org.wikimedia.discovery.cirrus.updater.producer.config;

import static org.wikimedia.discovery.cirrus.updater.common.config.ReadableConfigReader.getOptionalOrNull;
import static org.wikimedia.discovery.cirrus.updater.common.config.ReadableConfigReader.getRequired;

import java.time.Duration;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.Nullable;

import org.apache.flink.configuration.ConfigOption;
import org.apache.flink.configuration.ConfigOptions;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.configuration.description.Description;
import org.apache.flink.configuration.description.LinkElement;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.wikimedia.discovery.cirrus.updater.common.config.CommonConfig;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.experimental.Accessors;
import lombok.experimental.FieldDefaults;
import lombok.experimental.SuperBuilder;

@Getter
@SuperBuilder
@Accessors(fluent = true)
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
public class ProducerConfig extends CommonConfig {
    public static final ConfigOption<String> PARAM_ARTICLE_TOPIC_STREAM =
            ConfigOptions.key("article-topic-stream").stringType().noDefaultValue();
    public static final ConfigOption<String> PARAM_DRAFT_TOPIC_STREAM =
            ConfigOptions.key("draft-topic-stream").stringType().noDefaultValue();
    public static final ConfigOption<String> PARAM_PUBLIC_PAGE_CHANGE_STREAM =
            ConfigOptions.key("public-page-change-stream").stringType().noDefaultValue();
    public static final ConfigOption<String> PARAM_PRIVATE_PAGE_CHANGE_STREAM =
            ConfigOptions.key("private-page-change-stream").stringType().noDefaultValue();
    public static final ConfigOption<String> PARAM_PUBLIC_PAGE_RERENDER_STREAM =
            ConfigOptions.key("public-page-rerender-stream").stringType().noDefaultValue();
    public static final ConfigOption<String> PARAM_PRIVATE_PAGE_RERENDER_STREAM =
            ConfigOptions.key("private-page-rerender-stream").stringType().noDefaultValue();
    public static final ConfigOption<String> PARAM_PAGE_WEIGHTED_TAGS_CHANGE_STREAM =
            ConfigOptions.key("page-weighted-tags-change-stream").stringType().noDefaultValue();
    public static final ConfigOption<String> PARAM_PAGE_WEIGHTED_TAGS_CHANGE_LEGACY_STREAM =
            ConfigOptions.key("page-weighted-tags-change-legacy-stream").stringType().noDefaultValue();
    public static final ConfigOption<String> PARAM_RECOMMENDATION_CREATE_STREAM =
            ConfigOptions.key("recommendation-create-stream").stringType().noDefaultValue();
    public static final ConfigOption<String> PARAM_TOPIC_PREFIX_FILTER =
            ConfigOptions.key("topic-prefix-filter")
                    .stringType()
                    .noDefaultValue()
                    .withDescription(
                            "If given, only stream-derived-topics with the specified prefix are subscribed");
    public static final ConfigOption<String> PARAM_PUBLIC_UPDATE_TOPIC =
            ConfigOptions.key("public-update-topic").stringType().noDefaultValue();

    public static final ConfigOption<String> PARAM_PRIVATE_UPDATE_TOPIC =
            ConfigOptions.key("private-update-topic").stringType().noDefaultValue();

    public static final ConfigOption<Duration> PARAM_WINDOW_SIZE_MINUTES =
            ConfigOptions.key("window-size").durationType().defaultValue(Duration.ofMinutes(5));
    public static final ConfigOption<List<String>> PARAM_UPSERT_RERENDER_REASONS =
            ConfigOptions.key("upsert-rerender-reasons")
                    .stringType()
                    .asList()
                    .defaultValues("cirrus-force-refresh")
                    .withDescription("List of reasons for which a rerender event might enable an upsert");
    public static final ConfigOption<List<String>> PARAM_BYPASS_OPTIMIZATION_WIKI_IDS =
            ConfigOptions.key("bypass-optimization-wikiids")
                    .stringType()
                    .asList()
                    .defaultValues()
                    .withDescription(
                            Description.builder()
                                    .text(
                                            "List of wiki database names, that shall bypass optimization (deduplication).")
                                    .linebreak()
                                    .text(
                                            "When not provided all %s are subject to optimization.",
                                            LinkElement.link(CommonConfig.PARAM_WIKI_IDS.key()))
                                    .build());
    String articleTopicStream;
    String draftTopicStream;
    String publicPageChangeStream;
    String privatePageChangeStream;
    String publicPageRerenderStream;
    String privatePageRerenderStream;
    String recommendationCreateStream;
    String pageWeightedTagsChangeStream;
    @Nullable String pageWeightedTagsChangeLegacyStream;
    @Nullable String topicPrefix;
    String publicUpdateTopic;
    String privateUpdateTopic;
    Time windowSize;
    List<String> upsertRerenderReasons;
    Set<String> bypassOptimizationWikiIds;

    public static ProducerConfig of(Configuration configuration) {
        return CommonConfig.superBuilder(configuration, ProducerConfig::builder)
                .publicPageChangeStream(getRequired(configuration, PARAM_PUBLIC_PAGE_CHANGE_STREAM))
                .privatePageChangeStream(getRequired(configuration, PARAM_PRIVATE_PAGE_CHANGE_STREAM))
                .publicPageRerenderStream(getRequired(configuration, PARAM_PUBLIC_PAGE_RERENDER_STREAM))
                .privatePageRerenderStream(getRequired(configuration, PARAM_PRIVATE_PAGE_RERENDER_STREAM))
                .articleTopicStream(getRequired(configuration, PARAM_ARTICLE_TOPIC_STREAM))
                .draftTopicStream(getRequired(configuration, PARAM_DRAFT_TOPIC_STREAM))
                .recommendationCreateStream(getRequired(configuration, PARAM_RECOMMENDATION_CREATE_STREAM))
                .pageWeightedTagsChangeStream(
                        getRequired(configuration, PARAM_PAGE_WEIGHTED_TAGS_CHANGE_STREAM))
                .pageWeightedTagsChangeLegacyStream(
                        getOptionalOrNull(configuration, PARAM_PAGE_WEIGHTED_TAGS_CHANGE_LEGACY_STREAM))
                .topicPrefix(configuration.get(PARAM_TOPIC_PREFIX_FILTER))
                .windowSize(Time.seconds(getRequired(configuration, PARAM_WINDOW_SIZE_MINUTES).toSeconds()))
                .publicUpdateTopic(getRequired(configuration, PARAM_PUBLIC_UPDATE_TOPIC))
                .privateUpdateTopic(getRequired(configuration, PARAM_PRIVATE_UPDATE_TOPIC))
                .upsertRerenderReasons(getRequired(configuration, PARAM_UPSERT_RERENDER_REASONS))
                .bypassOptimizationWikiIds(
                        new HashSet<>(getRequired(configuration, PARAM_BYPASS_OPTIMIZATION_WIKI_IDS)))
                .build();
    }
}
