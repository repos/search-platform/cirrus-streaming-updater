package org.wikimedia.discovery.cirrus.updater.producer.graph;

import static java.util.Objects.requireNonNull;
import static org.wikimedia.discovery.cirrus.updater.common.logging.LoggingContext.log;
import static org.wikimedia.discovery.cirrus.updater.common.model.JsonPathUtils.getRequiredNode;

import java.net.URI;
import java.time.Duration;
import java.util.Locale;
import java.util.Map;

import org.apache.flink.annotation.VisibleForTesting;
import org.apache.flink.api.common.functions.RichMapFunction;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.metrics.Counter;
import org.apache.hc.client5.http.classic.methods.HttpGet;
import org.wikimedia.discovery.cirrus.updater.common.http.MediaWikiHttpClient;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateEvent;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import lombok.Value;
import lombok.extern.log4j.Log4j2;

@Log4j2
@SuppressFBWarnings({"SE_NO_SERIALVERSIONID", "NP_NULL_ON_SOME_PATH_FROM_RETURN_VALUE"})
public class CirrusNamespaceIndexMap extends RichMapFunction<UpdateEvent, UpdateEvent> {
    @VisibleForTesting
    public static final String API_PATH =
            "/w/api.php?action=cirrus-config-dump&prop=replicagroup%7Cnamespacemap&format=json&formatversion=2";

    private static final Duration TTL = Duration.ofHours(6);
    private static final String DEFAULT_INDEX_TYPE = "general";

    private final MediaWikiHttpClient.Factory clientFactory;
    private final ObjectMapper objectMapper = new ObjectMapper();

    private transient Cache<String, IndexSettings> cache;

    private transient Counter misses;
    private transient Counter total;
    private transient Counter skipped;
    private transient Counter unknownNamespace;
    private transient Counter failed;
    private transient MediaWikiHttpClient client;

    public CirrusNamespaceIndexMap(MediaWikiHttpClient.Factory clientFactory) {
        this.clientFactory = clientFactory;
    }

    @Override
    public void open(Configuration parameters) throws Exception {
        super.open(parameters);
        client = clientFactory.apply(getRuntimeContext().getMetricGroup());
        misses = getRuntimeContext().getMetricGroup().counter("namespace-map-info-misses");
        total = getRuntimeContext().getMetricGroup().counter("namespace-map-info-total");
        skipped = getRuntimeContext().getMetricGroup().counter("namespace-map-info-skipped");
        failed = getRuntimeContext().getMetricGroup().counter("namespace-map-info-failed");
        unknownNamespace =
                getRuntimeContext().getMetricGroup().counter("namespace-map-info-unknown-ns");
        cache = Caffeine.newBuilder().expireAfterWrite(TTL).build();
        client = clientFactory.apply(getRuntimeContext().getMetricGroup());
    }

    @Override
    @SuppressFBWarnings(
            value = "NP_NULL_ON_SOME_PATH_FROM_RETURN_VALUE",
            justification = "false positive on cache.get return a null value")
    public UpdateEvent map(UpdateEvent updateEvent) {
        total.inc();
        UpdateEvent.TargetDocument target = updateEvent.getTargetDocument();
        UpdateEvent.TargetDocument movedFrom = updateEvent.getMovedFrom();
        if (target.isComplete() && (movedFrom == null || movedFrom.isComplete())) {
            skipped.inc();
            return updateEvent;
        }
        String wikiId = requireNonNull(target.getWikiId());
        String domain = requireNonNull(target.getDomain());

        IndexSettings settings;
        try {
            settings = cache.get(wikiId, k -> load(wikiId, domain));
        } catch (IllegalStateException ise) {
            // IllegalStateException is thrown from MediaWikiHttpClient#load in case of repeated failures.
            failed.inc();
            log(
                    log::atWarn,
                    () ->
                            String.format(
                                    Locale.ENGLISH,
                                    "Failed to fetch settings for wiki %s, domain %s",
                                    wikiId,
                                    domain),
                    updateEvent);
            // With an incomplete target, this event will end up being dropped at the output stage.
            return updateEvent;
        }

        if (!target.isComplete()) {
            completeTarget(updateEvent, settings, wikiId, target);
        }
        if (movedFrom != null && !movedFrom.isComplete()) {
            completeTarget(updateEvent, settings, wikiId, movedFrom);
        }
        return updateEvent;
    }

    private void completeTarget(
            UpdateEvent updateEvent,
            IndexSettings settings,
            String wikiId,
            UpdateEvent.TargetDocument target) {
        String indexName = settings.getNamespaceMap().get(target.getPageNamespace());
        if (indexName == null) {
            unknownNamespace.inc();
            final String defaultIndex = wikiId + "_" + DEFAULT_INDEX_TYPE;
            indexName = defaultIndex;
            log(
                    log::atWarn,
                    () ->
                            String.format(
                                    Locale.ENGLISH,
                                    "Unknown namespace %s for wiki %s, using %s instead",
                                    target.getPageNamespace(),
                                    wikiId,
                                    defaultIndex),
                    updateEvent);
        }
        target.completeWith(settings.getCirrusReplica(), indexName);
    }

    private IndexSettings load(String wikiId, String domain) {
        misses.inc();
        return client.load(
                new HttpGet(buildUri(domain)),
                node -> {
                    JsonNode map = getRequiredNode(node, "/CirrusSearchConcreteNamespaceMap");
                    JsonNode clusterGroup = getRequiredNode(node, "/CirrusSearchConcreteReplicaGroup");
                    Map<Long, String> nsMap = objectMapper.convertValue(map, new TypeReference<>() {});
                    return new IndexSettings(wikiId, nsMap, clusterGroup.textValue());
                });
    }

    private URI buildUri(String domain) {
        return URI.create("https://" + domain + API_PATH);
    }

    @Value
    public static class IndexSettings {
        String wikiId;
        Map<Long, String> namespaceMap;
        String cirrusReplica;
    }
}
