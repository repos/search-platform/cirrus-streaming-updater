/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.wikimedia.discovery.cirrus.updater.producer.graph;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Deque;
import java.util.LinkedList;

import org.apache.flink.api.common.ExecutionConfig;
import org.apache.flink.api.common.typeutils.TypeSerializer;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.windowing.assigners.MergingWindowAssigner;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.triggers.EventTimeTrigger;
import org.apache.flink.streaming.api.windowing.triggers.Trigger;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;

import com.google.common.base.Verify;

/**
 * A very lightly modified version of EventTimeSessionWindows from flink. The change here is that a
 * session has a fixed length, it does not expand when new events come in. This purpose is to have
 * windows with a constant length that flush at the end of the time period.
 */
public class EventTimeConstantSessionWindows extends MergingWindowAssigner<Object, TimeWindow> {
    private final long sessionTimeout;

    public EventTimeConstantSessionWindows(long sessionTimeout) {
        Verify.verify(
                sessionTimeout > 0, "EventTimeConstantSessionWindows parameters must satisfy 0 < size");
        this.sessionTimeout = sessionTimeout;
    }

    @Override
    public Collection<TimeWindow> assignWindows(
            Object element, long timestamp, WindowAssignerContext context) {
        return Collections.singletonList(new TimeWindow(timestamp, timestamp + sessionTimeout));
    }

    @Override
    public Trigger<Object, TimeWindow> getDefaultTrigger(StreamExecutionEnvironment env) {
        return EventTimeTrigger.create();
    }

    public static EventTimeConstantSessionWindows withLength(Time size) {
        return new EventTimeConstantSessionWindows(size.toMilliseconds());
    }

    @Override
    public TypeSerializer<TimeWindow> getWindowSerializer(ExecutionConfig executionConfig) {
        return new TimeWindow.Serializer();
    }

    @Override
    public boolean isEventTime() {
        return true;
    }

    @Override
    public void mergeWindows(Collection<TimeWindow> windows, MergeCallback<TimeWindow> callback) {
        windows.stream()
                .sorted(Comparator.comparing(TimeWindow::getStart))
                .reduce(
                        new LinkedList<Deque<TimeWindow>>(),
                        (groups, window) -> {
                            final Deque<TimeWindow> lastGroup = groups.peekLast();
                            if (lastGroup != null && lastGroup.peekFirst().intersects(window)) {
                                lastGroup.add(window);
                            } else {
                                Deque<TimeWindow> newGroup = new LinkedList<>();
                                newGroup.add(window);
                                groups.add(newGroup);
                            }
                            return groups;
                        },
                        (left, right) -> {
                            throw new UnsupportedOperationException();
                        })
                .forEach(
                        group -> {
                            if (group.size() > 1) {
                                callback.merge(group, group.peekFirst());
                            }
                        });
    }
}
