package org.wikimedia.discovery.cirrus.updater.producer.graph;

import static java.util.Collections.singleton;
import static java.util.stream.Collectors.toList;

import java.time.Duration;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.StreamSupport;

import javax.annotation.Nonnull;

import org.apache.flink.annotation.VisibleForTesting;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.common.functions.FilterFunction;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.functions.RichFlatMapFunction;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.api.connector.sink2.Sink;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.connector.kafka.source.KafkaSource;
import org.apache.flink.connector.kafka.source.KafkaSourceBuilder;
import org.apache.flink.connector.kafka.source.enumerator.initializer.OffsetsInitializer;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.DataStreamUtils;
import org.apache.flink.streaming.api.datastream.KeyedStream;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.functions.KeyedProcessFunction;
import org.apache.flink.streaming.api.functions.ProcessFunction;
import org.apache.flink.types.Row;
import org.apache.flink.util.Collector;
import org.apache.flink.util.OutputTag;
import org.apache.flink.util.Preconditions;
import org.apache.flink.util.function.SerializableFunction;
import org.apache.kafka.clients.consumer.OffsetResetStrategy;
import org.wikimedia.discovery.cirrus.updater.common.graph.CommonGraphFactory;
import org.wikimedia.discovery.cirrus.updater.common.graph.IngestionTimeAssigner;
import org.wikimedia.discovery.cirrus.updater.common.graph.WeightedTagMonitor;
import org.wikimedia.discovery.cirrus.updater.common.model.PageKey;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateEvent;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateRowTranscoder;
import org.wikimedia.discovery.cirrus.updater.common.util.function.SerializableBiConsumer;
import org.wikimedia.discovery.cirrus.updater.common.util.function.SerializablePredicate;
import org.wikimedia.discovery.cirrus.updater.producer.config.ProducerConfig;
import org.wikimedia.discovery.cirrus.updater.producer.config.UpdateEventSource;
import org.wikimedia.eventutilities.flink.EventRowTypeInfo;
import org.wikimedia.eventutilities.flink.formats.json.KafkaRecordTimestampStrategy;
import org.wikimedia.eventutilities.flink.stream.EventDataStreamFactory;
import org.wikimedia.eventutilities.flink.stream.EventDataStreamFactory.Builder;
import org.wikimedia.eventutilities.flink.stream.EventTimeAssigner;

import com.google.common.base.Verify;
import com.google.common.collect.Lists;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;

@SuppressWarnings({"checkstyle:classfanoutcomplexity"})
public class ProducerGraphFactory extends CommonGraphFactory<ProducerConfig> {

    public static final TypeInformation<PageKey> KEY_TYPE_INFO = TypeInformation.of(PageKey.class);

    public ProducerGraphFactory(ProducerConfig config) {
        super(createStreamExecutionEnvironment(), createEventDataStreamFactory(config), config);
    }

    public List<UpdateEventSource> sources() {
        // Could be more dynamically sourced, but for now hard code the set of streams we will
        // listen to and their configurations.
        List<String> upsertRerenderReasons = config.upsertRerenderReasons();
        List<UpdateEventSource> sourceStreams =
                Lists.newArrayList(
                        UpdateEventSource.builder()
                                .streamName(config.articleTopicStream())
                                .converter(
                                        row ->
                                                singleton(
                                                        UpdateEventConverters.fromOutlinkPredictionChange(
                                                                row, "classification.ores.articletopic")))
                                .build(),
                        UpdateEventSource.builder()
                                .streamName(config.draftTopicStream())
                                .converter(
                                        row ->
                                                singleton(
                                                        UpdateEventConverters.fromRevisionScoring(
                                                                row, "classification.ores.drafttopic")))
                                .build(),
                        UpdateEventSource.builder()
                                .streamName(config.recommendationCreateStream())
                                .converter(row -> singleton(UpdateEventConverters.fromRecommendationCreate(row)))
                                .useKafkaTimestamp(true) // this stream does not yet set the /dt field.
                                .build(),
                        UpdateEventSource.builder()
                                .streamName(config.pageWeightedTagsChangeStream())
                                .converter(row -> singleton(UpdateEventConverters.fromPageWeightedTagsChange(row)))
                                .build(),
                        UpdateEventSource.builder()
                                .streamName(config.publicPageChangeStream())
                                .converter(UpdateEventConverters::fromPublicPageChange)
                                .build(),
                        UpdateEventSource.builder()
                                .streamName(config.privatePageChangeStream())
                                .converter(UpdateEventConverters::fromPrivatePageChange)
                                .build(),
                        UpdateEventSource.builder()
                                .streamName(config.publicPageRerenderStream())
                                .converter(
                                        e ->
                                                UpdateEventConverters.fromPublicPageRerender(
                                                        e, upsertRerenderReasons::contains))
                                .build(),
                        UpdateEventSource.builder()
                                .streamName(config.privatePageRerenderStream())
                                .converter(
                                        e ->
                                                UpdateEventConverters.fromPrivatePageRerender(
                                                        e, upsertRerenderReasons::contains))
                                .build());
        if (config.pageWeightedTagsChangeLegacyStream() != null) {
            sourceStreams.add(
                    UpdateEventSource.builder()
                            .streamName(config.pageWeightedTagsChangeLegacyStream())
                            .converter(row -> singleton(UpdateEventConverters.fromPageWeightedTagsChange(row)))
                            .build());
        }
        return sourceStreams;
    }

    public DataStream<UpdateEvent> createDataStreamSource(
            String streamName,
            SerializableFunction<Row, Iterable<UpdateEvent>> converter,
            FilterFunction<UpdateEvent> filter,
            boolean useKafkaTimestamp) {
        KafkaSourceBuilder<Row> sourceBuilder = createKafkaSourceBuilder(streamName);
        KafkaSource<Row> source = sourceBuilder.build();

        WatermarkStrategy<Row> wmStrategy =
                WatermarkStrategy.<Row>forBoundedOutOfOrderness(Duration.ofSeconds(30))
                        // set idleness of the source, useful to not block the stream if some sources might
                        // become idle
                        .withIdleness(Duration.ofSeconds(30));

        if (!useKafkaTimestamp) {
            wmStrategy = wmStrategy.withTimestampAssigner(EventTimeAssigner.create());
        }

        String sourceName = streamName + "-source";

        return env.fromSource(source, wmStrategy, sourceName)
                .uid(sourceName)
                .name(sourceName)
                .flatMap(
                        new ConvertFilterFlatMap(
                                converter, filter, ProducerGraphFactory::validateGeneratedEvent),
                        getOrCreateUpdateTypeInfo())
                .uid(sourceName + "-convert-filter_by_wiki-validate")
                .name(sourceName + "-convert-filter_by_wiki-validate")
                .map(new IngestionTimeAssigner(config.clock()), getOrCreateUpdateTypeInfo())
                .uid(sourceName + "-assign-ingestion-time")
                .name(sourceName + "-assign-ingestion-time");
    }

    private static void validateGeneratedEvent(Row row, UpdateEvent event) {
        Verify.verifyNotNull(event.getTargetDocument(), "Null TargetDocument from Row [%s]", row);
    }

    public Sink<Row> createPublicDataSink() {
        return createDataSink(config.publicUpdateStream(), config.publicUpdateTopic());
    }

    public Sink<Row> createPrivateDataSink() {
        return createDataSink(config.privateUpdateStream(), config.privateUpdateTopic());
    }

    private Sink<Row> createDataSink(String stream, String topic) {
        return createKafkaSinkBuilder(
                        stream,
                        topic,
                        // Prefer the dt field we pass instead of the flink record timestamp
                        // since we use a window operation, the results of the window operation are assigned the
                        // end time of the window. This is not what we want here since we would like to track
                        // the update lag based on the original event-time.
                        KafkaRecordTimestampStrategy.ROW_EVENT_TIME)
                .build();
    }

    public MapFunction<UpdateEvent, UpdateEvent> createNamespaceIndexMapper() {
        return new CirrusNamespaceIndexMap(createMediWikiHttpClientFactory(10));
    }

    @SuppressWarnings("unchecked")
    private <T> KeyedStream<UpdateEvent, PageKey> union(
            DataStream<T> head, Collection<DataStream<UpdateEvent>> tail) {
        return head.union(tail.toArray(new DataStream[0]))
                .keyBy(UpdateEvent.KEY_SELECTOR, KEY_TYPE_INFO);
    }

    private KeyedStream<UpdateEvent, PageKey> union(List<DataStream<UpdateEvent>> sources) {
        Preconditions.checkArgument(!sources.isEmpty(), "sources must not be empty");
        return union(sources.get(0), sources.subList(1, sources.size()));
    }

    /** Attempt to reorder the stream based on rev_id and event time. */
    private DataStream<UpdateEvent> reorderAndOptimize(KeyedStream<UpdateEvent, PageKey> stream) {
        final Set<String> bypassedWikiIds = config.bypassOptimizationWikiIds();
        final OutputTag<UpdateEvent> bypassedTag =
                new OutputTag<>("bypassed", getOrCreateUpdateTypeInfo());

        final SingleOutputStreamOperator<UpdateEvent> bypassOptimization =
                stream
                        .process(
                                new BypassDeduplicationSplitter(bypassedWikiIds, bypassedTag),
                                getOrCreateUpdateTypeInfo())
                        .name("bypass-optimization")
                        .uid("bypass-optimization");

        final KeyedStream<UpdateEvent, PageKey> optimizationCandidates =
                DataStreamUtils.reinterpretAsKeyedStream(bypassOptimization, UpdateEvent.KEY_SELECTOR);

        OutputTag<UpdateEvent> lateEventTag =
                new OutputTag<>("late_events", getOrCreateUpdateTypeInfo());
        OutputTag<UpdateEvent> prefilteredLateEventTag =
                new OutputTag<>("prefiltered_late_events", getOrCreateUpdateTypeInfo());
        // Ideally we should not need to pre-filter late events, but apparently
        // they still get through the window and cause it to fail with:
        // The end timestamp of an event-time window cannot become earlier than the current watermark by
        // merging.
        // Current watermark: 1714929783999 window: TimeWindow{start=1714929432000, end=1714929732000}
        // Workaround https://issues.apache.org/jira/browse/FLINK-22425
        SingleOutputStreamOperator<UpdateEvent> lateEventFiltered =
                optimizationCandidates
                        .process(
                                new LateEventProcessFunction(prefilteredLateEventTag), getOrCreateUpdateTypeInfo())
                        .uid("late-event-filter")
                        .name("late-event-filter");
        KeyedStream<UpdateEvent, PageKey> lateEventFilteredKeyedStream =
                DataStreamUtils.reinterpretAsKeyedStream(lateEventFiltered, UpdateEvent.KEY_SELECTOR);
        SingleOutputStreamOperator<UpdateEvent> combined =
                lateEventFilteredKeyedStream
                        .window(EventTimeConstantSessionWindows.withLength(config.windowSize()))
                        .sideOutputLateData(lateEventTag)
                        // afaict we do not want to set an allowed lateness, that would potentially
                        // double-process the merge windows. Instead, send late events to a side
                        // output that will be processed without merging.
                        .process(new DeduplicateAndMerge<>(), getOrCreateUpdateTypeInfo())
                        .uid("deduplicate-and-merge")
                        .name("deduplicate-and-merge");

        // Is this really safe? These events will be processed out of order which could
        // run a delete and undelete in the wrong order. Should be rare, but possible.
        return combined.union(
                lateEventFiltered.getSideOutput(prefilteredLateEventTag),
                combined.getSideOutput(lateEventTag),
                bypassOptimization.getSideOutput(bypassedTag));
    }

    private DataStream<UpdateEvent> namespaceAndIndexMapper(DataStream<UpdateEvent> stream) {
        final MapFunction<UpdateEvent, UpdateEvent> namespaceIndexMapper = createNamespaceIndexMapper();
        return stream
                .map(namespaceIndexMapper, getOrCreateUpdateTypeInfo())
                .uid("namespace-index-map")
                .name("namespace-index-map")
                // TODO: consider routing these docs to a side output?
                .filter(u -> u.getTargetDocument().isComplete())
                .uid("name-index-map-filter-incomplete")
                .name("name-index-map-filter-incomplete");
    }

    private void sink(DataStream<UpdateEvent> stream) {
        final EventRowTypeInfo outputTypeInfo = super.getOrCreateUpdateRowTypeInfo();
        final UpdateRowTranscoder updateRowEncoder = UpdateRowTranscoder.encoder(outputTypeInfo);

        OutputTag<UpdateEvent> privateEventTag =
                new OutputTag<>("private", getOrCreateUpdateTypeInfo());
        SingleOutputStreamOperator<UpdateEvent> outputStream =
                stream
                        .process(
                                new DataStreamSplitter<>(privateEventTag, UpdateEvent::getFromPrivateStream),
                                getOrCreateUpdateTypeInfo())
                        .uid("private-wiki-sink-split")
                        .name("private-wiki-sink-split");

        outputStream
                .map(updateRowEncoder::encode, outputTypeInfo)
                .uid("update-pojo-2-row")
                .name("update-pojo-2-row")
                .sinkTo(createPublicDataSink())
                .uid("update-sink")
                .name("update-sink");

        outputStream
                .getSideOutput(privateEventTag)
                .map(updateRowEncoder::encode, outputTypeInfo)
                .uid("private-update-pojo-2-row")
                .name("private-update-pojo-2-row")
                .sinkTo(createPrivateDataSink())
                .uid("private-update-sink")
                .name("private-update-sink");
    }

    @Nonnull
    @Override
    protected OffsetsInitializer getDefaultKafkaStartingOffsets() {
        return OffsetsInitializer.committedOffsets(OffsetResetStrategy.LATEST);
    }

    @Override
    protected void createStreamGraph() {
        FilterFunction<UpdateEvent> filter = createFilter(config.wikiIds());
        List<DataStream<UpdateEvent>> sources =
                sources().stream()
                        .map(
                                s ->
                                        createDataStreamSource(
                                                s.getStreamName(), s.getConverter(), filter, s.useKafkaTimestamp()))
                        .collect(toList());
        if (sources.isEmpty()) {
            throw new IllegalArgumentException("No sources available");
        }

        // We union all sources and send them to the fetch operation, reason is that we don't want to
        // advance streams that do not require fetching too quickly, this might skew the event times.
        // This will obviously impact throughput but the other strategy would have pushed many events to
        // the next steps that requires buffering these in a window. In short, we prefer to have a
        // non-optimal throughput vs many windows waiting for the slow events to arrive.
        KeyedStream<UpdateEvent, PageKey> allInputs = union(sources);
        DataStream<UpdateEvent> optimized = reorderAndOptimize(allInputs);
        DataStream<UpdateEvent> withNamespaceAndIndex = namespaceAndIndexMapper(optimized);
        DataStream<UpdateEvent> withCrossIndexDeletes = crossIndexDeletes(withNamespaceAndIndex);
        sink(withCrossIndexDeletes);
    }

    private DataStream<UpdateEvent> crossIndexDeletes(DataStream<UpdateEvent> withNamespaceAndIndex) {
        return withNamespaceAndIndex
                .process(new DeleteCrossIndexMovedPage(), getOrCreateUpdateTypeInfo())
                .uid("delete-cross-index-moved-page")
                .name("delete-cross-index-moved-page");
    }

    private static EventDataStreamFactory createEventDataStreamFactory(ProducerConfig config) {
        final Builder builder = createEventDataStreamFactoryBuilder(config);
        if (config.topicPrefix() != null && !config.topicPrefix().isBlank()) {
            builder.topicPrefix(config.topicPrefix());
        }
        return builder.build();
    }

    /** Flat-maps a source event ({@link Row}) into {@code 0+} {@link UpdateEvent InputEvents}. */
    @SuppressFBWarnings(
            value = "IMC_IMMATURE_CLASS_BAD_SERIALVERSIONUID",
            justification = "generated serialVersionUID is considered invalid")
    private static final class ConvertFilterFlatMap extends RichFlatMapFunction<Row, UpdateEvent> {

        private static final long serialVersionUID = 6615660944130209703L;
        private final SerializableFunction<Row, Iterable<UpdateEvent>> converter;
        private final FilterFunction<UpdateEvent> filter;
        private final SerializableBiConsumer<Row, UpdateEvent> validator;
        private transient WeightedTagMonitor weightedTagMonitor;

        @Override
        public void open(Configuration parameters) throws Exception {
            super.open(parameters);
            weightedTagMonitor =
                    new WeightedTagMonitor(getRuntimeContext().getMetricGroup().addGroup("weighted_tags"));
        }

        private ConvertFilterFlatMap(
                SerializableFunction<Row, Iterable<UpdateEvent>> converter,
                FilterFunction<UpdateEvent> filter,
                SerializableBiConsumer<Row, UpdateEvent> validator) {
            this.converter = converter;
            this.filter = filter;
            this.validator = validator;
        }

        @Override
        public void flatMap(Row row, Collector<UpdateEvent> collector) {
            StreamSupport.stream(converter.apply(row).spliterator(), false)
                    .filter(Objects::nonNull)
                    .filter(this::filter)
                    .peek(event -> validator.accept(row, event))
                    .peek(weightedTagMonitor::report)
                    .forEach(collector::collect);
        }

        @SneakyThrows
        private boolean filter(UpdateEvent updateEvent) {
            return filter == null || filter.filter(updateEvent);
        }
    }

    @VisibleForTesting
    @SuppressFBWarnings("IMC_IMMATURE_CLASS_BAD_SERIALVERSIONUID")
    static class BypassDeduplicationSplitter
            extends KeyedProcessFunction<PageKey, UpdateEvent, UpdateEvent> {

        private static final long serialVersionUID = -8462308690542192325L;
        private final OutputTag<UpdateEvent> bypassedTag;
        private final Set<String> bypassedWikiIds;

        BypassDeduplicationSplitter(Set<String> bypassedWikiIds, OutputTag<UpdateEvent> bypassedTag) {
            this.bypassedWikiIds = bypassedWikiIds;
            this.bypassedTag = bypassedTag;
        }

        @Override
        public void processElement(
                UpdateEvent value,
                KeyedProcessFunction<PageKey, UpdateEvent, UpdateEvent>.Context ctx,
                Collector<UpdateEvent> out) {
            if (bypassedWikiIds.contains(ctx.getCurrentKey().getWiki())
                    || Boolean.TRUE.equals(value.getBypassDeduplication())) {
                ctx.output(bypassedTag, value);
            } else {
                out.collect(value);
            }
        }
    }

    @RequiredArgsConstructor
    static class DataStreamSplitter<T> extends ProcessFunction<T, T> {
        private static final long serialVersionUID = 5996572795813512715L;
        private final OutputTag<T> tag;
        private final SerializablePredicate<T> predicate;

        @Override
        public void processElement(T value, ProcessFunction<T, T>.Context ctx, Collector<T> out)
                throws Exception {
            if (predicate.test(value)) {
                ctx.output(tag, value);
            } else {
                out.collect(value);
            }
        }
    }
}
