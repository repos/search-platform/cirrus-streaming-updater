package org.wikimedia.discovery.cirrus.updater.producer.graph;

import static com.google.common.base.Preconditions.checkArgument;
import static java.util.Collections.emptyList;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.apache.flink.types.Row;
import org.jetbrains.annotations.NotNull;
import org.wikimedia.discovery.cirrus.updater.common.logging.LoggingContext;
import org.wikimedia.discovery.cirrus.updater.common.model.ChangeType;
import org.wikimedia.discovery.cirrus.updater.common.model.MetaRowTranscoder;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateEvent;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateEvent.Meta;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateEvent.TargetDocument;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateFields;

import com.google.common.base.Verify;
import com.google.common.collect.ImmutableSet;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import lombok.experimental.UtilityClass;
import lombok.extern.log4j.Log4j2;

@Log4j2
@UtilityClass
@SuppressWarnings("checkstyle:HideUtilityClassConstructor")
@SuppressFBWarnings(value = "CE_CLASS_ENVY", justification = "false positive")
public class UpdateEventConverters {

    static final String PAGE_CHANGE_SCHEMA = "mediawiki/page/change";
    static final String PAGE_RERENDER_SCHEMA = "mediawiki/cirrussearch/page_rerender";
    static final String PAGE_WEIGHTED_TAGS_CHANGE_LEGACY_SCHEMA =
            "development/cirrussearch/page_weighted_tags_change";
    static final String PAGE_WEIGHTED_TAGS_CHANGE_SCHEMA =
            "mediawiki/cirrussearch/page_weighted_tags_change";
    static final String REVISION_SCORE_SCHEMA = "mediawiki/revision/score";
    static final String RECOMMENDATION_CREATE_SCHEMA = "mediawiki/revision/recommendation-create";
    static final String PREDICTION_CHANGE_SCHEMA = "mediawiki/page/prediction_classification_change";

    private static final MetaRowTranscoder META_TRANSCODER = MetaRowTranscoder.decoder();

    private static void checkSchema(Row row, String expectedSchema) {
        checkSchema(row, List.of(expectedSchema));
    }

    private static void checkSchema(Row row, List<String> acceptableSchemas) {
        String schema = row.getFieldAs("$schema");
        checkArgument(schema != null, "Undefined source schema");
        String actualSchema;
        if (schema.startsWith("/")) {
            actualSchema = schema.substring(1);
        } else {
            actualSchema = schema;
        }
        // In theory the schema should always have a leading /. In practice, we've seen
        // topics (outlinks) that do not include the leading /.
        boolean unsupportedSchema =
                acceptableSchemas.stream().filter(actualSchema::startsWith).findAny().isEmpty();
        if (unsupportedSchema) {
            checkArgument(
                    unsupportedSchema,
                    "Unknown source schema: "
                            + actualSchema
                            + " expected ["
                            + String.join(",", acceptableSchemas)
                            + "]");
        }
    }

    public static Iterable<UpdateEvent> fromPublicPageChange(Row row) {
        return fromPageChange(row, false);
    }

    public static Iterable<UpdateEvent> fromPrivatePageChange(Row row) {
        return fromPageChange(row, true);
    }

    /**
     * Maps a <a
     * href="https://schema.wikimedia.org/repositories/primary/jsonschema/mediawiki/page/change/latest">mediawiki/page/change</a>
     * event to {@code 0..n} derived {@link UpdateEvent InputEvents}.
     *
     * <p>Depending on the {@code page_change_kind}…
     *
     * <ul>
     *   <li>…<code>delete</code> results in…
     *       <ul>
     *         <li>…a delete operation
     *         <li>…an update operation of the redirect target page (if any)
     *       </ul>
     *   <li>…<code>create, edit, move, …</code> result in…
     *       <ul>
     *         <li>…a revision-based update if the source page <i>is not</i> a redirect
     *         <li>…a delete operation if the source page <i>is</i> a redirect
     *         <li>…an update operation of the redirect target page (if any)
     *       </ul>
     * </ul>
     *
     * @param row the original event
     * @return {@code 0..n} derived {@link UpdateEvent InputEvents}
     */
    private static Iterable<UpdateEvent> fromPageChange(Row row, boolean fromPrivateStream) {
        checkSchema(row, PAGE_CHANGE_SCHEMA);

        List<UpdateEvent> events = new ArrayList<>(2);
        UpdateEvent sourceEvent = new UpdateEvent();
        String pageChangeKind = row.getFieldAs("page_change_kind");
        sourceEvent.setEventTime(row.getFieldAs("dt"));
        sourceEvent.setMeta(extractMeta(row));
        sourceEvent.setFromPrivateStream(fromPrivateStream);

        Row page = row.getFieldAs("page");

        Row revision = row.getFieldAs("revision");
        sourceEvent.setRevId(revision.getFieldAs("rev_id"));

        final TargetDocument targetDocument =
                new TargetDocument(
                        sourceEvent.getMeta().getDomain(), row.getFieldAs("wiki_id"),
                        page.getFieldAs("namespace_id"), page.getFieldAs("page_id"));
        targetDocument.setPageTitle(page.getFieldAs("page_title"));
        sourceEvent.setTargetDocument(targetDocument);

        if ("move".equals(pageChangeKind)) {
            // Handle cross namespace page moves
            sourceEvent.setMovedFrom(extractPriorStateTarget(sourceEvent, row));
        }

        events.add(sourceEvent);
        if ("delete".equals(pageChangeKind) || "undelete".equals(pageChangeKind)) {
            // These events operate on pre-existing revisions, the rev_dt is in the
            // past. Use the event creation timestamp instead.
            sourceEvent.setEventTime(row.getFieldAs("dt"));
        } else {
            sourceEvent.setEventTime(revision.getFieldAs("rev_dt"));
        }

        if ("delete".equals(pageChangeKind)) {
            sourceEvent.setChangeType(ChangeType.PAGE_DELETE);
        } else {
            sourceEvent.setChangeType(ChangeType.REV_BASED_UPDATE);
        }

        deriveRedirectTargetUpdateEvent(row)
                .ifPresentOrElse(
                        targetEvent -> {
                            final ImmutableSet<TargetDocument> redirect =
                                    ImmutableSet.of(sourceEvent.getTargetDocument());
                            targetEvent.setFromPrivateStream(fromPrivateStream);
                            if (sourceEvent.getChangeType() == ChangeType.PAGE_DELETE) {
                                targetEvent.setUpdate(
                                        UpdateEvent.Update.forRedirect(emptyList(), redirect.asList()));
                            } else {
                                targetEvent.setUpdate(
                                        UpdateEvent.Update.forRedirect(redirect.asList(), emptyList()));
                            }

                            events.add(targetEvent);

                            sourceEvent.setChangeType(ChangeType.PAGE_DELETE);
                        },
                        () -> {
                            // Redirects are never represented directly in the index, only as a property
                            // of the page they redirect to. Delete the redirect in case it was not a redirect
                            // prior to this edit.
                            if (isFlaggedAsRedirect(page)) {
                                LoggingContext.log(log::atDebug, () -> "Redirect without target", sourceEvent);
                                sourceEvent.setChangeType(ChangeType.PAGE_DELETE);
                            }
                        });

        return events;
    }

    private static Boolean isFlaggedAsRedirect(Row page) {
        return Boolean.TRUE.equals(page.getFieldAs("is_redirect"));
    }

    /**
     * Maps an optional redirect target as {@link UpdateEvent}.
     *
     * @param row the source {@code page_change} event
     * @return An {@link UpdateEvent}, if {@code /page/redirect_page_link} is not {@code null}, and is
     *     not a redirect itself
     */
    private static Optional<UpdateEvent> deriveRedirectTargetUpdateEvent(Row row) {
        return Optional.ofNullable(row.<Row>getFieldAs("page"))
                .map(
                        page -> {
                            Row target = page.getFieldAs("redirect_page_link");
                            // Conditions where nothing is indexed:
                            // 1. There is no redirect
                            if (target == null
                                    // 2. It is a redirect to a redirect
                                    || isFlaggedAsRedirect(target)
                                    // 3. The redirected to page does not exist
                                    || target.getField("page_id") == null) {
                                return null;
                            }
                            Long pageNs = page.getFieldAs("namespace_id");
                            Long targetNs = target.getFieldAs("namespace_id");
                            Verify.verifyNotNull(pageNs);
                            Verify.verifyNotNull(targetNs);
                            // 4. The redirect is cross-namespace, except redirects from the main namespace
                            if (pageNs.equals(0L) || pageNs.equals(targetNs)) {
                                return target;
                            } else {
                                return null;
                            }
                        })
                .map(
                        link -> {
                            UpdateEvent targetEvent = new UpdateEvent();
                            targetEvent.setEventTime(row.getFieldAs("dt"));
                            targetEvent.setMeta(extractMeta(row));
                            targetEvent.setChangeType(ChangeType.REDIRECT_UPDATE);

                            final TargetDocument targetDocument =
                                    new TargetDocument(
                                            targetEvent.getMeta().getDomain(),
                                            row.getFieldAs("wiki_id"),
                                            link.getFieldAs("namespace_id"),
                                            link.getFieldAs("page_id"));
                            targetDocument.setPageTitle(link.getFieldAs("page_title"));
                            targetEvent.setTargetDocument(targetDocument);
                            return targetEvent;
                        });
    }

    /**
     * Identify the source of the page move and return the corresponding target document only if it is
     * not on the same namespace. Reason is that the target index might not be the same.
     */
    private TargetDocument extractPriorStateTarget(UpdateEvent sourceDoc, Row event) {
        Row priorState = event.getFieldAs("prior_state");
        if (priorState != null) {
            Row priorPage = priorState.getFieldAs("page");
            if (priorPage != null) {
                Long priorNs = priorPage.getFieldAs("namespace_id");
                if (priorNs == null) {
                    // BC code to support events created before
                    // https://gerrit.wikimedia.org/r/c/mediawiki/extensions/EventBus/+/1040228
                    return null;
                }
                String priorPageTitle = priorPage.getFieldAs("page_title");
                return sourceDoc.getTargetDocument().toBuilder()
                        .pageNamespace(priorNs)
                        .pageTitle(priorPageTitle)
                        // should not be set this early but forcibly set to null out of extra caution since we
                        // might change
                        // the namespace
                        .clusterGroup(null)
                        .indexName(null)
                        .build();
            }
        }
        return null;
    }

    public static Iterable<UpdateEvent> fromPrivatePageRerender(Row row, Predicate<String> upsert) {
        return fromPageRerender(row, upsert, true);
    }

    public static Iterable<UpdateEvent> fromPublicPageRerender(Row row, Predicate<String> upsert) {
        return fromPageRerender(row, upsert, false);
    }

    private static Iterable<UpdateEvent> fromPageRerender(
            Row row, Predicate<String> upsert, boolean fromPrivateStream) {
        checkSchema(row, PAGE_RERENDER_SCHEMA);
        if (row.getFieldAs("is_redirect")) {
            // Ignore redirects (ideally CirrusSearch should not send those)
            return emptyList();
        }
        UpdateEvent event = new UpdateEvent();
        event.setEventTime(row.getFieldAs("dt"));
        event.setMeta(extractMeta(row));
        event.setFromPrivateStream(fromPrivateStream);
        TargetDocument doc =
                new TargetDocument(
                        event.getMeta().getDomain(),
                        row.getFieldAs("wiki_id"),
                        row.getFieldAs("namespace_id"),
                        row.getFieldAs("page_id"));
        doc.setPageTitle(row.getFieldAs("page_title"));
        doc.setIndexName(row.getFieldAs(UpdateFields.INDEX_NAME));
        doc.setClusterGroup(row.getFieldAs(UpdateFields.CLUSTER_GROUP));
        event.setTargetDocument(doc);
        event.setChangeType(
                upsert.test(row.getFieldAs("reason"))
                        ? ChangeType.PAGE_RERENDER_UPSERT
                        : ChangeType.PAGE_RERENDER);
        return Collections.singletonList(event);
    }

    private static final int SCALE_FACTOR = 1000;

    private static final String TOPIC_SEPARATOR = "/";

    private static final String PROBABILITY_SEPARATOR = "|";

    private static String stringifyPrediction(String prefix, String topic, double prob) {
        return stringifyPrediction(prefix, topic, (int) (prob * SCALE_FACTOR));
    }

    private static String stringifyPrediction(String prefix, String topic, int prob) {
        checkArgument(!topic.contains(PROBABILITY_SEPARATOR), "Topic names must not contain |");
        return prefix + TOPIC_SEPARATOR + topic + PROBABILITY_SEPARATOR + prob;
    }

    private static List<String> preprocessRevisionScoring(Row row, String prefix) {
        Map<String, Row> scores = row.getFieldAs("scores");
        return scores.values().stream()
                .flatMap(
                        score -> {
                            String[] topics = score.getFieldAs("prediction");
                            Map<String, Double> probability = score.getFieldAs("probability");
                            return Arrays.stream(topics)
                                    .map(topic -> stringifyPrediction(prefix, topic, probability.get(topic)));
                        })
                .collect(Collectors.toList());
    }

    public static UpdateEvent fromRevisionScoring(Row row, String predictionPrefix) {
        checkSchema(row, REVISION_SCORE_SCHEMA);
        UpdateEvent event = new UpdateEvent();
        event.setEventTime(row.getFieldAs("dt"));
        event.setMeta(extractMeta(row));
        event.setChangeType(ChangeType.TAGS_UPDATE);
        final TargetDocument targetDocument =
                new TargetDocument(
                        event.getMeta().getDomain(),
                        row.getFieldAs("database"),
                        row.getFieldAs("page_namespace"),
                        row.getFieldAs("page_id"));
        targetDocument.setPageTitle(row.getFieldAs("page_title"));
        event.setTargetDocument(targetDocument);
        event.setRevId(row.getFieldAs("rev_id"));
        event.setUpdate(
                UpdateEvent.Update.forWeightedTags(preprocessRevisionScoring(row, predictionPrefix)));

        return event;
    }

    private static List<String> preprocessOutlinkPredictionChange(Row row, String prefix) {
        Row classification = row.getFieldAs("predicted_classification");
        String[] topics = classification.getFieldAs("predictions");
        Map<String, Double> probabilities = classification.getFieldAs("probabilities");

        return Arrays.stream(topics)
                .map(topic -> stringifyPrediction(prefix, topic, probabilities.get(topic)))
                .collect(Collectors.toList());
    }

    public static UpdateEvent fromOutlinkPredictionChange(Row row, String predictionPrefix) {
        checkSchema(row, PREDICTION_CHANGE_SCHEMA);
        UpdateEvent event = new UpdateEvent();
        event.setEventTime(row.getFieldAs("dt"));
        event.setMeta(extractMeta(row));
        event.setChangeType(ChangeType.TAGS_UPDATE);
        Row page = row.getFieldAs("page");
        final TargetDocument targetDocument =
                new TargetDocument(
                        event.getMeta().getDomain(),
                        row.getFieldAs("wiki_id"),
                        page.getFieldAs("namespace_id"),
                        page.getFieldAs("page_id"));
        targetDocument.setPageTitle(page.getFieldAs("page_title"));
        event.setTargetDocument(targetDocument);
        event.setRevId(row.<Row>getFieldAs("revision").getFieldAs("rev_id"));
        event.setUpdate(
                UpdateEvent.Update.forWeightedTags(
                        preprocessOutlinkPredictionChange(row, predictionPrefix)));

        return event;
    }

    private static List<String> preprocessRecommendationCreate(Row row) {
        String recommendationType = row.getFieldAs("recommendation_type");
        String prefix = "recommendation." + recommendationType;
        // The only information to share about recommendations is if they exist, provided a constant
        // topic and probability to match the use case.
        return Collections.singletonList(stringifyPrediction(prefix, "exists", 1));
    }

    public static UpdateEvent fromRecommendationCreate(Row row) {
        checkSchema(row, RECOMMENDATION_CREATE_SCHEMA);
        UpdateEvent event = new UpdateEvent();

        event.setChangeType(ChangeType.TAGS_UPDATE);
        // This stream does not set /dt so use meta.dt as fallback hoping that it is close to the kafka
        // timestamp
        event.setMeta(extractMeta(row));
        final TargetDocument targetDocument =
                new TargetDocument(
                        event.getMeta().getDomain(), row.getFieldAs("database"),
                        row.getFieldAs("page_namespace"), row.getFieldAs("page_id"));
        targetDocument.setPageTitle(row.getFieldAs("page_title"));
        event.setTargetDocument(targetDocument);
        event.setRevId(row.getFieldAs("rev_id"));
        event.setUpdate(UpdateEvent.Update.forWeightedTags(preprocessRecommendationCreate(row)));

        return event;
    }

    @Nonnull
    public static UpdateEvent fromPageWeightedTagsChange(Row row) {
        checkSchema(
                row, List.of(PAGE_WEIGHTED_TAGS_CHANGE_SCHEMA, PAGE_WEIGHTED_TAGS_CHANGE_LEGACY_SCHEMA));

        final Meta meta = extractMeta(row);
        final Row page = row.getFieldAs("page");
        final Row payload = row.getFieldAs("weighted_tags");
        final Map<String, Row[]> weightedTagsToBeSet = payload.getFieldAs("set");
        final List<String> encodedWeightedTags = new ArrayList<>();
        if (weightedTagsToBeSet != null) {
            extractWeightedTagsToBeSet(weightedTagsToBeSet).forEach(encodedWeightedTags::add);
        }
        final String[] prefixesToBeCleared = payload.getFieldAs("clear");
        if (prefixesToBeCleared != null) {
            Stream.of(prefixesToBeCleared)
                    .map(prefix -> encodeWeightedTag(prefix, UpdateEvent.WEIGHTED_TAGS_DELETE_GROUPING, null))
                    .forEach(encodedWeightedTags::add);
        }
        return UpdateEvent.builder()
                .meta(meta)
                .changeType(ChangeType.TAGS_UPDATE)
                .eventTime(row.getFieldAs("dt"))
                .targetDocument(
                        TargetDocument.builder()
                                .domain(meta.getDomain())
                                .wikiId(row.getFieldAs("wiki_id"))
                                .pageId(page.getFieldAs("page_id"))
                                .pageTitle(page.getFieldAs("page_title"))
                                .pageNamespace(page.getFieldAs("namespace_id"))
                                .build())
                .update(UpdateEvent.Update.forWeightedTags(encodedWeightedTags))
                .bypassDeduplication(Boolean.FALSE.equals(row.getFieldAs("rev_based")))
                .build();
    }

    @Nonnull
    private static Stream<String> extractWeightedTagsToBeSet(Map<String, Row[]> set) {
        return set.entrySet().stream()
                .flatMap(
                        entry ->
                                Stream.of(entry.getValue())
                                        .map(
                                                setTag ->
                                                        encodeWeightedTag(
                                                                entry.getKey(),
                                                                setTag.getFieldAs("tag"),
                                                                setTag.getFieldAs("score"))));
    }

    @Nonnull
    private static String encodeWeightedTag(
            @Nonnull String prefix, @Nonnull String tag, @Nullable Number score) {
        return prefix
                + "/"
                + tag
                + (score == null ? "" : "|" + ((int) (score.floatValue() * SCALE_FACTOR)));
    }

    /** Augments UpdateEvent with the cirrusdoc response from CirrusSearch API. */
    @NotNull
    private static Meta extractMeta(Row row) {
        final Row metaRow = row.getFieldAs(MetaRowTranscoder.DEFAULT_META_FIELD);

        Verify.verify(metaRow != null, "Missing meta information");

        return META_TRANSCODER.decode(metaRow);
    }
}
