package org.wikimedia.discovery.cirrus.updater.producer.config;

import org.apache.flink.types.Row;
import org.apache.flink.util.function.SerializableFunction;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateEvent;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;

@AllArgsConstructor
@Builder
@Data
public class UpdateEventSource {

    @NonNull private final String streamName;
    @NonNull private final SerializableFunction<Row, Iterable<UpdateEvent>> converter;

    @Accessors(fluent = true)
    @Builder.Default
    /**
     * Set to true to use the kafka message timestamp as the event-time. By default, the /dt field is
     * used.
     */
    private final boolean useKafkaTimestamp = false;
}
