package org.wikimedia.discovery.cirrus.updater.producer.graph;

import java.io.IOException;

import org.apache.flink.test.junit5.MiniClusterExtension;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.wikimedia.discovery.cirrus.updater.common.config.ParameterToolMerger;
import org.wikimedia.discovery.cirrus.updater.common.model.EventDataStreamUtilities;
import org.wikimedia.discovery.cirrus.updater.producer.config.ProducerConfig;

@ExtendWith(MiniClusterExtension.class)
class ProducerGraphFactoryTest {
    @Test
    void createStreamGraph() throws IOException {
        ProducerConfig.builder().build();
        final org.apache.flink.api.java.utils.ParameterTool params =
                ParameterToolMerger.fromDefaultsWithOverrides(
                        new String[] {
                            ProducerGraphFactoryTest.class
                                    .getResource(
                                            ProducerGraphFactoryTest.class.getSimpleName() + "/overrides.properties")
                                    .toString(),
                            "--event-stream-config-url",
                            EventDataStreamUtilities.STREAM_CONFIG,
                            "--event-stream-json-schema-urls",
                            EventDataStreamUtilities.SCHEMA_REPO,
                            "--topic-prefix-filter",
                            "eqiad",
                            "--pipeline.name",
                            "eqiad.test.producer",
                        });
        final ProducerConfig config = ProducerConfig.of(params.getConfiguration());
        Assertions.assertThatNoException()
                .isThrownBy(
                        () -> {
                            try (ProducerGraphFactory factory = new ProducerGraphFactory(config)) {
                                factory.prepareEnvironment();
                            }
                        });
    }
}
