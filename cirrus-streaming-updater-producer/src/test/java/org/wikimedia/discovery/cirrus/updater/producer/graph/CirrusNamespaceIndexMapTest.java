package org.wikimedia.discovery.cirrus.updater.producer.graph;

import static org.assertj.core.api.Assertions.assertThat;
import static org.wikimedia.discovery.cirrus.updater.common.wiremock.WireMockExtensionUtilities.getWireMockExtension;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Optional;

import org.apache.flink.streaming.api.operators.StreamMap;
import org.apache.flink.streaming.util.OneInputStreamOperatorTestHarness;
import org.apache.hc.core5.http.HttpHost;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Timeout;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.wikimedia.discovery.cirrus.updater.common.http.CustomRoutePlanner;
import org.wikimedia.discovery.cirrus.updater.common.http.HttpConfig;
import org.wikimedia.discovery.cirrus.updater.common.http.HttpHostMapping;
import org.wikimedia.discovery.cirrus.updater.common.http.MediaWikiHttpClient;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateEvent;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateEvent.Meta;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateEvent.TargetDocument;
import org.wikimedia.discovery.cirrus.updater.common.util.function.SerializableFunction;

import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.http.Fault;
import com.github.tomakehurst.wiremock.junit5.WireMockExtension;
import com.github.tomakehurst.wiremock.junit5.WireMockRuntimeInfo;

class CirrusNamespaceIndexMapTest {

    public static final String API_CALL_URL =
            "/w/api.php?action=cirrus-config-dump&prop=replicagroup%7Cnamespacemap&format=json&formatversion=2";
    static final String DOMAIN = "testwiki.local";
    static final String OTHER_DOMAIN = "otherwiki.local";
    static final String OTHER_WIKI = "otherwiki";
    static final int TIMEOUT_MILLIS = 1000;
    static final String WIKI = "testwiki";
    @RegisterExtension static WireMockExtension wireMockExtension = getWireMockExtension();
    private CirrusNamespaceIndexMap mapper;
    private OneInputStreamOperatorTestHarness<UpdateEvent, UpdateEvent> testHarness;

    @BeforeEach
    void createMapper(WireMockRuntimeInfo info) throws Exception {
        Duration requestTimeout = Duration.of(TIMEOUT_MILLIS, ChronoUnit.MILLIS);
        Duration connectionTimeout = Duration.of(TIMEOUT_MILLIS / 2, ChronoUnit.MILLIS);
        SerializableFunction<HttpHost, Optional<HttpHost>> httpHostMapper =
                CustomRoutePlanner.createHttpHostMapper(
                        List.of(
                                HttpHostMapping.create(DOMAIN, info.getHttpBaseUrl()),
                                HttpHostMapping.create(OTHER_DOMAIN, info.getHttpBaseUrl())));
        mapper =
                new CirrusNamespaceIndexMap(
                        MediaWikiHttpClient.factory(
                                HttpConfig.builder()
                                        .maxConnections(10)
                                        .requestTimeout(requestTimeout)
                                        .connectionTimeout(connectionTimeout)
                                        .httpHostMapper(httpHostMapper)
                                        .userAgent("Test/User Agent")
                                        .rateLimitPerSecond(100)
                                        .build()));
        testHarness = new OneInputStreamOperatorTestHarness<>(new StreamMap<>(mapper));
        testHarness.open();
    }

    UpdateEvent createOtherWikiUpdateEvent(long namespace) {
        return UpdateEvent.builder()
                .meta(Meta.builder().requestId("42").build())
                .targetDocument(
                        TargetDocument.builder()
                                .domain(OTHER_DOMAIN)
                                .wikiId(OTHER_WIKI)
                                .pageNamespace(namespace)
                                .pageId(1L)
                                .build())
                .build();
    }

    UpdateEvent createTestWikiUpdateEvent(long namespace) {
        return UpdateEvent.builder()
                .meta(Meta.builder().requestId("42").build())
                .targetDocument(
                        TargetDocument.builder()
                                .domain(DOMAIN)
                                .wikiId(WIKI)
                                .pageNamespace(namespace)
                                .pageId(1L)
                                .build())
                .build();
    }

    void happyPathScenario() {
        wireMockExtension.stubFor(
                WireMock.get(WireMock.urlEqualTo(API_CALL_URL))
                        .withHost(WireMock.equalTo(DOMAIN))
                        .inScenario("happy_path")
                        .willReturn(
                                WireMock.aResponse().withBodyFile("testwiki-namespace-map.json").withStatus(200)));
    }

    void retryScenario() {
        wireMockExtension.stubFor(
                WireMock.get(WireMock.urlEqualTo(API_CALL_URL))
                        .withHost(WireMock.equalTo(DOMAIN))
                        .inScenario("retry")
                        .willSetStateTo("after first failure")
                        .willReturn(WireMock.aResponse().withStatus(429)));

        wireMockExtension.stubFor(
                WireMock.get(WireMock.urlEqualTo(API_CALL_URL))
                        .withHost(WireMock.equalTo(DOMAIN))
                        .inScenario("retry")
                        .whenScenarioStateIs("after first failure")
                        .willSetStateTo("after second failure")
                        .willReturn(WireMock.aResponse().withFault(Fault.RANDOM_DATA_THEN_CLOSE)));

        wireMockExtension.stubFor(
                WireMock.get(WireMock.urlEqualTo(API_CALL_URL))
                        .withHost(WireMock.equalTo(DOMAIN))
                        .inScenario("retry")
                        .whenScenarioStateIs("after second failure")
                        .willSetStateTo("after third failure")
                        .willReturn(WireMock.aResponse().withFault(Fault.CONNECTION_RESET_BY_PEER)));

        wireMockExtension.stubFor(
                WireMock.get(WireMock.urlEqualTo(API_CALL_URL))
                        .withHost(WireMock.equalTo(DOMAIN))
                        .inScenario("retry")
                        .whenScenarioStateIs("after third failure")
                        .willSetStateTo("after fourth failure")
                        .willReturn(WireMock.aResponse().withFault(Fault.EMPTY_RESPONSE)));

        wireMockExtension.stubFor(
                WireMock.get(WireMock.urlEqualTo(API_CALL_URL))
                        .withHost(WireMock.equalTo(DOMAIN))
                        .inScenario("retry")
                        .whenScenarioStateIs("after fourth failure")
                        .willSetStateTo("after fourth failure")
                        .willReturn(
                                WireMock.aResponse().withBodyFile("testwiki-namespace-map.json").withStatus(200)));
    }

    @Test
    void test_call_is_cached() {
        happyPathScenario();
        mapper.map(createTestWikiUpdateEvent(0));
        mapper.map(createTestWikiUpdateEvent(0));
        verifyNbCall(1, DOMAIN);
    }

    @Test
    void test_happy_path() {
        happyPathScenario();
        UpdateEvent.TargetDocument t = mapper.map(createTestWikiUpdateEvent(0)).getTargetDocument();
        assertThat(t.getIndexName()).isEqualTo("testwiki_content");
        assertThat(t.getClusterGroup()).isEqualTo("omega");
        verifyNbCall(1, DOMAIN);
    }

    @Test
    void test_no_api_call_is_made_when_everything_is_provided() {
        UpdateEvent updateEvent = createTestWikiUpdateEvent(0);
        updateEvent.getTargetDocument().completeWith("random_replica", "random_index_name");
        UpdateEvent.TargetDocument t = mapper.map(updateEvent).getTargetDocument();

        assertThat(t.getIndexName()).isEqualTo("random_index_name");
        assertThat(t.getClusterGroup()).isEqualTo("random_replica");
        verifyNbCall(0, DOMAIN);
    }

    @Test
    @Timeout(5)
    void test_retry() {
        retryScenario();
        UpdateEvent.TargetDocument t = mapper.map(createTestWikiUpdateEvent(0)).getTargetDocument();

        assertThat(t.getIndexName()).isEqualTo("testwiki_content");
        assertThat(t.getClusterGroup()).isEqualTo("omega");
        verifyNbCall(5, DOMAIN);
    }

    @Test
    void test_two_wikis() {
        twoWikisScenario();
        UpdateEvent.TargetDocument t = mapper.map(createTestWikiUpdateEvent(0)).getTargetDocument();
        assertThat(t.getIndexName()).isEqualTo("testwiki_content");
        assertThat(t.getClusterGroup()).isEqualTo("omega");

        t = mapper.map(createOtherWikiUpdateEvent(6)).getTargetDocument();
        assertThat(t.getIndexName()).isEqualTo("otherwiki_file");
        assertThat(t.getClusterGroup()).isEqualTo("psi");
        verifyNbCall(1, DOMAIN);
        verifyNbCall(1, OTHER_DOMAIN);
    }

    @Test
    void test_unknown_namespace() {
        happyPathScenario();
        UpdateEvent.TargetDocument t = mapper.map(createTestWikiUpdateEvent(10000)).getTargetDocument();
        assertThat(t.getIndexName()).isEqualTo("testwiki_general");
        assertThat(t.getClusterGroup()).isEqualTo("omega");
        verifyNbCall(1, DOMAIN);
    }

    @Test
    @Timeout(5)
    void test_unrecoverable_failure() {
        unrecoverableFailureScenario();
        TargetDocument t = mapper.map(createOtherWikiUpdateEvent(6)).getTargetDocument();
        assertThat(t.isComplete()).isFalse();
        verifyNbCall(5, OTHER_DOMAIN);
    }

    @Test
    @Timeout(5)
    void test_unrecoverable_wiki_error() {
        unrecoverableWikiError();
        TargetDocument t = mapper.map(createOtherWikiUpdateEvent(6)).getTargetDocument();
        assertThat(t.isComplete()).isFalse();
        verifyNbCall(5, OTHER_DOMAIN);
    }

    void twoWikisScenario() {
        wireMockExtension.stubFor(
                WireMock.get(WireMock.urlEqualTo(API_CALL_URL))
                        .withHost(WireMock.equalTo(DOMAIN))
                        .inScenario("two_wikis")
                        .willReturn(
                                WireMock.aResponse().withBodyFile("testwiki-namespace-map.json").withStatus(200)));

        wireMockExtension.stubFor(
                WireMock.get(WireMock.urlEqualTo(API_CALL_URL))
                        .withHost(WireMock.equalTo(OTHER_DOMAIN))
                        .inScenario("two_wikis")
                        .willReturn(
                                WireMock.aResponse().withBodyFile("otherwiki-namespace-map.json").withStatus(200)));
    }

    void unrecoverableFailureScenario() {
        wireMockExtension.stubFor(
                WireMock.get(WireMock.urlEqualTo(API_CALL_URL))
                        .withHost(WireMock.equalTo(DOMAIN))
                        .inScenario("timeout")
                        .willReturn(
                                WireMock.aResponse().withBodyFile("otherwiki-namespace-map.json").withStatus(429)));
    }

    void unrecoverableWikiError() {
        wireMockExtension.stubFor(
                WireMock.get(WireMock.urlEqualTo(API_CALL_URL))
                        .withHost(WireMock.equalTo(DOMAIN))
                        .inScenario("unrecoverable_wiki_error")
                        .willReturn(
                                WireMock.aResponse()
                                        .withBody("{\"error\": \"misconfigured wiki\"}")
                                        .withStatus(200)));
    }

    void verifyNbCall(int nbCall, String hostname) {
        wireMockExtension.verify(
                WireMock.exactly(nbCall),
                WireMock.getRequestedFor(WireMock.urlEqualTo(API_CALL_URL))
                        .withHeader("host", WireMock.equalTo(hostname)));
    }
}
