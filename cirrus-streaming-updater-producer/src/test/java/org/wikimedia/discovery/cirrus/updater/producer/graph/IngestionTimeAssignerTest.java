package org.wikimedia.discovery.cirrus.updater.producer.graph;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.Instant;

import org.apache.flink.util.InstantiationUtil;
import org.junit.jupiter.api.Test;
import org.wikimedia.discovery.cirrus.updater.common.graph.IngestionTimeAssigner;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateEvent;

class IngestionTimeAssignerTest {
    @Test
    void test_map() {
        Instant actualIngestionTime = Instant.now();
        IngestionTimeAssigner assigner = new IngestionTimeAssigner(() -> actualIngestionTime);
        UpdateEvent event = assigner.map(new UpdateEvent());
        assertThat(event.getIngestionTime()).isEqualTo(actualIngestionTime);
    }

    @Test
    void test_serializable() {
        assertThat(InstantiationUtil.isSerializable(new IngestionTimeAssigner(Instant::now))).isTrue();
    }
}
