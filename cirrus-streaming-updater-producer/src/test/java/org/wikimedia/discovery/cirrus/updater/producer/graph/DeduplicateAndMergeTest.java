package org.wikimedia.discovery.cirrus.updater.producer.graph;

import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Sets.newHashSet;
import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.StreamSupport;

import org.apache.flink.api.common.ExecutionConfig;
import org.apache.flink.api.common.state.ListStateDescriptor;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.streaming.api.windowing.assigners.TumblingProcessingTimeWindows;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.triggers.ProcessingTimeTrigger;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.streaming.runtime.operators.windowing.WindowOperator;
import org.apache.flink.streaming.runtime.operators.windowing.functions.InternalIterableProcessWindowFunction;
import org.apache.flink.streaming.runtime.streamrecord.StreamRecord;
import org.apache.flink.streaming.util.KeyedOneInputStreamOperatorTestHarness;
import org.apache.flink.streaming.util.TestHarnessUtil;
import org.apache.flink.types.Row;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.wikimedia.discovery.cirrus.updater.common.model.ChangeType;
import org.wikimedia.discovery.cirrus.updater.common.model.EventDataStreamUtilities;
import org.wikimedia.discovery.cirrus.updater.common.model.PageKey;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateEvent;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateFields;
import org.wikimedia.eventutilities.flink.EventRowTypeInfo;
import org.wikimedia.eventutilities.flink.stream.EventDataStreamFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.google.common.collect.ImmutableMap;

class DeduplicateAndMergeTest {
    static final String TEST_RESOURCE_PATH =
            "/" + DeduplicateAndMergeTest.class.getCanonicalName().replace(".", "/");
    private static final EventRowTypeInfo EVENT_ROW_TYPE_INFO =
            EventDataStreamUtilities.buildUpdateTypeInfo();
    static final Row MOCK_RAW_FIELDS = EVENT_ROW_TYPE_INFO.createEmptySubRow(UpdateFields.FIELDS);
    static final UpdateEvent.TargetDocument MOCK_REDIRECT_TARGET_DOCUMENT_A =
            new UpdateEvent.TargetDocument("domain", "wikiId", 0L, 2L);
    static final UpdateEvent.TargetDocument MOCK_REDIRECT_TARGET_DOCUMENT_B =
            new UpdateEvent.TargetDocument("domain", "wikiId", 0L, 3L);

    private final EventDataStreamFactory factory =
            EventDataStreamFactory.from(
                    singletonList(DeduplicateAndMergeTest.class.getResource("/schema_repo").toString()),
                    this.getClass().getResource("/event-stream-config.json").toString());

    UpdateEvent.TargetDocument target = new UpdateEvent.TargetDocument("domain", "wikiId", 0L, 1L);
    Instant eventTime = Instant.EPOCH;

    private KeyedOneInputStreamOperatorTestHarness<PageKey, UpdateEvent, UpdateEvent> testHarness;
    private DeduplicateAndMerge<TimeWindow> dedupAndMerger;

    @Test
    void doesNothingWithOneEvent() throws Exception {
        List<UpdateEvent> updates =
                newArrayList(
                        newUpdateEvent(
                                ChangeType.TAGS_UPDATE,
                                1L,
                                UpdateEvent.Update.forWeightedTags(newArrayList("foo/bar|1"))));
        List<UpdateEvent> results = process(updates);
        assertThat(results).containsExactlyInAnyOrderElementsOf(updates);
    }

    @Test
    void mergesTwoWeightedTags() throws Exception {
        List<UpdateEvent> results =
                process(
                        newArrayList(
                                newUpdateEvent(
                                        ChangeType.TAGS_UPDATE,
                                        1L,
                                        UpdateEvent.Update.forWeightedTags(newArrayList("foo/exists|1"))),
                                newUpdateEvent(
                                        ChangeType.TAGS_UPDATE,
                                        1L,
                                        UpdateEvent.Update.forWeightedTags(newArrayList("bar/exists|1")))));
        assertThat(results).hasSize(1);
        assertThat(results.get(0).getUpdate().getWeightedTags())
                .containsExactlyInAnyOrder("foo/exists|1", "bar/exists|1");
    }

    @Test
    void mergesManyWeightedTags() throws Exception {
        List<UpdateEvent> results =
                process(
                        newArrayList(
                                newUpdateEvent(
                                        ChangeType.TAGS_UPDATE,
                                        1L,
                                        UpdateEvent.Update.forWeightedTags(newArrayList("exists|1"))),
                                newUpdateEvent(
                                        ChangeType.TAGS_UPDATE,
                                        1L,
                                        UpdateEvent.Update.forWeightedTags(newArrayList("bar/a|1", "bar/b|1"))),
                                newUpdateEvent(
                                        ChangeType.TAGS_UPDATE,
                                        1L,
                                        UpdateEvent.Update.forWeightedTags(newArrayList("qqq/q|2")))));
        assertThat(results).hasSize(1);
        assertThat(results.get(0).getUpdate().getWeightedTags())
                .containsExactlyInAnyOrder("exists|1", "bar/a|1", "bar/b|1", "qqq/q|2");
    }

    @Test
    void newestTagWinsWithOverlappingWeightedTagsForSameRevision() throws Exception {
        List<UpdateEvent> updates =
                newArrayList(
                        newUpdateEvent(
                                ChangeType.TAGS_UPDATE,
                                1L,
                                UpdateEvent.Update.forWeightedTags(newArrayList("foo/a|1"))),
                        newUpdateEvent(
                                ChangeType.TAGS_UPDATE,
                                1L,
                                UpdateEvent.Update.forWeightedTags(newArrayList("foo/b|1"))));
        List<UpdateEvent> results = process(updates);
        assertThat(results).containsExactly(updates.get(1));
        assertMetrics(updates.size(), results.size(), 1, 0);
    }

    @Test
    void mergesWeightedTagsIntoUpdates() throws Exception {
        List<String> weightedTags = newArrayList("foo/exists|1");
        List<UpdateEvent> updates =
                newArrayList(
                        newUpdateEvent(ChangeType.REV_BASED_UPDATE, 1L, null),
                        newUpdateEvent(
                                ChangeType.TAGS_UPDATE, 1L, UpdateEvent.Update.forWeightedTags(weightedTags)));

        List<UpdateEvent> results = process(updates);
        assertThat(results).hasSize(1);
        assertThat(results.get(0).getChangeType()).isEqualTo(ChangeType.REV_BASED_UPDATE);
        assertThat(results.get(0).getUpdate().getWeightedTags())
                .containsExactlyInAnyOrderElementsOf(weightedTags);
        assertThat(results.get(0).getUpdate().getRawFields()).isNull();
        assertMetrics(updates.size(), results.size(), 1, 0);
    }

    @Test
    void mergesWeightedTagsForOlderRevisionIntoUpdates() throws Exception {
        List<String> weightedTags = newArrayList("foo/exists|1");
        List<UpdateEvent> updates =
                newArrayList(
                        newUpdateEvent(
                                ChangeType.TAGS_UPDATE, 1L, UpdateEvent.Update.forWeightedTags(weightedTags)),
                        newUpdateEvent(ChangeType.REV_BASED_UPDATE, 2L, null));
        List<UpdateEvent> results = process(updates);
        assertThat(results).hasSize(1);
        assertThat(results.get(0).getUpdate().getWeightedTags())
                .containsExactlyInAnyOrderElementsOf(weightedTags);
        assertMetrics(updates.size(), results.size(), 1, 0);
    }

    @Test
    void dropsOverlappingWeightedTagsForOlderRevision() throws Exception {
        List<UpdateEvent> updates =
                newArrayList(
                        newUpdateEvent(
                                ChangeType.TAGS_UPDATE,
                                1L,
                                UpdateEvent.Update.forWeightedTags(newArrayList("foo/a|1"))),
                        newUpdateEvent(
                                ChangeType.TAGS_UPDATE,
                                2L,
                                UpdateEvent.Update.forWeightedTags(newArrayList("foo/b|1"))));
        List<UpdateEvent> results = process(updates);
        assertThat(results).containsExactly(updates.get(1));

        // Same, but with reversed event time ordering
        updates =
                newArrayList(
                        newUpdateEvent(
                                ChangeType.TAGS_UPDATE,
                                2L,
                                UpdateEvent.Update.forWeightedTags(newArrayList("foo/b|1"))),
                        newUpdateEvent(
                                ChangeType.TAGS_UPDATE,
                                1L,
                                UpdateEvent.Update.forWeightedTags(newArrayList("foo/a|1"))));
        this.createTestHarness();
        results = process(updates);
        assertThat(results).containsExactly(updates.get(0));
        assertMetrics(updates.size(), results.size(), 1, 0);
    }

    @Test
    void mergesMultipleWeightedTagsIntoUpdates() throws Exception {
        List<UpdateEvent> updates =
                newArrayList(
                        newUpdateEvent(ChangeType.REV_BASED_UPDATE, 1L, null),
                        newUpdateEvent(
                                ChangeType.TAGS_UPDATE,
                                1L,
                                UpdateEvent.Update.forWeightedTags(newArrayList("foo/exists|1"))),
                        newUpdateEvent(
                                ChangeType.TAGS_UPDATE,
                                1L,
                                UpdateEvent.Update.forWeightedTags(newArrayList("bar/exists|1"))));
        List<UpdateEvent> results = process(updates);
        assertThat(results).hasSize(1);
        assertThat(results.get(0).getChangeType()).isEqualTo(ChangeType.REV_BASED_UPDATE);
        assertThat(results.get(0).getUpdate().getWeightedTags())
                .containsExactlyInAnyOrder("foo/exists|1", "bar/exists|1");
        assertThat(results.get(0).getUpdate().getRawFields()).isNull();
        assertMetrics(updates.size(), results.size(), 2, 0);
    }

    @Test
    void emitsHighestRevisionForMultipleRevisions() throws Exception {
        List<UpdateEvent> updates =
                newArrayList(
                        newUpdateEvent(ChangeType.REV_BASED_UPDATE, 1L, null),
                        newUpdateEvent(ChangeType.REV_BASED_UPDATE, 2L, null));
        List<UpdateEvent> results = process(updates);
        assertThat(results).containsExactly(updates.get(1));

        // ingesting in reverse of event time shouldn't matter
        Collections.reverse(updates);

        this.createTestHarness();
        results = process(updates);
        assertThat(results).containsExactly(updates.get(0));

        // the older revision having a newer event time shouldn't matter
        updates =
                newArrayList(
                        newUpdateEvent(ChangeType.REV_BASED_UPDATE, 2L, null),
                        newUpdateEvent(ChangeType.REV_BASED_UPDATE, 1L, null));
        this.createTestHarness();
        results = process(updates);
        assertThat(results).containsExactly(updates.get(0));
        assertMetrics(updates.size(), results.size(), 0, 1);
    }

    @Test
    void emitsMostRecentEventForSameRevision() throws Exception {
        List<UpdateEvent> updates =
                newArrayList(
                        // note that event time increases on each call to newInputEvent
                        newUpdateEvent(ChangeType.REV_BASED_UPDATE, 1L, null),
                        newUpdateEvent(ChangeType.REV_BASED_UPDATE, 1L, null));

        List<UpdateEvent> results = process(updates);
        assertThat(results).containsExactly(updates.get(1));
        // reset test state
        this.createTestHarness();

        // ingesting in reverse of event time shouldn't matter
        Collections.reverse(updates);
        results = process(updates);
        assertThat(results).containsExactly(updates.get(0));
        assertMetrics(updates.size(), results.size(), 0, 1);
    }

    @Test
    void dedpulicatesRevBasedUpdates() throws Exception {
        List<UpdateEvent> updates =
                newArrayList(
                        newUpdateEvent(ChangeType.REV_BASED_UPDATE, 1L, null),
                        newUpdateEvent(ChangeType.REV_BASED_UPDATE, 1L, null));
        List<UpdateEvent> results = process(updates);
        assertThat(results).containsExactly(updates.get(1));
        assertMetrics(updates.size(), results.size(), 0, 1);
    }

    @Test
    void passesThroughDeletes() throws Exception {
        List<UpdateEvent> updates = newArrayList(newUpdateEvent(ChangeType.PAGE_DELETE, 1L, null));
        List<UpdateEvent> results = process(updates);
        assertThat(results).containsExactlyInAnyOrderElementsOf(updates);
        assertMetrics(updates.size(), results.size(), 0, 0);
    }

    @Test
    void dropsTagUpdatesWhenDeleted() throws Exception {
        List<UpdateEvent> updates =
                newArrayList(
                        newUpdateEvent(
                                ChangeType.TAGS_UPDATE,
                                1L,
                                UpdateEvent.Update.forWeightedTags(newArrayList("example/a|1"))),
                        newUpdateEvent(ChangeType.PAGE_DELETE, 2L, null),
                        newUpdateEvent(
                                ChangeType.TAGS_UPDATE,
                                1L,
                                UpdateEvent.Update.forWeightedTags(newArrayList("other/a|1"))));
        List<UpdateEvent> results = process(updates);
        assertThat(results).containsExactly(updates.get(1));
        assertMetrics(updates.size(), results.size(), 2, 0);
    }

    @Test
    void dropsOldRevUpdatesWhenDeleted() throws Exception {
        List<UpdateEvent> updates =
                newArrayList(
                        newUpdateEvent(ChangeType.REV_BASED_UPDATE, 1L, null),
                        newUpdateEvent(ChangeType.PAGE_DELETE, 2L, null));
        List<UpdateEvent> results = process(updates);
        assertThat(results).containsExactly(updates.get(1));
        assertMetrics(updates.size(), results.size(), 0, 1);
    }

    @Test
    void dropsDeletePriorToUndelete() throws Exception {
        List<UpdateEvent> updates =
                newArrayList(
                        newUpdateEvent(ChangeType.PAGE_DELETE, 1L, null),
                        newUpdateEvent(ChangeType.REV_BASED_UPDATE, 1L, null));
        List<UpdateEvent> results = process(updates);
        assertThat(results).containsExactly(updates.get(1));
        assertMetrics(updates.size(), results.size(), 0, 1);
    }

    @Test
    void deduplicatesDeletes() throws Exception {
        List<UpdateEvent> updates =
                newArrayList(
                        newUpdateEvent(ChangeType.PAGE_DELETE, 1L, null),
                        newUpdateEvent(ChangeType.PAGE_DELETE, 1L, null));
        List<UpdateEvent> results = process(updates);
        assertThat(results).containsExactly(updates.get(1));
        assertMetrics(updates.size(), results.size(), 0, 1);
    }

    @Test
    void mergesHintsWhenEqual() throws Exception {
        Map<String, String> hint = new HashMap<>();
        hint.put("field", "value");
        hint.put("weighted_tags", "multilist");

        List<UpdateEvent> updates =
                newArrayList(
                        newUpdateEvent(
                                ChangeType.REV_BASED_UPDATE,
                                1L,
                                UpdateEvent.Update.builder().noopHints(hint).rawFields(MOCK_RAW_FIELDS).build()),
                        newUpdateEvent(
                                ChangeType.TAGS_UPDATE,
                                1L,
                                UpdateEvent.Update.forWeightedTags(newArrayList("example/a|1"))));
        List<UpdateEvent> outputs = process(updates);
        assertThat(outputs)
                .extracting(r -> r.getUpdate().getNoopHints())
                .containsExactlyInAnyOrder(hint);
        assertMetrics(updates.size(), outputs.size(), 1, 0);
    }

    @Test
    void mergesNonOverlapingHints() throws Exception {
        // TODO: Not clear this is necessary anymore, with fetch moved to
        // post-merge we will only ever seen weighted tags here.
        List<UpdateEvent> updates =
                newArrayList(
                        newUpdateEvent(
                                ChangeType.REV_BASED_UPDATE,
                                1L,
                                UpdateEvent.Update.builder()
                                        .noopHints(Collections.singletonMap("field", "value"))
                                        .rawFields(MOCK_RAW_FIELDS)
                                        .build()),
                        newUpdateEvent(
                                ChangeType.TAGS_UPDATE,
                                1L,
                                UpdateEvent.Update.forWeightedTags(newArrayList("foo/a|1"))));
        List<UpdateEvent> outputs = process(updates);
        assertThat(outputs)
                .extracting(r -> r.getUpdate().getNoopHints())
                .containsExactlyInAnyOrder(
                        ImmutableMap.of(
                                "field", "value",
                                "weighted_tags", "multilist"));
        assertMetrics(updates.size(), outputs.size(), 1, 0);
    }

    @Test
    void mergesRedirectAdd() throws Exception {
        List<UpdateEvent> updates =
                newArrayList(
                        newUpdateEvent(ChangeType.REV_BASED_UPDATE, 1L, null),
                        newUpdateEvent(
                                ChangeType.REDIRECT_UPDATE,
                                1L,
                                UpdateEvent.Update.forRedirect(
                                        newHashSet(MOCK_REDIRECT_TARGET_DOCUMENT_A), newHashSet())));
        List<UpdateEvent> outputs = process(updates);
        assertThat(outputs).hasSize(1);
        final UpdateEvent merged = outputs.get(0);
        assertThat(merged.getUpdate().getNoopHints())
                .containsAllEntriesOf(ImmutableMap.of("redirect", "set"));
        assertThat(merged.getUpdate().getRedirectAdd()).contains(MOCK_REDIRECT_TARGET_DOCUMENT_A);
        assertThat(merged.getUpdate().getRedirectRemove()).isNull();
    }

    @Test
    void mergesRedirectRemove() throws Exception {
        List<UpdateEvent> updates =
                newArrayList(
                        newUpdateEvent(ChangeType.REV_BASED_UPDATE, 1L, null),
                        newUpdateEvent(
                                ChangeType.REDIRECT_UPDATE,
                                1L,
                                UpdateEvent.Update.forRedirect(
                                        newHashSet(), newHashSet(MOCK_REDIRECT_TARGET_DOCUMENT_B))));
        List<UpdateEvent> outputs = process(updates);
        assertThat(outputs).hasSize(1);
        final UpdateEvent merged = outputs.get(0);
        assertThat(merged.getUpdate().getNoopHints())
                .containsAllEntriesOf(ImmutableMap.of("redirect", "set"));
        assertThat(merged.getUpdate().getRedirectAdd()).isNull();
        assertThat(merged.getUpdate().getRedirectRemove()).contains(MOCK_REDIRECT_TARGET_DOCUMENT_B);
        assertMetrics(updates.size(), outputs.size(), 1, 0);
    }

    @Test
    void mergesRedirectChanges() throws Exception {
        List<UpdateEvent> updates =
                newArrayList(
                        newUpdateEvent(ChangeType.REV_BASED_UPDATE, 1L, null),
                        newUpdateEvent(
                                ChangeType.REDIRECT_UPDATE,
                                1L,
                                UpdateEvent.Update.forRedirect(
                                        newHashSet(MOCK_REDIRECT_TARGET_DOCUMENT_A), newHashSet())),
                        newUpdateEvent(
                                ChangeType.REDIRECT_UPDATE,
                                1L,
                                UpdateEvent.Update.forRedirect(
                                        newHashSet(), newHashSet(MOCK_REDIRECT_TARGET_DOCUMENT_B))));
        List<UpdateEvent> outputs = process(updates);
        assertThat(outputs).hasSize(1);
        final UpdateEvent merged = outputs.get(0);
        assertThat(merged.getUpdate().getNoopHints())
                .containsAllEntriesOf(ImmutableMap.of("redirect", "set"));
        assertThat(merged.getUpdate().getRedirectAdd()).contains(MOCK_REDIRECT_TARGET_DOCUMENT_A);
        assertThat(merged.getUpdate().getRedirectRemove()).contains(MOCK_REDIRECT_TARGET_DOCUMENT_B);
        assertMetrics(updates.size(), outputs.size(), 2, 0);
    }

    @Test
    void mergesRedirectChangesRespectingEventTime() throws Exception {
        List<UpdateEvent> updates =
                newArrayList(
                        newUpdateEvent(ChangeType.REV_BASED_UPDATE, 1L, null),
                        newUpdateEvent(
                                ChangeType.REDIRECT_UPDATE,
                                1L,
                                UpdateEvent.Update.forRedirect(
                                        newHashSet(MOCK_REDIRECT_TARGET_DOCUMENT_A),
                                        newHashSet(MOCK_REDIRECT_TARGET_DOCUMENT_B))),
                        newUpdateEvent(
                                ChangeType.REDIRECT_UPDATE,
                                1L,
                                UpdateEvent.Update.forRedirect(
                                        newHashSet(MOCK_REDIRECT_TARGET_DOCUMENT_B),
                                        newHashSet(MOCK_REDIRECT_TARGET_DOCUMENT_A))));
        List<UpdateEvent> outputs = process(updates);
        assertThat(outputs).hasSize(1);
        final UpdateEvent merged = outputs.get(0);
        assertThat(merged.getUpdate().getNoopHints())
                .containsAllEntriesOf(ImmutableMap.of("redirect", "set"));
        assertThat(merged.getUpdate().getRedirectAdd()).containsOnly(MOCK_REDIRECT_TARGET_DOCUMENT_B);
        assertThat(merged.getUpdate().getRedirectRemove())
                .containsOnly(MOCK_REDIRECT_TARGET_DOCUMENT_A);
        assertMetrics(updates.size(), outputs.size(), 2, 0);
    }

    @Test
    void mergesRedirectChangesRespectingRevision() throws Exception {
        List<UpdateEvent> updates =
                newArrayList(
                        newUpdateEvent(ChangeType.REV_BASED_UPDATE, 2L, null),
                        newUpdateEvent(
                                ChangeType.REDIRECT_UPDATE,
                                2L,
                                UpdateEvent.Update.forRedirect(
                                        newHashSet(MOCK_REDIRECT_TARGET_DOCUMENT_A),
                                        newHashSet(MOCK_REDIRECT_TARGET_DOCUMENT_B))),
                        newUpdateEvent(
                                ChangeType.REDIRECT_UPDATE,
                                1L,
                                UpdateEvent.Update.forRedirect(
                                        newHashSet(MOCK_REDIRECT_TARGET_DOCUMENT_B),
                                        newHashSet(MOCK_REDIRECT_TARGET_DOCUMENT_A))));
        List<UpdateEvent> outputs = process(updates);
        assertThat(outputs).hasSize(1);
        final UpdateEvent merged = outputs.get(0);
        assertThat(merged.getUpdate().getNoopHints())
                .containsAllEntriesOf(ImmutableMap.of("redirect", "set"));
        assertThat(merged.getUpdate().getRedirectAdd()).containsOnly(MOCK_REDIRECT_TARGET_DOCUMENT_A);
        assertThat(merged.getUpdate().getRedirectRemove())
                .containsOnly(MOCK_REDIRECT_TARGET_DOCUMENT_B);
        assertMetrics(updates.size(), outputs.size(), 2, 0);
    }

    @Test
    void dedupPageRerenders() throws Exception {
        List<UpdateEvent> updates = newArrayList(newRerender(), newRerender());
        List<UpdateEvent> output = process(updates);
        assertThat(output).hasSize(1);
        assertMetrics(updates.size(), output.size(), 0, 1);
    }

    @Test
    void dedupPageRerendersWithUpsert() throws Exception {
        List<UpdateEvent> updates = newArrayList(newRerender(true), newRerender());
        List<UpdateEvent> output = process(updates);
        assertThat(output).hasSize(1);
        assertThat(output.get(0).getChangeType()).isEqualTo(ChangeType.PAGE_RERENDER_UPSERT);
        assertMetrics(updates.size(), output.size(), 0, 1);
    }

    @Test
    void dedupSinglePageRerendersWithUpsert() throws Exception {
        List<UpdateEvent> updates = newArrayList(newRerender(true));
        List<UpdateEvent> output = process(updates);
        assertThat(output).hasSize(1);
        assertThat(output.get(0).getChangeType()).isEqualTo(ChangeType.PAGE_RERENDER_UPSERT);
        assertMetrics(updates.size(), output.size(), 0, 0);
    }

    @Test
    void dedupRevBasedAndPageRerenders() throws Exception {
        List<UpdateEvent> updates =
                newArrayList(newRerender(), newUpdateEvent(ChangeType.REV_BASED_UPDATE, 2L, null));
        List<UpdateEvent> output = process(updates);
        assertThat(output).hasSize(1);
        UpdateEvent event = output.get(0);
        assertThat(event).isEqualTo(updates.get(1));
        assertMetrics(updates.size(), output.size(), 0, 1);
    }

    @Test
    void doNotMergeTagsUpdateAndPageRerenders() throws Exception {
        List<UpdateEvent> updates =
                newArrayList(
                        newRerender(),
                        newUpdateEvent(
                                ChangeType.TAGS_UPDATE,
                                1L,
                                UpdateEvent.Update.forWeightedTags(newArrayList("example/a|1"))));
        List<UpdateEvent> output = process(updates);
        assertThat(output).hasSize(2);
        assertThat(output.get(0)).isEqualTo(updates.get(1));
        assertThat(output.get(1)).isEqualTo(updates.get(0));
        assertMetrics(updates.size(), output.size(), 0, 0);
    }

    @Test
    void dedupRevBasedTagsUpdateAndPageRerenders() throws Exception {
        List<UpdateEvent> updates =
                newArrayList(
                        newRerender(),
                        newUpdateEvent(ChangeType.REV_BASED_UPDATE, 2L, null),
                        newUpdateEvent(
                                ChangeType.TAGS_UPDATE,
                                1L,
                                UpdateEvent.Update.forWeightedTags(newArrayList("example/a|1"))));
        List<UpdateEvent> output = process(updates);
        assertThat(output).hasSize(1);
        UpdateEvent revBasedMerged = output.get(0);
        assertThat(revBasedMerged.getRevId()).isEqualTo(2L);
        assertThat(revBasedMerged.getUpdate().getWeightedTags())
                .isEqualTo(updates.get(2).getUpdate().getWeightedTags());
        assertMetrics(updates.size(), output.size(), 1, 1);
    }

    @Test
    void debuggableErrorMessagesOnMerge() throws Exception {
        List<UpdateEvent> updates =
                newArrayList(
                        newUpdateEvent(ChangeType.REV_BASED_UPDATE, 2L, null),
                        newUpdateEvent(ChangeType.TAGS_UPDATE, 1L, null));
        assertThatThrownBy(() -> process(updates))
                .hasMessageContaining("revId=1")
                .hasMessageContaining("revId=2")
                .hasMessageContaining("eventTime=1970-01-01T00:00:00Z");
    }

    @Test
    void dedupPageMoves() throws Exception {
        UpdateEvent.TargetDocument origin =
                new UpdateEvent.TargetDocument("domain", "wikiId", 0L, 1L, null, null, "Origin");
        UpdateEvent.TargetDocument finalTarget =
                new UpdateEvent.TargetDocument("domain", "wikiId", 0L, 1L, null, null, "Final Dest");
        UpdateEvent move1 = newUpdateEvent(ChangeType.REV_BASED_UPDATE, 2L, null);
        move1.setMovedFrom(origin);
        move1.getTargetDocument().setPageTitle("Intermediate");
        UpdateEvent edit = newUpdateEvent(ChangeType.REV_BASED_UPDATE, 3L, null);
        UpdateEvent move2 = newUpdateEvent(ChangeType.REV_BASED_UPDATE, 4L, null);
        move2.setTargetDocument(finalTarget);
        move2.setMovedFrom(
                new UpdateEvent.TargetDocument("domain", "wikiId", 0L, 1L, null, null, "Intermediate"));
        List<UpdateEvent> updates = newArrayList(move1, edit, move2);
        List<UpdateEvent> output = process(updates);
        assertThat(output).hasSize(1);
        UpdateEvent dedupPageMoves = output.get(0);
        assertThat(dedupPageMoves.getRevId()).isEqualTo(4L);
        assertThat(dedupPageMoves.getTargetDocument()).isEqualTo(finalTarget);
        assertThat(dedupPageMoves.getMovedFrom()).isEqualTo(origin);
        assertMetrics(updates.size(), output.size(), 0, 2);
    }

    @Test
    void retainsPrivateStreamFlag() throws Exception {
        List<String> weightedTags = newArrayList("foo/exists|1");
        List<UpdateEvent> updates =
                newArrayList(
                        newUpdateEvent(ChangeType.REV_BASED_UPDATE, 1L, null),
                        newUpdateEvent(
                                ChangeType.TAGS_UPDATE, 1L, UpdateEvent.Update.forWeightedTags(weightedTags)));

        updates.get(0).setFromPrivateStream(Boolean.TRUE);
        List<UpdateEvent> results = process(updates);

        assertThat(results).hasSize(1);
        assertThat(results).extracting(UpdateEvent::getFromPrivateStream).containsOnly(Boolean.TRUE);
    }
    /**
     * Reads json files generated by the failure investigation jupyter notebook.
     *
     * <p>The general idea here is that the failure investigation script can collect events from the
     * live system related to a particular event that the producer generated. Those events are loaded
     * here and re-run with the goal that this reproduces the error conditions that led to the
     * investigation.
     *
     * <p>To use run collect-events.py with the --output-fixture-path option, and copy the resulting
     * file into the errorInvestigation.json test resource for this class.
     *
     * <p>A significant current caveat is that all events are run as if they were in a single window.
     * You will likely need to manually curate the event list.
     *
     * @throws Exception
     */
    @Test
    @Disabled
    void debugFailureInvestigationEvents() throws Exception {
        ArrayNode events = (ArrayNode) load("errorInvestigation.json");
        List<UpdateEvent> updates = new ArrayList<>();
        for (int i = 0; i < events.size(); i++) {
            JsonNode event = events.get(i);
            String stream = event.path("meta").path("stream").textValue();
            Row row;
            switch (stream) {
                case "cirrussearch.update_pipeline.update.rc0":
                case "cirrussearch.update_pipeline.fetch_error.rc0":
                case "cirrussearch.update_pipeline.fetch_error.v1":
                    // These are output events, ignore
                    break;
                case "mediawiki.page_change.v1":
                    row = createEventRow(event);
                    row.setField("$schema", UpdateEventConverters.PAGE_CHANGE_SCHEMA);
                    StreamSupport.stream(UpdateEventConverters.fromPublicPageChange(row).spliterator(), false)
                            .forEach(updates::add);
                    break;
                case "mediawiki.cirrussearch.page_rerender.v1":
                    row = createEventRow(event);
                    row.setField("$schema", UpdateEventConverters.PAGE_RERENDER_SCHEMA);
                    StreamSupport.stream(
                                    UpdateEventConverters.fromPublicPageRerender(row, r -> false).spliterator(),
                                    false)
                            .forEach(updates::add);
                    break;
                case "mediawiki.page_outlink_topic_prediction_change.v1":
                    row = createEventRow(event);
                    row.setField("$schema", UpdateEventConverters.PREDICTION_CHANGE_SCHEMA);
                    updates.add(UpdateEventConverters.fromOutlinkPredictionChange(row, "unknown"));
                    break;
                default:
                    throw new RuntimeException("unknown stream: " + stream);
            }
        }
        // Note that this treats the events as a single window, we should check min/max event times
        // and throw an exception if they couldn't be windowed together.
        List<UpdateEvent> output = process(updates);

        assertThat(updates).hasSize(0);
    }

    UpdateEvent newUpdateEvent(ChangeType changeType, Long revId, UpdateEvent.Update update) {
        UpdateEvent e = new UpdateEvent();
        e.setChangeType(changeType);
        e.setUpdate(update);
        e.setTargetDocument(target);
        e.setRevId(revId);
        // Make event time ordering the same as creation order.
        e.setEventTime(eventTime);
        eventTime = eventTime.plus(Duration.ofMinutes(1));
        return e;
    }

    UpdateEvent newRerender() {
        return newRerender(false);
    }

    UpdateEvent newRerender(boolean upsert) {
        UpdateEvent e = new UpdateEvent();
        e.setChangeType(upsert ? ChangeType.PAGE_RERENDER_UPSERT : ChangeType.PAGE_RERENDER);
        e.setTargetDocument(target);
        // Make event time ordering the same as creation order.
        e.setEventTime(eventTime);
        eventTime = eventTime.plus(Duration.ofMinutes(1));
        return e;
    }

    private void assertMetrics(int inputSize, int outputSize, int merged, int dedup) {
        assertThat(dedupAndMerger.getMerged().getCount()).isEqualTo(merged);
        assertThat(dedupAndMerger.getDeduplicated().getCount()).isEqualTo(dedup);
        assertThat(inputSize).isEqualTo(outputSize + merged + dedup);
    }

    @BeforeEach
    void createTestHarness() throws Exception {
        final int windowSize = 3;
        TypeInformation<PageKey> keyTypeInfo = TypeInformation.of(PageKey.class);
        ListStateDescriptor<UpdateEvent> stateDesc =
                new ListStateDescriptor<>(
                        "window-contents",
                        TypeInformation.of(UpdateEvent.class).createSerializer(new ExecutionConfig()));
        this.dedupAndMerger = new DeduplicateAndMerge<>();
        WindowOperator<PageKey, UpdateEvent, Iterable<UpdateEvent>, UpdateEvent, TimeWindow> operator =
                new WindowOperator<>(
                        TumblingProcessingTimeWindows.of(Time.seconds(windowSize)),
                        new TimeWindow.Serializer(),
                        UpdateEvent.KEY_SELECTOR,
                        keyTypeInfo.createSerializer(new ExecutionConfig()),
                        stateDesc,
                        new InternalIterableProcessWindowFunction<>(dedupAndMerger),
                        ProcessingTimeTrigger.create(),
                        0,
                        null);

        this.testHarness =
                new KeyedOneInputStreamOperatorTestHarness<>(
                        operator, operator.getKeySelector(), keyTypeInfo);
    }

    List<UpdateEvent> process(Collection<UpdateEvent> collection) throws Exception {
        // We don't really use the full test harness, but something has to be inplace for the metrics
        // to not blow up everything else. Could probably use a less complete mock, but this is
        // available and works well enough.
        testHarness.open();
        // Note that processing time is in ms
        testHarness.setProcessingTime(5);
        for (UpdateEvent event : collection) {
            testHarness.processElement(new StreamRecord<>(event));
        }
        // Necessary to close the windows
        testHarness.setProcessingTime(5000);
        return TestHarnessUtil.getRawElementsFromOutput(testHarness.getOutput());
    }

    private static JsonNode load(String resourceName) throws IOException {
        return (new ObjectMapper())
                .reader()
                .readTree(
                        DeduplicateAndMergeTest.class.getResourceAsStream(
                                TEST_RESOURCE_PATH + "/" + resourceName));
    }

    private Row createEventRow(JsonNode raw) throws IOException {
        String stream = raw.path("meta").path("stream").textValue();
        byte[] eventData =
                (new ObjectMapper()).writeValueAsString(raw).getBytes(StandardCharsets.UTF_8);
        return factory.deserializer(stream).deserialize(eventData);
    }
}
