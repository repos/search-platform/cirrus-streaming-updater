package org.wikimedia.discovery.cirrus.updater.producer;

import static org.assertj.core.api.Assertions.assertThat;
import static org.wikimedia.discovery.cirrus.updater.common.wiremock.WireMockExtensionUtilities.getWireMockExtension;
import static org.wikimedia.discovery.cirrus.updater.common.wiremock.WireMockExtensionUtilities.mockJsonResponse;

import java.io.IOException;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.function.Function;
import java.util.function.UnaryOperator;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import org.apache.flink.test.junit5.MiniClusterExtension;
import org.apache.flink.types.Row;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.serialization.LongSerializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Timeout;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.wikimedia.discovery.cirrus.updater.common.model.ChangeType;
import org.wikimedia.discovery.cirrus.updater.common.model.EventDataStreamUtilities;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateEvent;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateRowTranscoder;
import org.wikimedia.discovery.cirrus.updater.producer.graph.CirrusNamespaceIndexMap;
import org.wikimedia.eventutilities.flink.EventRowTypeInfo;
import org.wikimedia.eventutilities.flink.formats.json.JsonRowDeserializationSchema;
import org.wikimedia.eventutilities.flink.formats.json.JsonRowDeserializationSchema.Builder;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.junit5.WireMockExtension;
import com.github.tomakehurst.wiremock.junit5.WireMockRuntimeInfo;
import com.salesforce.kafka.test.junit5.SharedKafkaTestResource;

@ExtendWith({MiniClusterExtension.class})
@SuppressWarnings("checkstyle:classfanoutcomplexity")
class ProducerApplicationIT {

    @RegisterExtension
    public static final SharedKafkaTestResource SHARED_KAFKA_TEST_RESOURCE =
            new SharedKafkaTestResource();

    @RegisterExtension static final WireMockExtension wireMockExtension = getWireMockExtension();

    public static final String EVENT_TIME_FIELD = "dt";

    static final EventRowTypeInfo UPDATE_TYPE_INFO = EventDataStreamUtilities.buildUpdateTypeInfo();
    static final JsonRowDeserializationSchema DESERIALIZATION_SCHEMA =
            new Builder(UPDATE_TYPE_INFO).build();

    @Test
    @Timeout(30)
    void test(WireMockRuntimeInfo runtimeInfo) throws Exception {

        wireMockExtension.stubFor(
                WireMock.get(WireMock.urlEqualTo(CirrusNamespaceIndexMap.API_PATH))
                        .willReturn(mockJsonResponse("testwiki-namespace-map.json")));

        final Instant now = Instant.now();
        final Instant endWM = now.plusSeconds(10);

        final String publicPageChangeTopic = "eqiad.mediawiki.page_change.v1";
        final String privatePageChangeTopic = "eqiad.mediawiki.page_change.private.v1";
        final String publicPageRerenderTopic = "eqiad.mediawiki.cirrussearch.page_rerender.v1";
        final String privatePageRerenderTopic = "eqiad.mediawiki.cirrussearch.page_rerender.private.v1";
        final String pageWeightedTagsChangeTopic =
                "eqiad.mediawiki.cirrussearch.page_weighted_tags_change.v1";
        final String pageWeightedTagsLegacyChangeTopic =
                "eqiad.mediawiki.cirrussearch.page_weighted_tags_change.rc0";
        final String articleTopicTopic = "eqiad.mediawiki.page_outlink_topic_prediction_change.v1";
        final String draftTopicTopic = "eqiad.mediawiki.revision_score_drafttopic";
        final String recommendationCreateTopic = "eqiad.mediawiki.revision-recommendation-create";
        final String publicOutputTopic = "eqiad.cirrussearch.update_pipeline.update.rc0";
        final String privateOutputTopic = "eqiad.cirrussearch.update_pipeline.update.private.rc0";

        Arrays.asList(
                        publicPageChangeTopic,
                        privatePageChangeTopic,
                        publicPageRerenderTopic,
                        privatePageRerenderTopic,
                        pageWeightedTagsChangeTopic,
                        pageWeightedTagsLegacyChangeTopic,
                        articleTopicTopic,
                        draftTopicTopic,
                        recommendationCreateTopic,
                        publicOutputTopic,
                        privateOutputTopic)
                .forEach(
                        topic ->
                                SHARED_KAFKA_TEST_RESOURCE.getKafkaTestUtils().createTopic(topic, 1, (short) 1));

        final Function<JsonNode, JsonNode> twentyMinutesAgo =
                allEventsBefore(now, Duration.ofMinutes(-20));

        final Function<JsonNode, JsonNode> fiveMinutesAgo =
                allEventsBefore(now, Duration.ofMinutes(-5));

        List<Instant> inputMessageTimestamps =
                produceRecords(twentyMinutesAgo, publicPageChangeTopic, "/page_change.events.json", endWM);
        produceRecords(
                twentyMinutesAgo, privatePageChangeTopic, "/page_change.events.private.json", endWM);

        // A rerender event to ensure we execute related code
        produceRecords(twentyMinutesAgo, publicPageRerenderTopic, "/page_rerender.events.json", endWM);
        produceRecords(
                fiveMinutesAgo, privatePageRerenderTopic, "/page_rerender.events.private.json", endWM);

        // Inclusion of article topics to trigger event merge functionality
        produceRecords(twentyMinutesAgo, articleTopicTopic, "/outlink.events.json", endWM);

        // Every stream needs at least one event or kafka tries to initialize with a negative offset
        produceRecords(twentyMinutesAgo, draftTopicTopic, null, endWM);
        produceRecords(twentyMinutesAgo, recommendationCreateTopic, null, endWM);

        ProducerApplication.main(
                new String[] {
                    "--event-stream-config-url",
                    EventDataStreamUtilities.STREAM_CONFIG,
                    "--event-stream-json-schema-urls",
                    EventDataStreamUtilities.SCHEMA_REPO,
                    "--public-page-change-stream",
                    "mediawiki.page_change.v1",
                    "--private-page-change-stream",
                    "mediawiki.page_change.private.v1",
                    "--public-page-rerender-stream",
                    "mediawiki.cirrussearch.page_rerender.v1",
                    "--private-page-rerender-stream",
                    "mediawiki.cirrussearch.page_rerender.private.v1",
                    "--page-weighted-tags-change-stream",
                    "mediawiki.cirrussearch.page_weighted_tags_change.v1",
                    "--page-weighted-tags-change-legacy-stream",
                    "mediawiki.cirrussearch.page_weighted_tags_change.rc0",
                    "--article-topic-stream",
                    "mediawiki.page_outlink_topic_prediction_change.v1",
                    "--draft-topic-stream",
                    "mediawiki.revision_score_drafttopic",
                    "--recommendation-create-stream",
                    "mediawiki.revision-recommendation-create",
                    "--topic-prefix-filter",
                    "eqiad",
                    "--kafka-source-config.bootstrap.servers",
                    SHARED_KAFKA_TEST_RESOURCE.getKafkaConnectString(),
                    "--kafka-source-config.group.id",
                    "test",
                    "--http-request-timeout",
                    "1s",
                    "--kafka-source-start-time",
                    inputMessageTimestamps.stream().min(Instant::compareTo).map(Instant::toString).get(),
                    "--kafka-source-end-time",
                    inputMessageTimestamps.stream().max(Instant::compareTo).map(Instant::toString).get(),
                    "--public-update-topic",
                    publicOutputTopic,
                    "--private-update-topic",
                    privateOutputTopic,
                    "--pipeline.name",
                    "eqiad.test.producer",
                    "--http-routes.99-mwapi",
                    ".*=" + runtimeInfo.getHttpBaseUrl()
                });

        final List<Row> publicEvents = readKafkaTopic(publicOutputTopic);

        UpdateRowTranscoder decoder = UpdateRowTranscoder.decoder();

        // While the source has three events, one gets deduplicated
        assertThat(publicEvents).hasSize(4);
        assertThat(
                        publicEvents.stream()
                                .map(decoder::decode)
                                .filter(row -> row.getChangeType().equals(ChangeType.PAGE_DELETE))
                                .findFirst())
                .map(inputEvent -> inputEvent.getTargetDocument().getIndexName())
                .hasValue("testwiki_general");
        assertThat(
                        publicEvents.stream()
                                .map(decoder::decode)
                                .filter(row -> row.getChangeType().equals(ChangeType.PAGE_RERENDER))
                                .findFirst())
                .map(inputEvent -> inputEvent.getTargetDocument().getIndexName())
                .hasValue("testwiki_content");

        assertThat(
                        publicEvents.stream()
                                .map(decoder::decode)
                                .filter(row -> row.getChangeType().equals(ChangeType.PAGE_RERENDER_UPSERT))
                                .findFirst())
                .map(inputEvent -> inputEvent.getTargetDocument().getIndexName())
                .hasValue("testwiki_content");

        assertThat(
                        publicEvents.stream()
                                .map(decoder::decode)
                                .filter(row -> row.getChangeType().equals(ChangeType.REV_BASED_UPDATE))
                                .filter(row -> row.getTargetDocument().getPageId().equals(147498L))
                                .findFirst()
                                .map(inputEvent -> inputEvent.getUpdate().getWeightedTags())
                                .get())
                .containsExactlyInAnyOrder(
                        "classification.ores.articletopic/Geography.Regions.Europe.Northern_Europe|997",
                        "classification.ores.articletopic/Geography.Regions.Europe.Europe*|974",
                        "classification.ores.articletopic/History_and_Society.Politics_and_government|901");

        final List<Row> privateEvents = readKafkaTopic(privateOutputTopic);
        List<UpdateEvent> pageMoveEvents =
                privateEvents.stream().map(decoder::decode).collect(Collectors.toList());
        assertThat(pageMoveEvents).hasSize(3);
        assertThat(pageMoveEvents.get(0).getChangeType()).isEqualTo(ChangeType.REV_BASED_UPDATE);
        assertThat(pageMoveEvents.get(0).getTargetDocument().getIndexName())
                .isEqualTo("testwiki_content");
        assertThat(pageMoveEvents.get(1).getChangeType()).isEqualTo(ChangeType.PAGE_DELETE);
        assertThat(pageMoveEvents.get(1).getTargetDocument().getIndexName())
                .isEqualTo("testwiki_general");
        assertThat(pageMoveEvents.get(2).getChangeType()).isEqualTo(ChangeType.PAGE_RERENDER_UPSERT);
        assertThat(pageMoveEvents.get(2).getTargetDocument().getIndexName())
                .isEqualTo("testwiki_content");
    }

    private List<Instant> produceRecords(
            Function<JsonNode, JsonNode> lineMapper, String topic, String resourceName, Instant endWM)
            throws IOException, ExecutionException, InterruptedException {

        List<Future<RecordMetadata>> eventSends = null;
        try (Producer<Long, String> producer =
                SHARED_KAFKA_TEST_RESOURCE
                        .getKafkaTestUtils()
                        .getKafkaProducer(LongSerializer.class, StringSerializer.class)) {

            if (resourceName != null) {
                final Spliterator<JsonNode> eventNodeSpliterator =
                        Spliterators.spliteratorUnknownSize(
                                EventDataStreamUtilities.parseJson(resourceName).iterator(), Spliterator.ORDERED);

                final Stream<JsonNode> eventNodes = StreamSupport.stream(eventNodeSpliterator, false);

                eventSends =
                        eventNodes
                                .map(lineMapper)
                                .map(line -> toProducerRecord(topic, line))
                                .map(producer::send)
                                .collect(Collectors.toList());
            }
            // Send a fake event at time endWM (this is necessary to position the end offsets when using
            // bounded kafka streams)
            producer.send(new ProducerRecord<>(topic, 0, endWM.toEpochMilli(), null, ""));
        }
        List<Instant> timestamps = new ArrayList<>();
        if (eventSends != null) {
            for (Future<RecordMetadata> f : eventSends) {
                timestamps.add(Instant.ofEpochMilli(f.get().timestamp()));
            }
        }
        timestamps.add(endWM);
        return timestamps;
    }

    @NotNull
    private static ProducerRecord<Long, String> toProducerRecord(String topic, JsonNode line) {
        return new ProducerRecord<>(
                topic,
                0,
                Instant.parse(line.get(EVENT_TIME_FIELD).asText()).toEpochMilli(),
                null,
                line.toString());
    }

    UnaryOperator<JsonNode> allEventsBefore(Instant now, Duration offset) {
        return (JsonNode event) -> {
            ((ObjectNode) event).put(EVENT_TIME_FIELD, now.plus(offset).toString());
            return event;
        };
    }

    private List<Row> readKafkaTopic(String topic) {
        return SHARED_KAFKA_TEST_RESOURCE.getKafkaTestUtils().consumeAllRecordsFromTopic(topic).stream()
                .map(
                        event -> {
                            try {
                                return DESERIALIZATION_SCHEMA.deserialize(event.value());
                            } catch (IOException e) {
                                throw new RuntimeException(e);
                            }
                        })
                .collect(Collectors.toList());
    }
}
