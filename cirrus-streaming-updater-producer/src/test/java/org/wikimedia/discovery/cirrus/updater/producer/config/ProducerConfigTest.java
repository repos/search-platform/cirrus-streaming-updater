package org.wikimedia.discovery.cirrus.updater.producer.config;

import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;

import java.net.URISyntaxException;
import java.time.Clock;
import java.time.Duration;
import java.time.Instant;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.flink.api.java.utils.ParameterTool;
import org.assertj.core.data.Offset;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Test;
import org.wikimedia.discovery.cirrus.updater.common.http.HttpHostMapping;

import com.google.common.collect.ImmutableList;

class ProducerConfigTest {

    final List<HttpHostMapping> httpRoutes =
            List.of(
                    HttpHostMapping.create("test\\.remote", "https://other.remote:8484"),
                    HttpHostMapping.create("test\\.local", "https://other.remote:4242"));
    final String jsonSchemaUrl = "http://localhost:8080/json-schema";
    final Instant kafkaEndTime = Instant.now();
    final String kafkaSinkBootstrapServers = "kafka-sink.local:4242";
    final String streamConfig = "https://streamconfig.local";
    final Duration retryFetchTimeout = Duration.ofSeconds(30);
    final String publicPageChangeStream = "mediawiki.page_change.v1";
    final String privatePageChangeStream = "mediawiki.page_change.private.v1";
    final String publicPageRerenderStream = "mediawiki.cirrussearch.page_rerender.v1";
    final String privatePageRerenderStream = "mediawiki.cirrussearch.page_rerender.private.v1";
    final String pageWeightedTagsChangeStream =
            "mediawiki.cirrussearch.page_weighted_tags_change.rc0";
    String topicPrefixPattern = "^eqiad\\.";
    final String kafkaSourceBootstrapServers = "kafka-source.local:4242";
    final String kafkaSourceGroupId = "test";
    final Instant kafkaStartTime = Instant.now();
    final String pipelineName = "eqiad.test.producer";
    final String publicUpdateTopic = "update";
    final String privateUpdateTopic = "update.private";
    final List<String> wikiIds = ImmutableList.of("testwiki", "testwikidata");
    final String mediawikiAuthToken = "random token";

    ProducerConfigTest() throws URISyntaxException {}

    private static Instant getNowUTC() {
        return Instant.now(Clock.systemUTC());
    }

    @NotNull
    private String encodeHttpRoute(int index) {
        return httpRoutes.get(index).toString();
    }

    @Test
    void fromArgs() {
        final ParameterTool params =
                ParameterTool.fromArgs(
                        new String[] {
                            "--event-stream-config-url",
                            streamConfig,
                            "--event-stream-json-schema-urls",
                            jsonSchemaUrl,
                            "--fetch-retry-max-age",
                            retryFetchTimeout.getSeconds() + "s",
                            "--public-page-change-stream",
                            publicPageChangeStream,
                            "--private-page-change-stream",
                            privatePageChangeStream,
                            "--public-page-rerender-stream",
                            publicPageRerenderStream,
                            "--private-page-rerender-stream",
                            privatePageRerenderStream,
                            "--page-weighted-tags-change-stream",
                            pageWeightedTagsChangeStream,
                            "--article-topic-stream",
                            "unused-empty-stream",
                            "--draft-topic-stream",
                            "unused-empty-stream",
                            "--recommendation-create-stream",
                            "unused-empty-stream",
                            "--kafka-source-config.bootstrap.servers",
                            kafkaSourceBootstrapServers,
                            "--kafka-source-config.group.id",
                            kafkaSourceGroupId,
                            "--kafka-source-start-time",
                            kafkaStartTime.toString(),
                            "--kafka-source-end-time",
                            kafkaEndTime.toString(),
                            "--kafka-sink-config.bootstrap.servers",
                            kafkaSinkBootstrapServers,
                            "--kafka-sink-config.linger.ms",
                            "42",
                            "--kafka-sink-config.batch.size",
                            "111",
                            "--pipeline.name",
                            pipelineName,
                            "--public-update-topic",
                            publicUpdateTopic,
                            "--private-update-topic",
                            privateUpdateTopic,
                            "--http-routes.99",
                            encodeHttpRoute(1),
                            "--http-routes.00",
                            encodeHttpRoute(0),
                            "--wikiids",
                            String.join(";", wikiIds),
                            "--topic-prefix-filter",
                            topicPrefixPattern,
                            "--mediawiki-auth-token",
                            mediawikiAuthToken
                        });
        final ProducerConfig config = ProducerConfig.of(params.getConfiguration());

        assertThat(config.eventStreamConfigUrl()).isEqualTo(streamConfig);
        assertThat(config.eventStreamJsonSchemaUrls()).isEqualTo(singletonList(jsonSchemaUrl));
        assertThat(config.publicPageChangeStream()).isEqualTo(publicPageChangeStream);
        assertThat(config.privatePageChangeStream()).isEqualTo(privatePageChangeStream);
        assertThat(config.publicPageRerenderStream()).isEqualTo(publicPageRerenderStream);
        assertThat(config.privatePageRerenderStream()).isEqualTo(privatePageRerenderStream);
        assertThat(config.pageWeightedTagsChangeStream()).isEqualTo(pageWeightedTagsChangeStream);
        assertThat(config.topicPrefix()).isEqualTo(topicPrefixPattern);
        assertThat(config.kafkaSourceBootstrapServers()).isEqualTo(kafkaSourceBootstrapServers);
        assertThat(config.kafkaSinkBootstrapServers()).isEqualTo(kafkaSinkBootstrapServers);
        assertThat(config.kafkaSinkProperties())
                .containsEntry("linger.ms", "42")
                .containsEntry("batch.size", "111");
        assertThat(config.kafkaSourceStartTime()).isEqualTo(kafkaStartTime);
        assertThat(config.kafkaSourceEndTime()).isEqualTo(kafkaEndTime);
        assertThat(config.publicUpdateTopic()).isEqualTo(publicUpdateTopic);
        assertThat(config.privateUpdateTopic()).isEqualTo(privateUpdateTopic);
        assertThat(config.clock().get().toEpochMilli())
                .isCloseTo(Instant.now().toEpochMilli(), Offset.offset(10L));

        assertThat(config.httpRoutes())
                .map(HttpHostMapping::toString)
                .containsAll(
                        httpRoutes.stream().map(HttpHostMapping::toString).collect(Collectors.toList()));
        assertThat(config.wikiIds()).containsExactlyInAnyOrderElementsOf(wikiIds);
        assertThat(config.mediawikiAuthToken()).isEqualTo(mediawikiAuthToken);
    }

    @Test
    void fromRequiredArgs() {
        final ParameterTool params =
                ParameterTool.fromArgs(
                        new String[] {
                            "--event-stream-config-url",
                            streamConfig,
                            "--event-stream-json-schema-urls",
                            jsonSchemaUrl,
                            "--kafka-source-config.bootstrap.servers",
                            kafkaSourceBootstrapServers,
                            "--kafka-source-config.group.id",
                            kafkaSourceGroupId,
                            "--public-page-change-stream",
                            publicPageChangeStream,
                            "--private-page-change-stream",
                            privatePageChangeStream,
                            "--public-page-rerender-stream",
                            publicPageRerenderStream,
                            "--private-page-rerender-stream",
                            privatePageRerenderStream,
                            "--page-weighted-tags-change-stream",
                            pageWeightedTagsChangeStream,
                            "--article-topic-stream",
                            "unused-empty-stream",
                            "--draft-topic-stream",
                            "unused-empty-stream",
                            "--recommendation-create-stream",
                            "unused-empty-stream",
                            "--pipeline.name",
                            pipelineName,
                            "--public-update-topic",
                            publicUpdateTopic,
                            "--private-update-topic",
                            privateUpdateTopic,
                            "--dry-run",
                            "true"
                        });
        final ProducerConfig config = ProducerConfig.of(params.getConfiguration());

        assertThat(config.eventStreamJsonSchemaUrls()).isEqualTo(singletonList(jsonSchemaUrl));
        assertThat(config.httpRequestTimeout()).isEqualTo(Duration.ofSeconds(10));
        assertThat(config.kafkaSourceBootstrapServers()).isEqualTo(kafkaSourceBootstrapServers);
        assertThat(config.kafkaSinkBootstrapServers()).isEqualTo(kafkaSourceBootstrapServers);
        assertThat(config.publicUpdateTopic()).isEqualTo(publicUpdateTopic);
        assertThat(config.privateUpdateTopic()).isEqualTo(privateUpdateTopic);
        assertThat(config.clock().get().toEpochMilli())
                .isCloseTo(getNowUTC().toEpochMilli(), Offset.offset(10L));
        assertThat(config.wikiIds()).isNull();
        assertThat(config.dryRun()).isTrue();
    }
}
