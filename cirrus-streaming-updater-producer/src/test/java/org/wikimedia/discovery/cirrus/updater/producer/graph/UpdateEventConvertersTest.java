package org.wikimedia.discovery.cirrus.updater.producer.graph;

import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.groups.Tuple.tuple;
import static org.wikimedia.discovery.cirrus.updater.common.graph.BypassingCirrusDocFetcher.fetchRequired;

import java.io.IOException;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.apache.commons.io.IOUtils;
import org.apache.flink.types.Row;
import org.junit.jupiter.api.MethodOrderer.MethodName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.wikimedia.discovery.cirrus.updater.common.model.ChangeType;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateEvent;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateEvent.TargetDocument;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateEvent.Update;
import org.wikimedia.eventutilities.flink.stream.EventDataStreamFactory;

@TestMethodOrder(MethodName.class)
class UpdateEventConvertersTest {

    private final EventDataStreamFactory factory =
            EventDataStreamFactory.from(
                    singletonList(UpdateEventConvertersTest.class.getResource("/schema_repo").toString()),
                    this.getClass().getResource("/event-stream-config.json").toString());

    // use cases:
    // 00) create A; -> [Insert A]
    // 01) update A; -> [Upsert A]
    // 02) create B (redirect to A); -> [Delete B, Upsert A]
    // 03) move A to A_Moved; -> [Insert A_Moved, Delete A]
    // 04) update B (redirect to A_Moved) -> [Upsert A_Moved]
    // 05) delete B -> [Upsert A_Moved]

    @Test
    void pageChange00CreateA() throws IOException {
        final List<UpdateEvent> events = mapPageChangeEvents("00.create_a");
        assertThat(events).hasSize(1);
        final UpdateEvent updateEvent = events.get(0);
        validateCommonPageChangeAttributes(updateEvent);
        assertThat(updateEvent.getChangeType()).isEqualTo(ChangeType.REV_BASED_UPDATE);
        assertThat(updateEvent.getTargetDocument().getPageTitle()).isEqualTo("Page_A");
    }

    @Test
    void pageChange01UpdateA() throws IOException {
        final List<UpdateEvent> events = mapPageChangeEvents("01.update_a");
        assertThat(events).hasSize(1);
        final UpdateEvent updateEvent = events.get(0);
        validateCommonPageChangeAttributes(updateEvent);
        assertThat(updateEvent.getChangeType()).isEqualTo(ChangeType.REV_BASED_UPDATE);
        assertThat(updateEvent.getTargetDocument().getPageTitle()).isEqualTo("Page_A");
    }

    @Test
    void pageChange02CreateB() throws IOException {
        final List<UpdateEvent> events = mapPageChangeEvents("02.create_b");
        assertThat(events).hasSize(2);

        final UpdateEvent deleteEvent = events.get(0);
        validateCommonPageChangeAttributes(deleteEvent);
        assertThat(deleteEvent.getChangeType()).isEqualTo(ChangeType.PAGE_DELETE);
        assertThat(deleteEvent.getTargetDocument().getPageTitle()).isEqualTo("Page_B");

        final UpdateEvent linksEvent = events.get(1);
        assertThat(linksEvent.getChangeType()).isEqualTo(ChangeType.REDIRECT_UPDATE);
        assertThat(linksEvent.getTargetDocument().getPageTitle()).isEqualTo("Page_A");
    }

    @Test
    void pageChange03MoveA() throws IOException {
        final List<UpdateEvent> events = mapPageChangeEvents("03.move_a");
        assertThat(events).hasSize(1);

        final UpdateEvent deleteEvent = events.get(0);
        validateCommonPageChangeAttributes(deleteEvent);
        assertThat(deleteEvent.getChangeType()).isEqualTo(ChangeType.REV_BASED_UPDATE);
        UpdateEvent.TargetDocument targetDocument = deleteEvent.getTargetDocument();
        assertThat(targetDocument.getPageTitle()).isEqualTo("Page_A_Moved");
        UpdateEvent.TargetDocument movedFrom = deleteEvent.getMovedFrom();
        assertThat(movedFrom.getPageTitle()).isEqualTo("Page_A");
        assertThat(movedFrom.getPageNamespace()).isEqualTo(0);
        assertThat(movedFrom.getPageId()).isEqualTo(targetDocument.getPageId());
        assertThat(movedFrom.getWikiId()).isEqualTo(targetDocument.getWikiId());
        assertThat(movedFrom.getDomain()).isEqualTo(targetDocument.getDomain());
    }

    @Test
    void pageChange03UpdateA() throws IOException {
        final List<UpdateEvent> events = mapPageChangeEvents("03.update_a");
        assertThat(events).hasSize(2);

        final UpdateEvent deleteEvent = events.get(0);
        validateCommonPageChangeAttributes(deleteEvent);
        assertThat(deleteEvent.getChangeType()).isEqualTo(ChangeType.PAGE_DELETE);
        assertThat(deleteEvent.getTargetDocument().getPageTitle()).isEqualTo("Page_A");

        final UpdateEvent linksEvent = events.get(1);
        assertThat(linksEvent.getChangeType()).isEqualTo(ChangeType.REDIRECT_UPDATE);
        assertThat(linksEvent.getTargetDocument().getPageTitle()).isEqualTo("Page_A_Moved");
    }

    @Test
    void pageChange04UpdateB() throws IOException {
        final List<UpdateEvent> events = mapPageChangeEvents("04.update_b");
        assertThat(events).hasSize(2);

        final UpdateEvent deleteEvent = events.get(0);
        validateCommonPageChangeAttributes(deleteEvent);
        assertThat(deleteEvent.getChangeType()).isEqualTo(ChangeType.PAGE_DELETE);
        assertThat(deleteEvent.getTargetDocument().getPageTitle()).isEqualTo("Page_B");

        final UpdateEvent linksEvent = events.get(1);
        assertThat(linksEvent.getChangeType()).isEqualTo(ChangeType.REDIRECT_UPDATE);
        assertThat(linksEvent.getTargetDocument().getPageTitle()).isEqualTo("Page_A_Moved");
    }

    @Test
    void pageChange05DeleteB() throws IOException {
        final List<UpdateEvent> events = mapPageChangeEvents("05.delete_b");
        assertThat(events).hasSize(2);

        final UpdateEvent deleteEvent = events.get(0);
        validateCommonPageChangeAttributes(deleteEvent);
        assertThat(deleteEvent.getChangeType()).isEqualTo(ChangeType.PAGE_DELETE);
        assertThat(deleteEvent.getTargetDocument().getPageTitle()).isEqualTo("Page_B");
        assertThat(deleteEvent.getEventTime()).isEqualTo("2023-06-16T14:05:49Z");

        final UpdateEvent linksEvent = events.get(1);
        assertThat(linksEvent.getChangeType()).isEqualTo(ChangeType.REDIRECT_UPDATE);
        assertThat(linksEvent.getTargetDocument().getPageTitle()).isEqualTo("Page_A_Moved");
        assertThat(linksEvent.getTargetDocument().getPageId()).isNotNull();
    }

    @Test
    void deleteRedlinkRedirect() throws IOException {
        final List<UpdateEvent> events = mapPageChangeEvents("06.delete_redlink_redirect");
        assertThat(events).hasSize(1);
        assertThat(events.get(0).getChangeType()).isEqualTo(ChangeType.PAGE_DELETE);
    }

    @Test
    void createRedlinkRedirect() throws IOException {
        final List<UpdateEvent> events = mapPageChangeEvents("07.create_redlink_redirect");
        assertThat(events).hasSize(1);
        assertThat(events.get(0).getChangeType()).isEqualTo(ChangeType.PAGE_DELETE);
    }

    @Test
    void createRedirect() throws IOException {
        final List<UpdateEvent> events = mapPageChangeEvents("08.create_redirect");
        assertThat(events).hasSize(2);
        // The order isn't really important, but it was inconvenient to do otherwise
        UpdateEvent delete = events.get(0);
        assertThat(delete.getChangeType()).isEqualTo(ChangeType.PAGE_DELETE);
        assertThat(delete.getTargetDocument().getPageTitle()).isEqualTo("Redirect");

        UpdateEvent update = events.get(1);
        assertThat(update.getChangeType()).isEqualTo(ChangeType.REDIRECT_UPDATE);
        assertThat(update.getTargetDocument().getPageTitle()).isEqualTo("Main_Page");
        assertThat(update.getUpdate().getNoopHints()).isEqualTo(UpdateEvent.Update.REDIRECT_HINT);
        assertThat(update.getUpdate().getRedirectRemove()).isNull();
        assertThat(update.getUpdate().getRedirectAdd())
                .extracting(UpdateEvent.TargetDocument::getPageTitle)
                .containsExactly("Redirect");
    }

    @Test
    void createRedirectWithinOtherNamespace() throws IOException {
        final List<UpdateEvent> events = mapPageChangeEvents("11.create_redirect_within_other_ns");
        // Redirects within the same namespace are indexed
        assertThat(events).hasSize(2);

        UpdateEvent delete = events.get(0);
        assertThat(delete.getChangeType()).isEqualTo(ChangeType.PAGE_DELETE);
        assertThat(delete.getTargetDocument().getPageNamespace()).isEqualTo(4);
        assertThat(delete.getTargetDocument().getPageTitle()).isEqualTo("Project:RedirectFrom");

        UpdateEvent update = events.get(1);
        assertThat(update.getChangeType()).isEqualTo(ChangeType.REDIRECT_UPDATE);
        assertThat(update.getTargetDocument().getPageNamespace()).isEqualTo(4);
        assertThat(update.getTargetDocument().getPageTitle()).isEqualTo("Project:SameNamespace");
        assertThat(update.getUpdate().getNoopHints()).isEqualTo(UpdateEvent.Update.REDIRECT_HINT);
        assertThat(update.getUpdate().getRedirectRemove()).isNull();
        assertThat(update.getUpdate().getRedirectAdd())
                .extracting(
                        UpdateEvent.TargetDocument::getPageNamespace, UpdateEvent.TargetDocument::getPageTitle)
                .containsExactly(tuple(4L, "Project:RedirectFrom"));
    }

    @Test
    void createRedirectFromOtherNamespace() throws IOException {
        final List<UpdateEvent> events = mapPageChangeEvents("12.create_redirect_from_other_ns");
        // We do not index cross-namespace redirects unless they come from ns_main
        assertThat(events).hasSize(1);

        UpdateEvent delete = events.get(0);
        assertThat(delete.getChangeType()).isEqualTo(ChangeType.PAGE_DELETE);
        assertThat(delete.getTargetDocument().getPageNamespace()).isEqualTo(4);
        assertThat(delete.getTargetDocument().getPageTitle()).isEqualTo("Project:RedirectFrom");
    }

    @Test
    void createRedirectFromMainNamespace() throws IOException {
        final List<UpdateEvent> events = mapPageChangeEvents("13.create_redirect_from_main_ns");
        // Cross namespace redirect from ns_main must be indexed
        assertThat(events).hasSize(2);

        UpdateEvent delete = events.get(0);
        assertThat(delete.getChangeType()).isEqualTo(ChangeType.PAGE_DELETE);
        assertThat(delete.getTargetDocument().getPageNamespace()).isEqualTo(0);
        assertThat(delete.getTargetDocument().getPageTitle()).isEqualTo("RedirectFrom");

        UpdateEvent update = events.get(1);
        assertThat(update.getChangeType()).isEqualTo(ChangeType.REDIRECT_UPDATE);
        assertThat(update.getTargetDocument().getPageNamespace()).isEqualTo(4);
        assertThat(update.getTargetDocument().getPageTitle()).isEqualTo("Project:OtherNamespace");
        assertThat(update.getUpdate().getNoopHints()).isEqualTo(UpdateEvent.Update.REDIRECT_HINT);
        assertThat(update.getUpdate().getRedirectRemove()).isNull();
        assertThat(update.getUpdate().getRedirectAdd())
                .extracting(
                        UpdateEvent.TargetDocument::getPageNamespace, UpdateEvent.TargetDocument::getPageTitle)
                .containsExactly(tuple(0L, "RedirectFrom"));
    }

    @Test
    void deleteRedirect() throws IOException {
        final List<UpdateEvent> events = mapPageChangeEvents("09.delete_redirect");
        assertThat(events).hasSize(2);

        UpdateEvent delete = events.get(0);
        assertThat(delete.getChangeType()).isEqualTo(ChangeType.PAGE_DELETE);
        assertThat(delete.getTargetDocument().getPageTitle()).isEqualTo("Redirect");

        UpdateEvent update = events.get(1);
        assertThat(update.getChangeType()).isEqualTo(ChangeType.REDIRECT_UPDATE);
        assertThat(update.getTargetDocument().getPageTitle()).isEqualTo("Main_Page");
        assertThat(update.getUpdate().getNoopHints()).isEqualTo(UpdateEvent.Update.REDIRECT_HINT);
        assertThat(update.getUpdate().getRedirectAdd()).isNull();
        assertThat(update.getUpdate().getRedirectRemove())
                .extracting(UpdateEvent.TargetDocument::getPageTitle)
                .containsExactly("Redirect");
    }

    @Test
    void undeletePage() throws IOException {
        final List<UpdateEvent> events = mapPageChangeEvents("10.undelete");
        assertThat(events).hasSize(1);

        UpdateEvent undelete = events.get(0);
        assertThat(undelete.getChangeType()).isEqualTo(ChangeType.REV_BASED_UPDATE);
        // must use event creation dt and not rev_dt
        assertThat(undelete.getEventTime()).isEqualTo("2024-02-08T22:17:22Z");
    }

    @Test
    void plainPageRerender() throws IOException {
        final List<UpdateEvent> events = mapPageRerenderEvents("plain-page", r -> false);
        assertThat(events).hasSize(1);
        UpdateEvent event = events.get(0);
        assertThat(event.getEventStream()).isEqualTo("mediawiki.cirrussearch_page_rerender.v1");
        assertThat(event.getRequestId()).isEqualTo("my_request_id");
        assertThat(event.getTargetDocument().getPageId()).isEqualTo(123L);
        assertThat(event.getTargetDocument().getPageNamespace()).isEqualTo(0L);
        assertThat(event.getTargetDocument().getPageTitle()).isEqualTo("MyPage");
        assertThat(event.getTargetDocument().getIndexName()).isEqualTo("mywiki_content");
        assertThat(event.getChangeType()).isEqualTo(ChangeType.PAGE_RERENDER);
        assertThat(event.getRevId()).isNull();
    }

    @Test
    void plainPageRerenderUpsert() throws IOException {
        final List<UpdateEvent> events = mapPageRerenderEvents("plain-page", "my_reason"::equals);
        assertThat(events).hasSize(1);
        UpdateEvent event = events.get(0);
        assertThat(event.getEventStream()).isEqualTo("mediawiki.cirrussearch_page_rerender.v1");
        assertThat(event.getRequestId()).isEqualTo("my_request_id");
        assertThat(event.getTargetDocument().getPageId()).isEqualTo(123L);
        assertThat(event.getTargetDocument().getPageNamespace()).isEqualTo(0L);
        assertThat(event.getTargetDocument().getPageTitle()).isEqualTo("MyPage");
        assertThat(event.getTargetDocument().getIndexName()).isEqualTo("mywiki_content");
        assertThat(event.getChangeType()).isEqualTo(ChangeType.PAGE_RERENDER_UPSERT);
        assertThat(event.getRevId()).isNull();
    }

    @Test
    void redirectPageRerender() throws IOException {
        final List<UpdateEvent> events = mapPageRerenderEvents("redirect-page", r -> false);
        assertThat(events).hasSize(0);
    }

    @Test
    void revisionScoring() throws IOException {
        UpdateEvent event = mapRevScoringEvent("example", "articletopic");
        assertThat(event).isNotNull();
        assertThat(event.getEventStream()).isEqualTo("mediawiki.revision-score");
        assertThat(event.getTargetDocument().getPageId()).isEqualTo(123L);
        assertThat(event.getTargetDocument().getPageNamespace()).isEqualTo(0L);
        assertThat(event.getTargetDocument().getPageTitle()).isEqualTo("TestPage10");
        assertThat(event.getTargetDocument().getIndexName()).isNull();
        assertThat(event.getChangeType()).isEqualTo(ChangeType.TAGS_UPDATE);
        assertThat(event.getRevId()).isEqualTo(123L);
        assertThat(event.getUpdate().getWeightedTags())
                .containsExactlyInAnyOrder("testsuite/yes|990", "testsuite/mostly|900");
    }

    @Test
    void recommendationCreate() throws IOException {
        UpdateEvent event = mapRecommendationCreateEvent("example");
        assertThat(event).isNotNull();
        assertThat(event.getEventStream()).isEqualTo("mediawiki.revision-recommendation-create");
        assertThat(event.getTargetDocument().getPageId()).isEqualTo(123L);
        assertThat(event.getTargetDocument().getPageNamespace()).isEqualTo(0L);
        assertThat(event.getTargetDocument().getPageTitle()).isEqualTo("TestPage10");
        assertThat(event.getTargetDocument().getIndexName()).isNull();
        assertThat(event.getTargetDocument().getDomain()).isEqualTo("test.wikipedia.org");
        assertThat(event.getChangeType()).isEqualTo(ChangeType.TAGS_UPDATE);
        assertThat(event.getRevId()).isEqualTo(123L);
        assertThat(event.getUpdate().getWeightedTags())
                .containsExactlyInAnyOrder("recommendation.link/exists|1");
    }

    @Test
    void outlinkPredictionChange() throws IOException {
        UpdateEvent event = mapOutlinkPredictionChangeEvent("example");
        assertThat(event).isNotNull();
        assertThat(event.getEventStream())
                .isEqualTo("mediawiki.page_outlink_topic_prediction_change.v1");
        assertThat(event.getTargetDocument().getPageId()).isEqualTo(1L);
        assertThat(event.getTargetDocument().getPageNamespace()).isEqualTo(1L);
        assertThat(event.getTargetDocument().getPageTitle()).isEqualTo("example");
        assertThat(event.getTargetDocument().getIndexName()).isNull();
        assertThat(event.getChangeType()).isEqualTo(ChangeType.TAGS_UPDATE);
        assertThat(event.getRevId()).isEqualTo(2L);
        assertThat(event.getUpdate().getWeightedTags())
                .containsExactlyInAnyOrder("testsuite/yes|990", "testsuite/mostly|900");
    }

    @Test
    void pageWeightedTagsChangeSet() throws IOException {
        final UpdateEvent updateEvent = mapPageWeightedTagsChangeEvent("set");
        validateCommonPageWeightedTagsChangeAttributes(updateEvent);

        final Update update = updateEvent.getUpdate();
        assertThat(update.getWeightedTags())
                .hasSize(3)
                .containsOnly("tag.group.A/A.1|500", "tag.group.A/A.2", "tag.group.B/B.1");
    }

    @Test
    void pageWeightedTagsChangeClear() throws IOException {
        final UpdateEvent updateEvent = mapPageWeightedTagsChangeEvent("clear");
        validateCommonPageWeightedTagsChangeAttributes(updateEvent);

        final Update update = updateEvent.getUpdate();
        assertThat(update.getWeightedTags()).hasSize(1).containsOnly("tag.group.B/__DELETE_GROUPING__");
    }

    @Test
    void pageWeightedTagsChangeSetAndClear() throws IOException {
        final UpdateEvent updateEvent = mapPageWeightedTagsChangeEvent("set_and_clear");
        validateCommonPageWeightedTagsChangeAttributes(updateEvent);

        final Update update = updateEvent.getUpdate();

        assertThat(update.getWeightedTags())
                .hasSize(2)
                .containsOnly("tag.group.A/A.1", "tag.group.C/__DELETE_GROUPING__");
    }

    @Test
    void pageLegacyWeightedTagsChangeSetAndClear() throws IOException {
        final UpdateEvent updateEvent = mapLegacyPageWeightedTagsChangeEvent("set_and_clear");
        validateCommonPageWeightedTagsChangeAttributes(updateEvent);

        final Update update = updateEvent.getUpdate();

        assertThat(update.getWeightedTags())
                .hasSize(2)
                .containsOnly("tag.group.A/A.1", "tag.group.C/__DELETE_GROUPING__");
    }

    private void validateCommonPageWeightedTagsChangeAttributes(UpdateEvent updateEvent) {
        assertThat(updateEvent.getChangeType()).isEqualTo(ChangeType.TAGS_UPDATE);
        assertThat(updateEvent.getEventTime()).isNotNull();
        assertThat(updateEvent.getEventStream())
                .isEqualTo("cirrussearch.page_weighted_tags_change.rc0");
        assertThat(updateEvent.getRequestId()).isNotEmpty();
        final TargetDocument target = updateEvent.getTargetDocument();
        assertThat(target.getWikiId()).isEqualTo("my_wiki");
        assertThat(target.getPageNamespace()).isEqualTo(0);
        assertThat(target.getPageId()).isGreaterThan(0);
    }

    private static void validateCommonPageChangeAttributes(UpdateEvent updateEvent) {
        UpdateEvent.TargetDocument target = updateEvent.getTargetDocument();
        if (!fetchRequired(updateEvent)
                && !updateEvent.getChangeType().equals(ChangeType.PAGE_DELETE)) {
            assertThat(updateEvent.getUpdate()).isNotNull();
        }
        assertThat(updateEvent.getEventTime()).isNotNull();
        assertThat(updateEvent.getEventStream()).isEqualTo("mediawiki.page_change");
        assertThat(target.getWikiId()).isEqualTo("my_wiki");
        assertThat(updateEvent.getRequestId()).isNotEmpty();
        assertThat(target.getPageNamespace()).isEqualTo(0);
        assertThat(target.getPageId()).isGreaterThan(0);
        assertThat(updateEvent.getRevId()).isGreaterThan(0);
        assertThat(updateEvent.getIngestionTime()).isNull();
    }

    private List<UpdateEvent> mapPageChangeEvents(String sourceEventFileName) throws IOException {
        // FIXME: stop using rc0.* stream names in tests
        final Row row =
                createEventRow(
                        "/page_change-event." + sourceEventFileName + ".json", "mediawiki.page_change.v1");
        return StreamSupport.stream(
                        UpdateEventConverters.fromPublicPageChange(row).spliterator(), false)
                .collect(Collectors.toList());
    }

    private Row createEventRow(String sourceEventFileName, String stream) throws IOException {
        byte[] eventData =
                IOUtils.toByteArray(
                        getClass().getResourceAsStream(getClass().getSimpleName() + sourceEventFileName));
        return factory.deserializer(stream).deserialize(eventData);
    }

    private List<UpdateEvent> mapPageRerenderEvents(
            String sourceEventFileName, Predicate<String> upsert) throws IOException {
        final Row row =
                createEventRow(
                        "/page_rerender." + sourceEventFileName + ".json",
                        "mediawiki.cirrussearch_page_rerender.v1");
        return StreamSupport.stream(
                        UpdateEventConverters.fromPublicPageRerender(row, upsert).spliterator(), false)
                .collect(Collectors.toList());
    }

    private UpdateEvent mapRevScoringEvent(String sourceEventFileName, String model)
            throws IOException {
        final String prefix = "testsuite";
        final Row row =
                createEventRow(
                        "/rev_scoring." + sourceEventFileName + ".json", "mediawiki.revision_score_" + model);
        return UpdateEventConverters.fromRevisionScoring(row, prefix);
    }

    private UpdateEvent mapRecommendationCreateEvent(String sourceEventFileName) throws IOException {
        final Row row =
                createEventRow(
                        "/recommendation_create." + sourceEventFileName + ".json",
                        "mediawiki.revision-recommendation-create");
        return UpdateEventConverters.fromRecommendationCreate(row);
    }

    private UpdateEvent mapOutlinkPredictionChangeEvent(String sourceEventFileName)
            throws IOException {
        final String prefix = "testsuite";
        final Row row =
                createEventRow(
                        "/outlink_prediction_change." + sourceEventFileName + ".json",
                        "mediawiki.page_outlink_topic_prediction_change.v1");
        return UpdateEventConverters.fromOutlinkPredictionChange(row, prefix);
    }

    private UpdateEvent mapPageWeightedTagsChangeEvent(String sourceEventFileName)
            throws IOException {
        final Row row =
                createEventRow(
                        "/page_weighted_tags_change." + sourceEventFileName + ".json",
                        "mediawiki.cirrussearch.page_weighted_tags_change.v1");
        return UpdateEventConverters.fromPageWeightedTagsChange(row);
    }

    private UpdateEvent mapLegacyPageWeightedTagsChangeEvent(String sourceEventFileName)
            throws IOException {
        final Row row =
                createEventRow(
                        "/legacy_page_weighted_tags_change." + sourceEventFileName + ".json",
                        "mediawiki.cirrussearch.page_weighted_tags_change.rc0");
        return UpdateEventConverters.fromPageWeightedTagsChange(row);
    }
}
