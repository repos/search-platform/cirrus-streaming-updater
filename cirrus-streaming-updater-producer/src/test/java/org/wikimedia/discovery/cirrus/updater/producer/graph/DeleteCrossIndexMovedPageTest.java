package org.wikimedia.discovery.cirrus.updater.producer.graph;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

import org.apache.flink.api.common.functions.util.ListCollector;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.wikimedia.discovery.cirrus.updater.common.model.ChangeType;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateEvent;

@ExtendWith(MockitoExtension.class)
public class DeleteCrossIndexMovedPageTest {
    private final DeleteCrossIndexMovedPage processFunction = new DeleteCrossIndexMovedPage();
    @Mock private DeleteCrossIndexMovedPage.Context context;

    @Test
    public void test_with_cross_index_delete() {
        UpdateEvent event = buildTestEvent(true);

        List<UpdateEvent> output = new ArrayList<>();
        processFunction.processElement(event, context, new ListCollector<>(output));
        UpdateEvent expectedDelete = new UpdateEvent();
        expectedDelete.setTargetDocument(
                new UpdateEvent.TargetDocument("domain", "wiki", 1L, 1L, "group", "other", "Title"));
        expectedDelete.setMeta(
                new UpdateEvent.Meta("domain", Instant.EPOCH, null, "321", "somestream", "someurl"));
        expectedDelete.setEventTime(Instant.EPOCH);
        expectedDelete.setIngestionTime(Instant.EPOCH);
        expectedDelete.setChangeType(ChangeType.PAGE_DELETE);
        expectedDelete.setRevId(1L);
        assertThat(output).containsExactly(event, expectedDelete);
    }

    @Test
    public void test_without_cross_index_delete() {
        UpdateEvent event = buildTestEvent(false);

        List<UpdateEvent> output = new ArrayList<>();
        processFunction.processElement(event, context, new ListCollector<>(output));
        assertThat(output).containsExactly(event);
    }

    @Test
    void test_is_moved_from_another_index() {
        UpdateEvent event = new UpdateEvent();
        event.setTargetDocument(new UpdateEvent.TargetDocument("domain", "wiki", 1L, 1L));
        assertThat(processFunction.isMovedFromAnotherIndex(event)).isFalse();
        event.setMovedFrom(new UpdateEvent.TargetDocument("domain", "wiki", 2L, 1L));
        // the targets are not yet complete
        assertThat(processFunction.isMovedFromAnotherIndex(event)).isFalse();
        event.getTargetDocument().setIndexName("foo");
        event.getTargetDocument().setClusterGroup("group");
        assertThat(processFunction.isMovedFromAnotherIndex(event)).isFalse();
        // the targets are complete but the index is the same
        event.getMovedFrom().setIndexName("foo");
        event.getMovedFrom().setClusterGroup("group");
        assertThat(processFunction.isMovedFromAnotherIndex(event)).isFalse();
        event.getMovedFrom().setIndexName("bar");
        assertThat(processFunction.isMovedFromAnotherIndex(event)).isTrue();
    }

    private static @NotNull UpdateEvent buildTestEvent(boolean crossIndexMove) {
        UpdateEvent event = new UpdateEvent();
        event.setTargetDocument(
                new UpdateEvent.TargetDocument("domain", "wiki", 1L, 1L, "group", "index", "Title"));
        event.setMovedFrom(
                new UpdateEvent.TargetDocument(
                        "domain", "wiki", 1L, 1L, "group", crossIndexMove ? "other" : "index", "Title"));
        event.setMeta(
                new UpdateEvent.Meta("domain", Instant.EPOCH, "123", "321", "somestream", "someurl"));
        event.setEventTime(Instant.EPOCH);
        event.setIngestionTime(Instant.EPOCH);
        event.setChangeType(ChangeType.REV_BASED_UPDATE);
        event.setRevId(1L);
        return event;
    }
}
