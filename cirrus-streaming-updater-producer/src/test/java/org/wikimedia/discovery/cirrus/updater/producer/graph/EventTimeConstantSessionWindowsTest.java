/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.wikimedia.discovery.cirrus.updater.producer.graph;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyCollection;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.hamcrest.MockitoHamcrest.argThat;

import java.util.Collection;

import org.apache.flink.api.common.ExecutionConfig;
import org.apache.flink.streaming.api.windowing.assigners.MergingWindowAssigner;
import org.apache.flink.streaming.api.windowing.assigners.WindowAssigner;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.google.common.collect.Lists;

/** Tests for {@link EventTimeConstantSessionWindows}. */
@RunWith(MockitoJUnitRunner.class)
public class EventTimeConstantSessionWindowsTest {

    @Mock MergingWindowAssigner.MergeCallback<TimeWindow> callback;

    @Test
    public void testWindowAssignment() {
        final int sessionGap = 5000;

        WindowAssigner.WindowAssignerContext mockContext =
                mock(WindowAssigner.WindowAssignerContext.class);

        EventTimeConstantSessionWindows assigner =
                EventTimeConstantSessionWindows.withLength(Time.milliseconds(sessionGap));

        assertThat(assigner.assignWindows("String", 0L, mockContext))
                .containsExactly(new TimeWindow(0, sessionGap));
        assertThat(assigner.assignWindows("String", 4999L, mockContext))
                .containsExactly(new TimeWindow(4999, 4999 + sessionGap));
        assertThat(assigner.assignWindows("String", 5000L, mockContext))
                .containsExactly(new TimeWindow(5000, 5000 + sessionGap));
    }

    @Test
    public void testMergeSinglePointWindow() {
        EventTimeConstantSessionWindows assigner =
                EventTimeConstantSessionWindows.withLength(Time.milliseconds(5000));

        assigner.mergeWindows(Lists.newArrayList(new TimeWindow(0, 0)), callback);
        verify(callback, never()).merge(anyCollection(), any());
    }

    @Test
    public void testMergeSingleWindow() {
        EventTimeConstantSessionWindows assigner =
                EventTimeConstantSessionWindows.withLength(Time.milliseconds(5000));

        assigner.mergeWindows(Lists.newArrayList(new TimeWindow(0, 1)), callback);

        verify(callback, never()).merge(anyCollection(), any());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testMergeConsecutiveWindows() {
        EventTimeConstantSessionWindows assigner =
                EventTimeConstantSessionWindows.withLength(Time.milliseconds(5000));

        assigner.mergeWindows(
                Lists.newArrayList(
                        new TimeWindow(0, 3),
                        new TimeWindow(1, 4),
                        new TimeWindow(2, 5),
                        new TimeWindow(4, 6),
                        new TimeWindow(5, 7)),
                callback);

        verify(callback, times(1))
                .merge(
                        (Collection<TimeWindow>)
                                argThat(
                                        containsInAnyOrder(
                                                new TimeWindow(0, 3), new TimeWindow(1, 4), new TimeWindow(2, 5))),
                        eq(new TimeWindow(0, 3)));

        verify(callback, times(1))
                .merge(
                        (Collection<TimeWindow>)
                                argThat(containsInAnyOrder(new TimeWindow(4, 6), new TimeWindow(5, 7))),
                        eq(new TimeWindow(4, 6)));

        verify(callback, times(2)).merge(anyCollection(), any());
    }

    @Test
    public void testMergeCoveringWindow() {
        EventTimeConstantSessionWindows assigner =
                EventTimeConstantSessionWindows.withLength(Time.milliseconds(5000));

        assigner.mergeWindows(
                Lists.newArrayList(
                        new TimeWindow(1, 1), new TimeWindow(0, 2), new TimeWindow(4, 7), new TimeWindow(5, 6)),
                callback);

        verify(callback, times(1))
                .merge(
                        (Collection<TimeWindow>)
                                argThat(containsInAnyOrder(new TimeWindow(1, 1), new TimeWindow(0, 2))),
                        eq(new TimeWindow(0, 2)));

        verify(callback, times(1))
                .merge(
                        (Collection<TimeWindow>)
                                argThat(containsInAnyOrder(new TimeWindow(5, 6), new TimeWindow(4, 7))),
                        eq(new TimeWindow(4, 7)));

        verify(callback, times(2)).merge(anyCollection(), any());
    }

    @Test
    public void testInvalidParameters() {
        assertThatThrownBy(
                        () -> {
                            EventTimeConstantSessionWindows.withLength(Time.seconds(-1));
                        })
                .hasMessageContaining("0 < size");

        assertThatThrownBy(
                        () -> {
                            EventTimeConstantSessionWindows.withLength(Time.seconds(0));
                        })
                .hasMessageContaining("0 < size");
    }

    @Test
    public void testProperties() {
        EventTimeConstantSessionWindows assigner =
                EventTimeConstantSessionWindows.withLength(Time.seconds(5));

        assertThat(assigner.isEventTime()).isTrue();
        assertThat(new TimeWindow.Serializer())
                .isEqualTo(assigner.getWindowSerializer(new ExecutionConfig()));
    }
}
