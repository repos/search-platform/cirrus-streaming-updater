package org.wikimedia.discovery.cirrus.updater.producer.graph;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.Instant;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.flink.api.common.functions.util.ListCollector;
import org.apache.flink.streaming.api.TimerService;
import org.apache.flink.streaming.api.functions.KeyedProcessFunction;
import org.apache.flink.util.OutputTag;
import org.junit.jupiter.api.Test;
import org.wikimedia.discovery.cirrus.updater.common.model.EventDataStreamUtilities;
import org.wikimedia.discovery.cirrus.updater.common.model.PageKey;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateEvent;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateEvent.TargetDocument;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateEventTypeInfo;
import org.wikimedia.discovery.cirrus.updater.producer.graph.ProducerGraphFactory.BypassDeduplicationSplitter;

import lombok.SneakyThrows;

public class BypassDeduplicationSplitterTest {

    public static final OutputTag<UpdateEvent> OUTPUT_TAG =
            new OutputTag<>(
                    "test", UpdateEventTypeInfo.create(EventDataStreamUtilities.buildUpdateTypeInfo()));

    @Test
    void bypass() {
        final TestBypassDeduplicationSplitter splitter =
                new TestBypassDeduplicationSplitter(Set.of("wikiA"), OUTPUT_TAG);

        final UpdateEvent wikiA = createEvent("wikiA", null);
        final UpdateEvent wikiB = createEvent("wikiB", null);
        final UpdateEvent wikiC = createEvent("wikiC", true);
        final UpdateEvent wikiD = createEvent("wikiD", false);
        final LinkedList<UpdateEvent> untaggedEvents = new LinkedList<>();
        final ListCollector<UpdateEvent> untaggedEventsCollector = new ListCollector<>(untaggedEvents);
        splitter.processElement(wikiA, splitter.createContext(wikiA), untaggedEventsCollector);
        splitter.processElement(wikiB, splitter.createContext(wikiB), untaggedEventsCollector);
        splitter.processElement(wikiC, splitter.createContext(wikiC), untaggedEventsCollector);
        splitter.processElement(wikiD, splitter.createContext(wikiD), untaggedEventsCollector);

        assertThat(untaggedEvents).containsOnly(wikiB, wikiD);
        assertThat(splitter.getTaggedEvents(OUTPUT_TAG)).containsOnly(wikiA, wikiC);
    }

    private UpdateEvent createEvent(String wikiId, Boolean bypassDeduplication) {
        return UpdateEvent.builder()
                .eventTime(Instant.now())
                .targetDocument(TargetDocument.builder().wikiId(wikiId).pageId(1L).build())
                .bypassDeduplication(bypassDeduplication)
                .build();
    }
}

class TestBypassDeduplicationSplitter extends BypassDeduplicationSplitter {

    private final Map<OutputTag<?>, List<Object>> taggedEvents = new ConcurrentHashMap<>();

    TestBypassDeduplicationSplitter(Set<String> bypassedWikiIds, OutputTag<UpdateEvent> bypassedTag) {
        super(bypassedWikiIds, bypassedTag);
    }

    TestBypassDeduplicationSplitterContext createContext(UpdateEvent event) {
        return new TestBypassDeduplicationSplitterContext(event);
    }

    List<Object> getTaggedEvents(OutputTag<?> tag) {
        return taggedEvents.getOrDefault(tag, List.of());
    }

    class TestBypassDeduplicationSplitterContext
            extends KeyedProcessFunction<PageKey, UpdateEvent, UpdateEvent>.Context {

        private final PageKey key;

        @SneakyThrows
        TestBypassDeduplicationSplitterContext(UpdateEvent delegate) {
            key = UpdateEvent.KEY_SELECTOR.getKey(delegate);
        }

        @Override
        public Long timestamp() {
            throw new UnsupportedOperationException();
        }

        @Override
        public TimerService timerService() {
            throw new UnsupportedOperationException();
        }

        @Override
        public <X> void output(OutputTag<X> outputTag, X value) {
            taggedEvents.computeIfAbsent(outputTag, (key) -> new LinkedList<>()).add(value);
        }

        @Override
        @SneakyThrows
        public PageKey getCurrentKey() {
            return key;
        }
    }
}
